jQuery.sap.declare("com.sapui5.app.Component");

sap.ui.core.UIComponent.extend("com.sapui5.app.Component",{
	metadata: {
		routing: {
			config: {
				viewType: "JS",
				viewPath: "ui5app",
				targetControl: "appContainer",
				targetAggregation: "pages",
				clearTarget: false
			},
			routes: [
				 /*
				  * pattern: stringa indicata nel percorso dell'URL
				  * name: nome utilizzato nel comando navTo
				  * view: nome effettivo della view (e percorso se in una sottocartella)
				  * viewId: id della view, utilizzato per recuperarla con il comando sap.ui.getCore().byId()
				  *
				  */
				{
					pattern: "", //"login:?query:",
					name: "login",
					view: "login",
					viewId: "loginView"
				},
				{
					pattern: "menu:?query:",
					name: "menu",
					view: "menu",
					viewId: "menuView"
				},
				{
					pattern: "prodDecl:?query:",
					name: "prodDecl",
					view: "PRODDECL.prodDecl",
					viewId: "prodDeclView"
				},
				{
					pattern: "prodDecl2:?query:",
					name: "prodDecl2",
					view: "PRODDECL2.prodDecl2",
					viewId: "prodDecl2View",
					subroutes : [
						{
							pattern: "prodDecl2Qty:?query:",
							name: "prodDecl2Qty",
							view: "PRODDECL2.prodDecl2Qty",
							viewId: "prodDecl2QtyView"
						},
						{
							pattern: "prodDecl2Susp:?query:",
							name: "prodDecl2Susp",
							view: "PRODDECL2.prodDecl2Susp",
							viewId: "prodDecl2SuspView"
						}
					 ]
				},
				{
					pattern: "pacUdc:?query:",
					name: "pacUdc",
					view: "PAC.pacUdc",
					viewId: "pacUdcView",
					subroutes : [
						{
							pattern: "pacDm:?query:",
							name: "pacDm",
							view: "PAC.pacDm",
							viewId: "pacDmView"
						},
						{
							pattern: "pacNewUdc:?query:",
							name: "pacNewUdc",
							view: "PAC.pacNewUdc",
							viewId: "pacNewUdcView"
						}
					 ]
				},
				{
					pattern: "ocr:?query:",
					name: "ocr",
					view: "OCR.ocr",
					viewId: "ocrView"
				},
				{
					pattern: "suspDecl:?query:",
					name: "suspDecl",
					view: "SUSPDECL.suspDecl",
					viewId: "suspDeclView",
					subroutes : [
						{
							pattern: "suspDeclInput:?query:",
							name: "suspDeclInput",
							view: "SUSPDECL.suspDeclInput",
							viewId: "suspDeclInputView"
						},
						{
							pattern: "suspDeclReas:?query:",
							name: "suspDeclReas",
							view: "SUSPDECL.suspDeclReas",
							viewId: "suspDeclReasView"
						}
					 ]
				},
				{
					pattern: "suspDet:?query:",
					name: "suspDet",
					view: "SUSPDET.suspDet",
					viewId: "suspDetView",
					subroutes : [
						{
							pattern: "suspDetRep:?query:",
							name: "suspDetRep",
							view: "SUSPDET.suspDetRep",
							viewId: "suspDetRepView"
						},
						{
							pattern: "suspDetDisc:?query:",
							name: "suspDetDisc",
							view: "SUSPDET.suspDetDisc",
							viewId: "suspDetDiscView"
						},
						{
							pattern: "suspDetYeld:?query:",
							name: "suspDetYeld",
							view: "SUSPDET.suspDetYeld",
							viewId: "suspDetYeldView"
						},
						{
							pattern: "suspDetGood:?query:",
							name: "suspDetGood",
							view: "SUSPDET.suspDetGood",
							viewId: "suspDetGoodView"
						}
					 ]
				},
				{
					pattern: "boxDecl3:?query:",
					name: "boxDecl3",
					view: "BOXDECL3.boxDecl3",
					viewId: "boxDeclView3",
					subroutes : [
						{
							pattern: "boxDeclInput3:?query:",
							name: "boxDeclInput3",
							view: "BOXDECL3.boxDeclInput3",
							viewId: "boxDeclInput3View"
						}
					]
				},
				{
					pattern: "boxDecl:?query:",
					name: "boxDecl",
					view: "BOXDECL.boxDecl",
					viewId: "boxDeclView",
					subroutes : [
						{
							pattern: "boxDeclInput:?query:",
							name: "boxDeclInput",
							view: "BOXDECL.boxDeclInput",
							viewId: "boxDeclInputView"
						}
					]
				},
				{
					pattern: "boxDeclInc:?query:",
					name: "boxDeclInc",
					view: "BOXDECL2.boxDecl",
					viewId: "boxDeclView"
				},
				{
					pattern: "boxDeclFull:?query:",
					name: "boxDeclFull",
					view: "BOXDECL2.boxDecl",
					viewId: "boxDeclView"
				},
				{
					pattern: "boxDeclInput2:?query:",
					name: "boxDeclInput2",
					view: "BOXDECL2.boxDeclInput",
					viewId: "boxDeclInput2View"
				},
				{
					pattern: "shiftDecl:?query:",
					name: "shiftDecl",
					view: "SHIFTDECL.shiftDecl",
					viewId: "shiftDeclView",
					subroutes : [
						{
							pattern: "shiftDeclInfo:?query:",
							name: "shiftDeclInfo",
							view: "CLOSEUDC.closeUdcConfirm",
							viewId: "closeUdcConfirmView"
						}
					]
				},
				{
					pattern: "prodDeclOrder:?query:",
					name: "prodDeclOrder",
					view: "PRODDECLORDER.prodDeclOrder",
					viewId: "prodDeclOrderView"
				},
				{
					pattern: "inputUdcDecl:?query:",
					name: "inputUdcDecl",
					view: "INPUTUDCDECL.inputUdcDecl",
					viewId: "inputUdcDeclView"
				},
				{
					pattern: "stopsLine:?query:",
					name: "stopsLine",
					view: "STOPSLINE.stopsLine",
					viewId: "stopsLineView"
				},
				{
					pattern: "serialNoConn:?query:",
					name: "serialNoConn",
					view: "SERIALNOCONN.checkOrderPoper",
					viewId: "checkOrderPoperView",
					subroutes : [
						{
							pattern: "serialCheck:?query:",
							name: "serialCheck",
							view: "SERIALNOCONN.serialCheck",
							viewId: "serialCheckView"
						},
						{
							pattern: "serialSelect:?query:",
							name: "serialSelect",
							view: "SERIALNOCONN.serialSelect",
							viewId: "serialSelectView"
						}
					]
				},
				{
					pattern: "repUdc:?query:",
					name: "repUdc",
					view: "REPUDC.repUdc",
					viewId: "repUdcView",
					subroutes : [
						{
							pattern: "repUdcInput:?query:",
							name: "repUdcInput",
							view: "REPUDC.repUdcInput",
							viewId: "repUdcInputView"
						},
						{
							pattern: "repUdcReas:?query:",
							name: "repUdcReas",
							view: "SUSPDECL.suspDeclReas",
							viewId: "suspDeclReasView"
						}
					]
				},
				{
					pattern: "rewUdc:?query:",
					name: "rewUdc",
					view: "REPUDC.repUdc",
					viewId: "repUdcView",
					subroutes : [
						{
							pattern: "rewUdcInput:?query:",
							name: "rewUdcInput",
							view: "REPUDC.repUdcInput",
							viewId: "repUdcInputView"
						}
					]
				},
				{
					pattern: "closeUdc:?query:",
					name: "closeUdc",
					view: "CLOSEUDC.closeUdc",
					viewId: "closeUdcView",
					subroutes : [
						{
							pattern: "closeUdcConfirm:?query:",
							name: "closeUdcConfirm",
							view: "CLOSEUDC.closeUdcConfirm",
							viewId: "closeUdcConfirmView"
						}
					]
				},
				{
					pattern: "scrapUdc:?query:",
					name: "scrapUdc",
					view: "CLOSEUDC.closeUdc",
					viewId: "closeUdcView",
					subroutes : [
						{
							pattern: "scrapUdcInput:?query:",
							name: "scrapUdcInput",
							view: "CLOSEUDC.closeUdcConfirm",
							viewId: "closeUdcConfirmView"
						}
					]
				},
				{
					pattern: "reprintUdc:?query:",
					name: "reprintUdc",
					view: "SHIFTDECL.shiftDecl",
					viewId: "shiftDeclView",
					subroutes : [
						{
							pattern: "reprintUdcConfirm:?query:",
							name: "reprintUdcConfirm",
							view: "CLOSEUDC.closeUdcConfirm",
							viewId: "closeUdcConfirmView"
						}
					]
				},
				{
					pattern: "serviceDecl:?query:",
					name: "serviceDecl",
					view: "SERVICEDECL.serviceDecl",
					viewId: "serviceDeclView",
					subroutes : [
						{
							pattern: "serviceDeclInput:?query:",
							name: "serviceDeclInput",
							view: "SERVICEDECL.serviceDeclInput",
							viewId: "serviceDeclInputView"
						}
					]
				},
				{
					pattern: "scrapGoods:?query:",
					name: "scrapGoods",
					view: "SERIALNOCONN.checkOrderPoper",
					viewId: "checkOrderPoperView",
					subroutes : [
						{
							pattern: "scrapGoodsInput:?query:",
							name: "scrapGoodsInput",
							view: "CLOSEUDC.closeUdcConfirm",
							viewId: "closeUdcConfirmView"
						}
					]
				},
				{
					pattern: "readUdc:?query:",
					name: "readUdc",
					view: "CLOSEUDC.closeUdc",
					viewId: "closeUdcView",
					subroutes : [
						{
							pattern: "readUdcInput:?query:",
							name: "readUdcInput",
							view: "CLOSEUDC.closeUdcConfirm",
							viewId: "closeUdcConfirmView"
						}
					]
				},
				{
					pattern: "boxDecl4:?query:",
					name: "boxDecl4",
					view: "BOXDECL4.boxDecl4",
					viewId: "boxDeclView4",
					subroutes : [
						{
							pattern: "boxDeclMenu4:?query:",
							name: "boxDeclMenu4",
							view: "BOXDECL4.boxDeclMenu4",
							viewId: "boxDeclMenu4View"
						},
						{
							pattern: "boxDeclInput4:?query:",
							name: "boxDeclInput4",
							view: "BOXDECL4.boxDeclInput4",
							viewId: "boxDeclInput4View"
						}
					]
				},
				{
					pattern: "boxDecl5:?query:",
					name: "boxDecl5",
					view: "BOXDECL5.boxDecl5",
					viewId: "boxDeclView5",
					subroutes : [
						{
							pattern: "boxDeclInput5:?query:",
							name: "boxDeclInput5",
							view: "BOXDECL5.boxDeclInput5",
							viewId: "boxDeclInput5View"
						},
						{
							pattern: "boxDeclInput6:?query:",
							name: "boxDeclInput6",
							view: "BOXDECL5.boxDeclInput6",
							viewId: "boxDeclInput6View"
						},
						{
							pattern: "suspDeclInput6:?query:",
							name: "suspDeclInput6",
							view: "BOXDECL5.suspDeclInput6",
							viewId: "suspDeclInput6View"
						},
						{
							pattern: "closeUdcConfirm6:?query:",
							name: "closeUdcConfirm6",
							view: "BOXDECL5.closeUdcConfirm6",
							viewId: "closeUdcConfirm6View"
						}
					]
				}
			]
		}
	}
});

com.sapui5.app.Component.prototype.init = function(){
	jQuery.sap.require("sap.ui.core.routing.History");
	jQuery.sap.require("sap.m.routing.RouteMatchedHandler");

	sap.ui.core.UIComponent.prototype.init.apply(this);

	var router = this.getRouter();
	this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
	router.initialize();
};

com.sapui5.app.Component.prototype.destroy = function(){
	if(this.routeHandler){
		this.routeHandler.destroy();
	}
	sap.ui.core.UIComponent.destroy.apply(this,arguments);
};

com.sapui5.app.Component.prototype.createContent = function(){
	this.view = sap.ui.view({id:"app",viewName:"ui5app.app",type:sap.ui.core.mvc.ViewType.JS});
	return this.view;
};
