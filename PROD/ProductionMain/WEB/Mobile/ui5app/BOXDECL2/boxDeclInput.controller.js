// Controller Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL.boxDeclInput", {
	
	_oArguments: undefined,
	_sWsName: '',
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("boxDeclInput").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/
	
	onObjectMatched: function (oEvent) {
		
		const oArgParams = ['plant','pernr','full','order','poper','idline'];
		const oArguments = oAppController.getPatternArguments(oEvent,oArgParams);
		this._oArguments = oArguments;
		
		sap.ui.getCore().byId('boxDeclInputPage').setTitle(
			(oArguments.full === 'true') ? oLng_Opr.getText('BoxDecl_TitleFull')
				: oLng_Opr.getText('BoxDecl_TitleInc')
		);
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		
		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclInputPage'),sPlant,sPernr);
		
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oWorkstInfo = oStorage.get('WORKST_INFO');
		if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
			this._sWsName = oWorkstInfo.WSNAME;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');

		var bDisabledMode = false;
		if(!oUdcObject || !oUdcObject.hasOwnProperty('UDCNR') || 
			(oUdcObject.UDCNR !== oArguments.udc && !(oUdcObject.PRINT_MODE && oArguments.udc === '')))
		{
			oUdcObject = {};
			bDisabledMode = true;
		}
		
		this.displayInfo(oUdcObject, bDisabledMode);
	},

/***********************************************************************************************/
// Visualizzazione delle informazioni
/***********************************************************************************************/
	
	displayInfo: function(oUdcObject,bDisabledMode) {
		
		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		var oView = sap.ui.getCore().byId('boxDeclInputView');
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oUdcObject);
		
		const sNewUdc = oUdcObject.UDCNR === '';
		
		// Per nuovo UdC il campo UdC non è visibile
		$.UIbyID('boxDeclUDCLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclUDCText').setVisible(!sNewUdc);
		$.UIbyID('boxDeclDeclQtyLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclDeclQtyText').setVisible(!sNewUdc);
		$.UIbyID('boxDeclOpLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclOpText').setVisible(!sNewUdc);
		$.UIbyID('boxDeclTimeLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclTimeText').setVisible(!sNewUdc);
		
		var bPrintMode = false;
		if(oUdcObject.hasOwnProperty('PRINT_MODE'))
			bPrintMode = oUdcObject.PRINT_MODE;
		
		$.UIbyID('boxDeclInputWorkTime').setValue(oUdcObject.WTDUR);
		$.UIbyID('boxDeclInputNrPrnt').setValue(oUdcObject.NRPRNT);
		$.UIbyID('boxDeclInputStdQty').setValue(oUdcObject.NMIMB);
		$.UIbyID('boxDeclInputBasketTT').setValue(oUdcObject.BASKETTT);;
		$.UIbyID('boxDeclInputDater').setValue(oUdcObject.DATER);;

		$.UIbyID('boxDeclInputQty').setValue((bPrintMode) ? oUdcObject.QTYDECL : '');
		
		$.UIbyID('boxDeclConfirmButton').setEnabled(!bDisabledMode && !bPrintMode);
		$.UIbyID('boxDeclPrintButton').setEnabled(!bDisabledMode && bPrintMode);
	},
	
/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('boxDeclInputView').getController();
		var oArguments = oController._oArguments;
		
		var sNavPath;
		if(oArguments.full === 'true')
			sNavPath = 'boxDeclFull';
		else
			sNavPath = 'boxDeclInc';
		
		delete oArguments.full;
		delete oArguments.order;
		delete oArguments.poper;
		delete oArguments.idline;
		delete oArguments.udc;
		
		oAppRouter.navTo(sNavPath,{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Conferma
/***********************************************************************************************/

	onConfirm: function() {
		var oView = sap.ui.getCore().byId('boxDeclInputView');
		var oController = oView.getController();
		var oModel = oView.getModel();
		const oArguments = oController._oArguments;
		
		var oBasketTTInput = $.UIbyID('boxDeclInputBasketTT');
		var oDaterInput = $.UIbyID('boxDeclInputDater');
		var oWorkTimeInput = $.UIbyID('boxDeclInputWorkTime');
		var oQtyInput = $.UIbyID('boxDeclInputQty');
		
		oQtyInput.setValueState(sap.ui.core.ValueState.None);
		oQtyInput.setValueStateText();
		oBasketTTInput.setValueState(sap.ui.core.ValueState.None);
		oBasketTTInput.setValueStateText();
		oDaterInput.setValueState(sap.ui.core.ValueState.None);
		oDaterInput.setValueStateText();
		oWorkTimeInput.setValueState(sap.ui.core.ValueState.None);
		oWorkTimeInput.setValueStateText();
		
		const sBasketTT = oBasketTTInput.getValue();
		const sDater = oDaterInput.getValue();
		const sQtyDecl = oQtyInput.getValue();
		const iQtyDecl = parseFloat(sQtyDecl);
		const sWorkTime = oWorkTimeInput.getValue();
		const sNrPrnt = $.UIbyID('boxDeclInputNrPrnt').getValue();
		const sNmImb = $.UIbyID('boxDeclInputStdQty').getValue();
		
		var iQtyRes = parseFloat(oModel.getProperty("/QTYRES"));
		if(isNaN(iQtyRes))
			iQtyRes = 0;
		
		var iMatPzUdc = parseFloat(oModel.getProperty("/MAT_PZUDC"));
		if(isNaN(iMatPzUdc))
			iMatPzUdc = 0;
				
		var sErrorMessage;
		var oErrorField;
		if(!sDater.match(/([0-9]{2})(0[1-9]|1[0-2])/) || sDater.length != 4) // solo caratteri alfanumerici
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterErrorMsg");
			oErrorField = oDaterInput;
		}
		else if(sBasketTT && !sBasketTT.match(/^[0-9a-zA-Z]+$/))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_BasketTTErrorMsg");
			oErrorField = oBasketTTInput;
		}
		else if(isNaN(sWorkTime) || sWorkTime<=0)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_WorkTimeErrorMsg");
			oErrorField = oWorkTimeInput;
		}
		
		if(!sErrorMessage)
		{
			if(isNaN(iQtyDecl) || iQtyDecl <= 0)
				sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyErrorMsg");
			else if(iMatPzUdc && iMatPzUdc > 0)
			{
				if( iQtyRes + iQtyDecl > iMatPzUdc)
					sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg");
				else if( (iQtyRes + iQtyDecl === iMatPzUdc) && oArguments.full !== 'true')
					sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyFullErrorMsg");
			}
			
			if(sErrorMessage)
				oErrorField = oQtyInput;
		}
		
		if(sErrorMessage)
		{
			oErrorField.setValueState(sap.ui.core.ValueState.Warning);
			oErrorField.setValueStateText(sErrorMessage);
			oErrorField.focus();
				
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			const sFIUdC = (oArguments.full === 'true') ? 'C' : 'I';
			const sDatetime = getDatetimeString(new Date());
			
			var sSaveQuery = sMovementPath + "UDC/saveUDCandPrintQR";
			sSaveQuery += "&Param.1=" + oArguments.plant;
			sSaveQuery += "&Param.2=" + oArguments.pernr;
			sSaveQuery += "&Param.3=" + oAppController._sShift;
			sSaveQuery += "&Param.4=" + oAppController._sDate;
			sSaveQuery += "&Param.5=" + oArguments.order + oArguments.poper;
			sSaveQuery += "&Param.6=" + oArguments.idline;
			sSaveQuery += "&Param.7=" + oArguments.udc;
			sSaveQuery += "&Param.8=" + sQtyDecl;
			sSaveQuery += "&Param.9=" + sWorkTime;
			sSaveQuery += "&Param.10=" + sNrPrnt;
			sSaveQuery += "&Param.11=" + sNmImb;
			sSaveQuery += "&Param.12=" + sDater;
			sSaveQuery += "&Param.13=" + sBasketTT;
			sSaveQuery += "&Param.14=" + oController._sWsName;
			sSaveQuery += "&Param.15=" + sDatetime;
			sSaveQuery += "&Param.16=" + sFIUdC;
			sSaveQuery += "&Param.17=";
			sSaveQuery += "&Param.18=" + sLanguage;
			
			var successFunction = function(oResults) {
			
				var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
				var oUdcObject = oSessionStorage.get('FUNCDATA');
				
				if(!oUdcObject)
					oUdcObject = {};
				
				if(oResults.hasOwnProperty('outputUdc') && oResults.outputUdc)
					oUdcObject.UDCNR = oResults.outputUdc;
				oUdcObject.PRINT_MODE = true;
				oUdcObject.QTYRES = iQtyRes + iQtyDecl;
				oUdcObject.QTYDECL = iQtyDecl;
				oUdcObject.WTDUR = sWorkTime;
				oUdcObject.NRPRNT = sNrPrnt;
				oUdcObject.NMIMB = sNmImb;
				oUdcObject.DATER = sDater;
				oUdcObject.BASKETTT = sBasketTT;
				oUdcObject.TIMEID = sDatetime;
				oUdcObject.LAST_OPEID = oArguments.pernr;
					
				const oLoginInfo = oSessionStorage.get('LOGIN_INFO');
				if(oLoginInfo.hasOwnProperty("opeName"))
					oUdcObject.LAST_OPENAME = oLoginInfo.opeName;
				
				oSessionStorage.put('FUNCDATA',oUdcObject);
				oController.displayInfo(oUdcObject,false);
			};
			
			oAppController.handleRequest(sSaveQuery,{
				onSuccess: successFunction,
				onWarning: successFunction,
				results: ['outputUdc']
			});
		}
	},
	
/***********************************************************************************************/
// Stampa
/***********************************************************************************************/

	onPrint: function() {
		var oView = sap.ui.getCore().byId('boxDeclInputView');
		var oController = oView.getController();
		var oModel = oView.getModel();
		const oArguments = oController._oArguments;
		
		const sIsFull = (oArguments.full === 'true') ? oArguments.full : 'false';
		
		var sPrintUrl = "http://10.0.0.62:50000/XMII/Runner?";
		sPrintUrl += "Transaction=ProductionMain/Movement/Print/print_ETI_PROD_TR";
		sPrintUrl += "&OutputParameter=pdfString&Content-Type=application/pdf&isBinary=true";
		sPrintUrl += "&UDCNR=" + oModel.getProperty("/UDCNR");
		sPrintUrl += "&IDLINE=" + oArguments.idline;
		sPrintUrl += "&location=" + oController._sWsName;
		sPrintUrl += "&full=" + sIsFull;
		sPrintUrl += "&Language=" + sLanguage;
		
		//window.open(sPrintUrl, '_blank');
		
		oController.showPdfDialog(sPrintUrl);
	},
	
	showPdfDialog(sPrintUrl,bBase64) {
		
		if(bBase64)
			sPrintUrl = 'data:application/pdf;base64,' + sPrintUrl;
		
		var oHtml = new sap.ui.core.HTML();
		oHtml.setContent('<iframe width=100% height=100% src="' + sPrintUrl + '"></iframe>');
						
		var oPrintDialog = new sap.m.Dialog({
			//resizable: true,
			horizontalScrolling : false,
			verticalScrolling : false,
			contentWidth : '800px',
			contentHeight : '600px',
			customHeader: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://decline",
						press: function() {
							oHtml.destroy();
							oPrintDialog.close();
						}
					})
				]
			}),
			content: oHtml,
			afterClose: function() {
				oPrintDialog.destroy();
			}
		}).addStyleClass('noPaddingDialog fullHeightScrollCont');
		
		oPrintDialog.open();
	}

});
