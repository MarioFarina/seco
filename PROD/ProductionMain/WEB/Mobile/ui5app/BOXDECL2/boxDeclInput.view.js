// View Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.BOXDECL.boxDeclInput", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.BOXDECL.boxDeclInput";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oView = this;

		var idTextFormatter = function(sId,sTxt) {
			return sId + " " + sTxt;
		};
		
		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					text: '{/ORDER}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Phase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/POPER" },
							{path: "/CICTXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowPoperInfo(oView),
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Line") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/LINEID" },
							{path: "/LINETXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/LINEID',
						description: '/LINETXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/MATERIAL" },
							{path: "/MAT_DESC"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: 'boxDeclUDCLabel',
					text: oLng_Opr.getText("BoxDeclInput_UDC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					id: 'boxDeclUDCText',
					text: '{/UDCNR}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'boxDeclDeclQtyLabel',
					text: oLng_Opr.getText("BoxDeclInput_DeclQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'boxDeclDeclQtyText',
					text: '{/QTYRES}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'boxDeclOpLabel',
					text: oLng_Opr.getText("BoxDeclInput_Operator") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					id: 'boxDeclOpText',
					text: '{/LAST_OPENAME}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'boxDeclTimeLabel',
					text: oLng_Opr.getText("BoxDeclInput_Datetime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'boxDeclTimeText',
					text: {
						parts: [
							{path: "/TIMEID"}
						],
						formatter: dateFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Dater") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.MaskInput({
					id: 'boxDeclInputDater',
					mask: "9999",
					placeholderSymbol: " ",
					placeholder: 'YYMM',
					width: '160px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'boxDeclInputBasketTT',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_WorkTime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'boxDeclInputWorkTime',
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Mold") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'boxDeclInputNrPrnt',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Qty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'boxDeclInputQty',
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_StdQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.ComboBox({
					id: 'boxDeclInputStdQty',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				})
			]
		});
		
		var oPanel = new sap.m.Panel({
			content: oGrid
		});
			
		var oPage = new sap.m.Page({
			id: "boxDeclInputPage",
			enableScrolling: false,
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							id: 'boxDeclConfirmButton',
							text: oLng_Opr.getText("BoxDeclInput_Confirm"),
							icon: "sap-icon://accept",
							press: oController.onConfirm
						}).addStyleClass("greenButton bigButton"),
						new sap.m.Button({
							id: 'boxDeclPrintButton',
							enabled: false,
							text: oLng_Opr.getText("BoxDeclInput_Print"),
							icon: "sap-icon://print",
							press: oController.onPrint
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
					]
				}).addStyleClass("sapUiSmallMarginTop")
			],
			footer: new sap.m.Toolbar({})
		});
		
	  	return oPage;
	}

});