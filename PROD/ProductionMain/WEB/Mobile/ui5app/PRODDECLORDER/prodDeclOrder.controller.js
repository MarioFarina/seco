// Controller Dichiarazioni di Produzione (fine turno su commessa)
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.PRODDECLORDER.prodDeclOrder", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("prodDeclOrder").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		
		oAppController.loadSubHeaderInfo($.UIbyID('prodDeclOrderPage'),sPlant,sPernr);
		
		var oOrderPhaseInput = $.UIbyID('prodDeclOrderInput');
		oOrderPhaseInput.setValue();
		oOrderPhaseInput.setValueState(sap.ui.core.ValueState.None);
		oOrderPhaseInput.setValueStateText();
		
		$.UIbyID('prodDeclOrderQty').setValue();
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {	
		var oController = sap.ui.getCore().byId('prodDeclOrderView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Verifica dei dati inseriti
/***********************************************************************************************/
	
	onProcess: function() {
		var oAppController = sap.ui.getCore().byId('app').getController();
		var oController = sap.ui.getCore().byId('prodDeclOrderView').getController();
		
		var oOrderPhaseInput = $.UIbyID('prodDeclOrderInput');
		oOrderPhaseInput.setValueState(sap.ui.core.ValueState.None);
		oOrderPhaseInput.setValueStateText();
		
		var oQtyInput = $.UIbyID('prodDeclOrderQty');
		oQtyInput.setValueState(sap.ui.core.ValueState.None);
		oQtyInput.setValueStateText();
		
		var sOrder;
		var sPhase;		
		const sOrderPhase = oOrderPhaseInput.getValue();
		const sQuantity = oQtyInput.getValue();
		
		var sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage) {
			oOrderPhaseInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderPhaseInput.setValueStateText(sErrorMessage);
		}
		else if(!sQuantity || isNaN(parseInt(sQuantity)) || parseInt(sQuantity) <= 0) {
			sErrorMessage = oLng_Opr.getText("ProdDeclOrder_QtyErrorMsg");
			oQtyInput.setValueState(sap.ui.core.ValueState.Warning);
			oQtyInput.setValueStateText(sErrorMessage);
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
				aLines = oSettings.LINES;
			
			// Controlla commessa e fase e se sono validi effettua l'iserimento
			var sCheckQuery = sMovementPath + "ProdDecl/checkConfProdEasyQR";
			sCheckQuery += "&Param.1=" + sOrderPhase;
			sCheckQuery += "&Param.2=" + sQuantity;
			sCheckQuery += "&Param.3=" + oController._oArguments.plant;
			sCheckQuery += "&Param.4=" + oAppController._sDate;
			sCheckQuery += "&Param.5=" + oAppController._sShift;
			sCheckQuery += "&Param.6=" + oController._oArguments.pernr;
			sCheckQuery += "&Param.7=" + aLines.join();
			sCheckQuery += "&Param.8=" + sLanguage;
			
			// callback per scegliere la linea di postazione
			var warningFunction = function(oResults) 
			{
				if(oResults.hasOwnProperty('outputLines') && oResults.outputLines)
					aLines = oResults.outputLines.split(",");
				
				var lineFunction = function(sSelectedLine) 
				{
					var sInsertQuery = sMovementPath + "ProdDecl/saveConfProdEasyQR";
					sInsertQuery += "&Param.1=" + sOrderPhase;
					sInsertQuery += "&Param.2=" + sQuantity;
					sInsertQuery += "&Param.3=" + sSelectedLine;
					sInsertQuery += "&Param.4=" + oController._oArguments.plant;
					sInsertQuery += "&Param.5=" + oAppController._sDate;
					sInsertQuery += "&Param.6=" + oAppController._sShift;
					sInsertQuery += "&Param.7=" + oController._oArguments.pernr;
					
					oAppController.handleRequest(sInsertQuery);
				};
				
				oAppController.selectFromDialog({
					title: oLng_Opr.getText("General_SelectLine"),
					noDataText: oLng_Opr.getText("General_NoLine"),
					callback: lineFunction,
					items: aLines
				});
			}
			
			oAppController.handleRequest(sCheckQuery,{
				onWarning: warningFunction,
				results: ['outputLines']
			});
		}

		oOrderPhaseInput.setValue();
		oQtyInput.setValue();
	}

});