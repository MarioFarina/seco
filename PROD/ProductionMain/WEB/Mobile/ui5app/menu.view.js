// View Menu Operatore

sap.ui.jsview("ui5app.menu", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.menu";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {

		var oAppController = sap.ui.getCore().byId('app').getController();
	
		
		
		var newMenuTileContainer = function() {
			return new sap.m.TileContainer({
				id: "functionsList",
				tiles: {
					path : "/Rowset/Row",
					template : new sap.m.StandardTile({
						title : "{FuncName}",
						icon : {
							parts: [
								{path: "ICON"},
								{path: "COLOR"},
							],		     
							formatter: function(sIcon,sColor) {
								if(!sIcon)
									sIcon = "inbox";
								if(sColor)
								{
									var css = document.createElement("style");
									css.type = "text/css";
									css.innerHTML = "." + sColor + "Tile .sapMStdIconMonitor { color: " + 
										sColor + " !important; } ." + sColor + 
										"Tile .sapMStdTileTitle { color: " + sColor + " !important;}";
									document.body.appendChild(css);
									this.addStyleClass(sColor+'Tile');
								}
								return "sap-icon://" + sIcon;
							}
						},
						type : sap.m.StandardTileType.Monitor,
						press: oController.onFunctionSelected
					})
				}
			})
		};
		
		var newMenuList = function() {
			return new sap.m.List({
				id: "functionsList",
				items: {
					path : "/Rowset/Row",
					template : new sap.m.ActionListItem({
						text : "{FuncName}",
						press: oController.onFunctionSelected
					})
				}
			});
		};
		
		var oPage = new sap.m.Page({
			id: "menuPage",
			title: oLng_Opr.getText("Menu_Menu"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://multiselect-none",
					press: function() {
						oController._bListView = !oController._bListView;
						$.UIbyID('functionsList').destroy();
						var sIdParent = '';
						if(oController._aParents)
						{
							const iParentsCount = oController._aParents.length;
							if(iParentsCount > 0)
							{
								const oParent = oController._aParents[iParentsCount-1];
								sIdParent = oParent.id;
							}
						}
						if(oController._bListView)
						{
							oPage.addContent(newMenuList());
							this.setIcon("sap-icon://table-view");
						}
						else
						{
							oPage.addContent(newMenuTileContainer());
							this.setIcon("sap-icon://multiselect-none");
						}
						oController.setViewFilter(sIdParent);
					}
				}),
				new sap.m.Button({
					icon: "sap-icon://home",
					press: [oAppController.onLogout,{
						logoutFunc:oController.onLogout
					}]
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: newMenuTileContainer(),
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});

	  	return oPage;
	}

});