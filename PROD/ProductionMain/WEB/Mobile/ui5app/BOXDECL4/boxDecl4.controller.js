// Controller Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL4.boxDecl4", {

	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute('boxDecl4').attachPatternMatched(this.onObjectMatched, this);

		var oOrderInput = $.UIbyID('boxDeclOrderInput4');

		var nextFunc = this.onNext;
		oOrderInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclPage4'),sPlant,sPernr);

		var oOrderInput = sap.ui.getCore().byId('boxDeclOrderInput4');
		oOrderInput.setValue();
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();

		jQuery.sap.delayedCall(500, null, function() {
			oOrderInput.focus();
		});
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('boxDeclView4').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},

/***********************************************************************************************/
// Ricerca ordine fase per UdC precedente o materiale
/***********************************************************************************************/

	onSearchOrder: function() {

		var callbackFunc = this.callback;
		if(typeof(callbackFunc) != 'function')
			return;

		var oController = this.controller;
		var oArguments = oController._oArguments;

		const sUdcKey = "udc";
		const sMaterialKey = "material";

		var oPoperCombo = new sap.m.ComboBox({
			width: '100%',
			layoutData : new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oPoperCombo.setModel(new sap.ui.model.xml.XMLModel());

		var oPoperTemplate = new sap.ui.core.Item({
			key : "{POPER}",
			text: {
				parts: [
					{path: "POPER" },
					{path: "CICTXT" }
				],
				formatter: function(sPoper,sCictxt) {
					return sPoper + " - " + sCictxt;
				}
			}
		});
		oPoperCombo.bindAggregation("items","/Rowset/Row",oPoperTemplate);

		var oOrderCombo = new sap.m.ComboBox({
			width: '100%',
			selectionChange: function() {
				oPoperCombo.clearSelection();
				oPoperCombo.setValue();

				const sMaterial = this.getSelectedItem().getBindingContext().getProperty('MATERIAL');

				var sPoperQuery = QService + sMovementPath + "ProdDecl/getPopersByOrderSQ";
				sPoperQuery += "&Param.1=" + oArguments.plant;
				sPoperQuery += "&Param.2=" + sMaterial;
				sPoperQuery += "&Param.3=" + this.getSelectedKey();

				var oModel = oPoperCombo.getModel();
				oModel.loadData(sPoperQuery,false,false);
			},
			layoutData : new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oOrderCombo.setModel(new sap.ui.model.xml.XMLModel());

		var oOrdersTemplate = new sap.ui.core.Item({
			key : "{ORDER}",
			text: {
				parts: [
					{path: "ORDER" },
					{path: "MATERIAL" },
					{path: "DESC" }
				],
				formatter: function(sOrder,sMaterial,sDesc) {
					return sOrder + " - " + sMaterial + " " + sDesc;
				}
			}
		});
		oOrderCombo.bindAggregation("items","/Rowset/Row",oOrdersTemplate);

		var oSearchInput = new sap.m.Input({
			//type: sap.m.InputType.Number,
			change: function() {
				oOrderCombo.clearSelection();
				oOrderCombo.setValue();
				oPoperCombo.clearSelection();
				oPoperCombo.setValue();

				var sQueryName;
				if(oSearchSelect.getSelectedKey() === sUdcKey)
					sQueryName = "getOrdersByUdcSQ";
				else if(oSearchSelect.getSelectedKey() === sMaterialKey)
					sQueryName = "getOrdersByMaterialSQ";
				else
					return;

				var sOrdersQuery = QService + sMovementPath + "ProdDecl/" + sQueryName;
				sOrdersQuery += "&Param.1=" + oArguments.plant;
				sOrdersQuery += "&Param.2=" + this.getValue();

				var oModel = oOrderCombo.getModel();
				oModel.loadData(sOrdersQuery,false,false);
			},
			layoutData : new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oSearchInput.onsapenter = function (oEvent)
		{
			if(oSearchInput.getValue())
			{
				oSearchInput.fireChange();
				oOrderCombo.focus();
			}
		};

		var oSearchSelect = new sap.m.Select({
			width: '100%',
			textAlign: sap.ui.core.TextAlign.Right,
			items: [
				new sap.ui.core.Item({
					key : sUdcKey,
					text: oLng_Opr.getText("BoxDecl_Udc")
				}),
				new sap.ui.core.Item({
					key : sMaterialKey,
					text: oLng_Opr.getText("BoxDecl_Material")
				})
			],
			change: function() {
				oSearchInput.setValue();
				oSearchInput.fireChange();
				oSearchInput.focus();
			},
			layoutData : new sap.ui.layout.GridData({
				span: "L4 M4 S12"
			})
		}).addStyleClass("customSelect");

		var oDialog = new sap.m.Dialog({
			title: oLng_Opr.getText("BoxDecl_SearchOrderPhase"),
			contentWidth: "600px",
			//contentHeight: "160px",
			content: [
				new sap.ui.layout.Grid({
					hSpacing: 1,
					vSpacing: 0.5,
					content: [
						oSearchSelect,
						oSearchInput,
						new sap.m.Label({
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							text: "Commessa:",
							layoutData : new sap.ui.layout.GridData({
								span: "L4 M4 S12",
								linebreak: true
							})
						}).addStyleClass("inputLabel"),
						oOrderCombo,
						new sap.m.Label({
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							text: "Fase:",
							layoutData : new sap.ui.layout.GridData({
								span: "L4 M4 S12",
								linebreak: true
							})
						}).addStyleClass("inputLabel"),
						oPoperCombo
					]
				}).addStyleClass("sapUiMediumMarginBegin sapUiTinyMarginTop")
			],
			buttons: [
				new sap.m.Button({
					text: oLng_Opr.getText("BoxDecl_Confirm"),
					icon: "sap-icon://accept",
					press: function() {
						const sOrder = oOrderCombo.getSelectedKey();
						const sPoper = oPoperCombo.getSelectedKey();

						oOrderCombo.setValueState(sap.ui.core.ValueState.None);
						oOrderCombo.setValueStateText();
						oPoperCombo.setValueState(sap.ui.core.ValueState.None);
						oPoperCombo.setValueStateText();

						var sErrorMessage, oErrorField;
						if(!sOrder)
						{
							sErrorMessage = oLng_Opr.getText("BoxDecl_OrderErrorMessage");
							oErrorField = oOrderCombo;
						}
						else if(!sPoper)
						{
							sErrorMessage = oLng_Opr.getText("BoxDecl_PoperErrorMessage");
							oErrorField = oPoperCombo;
						}

						if(sErrorMessage)
						{
							oErrorField.setValueState(sap.ui.core.ValueState.Warning);
							oErrorField.setValueStateText(sErrorMessage);

							sap.m.MessageBox.show(sErrorMessage, {
								icon: sap.m.MessageBox.Icon.WARNING,
								title: oLng_Opr.getText("General_WarningText"),
								actions: sap.m.MessageBox.Action.OK
							});
						}
						else
						{
							oDialog.close();
							callbackFunc(sOrder + sPoper);
						}
					}
				}),
				new sap.m.Button({
					text: oLng_Opr.getText("BoxDecl_Cancel"),
					icon: "sap-icon://decline",
					press: function() {
						oDialog.close();
					}
				})
			],
			afterOpen: function() {
				oSearchInput.focus();
			},
			afterClose: function() {
				oDialog.destroy();
			}
		});

		oDialog.open();
	},

/***********************************************************************************************/
// Avanzamento alla pagina successiva
/***********************************************************************************************/

	onNext: function(oParams){

		var oOrderInput = $.UIbyID('boxDeclOrderInput4');
		const sOrderPhase = oOrderInput.getValue();

		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();

		const sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage) {
			oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderInput.setValueStateText(sErrorMessage);

			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else // Controlla commessa e fase, selezione linea e udc nuovo o incompleto
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			var aLinesObjects = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
			{
				aLinesObjects = oSettings.LINES;
				for(var i=0; i<aLinesObjects.length; i++)
					aLines.push(aLinesObjects[i].lineid);
			}

			const aSpecialWarningCodes = [2,3];

			var oController = sap.ui.getCore().byId('boxDeclView4').getController();
			var oArguments = oController._oArguments;

			var sCheckQuery = sMovementPath + "UDC/checkUDCOutPartLineQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + sOrderPhase;
			sCheckQuery += "&Param.3=" + aLines.join();
			sCheckQuery += "&Param.4=M";
			sCheckQuery += "&Param.5=" + sLanguage;

			var navFunction = function(oCallbackParams)
			{
				oArguments['order'] = oCallbackParams.order;
				oArguments['poper'] = oCallbackParams.poper;
				oArguments['idline'] = oCallbackParams.lineData.lineid;
				oArguments['udc'] = oCallbackParams.udcnr;

				oAppRouter.navTo('boxDeclInput4',{
					query: oArguments
				});
			}

			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oController.onLineSuccess,
				showWarning: aSpecialWarningCodes, 			// array codici di errore per cui visualizza il messaggio
				onWarning: oController.onLineWarning,
				callbackParams: {
					warningCodes: aSpecialWarningCodes,		// come showWarning per recuperarlo in onWarning
					linesObjects: aLinesObjects,			// linee di postazione con linetxt corrispondente (match in onSuccess e onWarning)
					oneLineCallback: oController.onLineSuccess,		// callback per onWarning quando c'e una sola linea
					lineCallback: oController.onUdcSelection,		// callback a seguito della selezione della linea
					udcCallback: oController.onNav,					// callback di salvataggio nello storage dei dati recuperati
					navCallback: navFunction,						// callback di navigazione alla pagina successiva
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					plant: oArguments.plant
				},
				results: {
					returnProperties: ['outputLines','cicTxt','material','matDesc','cdlId','cdlDesc','imbObbl'],
					returnXMLDoc: true
				}
			});
		}
	},

	onLineSuccess: function(oResults,oCallbackParams) // linea di postazione trovata
	{
		oCallbackParams.returnedData = oResults;

		var oLine;
		var sLine = '';
		if(oResults.hasOwnProperty('outputLines') && oResults.outputLines)
		{
			var aOutputLines = oResults.outputLines.split(',');
			sLine = aOutputLines[0];
			if(oCallbackParams.linesObjects.length > 0)
			{
				oLine = selectSingleObject({
					objects: oCallbackParams.linesObjects,
					propertyName: 'lineid',
					propertyValue: sLine
				});
			}
			else // recupero il testo quando linesObjects vuoto
			{
				var sLineRequest = sMasterDataPath + "Lines/getLinebyIdSQ";
				sLineRequest += "&Param.1=" + sLine;
				var sLineTxt = fnGetAjaxVal(sLineRequest,'LINETXT',false);

				oLine = {
					lineid: sLine,
					linetxt: sLineTxt
				};
			}
		}

		if(!oLine)
		{
			oLine = {
				lineid: sLine,
				linetxt: ''
			};
		}

		oCallbackParams.lineCallback(oLine,oCallbackParams);
	},

	onLineWarning: function(oResults,oCallbackParams) // Selezione linea
	{
		var oModel;
		var sModelTitle = '';
		var sModelDesc = '';
		var callbackFunc;
		if(oResults.hasOwnProperty('outputLines'))
		{
			if(oResults.outputLines)
			{
				var aOutputLines = oResults.outputLines.split(",");
				if(aOutputLines.length === 1) // una sola linea disponibile alla selezione
				{
					oCallbackParams.oneLineCallback(oResults,oCallbackParams);
					return;
				}
				else
				{
					var aObjectOutLines = [];
					for(var i=0; i<aOutputLines.length;i++)
					{
						const sCurrLine = aOutputLines[i];
						const oCurrOutLine = selectSingleObject({
							objects: oCallbackParams.linesObjects,
							propertyName: 'lineid',
							propertyValue: sCurrLine
						});

						if(oCurrOutLine)
							aObjectOutLines.push(oCurrOutLine);
						else
							aObjectOutLines.push({ // recuperare linetxt tramite query?
								lineid: sCurrLine,
								linetxt: ''
							});
					}
					oModel = new sap.ui.model.json.JSONModel(aObjectOutLines);
					sModelTitle = 'lineid';
					sModelDesc = 'linetxt';
					callbackFunc = oCallbackParams.lineCallback;
				}
			}
			else if(oCallbackParams.warningCodes.indexOf(parseInt(oResults.code)) > -1) // commessa e/o fase non trovate
			{
				oModel = new sap.ui.model.xml.XMLModel();

				var sLoadQuery = QService + sMasterDataPath + 'Lines/getLinesbyPlantSQ';
				sLoadQuery += '&Param.1=' + oCallbackParams.plant;
				oModel.loadData(sLoadQuery);

				sModelTitle = 'IDLINE';
				sModelDesc = 'LINETXT';

				callbackFunc = function(oLineInfo,oCallbackParams)
				{
					oLineInfo = {
						lineid: oLineInfo.IDLINE,
						linetxt: oLineInfo.LINETXT
					};
					oCallbackParams.lineCallback(oLineInfo,oCallbackParams);
				};
			}
		}

		oCallbackParams.returnedData = oResults;

		oAppController.selectFromDialog({
			title: oLng_Opr.getText("General_SelectLine"),
			noDataText: oLng_Opr.getText("General_NoLine"),
			callback: callbackFunc,
			callbackParams: oCallbackParams,
			model: oModel,
			modelTitle: sModelTitle,
			modelDesc: sModelDesc
		});
	},

	onUdcSelection: function(oLineInfo,oCallbackParams) // presenza udc incompleti
	{
		oCallbackParams.lineData = oLineInfo;

		var nextFunction = oCallbackParams.udcCallback;

		var oUdcModel = new sap.ui.model.xml.XMLModel(oCallbackParams.returnedData.data);
		var oRowsetObject = oUdcModel.getObject("/Rowset/1/");
		if(oRowsetObject && oRowsetObject.childNodes.length-1>0) //numero di UDC nel modello
		{
			const sExistingTxt = oLng_Opr.getText('BoxDecl_SelectUdcExisting');
			sap.m.MessageBox.show(oLng_Opr.getText('BoxDecl_SelectUdcMessage'), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText('General_WarningText'),
				actions: [
					sExistingTxt,
					oLng_Opr.getText('BoxDecl_SelectUdcNew')
				],
				onClose: function(oAction) {
					if (oAction === sExistingTxt)
						oAppController.selectFromDialog({
							title: oLng_Opr.getText('BoxDecl_SelectUdcTitle'),
							noDataText: oLng_Opr.getText('BoxDecl_SelectUdcTitle'),
							callback: nextFunction,
							callbackParams: oCallbackParams,
							model: oUdcModel,
							modelPath: "/Rowset/1/Row",
							modelTitle: 'UDCNR',
							modelDesc: 'MATERIAL',
							template: oAppController.customListTemplate({
								titleText: "{UDCNR}",
								bottomLeftText: "{MATERIAL}",
								topRightText: {
									parts: [
										{path: "QTYRES" }
									],
									formatter: function(sQtyRes) {
										return oLng_Opr.getText('BoxDecl_Qty') + ": " + sQtyRes;
									}
								},
								bottomRightText: {
									parts: [
										{path: "TIMEID" }
									],
									formatter: dateFormatter
								},
								left: oCallbackParams.leftUdcData,
								right: [
									new sap.m.Label({
										text: "{LAST_OPENAME}",
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right
									}).addStyleClass("smallLabel sapUiTinyMarginTop")
								]
							})
						});
					else
						nextFunction(undefined,oCallbackParams);
				}
			});
		}
		else
			nextFunction(undefined,oCallbackParams);
	},

	onNav: function(oUdcObject,oCallbackParams) // passaggio alla schermata successiva
	{
		var sUdc = '';
		if(oUdcObject && oUdcObject.hasOwnProperty('UDCNR'))
			sUdc = oUdcObject.UDCNR;
		else
			oUdcObject = {
				ORDER: oCallbackParams.order,
				POPER: oCallbackParams.poper,
				UDCNR: sUdc,
				MATERIAL: oCallbackParams.returnedData.material,
				MAT_DESC: oCallbackParams.returnedData.matDesc
			};

		oCallbackParams.udcnr = sUdc;

		oUdcObject.CICTXT = oCallbackParams.returnedData.cicTxt;
		oUdcObject.IMBOBBL = oCallbackParams.returnedData.imbObbl;
		oUdcObject.LINEID = oCallbackParams.lineData.lineid;
		oUdcObject.LINETXT = oCallbackParams.lineData.linetxt;
		oUdcObject.CDLID = oCallbackParams.returnedData.cdlId;
		oUdcObject.CDLDESC = oCallbackParams.returnedData.cdlDesc;

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);

		oCallbackParams.navCallback(oCallbackParams);
	}
});
