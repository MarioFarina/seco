// View Dichiarazioni di Produzione V2 Sospesi
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PRODDECL2.prodDecl2Susp", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PRODDECL2.prodDecl2Susp";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oFormText = new sap.ui.layout.form.SimpleForm({
			editable: false,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 4,
			labelSpanM: 4,
			emptySpanL: 3,
			emptySpanM: 3,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2Susp_SuspQty")
				}),
				new sap.m.Text({
					id: 'prodDecl2SuspQtyText'
				})
			]
		});
		
		var oTable = new sap.m.Table({
			id: 'prodDecl2SuspTable',
			columns: [
		          new sap.m.Column({
		        	  header: new sap.m.Text({text: oLng_Opr.getText("ProdDecl2Susp_Causal")}),
		        	  //width: "28%"
		          }),
		          new sap.m.Column({
		        	  header: new sap.m.Text({text: oLng_Opr.getText("ProdDecl2Susp_Qty")}),
		        	  /*hAlign: sap.ui.core.TextAlign.Right,
		        	  width: "15%"*/
		          }),
				  new sap.m.Column({
		        	  width: "48px"
		          })
    		]
		});
		
		var oPage = new sap.m.Page({
			id: "prodDecl2SuspPage",
			enableScrolling: true,
			title: oLng_Opr.getText("ProdDecl2Susp_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oFormText,
				oTable
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						text: oLng_Opr.getText("ProdDecl2Susp_Complete"),
						icon: "sap-icon://accept",
						press: oController.onComplete
					}).addStyleClass("greenButton noBorderButton")
				]
			})
		});
		
	  	return oPage;
	}

});