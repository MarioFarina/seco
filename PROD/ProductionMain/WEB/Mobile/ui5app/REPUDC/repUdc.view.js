// View Riparazione / Rilavorazione
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.REPUDC.repUdc", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.REPUDC.repUdc";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		/*
		var oOrderInput = new sap.m.Input({
			id: 'repUdcOrder',
			width: '100%',
			layoutData : new sap.ui.layout.GridData({
				span: "L6 M6 S12"
			})
		});
		this.addDependent(oOrderInput);*/
		
		var oOrderCombo = new sap.m.ComboBox({
			id: 'repUdcOrderBox',
			width: '100%',
			layoutData : new sap.ui.layout.GridData({
				span: "L6 M6 S12"
			})
		});
		//this.addDependent(oOrderCombo);
		
		var oRepOrdersTemplate = new sap.ui.core.Item({
			key : "{repOrders>ORDER_POPER}",
			text: {
				parts: [
					{path: "/VIEWTYPE" },
					{path: "repOrders>ORDER_POPER" },
					{path: "repOrders>CICTXT" },
					{path: "repOrders>MATERIAL" },
					{path: "repOrders>VERSION" }
				],		     
				formatter: function(sViewType,sOrderPoper,sCicTxt,sMaterial,sVersion) {
					if(sOrderPoper)
						sOrderPoper = sOrderPoper.replace(/^0+/, '');
					var sText;
					if(sViewType === 'R')
						sText = sOrderPoper + " - ";
					else if(sViewType === 'W')
						sText = sOrderPoper + " - " + sMaterial + " v." + sVersion + " ";
					sText += sCicTxt;
					return sText;
				}
			}
		});
		oOrderCombo.bindAggregation("items","repOrders>/Rowset/Row",oRepOrdersTemplate);
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdc_UdCFrom") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.HBox({
					items: [
						new sap.m.Input({
							id: 'repUdcFromInput',
							type: sap.m.InputType.Number,
							change: oController.onUdcFromChange,
							layoutData: [
								 new sap.m.FlexItemData({
									 growFactor: 1
								 })
							]
						}),
						new sap.m.Button({
							icon: "sap-icon://decline",
							press: function() {
								var oUdcFromInput = $.UIbyID('repUdcFromInput');
								oUdcFromInput.setValue();
								
								var oOrderCombo = sap.ui.getCore().byId("repUdcOrderBox");
								var oModel = oOrderCombo.getModel('repOrders');
								if(oModel)
									oModel.destroy()
								oOrderCombo.setModel(new sap.ui.model.xml.XMLModel(),'repOrders');
							}
						})
					],
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S12"
					})
				}),
				new sap.m.Label({
					text: {
						parts: [
							{path: "/VIEWTYPE" }
						],		     
						formatter: function(sViewType) {
							var sTitleKey = '';
							switch(sViewType)
							{
								case 'R':
									sTitleKey = 'RepUdc_RepPhase';
									break;
								case 'W':
									sTitleKey = 'RepUdc_OrderPhase';
									break;
							}
							return oLng_Opr.getText(sTitleKey);
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				oOrderCombo
				
			]
		}).addStyleClass("sapUiMediumMarginTop");
		
		/*this.attachModelContextChange(function () {
			var oModel = this.getModel();
			if(oModel)
			{
				const sViewType = oModel.getProperty("/VIEWTYPE");
				switch(sViewType)
				{
					case 'R':
						oGridForm.addContent(oOrderCombo);
						oGridForm.removeContent(oOrderInput);
						break;
					case 'W':
						oGridForm.addContent(oOrderInput);
						oGridForm.removeContent(oOrderCombo);
						break;	
				}
			}
		});*/
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
						  
		var oPage = new sap.m.Page({
			id: "repUdcPage",
			enableScrolling: false,
			title: {
				parts: [
					{path: "/VIEWTYPE" }
				],		     
				formatter: function(sViewType) {
					var sTitleKey = '';
					switch(sViewType)
					{
						case 'R':
							sTitleKey = 'RepUdc_Title';
							break;
						case 'W':
							sTitleKey = 'RewUdc_Title';
							break;
					}
					return oLng_Opr.getText(sTitleKey);
				}
			},
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("RepUdc_Next"),
							icon: "sap-icon://navigation-right-arrow",
							press: oController.onNext
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});