// Controller Pac - Lettura UDC
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.PAC.pacNewUdc", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute('pacNewUdc').attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		var oController = this;
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']); 
		oController._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('pacNewUdcPage'),sPlant,sPernr);
		
		var oOrderInput = sap.ui.getCore().byId('pacNewUdcOrder');
		oOrderInput.setValue();
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();
		
		var oStdQtyCombo = sap.ui.getCore().byId('pacNewUdcStdQty');
		var oStdQtyModel = oStdQtyCombo.getModel();
		if(oStdQtyModel)
			oStdQtyModel.destroy();
		oStdQtyCombo.setModel(new sap.ui.model.xml.XMLModel());
		oStdQtyCombo.setSelectedKey();
		oStdQtyCombo.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			oOrderInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('pacNewUdcView').getController();
		oAppRouter.navTo('pacUdc',{
			query: oController._oArguments
		});
	},
	
	onOrderPoperLiveChange: function() {
		var oStdQtyCombo = sap.ui.getCore().byId('pacNewUdcStdQty');
		oStdQtyCombo.setValueState(sap.ui.core.ValueState.None);
		oStdQtyCombo.setValueStateText();
		var oStdQtyModel = oStdQtyCombo.getModel();
		const sOrderPhase = this.getValue();
		if(!oAppController.checkOrderPhase(sOrderPhase)) // errore
		{
			var oController = sap.ui.getCore().byId('pacNewUdcView').getController();
			
			var sStdQtyQuery = QService + sMasterDataPath + "Nmimb/getNmimbByOrderPoperSQ";
			sStdQtyQuery += "&Param.1=" + oController._oArguments.plant;
			sStdQtyQuery += "&Param.2=" + sOrderPhase.substring(0,12);
			sStdQtyQuery += "&Param.3=" + sOrderPhase.substring(12,16);
			oStdQtyModel.loadData(sStdQtyQuery,false,false);
		}
		else
		{
			oStdQtyModel.destroy();
			oStdQtyCombo.setModel(new sap.ui.model.xml.XMLModel());
		}
		
		if(oStdQtyCombo.getItems().length === 0)
		{
			oStdQtyCombo.setValueState(sap.ui.core.ValueState.Warning);
			oStdQtyCombo.setValueStateText(oLng_Opr.getText("PacNewUdc_NoStQty"));
		}
	},
	
	onCreate: function() {
		
		var oOrderInput = $.UIbyID('pacNewUdcOrder');
		const sOrderPhase = oOrderInput.getValue();
		
		var oStdQtyCombo = sap.ui.getCore().byId('pacNewUdcStdQty');
		const sStdQty = oStdQtyCombo.getSelectedKey();
		
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();
		
		var sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage)
		{
			oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderInput.setValueStateText(sErrorMessage);
		}
		else if(!sStdQty)
		{
			if(oStdQtyCombo.getItems().length === 0)
				sErrorMessage = oLng_Opr.getText("PacNewUdc_NoStQty");
			else
				sErrorMessage = oLng_Opr.getText("PacNewUdc_StdQtyErrorMessage");
			oStdQtyCombo.setValueState(sap.ui.core.ValueState.Warning);
			oStdQtyCombo.setValueStateText(sErrorMessage);
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else // Controlla commessa e fase, selezione linea e udc nuovo o incompleto
		{			
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			var aLinesObjects = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
			{
				aLinesObjects = oSettings.LINES;
				for(var i=0; i<aLinesObjects.length; i++)
					aLines.push(aLinesObjects[i].lineid);
			}
			
			const aSpecialWarningCodes = [2,3];
			
			var oBoxDeclController = sap.ui.controller('ui5app.BOXDECL.boxDecl');
			
			var oController = sap.ui.getCore().byId('pacNewUdcView').getController();
			const oArguments = oController._oArguments;
			
			var sCheckQuery = sMovementPath + "PAC/checkAndCreateUDCforPACQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + oArguments.pernr;
			sCheckQuery += "&Param.3=" + oAppController._sShift;
			sCheckQuery += "&Param.4=" + oAppController._sDate;
			sCheckQuery += "&Param.5=" + aLines.join();
			sCheckQuery += "&Param.6=" + sOrderPhase;
			sCheckQuery += "&Param.7=" + sStdQty;
			sCheckQuery += "&Param.8=" + getDatetimeString(new Date());
			sCheckQuery += "&Param.9=" + sLanguage;
					
			oAppController.handleRequest(sCheckQuery,{
				onSuccess: oController.onNavBack,
				showWarning: aSpecialWarningCodes,
				onWarning: oBoxDeclController.onLineWarning, // callbackParams: linesObjects e lineCallback
				callbackParams: {
					linesObjects: aLinesObjects,
					oneLineCallback: oBoxDeclController.onLineSuccess,
					lineCallback: oController.createUdC, // callbackParams: createCallback
					createCallback: oController.onNavBack,
					plant: oArguments.plant,
					pernr: oArguments.pernr,
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					stdQty: sStdQty
				},
				results: {
					returnProperties: ['outputLines'],
					returnXMLDoc: true
				}
			});
		}
	},
	
	createUdC: function(oLineInfo,oCallbackParams) {
		
		const sSelectedLine = oLineInfo.lineid;
		var returnFunction = oCallbackParams.createCallback;
		
		const sOrder = oCallbackParams.order;
		const sPhase = oCallbackParams.poper;
		
		var sCreateQuery = sMovementPath + "PAC/createAndPrintUDCMovForPACQR";
		sCreateQuery += "&Param.1=" + oCallbackParams.plant;
		sCreateQuery += "&Param.2=" + oCallbackParams.pernr;
		sCreateQuery += "&Param.3=" + oAppController._sShift;
		sCreateQuery += "&Param.4=" + oAppController._sDate;
		sCreateQuery += "&Param.5=" + sSelectedLine;
		sCreateQuery += "&Param.6=" + sOrder;
		sCreateQuery += "&Param.7=" + sPhase;
		sCreateQuery += "&Param.8=" + oCallbackParams.stdQty;
		sCreateQuery += "&Param.9=" + getDatetimeString(new Date());
		sCreateQuery += "&Param.10=" + sLanguage;
		
		oAppController.handleRequest(sCreateQuery,{
			onSuccess: returnFunction
		});
	}

});