// View Pac - Lettura UDC
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PAC.pacUdc", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PAC.pacUdc";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("PacUdc_UdC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'pacUdcInput',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S12"
					})
				}),
				new sap.m.Button({
					text: oLng_Opr.getText("PacUdc_New"),
					icon: "sap-icon://add",
					press: oController.onNewUdC,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).addStyleClass("greenButton noBorderButton")
			]
		}).addStyleClass("sapUiMediumMarginTop");
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
						  
		var oPage = new sap.m.Page({
			id: "pacUdcPage",
			enableScrolling: false,
			title: oLng_Opr.getText("PacUdc_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("PacUdc_Next"),
							icon: "sap-icon://navigation-right-arrow",
							press: oController.onNext
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});