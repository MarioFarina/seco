// Controller Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL5.boxDecl5", {

	_oArguments: undefined,
	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 */
	onInit: function () {
		oAppRouter.getRoute('boxDecl5').attachPatternMatched(this.onObjectMatched, this);

		var oOrderInput = $.UIbyID('boxDeclOrderInput5');

		var nextFunc = this.onNext;
		oOrderInput.onsapenter = function (oEvent) {
			nextFunc();
		}
	},

	/***********************************************************************************************/
	// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
	/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArguments = oAppController.getPatternArguments(oEvent, ['plant', 'pernr']);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclPage5'), sPlant, sPernr);

		var oOrderInput = sap.ui.getCore().byId('boxDeclOrderInput5');
		oOrderInput.setValue();
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();

		jQuery.sap.delayedCall(500, null, function () {
			oOrderInput.focus();
		});
	},

	/***********************************************************************************************/
	// Torna al menu
	/***********************************************************************************************/

	onNavBack: function () {
		var oController = sap.ui.getCore().byId('boxDeclView5').getController();
		oAppRouter.navTo('menu', {
			query: oController._oArguments
		});
	},

	/***********************************************************************************************/
	// Ricerca ordine fase per UdC precedente o materiale
	/***********************************************************************************************/

	onSearchOrder: function (oEvent, oData) {

		var callbackFunc = this.callback ? this.callback : oEvent.callback;
		if (typeof (callbackFunc) != 'function')
			return;

		var oController = this.controller ? this.controller : oEvent.controller;
		var oArguments = oController._oArguments;

		let udc = this.udc ? this.udc : oEvent.udc;

		const sUdcKey = "udc";
		const sMaterialKey = "material";
		const sOrderKey = "order";

		var oPoperCombo = new sap.m.ComboBox({
			width: '100%',
			layoutData: new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oPoperCombo.setModel(new sap.ui.model.xml.XMLModel());

		var oPoperTemplate = new sap.ui.core.Item({
			key: "{POPER}",
			text: {
				parts: [{
						path: "POPER"
				},
					{
						path: "CICTXT"
								}
							 ],
				formatter: function (sPoper, sCictxt) {
					return sPoper + " - " + sCictxt;
				}
			}
		});
		oPoperCombo.bindAggregation("items", "/Rowset/Row", oPoperTemplate);

		var oOrderCombo = new sap.m.ComboBox({
			width: '100%',
			selectionChange: function () {
				oPoperCombo.clearSelection();
				oPoperCombo.setValue();

				const sMaterial = this.getSelectedItem().getBindingContext().getProperty('MATERIAL');

				var sPoperQuery = QService + sMovementPath + "ProdDecl/getPopersByOrderSQ";
				sPoperQuery += "&Param.1=" + oArguments.plant;
				sPoperQuery += "&Param.2=" + sMaterial;
				sPoperQuery += "&Param.3=" + this.getSelectedKey();

				var oModel = oPoperCombo.getModel();
				oModel.loadData(sPoperQuery, false, false);
			},
			layoutData: new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oOrderCombo.setModel(new sap.ui.model.xml.XMLModel());

		var oOrdersTemplate = new sap.ui.core.Item({
			key: "{ORDER}",
			text: {
				parts: [{
						path: "ORDER"
				},
					{
						path: "MATERIAL"
								},
					{
						path: "DESC"
								}
							 ],
				formatter: function (sOrder, sMaterial, sDesc) {
					return sOrder + " - " + sMaterial + " " + sDesc;
				}
			}
		});
		oOrderCombo.bindAggregation("items", {
			path: "/Rowset/Row",
			length: 10000,
			template: oOrdersTemplate
		});

		var oSearchInput = new sap.m.Input({
			//type: sap.m.InputType.Number,
			id: "searchInput",
			visible: true,
			value: udc,
			change: function () {
				oOrderCombo.clearSelection();
				oOrderCombo.setValue();
				oPoperCombo.clearSelection();
				oPoperCombo.setValue();

				var sQueryName;
				if (oSearchSelect.getSelectedKey() === sUdcKey)
					sQueryName = "getOrdersByUdcQR";
				else if (oSearchSelect.getSelectedKey() === sMaterialKey)
					sQueryName = "getOrdersByMaterialSQ";
				else if (oSearchSelect.getSelectedKey() === sOrderKey)
					sQueryName = "getOrdersSQ";
				else
					return;

				var sOrdersQuery = QService + sMovementPath + "ProdDecl/" + sQueryName;
				sOrdersQuery += "&Param.1=" + oArguments.plant;
				sOrdersQuery += "&Param.2=" + this.getValue();

				var oModel = oOrderCombo.getModel();
				oModel.loadData(sOrdersQuery, false, false);
			},
			layoutData: new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});

		oSearchInput.onsapenter = function (oEvent) {
			if (oSearchInput.getValue()) {
				oSearchInput.fireChange();
				oOrderCombo.focus();
			}
		};

		var oSearchSelect = new sap.m.Select({
			width: '100%',
			textAlign: sap.ui.core.TextAlign.Right,
			items: [
				new sap.ui.core.Item({
					key: sUdcKey,
					text: oLng_Opr.getText("BoxDecl_Udc")
				}),
				new sap.ui.core.Item({
					key: sMaterialKey,
					text: oLng_Opr.getText("BoxDecl_Material")
				}),
				new sap.ui.core.Item({
					key: sOrderKey,
					text: oLng_Opr.getText("ProdDecl_Order")
				})
			],
			change: function () {
				if (this.getSelectedKey() === sOrderKey) {
					oSearchInput.setVisible(false);
				} else {
					oSearchInput.setVisible(true);
				}
				oSearchInput.setValue();
				oSearchInput.fireChange();
				oSearchInput.focus();
			},
			layoutData: new sap.ui.layout.GridData({
				span: "L4 M4 S12"
			})
		}).addStyleClass("customSelect");

		var oDialog = new sap.m.Dialog({
			title: oLng_Opr.getText("BoxDecl_SearchOrderPhase"),
			contentWidth: "60%",
			content: [
				new sap.ui.layout.Grid({
					hSpacing: 1,
					vSpacing: 0.5,
					content: [
						oSearchSelect,
						oSearchInput,
						new sap.m.Label({
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							text: oLng_Opr.getText("ProdDecl_Order") + ':',
							layoutData: new sap.ui.layout.GridData({
								span: "L4 M4 S12",
								linebreak: true
							})
						}).addStyleClass("inputLabel"),
						oOrderCombo,
						new sap.m.Label({
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							text: oLng_Opr.getText("ProdDecl_Phase") + ':',
							layoutData: new sap.ui.layout.GridData({
								span: "L4 M4 S12",
								linebreak: true
							})
						}).addStyleClass("inputLabel"),
						oPoperCombo
					]
				}).addStyleClass("sapUiMediumMarginBegin sapUiTinyMarginTop")
			],
			buttons: [
				new sap.m.Button({
					text: oLng_Opr.getText("BoxDecl_Confirm"),
					icon: "sap-icon://accept",
					press: function () {
						const sOrder = oOrderCombo.getSelectedKey();
						const sPoper = oPoperCombo.getSelectedKey();

						oOrderCombo.setValueState(sap.ui.core.ValueState.None);
						oOrderCombo.setValueStateText();
						oPoperCombo.setValueState(sap.ui.core.ValueState.None);
						oPoperCombo.setValueStateText();

						var sErrorMessage, oErrorField;
						if (!sOrder) {
							sErrorMessage = oLng_Opr.getText("BoxDecl_OrderErrorMessage");
							oErrorField = oOrderCombo;
						} else if (!sPoper) {
							sErrorMessage = oLng_Opr.getText("BoxDecl_PoperErrorMessage");
							oErrorField = oPoperCombo;
						}

						if (sErrorMessage) {
							oErrorField.setValueState(sap.ui.core.ValueState.Warning);
							oErrorField.setValueStateText(sErrorMessage);

							sap.m.MessageBox.show(sErrorMessage, {
								icon: sap.m.MessageBox.Icon.WARNING,
								title: oLng_Opr.getText("General_WarningText"),
								actions: sap.m.MessageBox.Action.OK
							});
						} else {
							oDialog.close();
							callbackFunc(sOrder + sPoper);
						}
					}
				}),
				new sap.m.Button({
					text: oLng_Opr.getText("BoxDecl_Cancel"),
					icon: "sap-icon://decline",
					press: function () {
						oDialog.close();
					}
				})
			],
			afterOpen: function () {
				if (udc) {
					oSearchInput.fireChange();
					oOrderCombo.focus();
				}
				oSearchInput.focus();
			},
			afterClose: function () {
				oDialog.destroy();
			}
		});

		oDialog.open();
	},

	onSearchOrderNew: function (oEvent, oData) {

		var callbackFunc = this.callback ? this.callback : oEvent.callback;
		if (typeof (callbackFunc) != 'function')
			return;

		var oController = this.controller ? this.controller : oEvent.controller;
		var oArguments = oController._oArguments;

		let funzione = this.funzione ? this.funzione : oEvent.funzione;
		let udc = this.udc ? this.udc : oEvent.udc;
		let commessa = this.order ? this.order : oEvent.order;

		var oPoperCombo = new sap.m.ComboBox({
			width: '100%',
			layoutData: new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oPoperCombo.setModel(new sap.ui.model.xml.XMLModel());

		var oPoperTemplate = new sap.ui.core.Item({
			key: "{POPER}",
			text: {
				parts: [{
						path: "POPER"
				},
					{
						path: "CICTXT"
								}
							 ],
				formatter: function (sPoper, sCictxt) {
					return sPoper + " - " + sCictxt;
				}
			}
		});
		oPoperCombo.bindAggregation("items", "/Rowset/Row", oPoperTemplate);

		var sPoperQuery = QService + sMasterDataPath + "Cycles/getCyclesDetailsByOrdertotemSQ";
		sPoperQuery += "&Param.1=" + oArguments.plant;
		sPoperQuery += "&Param.2=" + commessa;

		var oModel = oPoperCombo.getModel();
		oModel.loadData(sPoperQuery, false, false);

		var oOrderCombo = new sap.m.Input({
			value: commessa,
			selectedKey: commessa,
			editable: false
		});

		/*new sap.m.ComboBox({
			width: '100%',
			value: commessa,
			selectionChange: function () {
				oPoperCombo.clearSelection();
				oPoperCombo.setValue();

				var sPoperQuery = QService + sMasterDataPath + "Cycles/getCyclesDetailsByOrdertotemSQ";
				sPoperQuery += "&Param.1=" + oArguments.plant;
				sPoperQuery += "&Param.2=" + this.getSelectedKey();

				var oModel = oPoperCombo.getModel();
				oModel.loadData(sPoperQuery, false, false);
			},
			layoutData: new sap.ui.layout.GridData({
				span: "L8 M8 S12"
			})
		});
		oOrderCombo.setModel(new sap.ui.model.xml.XMLModel());

		var oOrdersTemplate = new sap.ui.core.Item({
			key: "{ORDER}",
			text: {
				parts: [{
						path: "ORDER"
					},
					{
						path: "MATERIAL"
					},
					{
						path: "DESC"
					}
				],
				formatter: function (sOrder, sMaterial, sDesc) {
					return sOrder + " - " + sMaterial + " " + sDesc;
				}
			}
		});
		oOrderCombo.bindAggregation("items", {
			path: "/Rowset/Row",
			length: 10000,
			template: oOrdersTemplate
		});

		var sOrdersQuery = QService + sMovementPath + "ProdDecl/getOrdersSQ";
		sOrdersQuery += "&Param.1=" + oArguments.plant;
		sOrdersQuery += "&Param.2=" + udc;

		var oModel = oOrderCombo.getModel();
		oModel.loadData(sOrdersQuery, false, false);*/

		var oDialog = new sap.m.Dialog({
			title: oLng_Opr.getText("BoxDecl_SearchOrderPhase"),
			contentWidth: "60%",
			content: [
				new sap.ui.layout.Grid({
					hSpacing: 1,
					vSpacing: 0.5,
					content: [
						new sap.m.Label({
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							text: oLng_Opr.getText("ProdDecl_Order") + ':',
							layoutData: new sap.ui.layout.GridData({
								span: "L4 M4 S12",
								linebreak: true
							})
						}).addStyleClass("inputLabel"),
						oOrderCombo,
						new sap.m.Label({
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							text: oLng_Opr.getText("ProdDecl_Phase") + ':',
							layoutData: new sap.ui.layout.GridData({
								span: "L4 M4 S12",
								linebreak: true
							})
						}).addStyleClass("inputLabel"),
						oPoperCombo
					]
				}).addStyleClass("sapUiMediumMarginBegin sapUiTinyMarginTop")
			],
			buttons: [
				new sap.m.Button({
					text: oLng_Opr.getText("BoxDecl_Confirm"),
					icon: "sap-icon://accept",
					press: function () {
						const sOrder = oOrderCombo.getValue();
						const sPoper = oPoperCombo.getSelectedKey();

						oOrderCombo.setValueState(sap.ui.core.ValueState.None);
						oOrderCombo.setValueStateText();
						oPoperCombo.setValueState(sap.ui.core.ValueState.None);
						oPoperCombo.setValueStateText();

						var sErrorMessage, oErrorField;
						if (!sOrder) {
							sErrorMessage = oLng_Opr.getText("BoxDecl_OrderErrorMessage");
							oErrorField = oOrderCombo;
						} else if (!sPoper) {
							sErrorMessage = oLng_Opr.getText("BoxDecl_PoperErrorMessage");
							oErrorField = oPoperCombo;
						}

						if (sErrorMessage) {
							oErrorField.setValueState(sap.ui.core.ValueState.Warning);
							oErrorField.setValueStateText(sErrorMessage);

							sap.m.MessageBox.show(sErrorMessage, {
								icon: sap.m.MessageBox.Icon.WARNING,
								title: oLng_Opr.getText("General_WarningText"),
								actions: sap.m.MessageBox.Action.OK
							});
						} else {
							oDialog.close();
							callbackFunc(sOrder + sPoper);
						}
					}
				}),
				new sap.m.Button({
					text: oLng_Opr.getText("BoxDecl_Cancel"),
					icon: "sap-icon://decline",
					press: function () {
						oDialog.close();
					}
				})
			],
			afterOpen: function () {
				if (commessa) {
					//oOrderCombo.setSelectedKey(commessa);
					//oOrderCombo.fireChange();
					//oOrderCombo.fireSelectionChange();
					oPoperCombo.focus();
					oPoperCombo.open();
				}
			},
			afterClose: function () {
				oDialog.destroy();
			}
		});

		oDialog.open();

	},

	/***********************************************************************************************/
	// Avanzamento alla pagina successiva
	/***********************************************************************************************/

	onNext: function (oParams) {

		var navFunction = function (oCallbackParams) {
			oArguments['order'] = oCallbackParams.order;
			oArguments['poper'] = oCallbackParams.poper;
			oArguments['idline'] = "";
			oArguments['udc'] = oCallbackParams.udcnr;
			oAppRouter.navTo('boxDeclInput5', {
				query: oArguments
			});
		}

		var oController = sap.ui.getCore().byId('boxDeclView5').getController();
		var oArguments = oController._oArguments;
		var oOrderInput = $.UIbyID('boxDeclOrderInput5');
		const sOrderPhase = oOrderInput.getValue();

		oController.onNav(undefined, {
			order: sOrderPhase.substring(0, 12),
			poper: sOrderPhase.substring(12, 16),
			plant: oArguments.plant,
			navCallback: navFunction
		});
	},

	onNav: function (oUdcObject, oCallbackParams) // passaggio alla schermata successiva
	{
		// recupero i nuovi dati per visualizzare o meno i tile
		var oController = sap.ui.getCore().byId('boxDeclView5').getController();
		var oArguments = oController._oArguments;
		var oOrderInput = $.UIbyID('boxDeclOrderInput5');
		const sOrderPhase = oOrderInput.getValue();
		var aLines = [];

		var sCheckQuery = sMovementPath + "NextGen/getInterfaceByUDC_OrderPoperQR";
		sCheckQuery += "&Param.1=" + oArguments.plant;
		sCheckQuery += "&Param.2=" + sOrderPhase;
		sCheckQuery += "&Param.3=" + aLines.join();
		sCheckQuery += "&Param.4=M";
		sCheckQuery += "&Param.5=" + sLanguage;

		var oCheckResult = fnGetAjaxData(sCheckQuery, false);
		var rows = $(oCheckResult).find('Row');
		var order = $(oCheckResult).find('ORDER')[0].textContent;
		var poper = $(oCheckResult).find('POPER')[0].textContent;
		var udc = $(oCheckResult).find('UDC')[0].textContent;

		let code = rows[0] && rows[0].firstChild.textContent;

		if (code < 0) {

			if (code == -99 || code == -98) {
				this.onSearchOrderNew({
					controller: oController,
					callback: function (sOrderPoper) {
						$.UIbyID('boxDeclOrderInput5').setValue(sOrderPoper);
						oController.onNext();
					},
					funzione: code,
					udc: udc,
					order: order
				});

			} else {
				sap.m.MessageBox.show(rows[0].getElementsByTagName('message')[0].textContent, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: oLng_Opr.getText('General_ErrorText'),
					actions: sap.m.MessageBox.Action.OK
				});
			}
		} else {
			var fmbtotem = [];
			var totem = {};
			$.each(rows, function (i, row) {
				totem.PLANT = row.getElementsByTagName('PLANT')[0].textContent;
				totem.FLOWID = row.getElementsByTagName('FLOWID')[0].textContent;
				totem.ORDTYPE = row.getElementsByTagName('ORDTYPE')[0].textContent;
				totem.MSTEUS = row.getElementsByTagName('MSTEUS')[0].textContent;
				totem.TPOPE = row.getElementsByTagName('TPOPE')[0].textContent;
				totem.QTYMDT = row.getElementsByTagName('QTYMDT')[0].textContent;
				totem.UDCFULL = row.getElementsByTagName('UDCFULL')[0].textContent;
				totem.UDCPRINT = row.getElementsByTagName('UDCPRINT')[0].textContent;
				totem.COLLECTION = row.getElementsByTagName('COLLECTION')[0].textContent;
				totem.SENDTOSAP = row.getElementsByTagName('SENDTOSAP')[0].textContent;
				totem.NOTE = row.getElementsByTagName('NOTE')[0].textContent;
				totem.UDCNOTE = row.getElementsByTagName('UDCNOTE')[0].textContent;
				fmbtotem.push(totem);
				totem = {};
			});

			oUdcObject = {
				ORDER: order,
				POPER: poper,
				UDCNR: udc
			};

			oCallbackParams.udcnr = udc;
			oCallbackParams.order = order;
			oCallbackParams.poper = poper;

			var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
			oSessionStorage.remove('FUNCDATA');
			oSessionStorage.remove('FMBTOTEM');
			oSessionStorage.put('FUNCDATA', oUdcObject);
			oSessionStorage.put('FMBTOTEM', fmbtotem);

			oCallbackParams.navCallback(oCallbackParams);
		}
	}
});
