// +
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL5.boxDeclInput6", {

	_oArguments: undefined,
	_sWsName: '',
	_oFMBTotem: undefined,
	_sFunzione: 'BU',
	_fieldMandatory: [],
	_currentDate: '',
	_currentShift: '',
	_currentRequestDate: '',

	/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("boxDeclInput6").attachPatternMatched(this.onObjectMatched, this);
	},

	/***********************************************************************************************/
	// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
	/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArgParams = ['plant', 'pernr', 'order', 'poper', 'idline'];
		const oArguments = oAppController.getPatternArguments(oEvent, oArgParams);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		this._fieldMandatory = [];

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclInputPage6'), sPlant, sPernr);

		var oStdQtyCombo = $.UIbyID('boxDeclInputStdQty6');
		var oStdQtyModel = oStdQtyCombo.getModel();
		if(!oStdQtyModel)
		{
			oStdQtyModel = new sap.ui.model.xml.XMLModel();
			oStdQtyCombo.setModel(oStdQtyModel);
		}
		var sStdQtyQuery = QService + sMasterDataPath + "Nmimb/getNmimbByOrderPoperSQ";
		sStdQtyQuery += "&Param.1=" + sPlant;
		sStdQtyQuery += "&Param.2=" + oArguments.order;
		sStdQtyQuery += "&Param.3=" + oArguments.poper;
		oStdQtyModel.loadData(sStdQtyQuery, false, false);

		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oWorkstInfo = oStorage.get('WORKST_INFO');
		if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
			this._sWsName = oWorkstInfo.WSNAME;

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		var funzioneTotem = oSessionStorage.get('FMBTOTEM');
		var that = this;
		$.each(funzioneTotem, function(i, row) {
			if(row.TPOPE == that._sFunzione) {
				that._oFMBTotem = row;
			}
		});

		var bDisabledMode = false;
		if(!oUdcObject || !oUdcObject.hasOwnProperty('UDCNR') ||
			 (oUdcObject.UDCNR !== oArguments.udc && !(oUdcObject.PRINT_MODE && oArguments.udc === '')))
		{
			oUdcObject = {};
			bDisabledMode = true;
		}

		var oNextPhaseCombo = $.UIbyID("boxDeclInputNextPhase6");

		var oNextPhaseModel = oNextPhaseCombo.getModel();
		if(oNextPhaseModel)
			oNextPhaseModel.destroy();
		oNextPhaseModel = new sap.ui.model.xml.XMLModel();
		oNextPhaseCombo.setModel(oNextPhaseModel);

		var sNextPhaseQuery = QService + sMovementPath + "NextGen/getNextOrderPoperQR";
		sNextPhaseQuery += "&Param.1=" + sPlant;
		sNextPhaseQuery += "&Param.2=" + this._oFMBTotem.FLOWID;
		sNextPhaseQuery += "&Param.3=" + oArguments.order;
		sNextPhaseQuery += "&Param.4=" + oArguments.poper;
		oNextPhaseModel.loadData(sNextPhaseQuery, false, false);

		let defaultOper = "";
		let nextOrder = "";

		try {
			defaultOper = $(oNextPhaseModel.getData()).find('Rowset')[0].getElementsByTagName('POPER')[0].textContent;
			nextOrder = $(oNextPhaseModel.getData()).find('Rowset')[0].getElementsByTagName('ORDER')[0].textContent;
		}
		catch(err) {}

			if(defaultOper !== ""){
				oNextPhaseCombo.setSelectedKey(defaultOper);
			}
		if(nextOrder !== ""){
			this._oFMBTotem.nextOrder = nextOrder;
		}				

		$.UIbyID("boxDeclNote6").setValue("");
		$.UIbyID("boxDeclInputCodImb6").setValue("");
		$.UIbyID("boxDeclInputCodImb6").setSelectedKey("");
		$.UIbyID("boxDeclInputCodImb6").setValueState(sap.ui.core.ValueState.None);

		var oCodImbCombo = $.UIbyID('boxDeclInputCodImb6');
		var oCodImbModel = oCodImbCombo.getModel();
		if(!oCodImbModel)
		{
			oCodImbModel = new sap.ui.model.xml.XMLModel();
			oCodImbCombo.setModel(oCodImbModel);
		}

		var sCodImbQuery = QService + sMasterDataPath + "Nmimb/getCodImbsSQ";
		sCodImbQuery += "&Param.1=" + sPlant;
		oCodImbModel.loadData(sCodImbQuery, false, false);

		this.displayInfo(oUdcObject, bDisabledMode);
	},

	/***********************************************************************************************/
	// Visualizzazione delle informazioni
	/***********************************************************************************************/

	displayInfo: function(oUdcObject, bDisabledMode) {

		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		var oView = sap.ui.getCore().byId('boxDeclInput6View');
		var oModel = oView.getModel();

		var oController = oView.getController();
		const oArguments = oController._oArguments;
		const oFMBTotem = oController._oFMBTotem;

		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oModel.forceNoCache(true);
			oModel.updateBindings(true);
			oView.setModel(oModel);
		} else {
			oModel.forceNoCache(true);
			oModel.updateBindings(true);
		}
		oModel.setData(oUdcObject);

		const sNewUdc = oUdcObject.UDCNR === '';

		var sCheckQuery = sMovementPath + "NextGen/getFieldsByFlowIdSQ";
		sCheckQuery += "&Param.1=" + oArguments.plant;
		sCheckQuery += "&Param.2=" + oFMBTotem.FLOWID;

		var oCheckResult = fnGetAjaxData(sCheckQuery, false);
		var rows = $(oCheckResult).find('Row');

		let fieldname;
		let fstate;
		let cond = false;
		$.each(rows, function( i, row ) {
			fieldname = row.getElementsByTagName('FIELDNAME')[0].textContent;
			fstate = row.getElementsByTagName('FSTATE')[0].textContent;
			if(fstate == 'M') {
				oController._fieldMandatory.push(fieldname);
			}
			cond = (fstate == 'V' || fstate == 'M' ? true : false);
			if($.UIbyID(fieldname))
				$.UIbyID(fieldname).setVisible(cond);
			else
				console.log("ID non trovato: " + fieldname);
		});

		$.each(oController._fieldMandatory, function( i, row ) {
			let field = $.UIbyID(row);
			field.setValueState(sap.ui.core.ValueState.None);
			field.setValueStateText();
		});

		if(oFMBTotem && oFMBTotem.UDCFULL == 1) {
			$.UIbyID("boxDeclUDCLabel6").setVisible(false);
			$.UIbyID("boxDeclUDCText6").setVisible(false);
			$.UIbyID("boxDeclDeclQtyLabel6").setVisible(false);
			$.UIbyID("boxDeclDeclQtyText6").setVisible(false);
			$.UIbyID("boxDeclOpLabel6").setVisible(false);
			$.UIbyID("boxDeclOpText6").setVisible(false);
			$.UIbyID("boxDeclTimeLabel6").setVisible(false);
			$.UIbyID("boxDeclTimeText6").setVisible(false);
		}

		var bPrintMode = false;
		if(oUdcObject.hasOwnProperty('PRINT_MODE'))
			bPrintMode = oUdcObject.PRINT_MODE;

		$.UIbyID('boxDeclInputWorkTime6').setValue(oUdcObject.WTDUR);
		$.UIbyID('boxDeclInputNrPrnt6').setValue(oUdcObject.NRPRNT);

		var oStdQtyCombo = $.UIbyID('boxDeclInputStdQty6');
		oStdQtyCombo.clearSelection();
		oStdQtyCombo.setValue();
		oStdQtyCombo.data('NMIMB', undefined);
		const sSelectedNmImb = oUdcObject.NMIMB;
		if(!sSelectedNmImb)
		{
			const sDefaultKey = getValueFromXMLModel(oStdQtyCombo.getModel(),"DEFAULT","1","NMIMB");
			if(sDefaultKey)
				oStdQtyCombo.setSelectedKey(sDefaultKey);
		}
		else
		{
			if(oStdQtyCombo.getItemByKey(sSelectedNmImb))
				oStdQtyCombo.setSelectedKey(sSelectedNmImb);
			else
			{
				oStdQtyCombo.setValue(sSelectedNmImb);
				oStdQtyCombo.data('NMIMB',{
					QTY: 0, // parametro non utilizzato al momento (quantità corrispondente a NMIMB)
					NMIMB: sSelectedNmImb
				});
			}
		}

		$.UIbyID('boxDeclInputBasketTT6').setValue(oUdcObject.BASKETTT);
		$.UIbyID('boxDeclInputDater6').setValue(oUdcObject.DATER);

		$.UIbyID('boxDeclInputQty6').setValue((bPrintMode) ? oUdcObject.QTYDECL : '');

		/*var qtyBuoniReq = sMovementPath + "ProdDecl/getQtyFromDicSQ";
		qtyBuoniReq += "&Param.1=" + oUdcObject.LINEID;
		qtyBuoniReq += "&Param.2=" + oAppController._sShift;
		qtyBuoniReq += "&Param.3=" + oAppController._sDate;
		qtyBuoniReq += "&Param.4=" + oUdcObject.MATERIAL;
		var qtyBuoni = fnGetAjaxVal(qtyBuoniReq, 'TOTALE', false);

		if(qtyBuoni < 0 || qtyBuoni == "") {
			qtyBuoni = 0;
		}

		$.UIbyID('QtyBuoni6').setVisible(true);
		$.UIbyID('QtyBuoni6').setText(qtyBuoni);
		if(qtyBuoni == 0)
			$.UIbyID('boxDeclInputQty6').setValue("");
		else
			$.UIbyID('boxDeclInputQty6').setValue(qtyBuoni);
		*/

		$.UIbyID('boxDeclConfirmButton6').setEnabled(!bDisabledMode && !bPrintMode);
		$.UIbyID('boxDeclPrintButton6').setEnabled(!bDisabledMode && bPrintMode);

		/*var oNextPhaseCombo = $.UIbyID("boxDeclInputNextPhase6");
		oNextPhaseCombo.setValue();
		oNextPhaseCombo.setSelectedKey();
		oNextPhaseCombo.setValueState();
		oNextPhaseCombo.setValueStateText();*/
	},

	/***********************************************************************************************/
	// Torna al menu
	/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('boxDeclInput6View').getController();

		oController._currentRequestDate = "";
		oController._currentShift = "";
		oController._currentDate = "";

		$.UIbyID('shiftDeclDate6').setText("");
		$.UIbyID('shiftDeclShift6').setText("");

		oAppRouter.navTo('boxDecl5',{
			query: oController._oArguments
		});
	},

	/***********************************************************************************************/
	// Selezione qtà standard (norma di imballo)
	/***********************************************************************************************/

	onSelectNmimb: function() {
		var oContext = this;
		var oView = oContext.view;
		var oController = oView.getController();

		var oStdQtyModel = new sap.ui.model.xml.XMLModel();
		var sStdQtyQuery = QService + sMasterDataPath + "Nmimb/getNmimbsSQ";
		sStdQtyQuery += "&Param.1=" + oController._oArguments.plant;
		oStdQtyModel.loadData(sStdQtyQuery,false,false);

		var oTemplate = new sap.m.ColumnListItem({
			type: sap.m.ListType.Active,
			cells: [
				new sap.m.Text({
					text: "{NMIMB}"
				}),
				new sap.m.Text({
					text: "{DESCR}"
				}),
				new sap.m.Text({
					text: "{MATERIAL}"
				}),
				new sap.m.Text({
					text: "{QTY}"
				}),
				new sap.m.Text({
					text: "{MATIMB}"
				})
			]
		});

		var oDeclDialog = new sap.m.Dialog({
			title: oLng_Opr.getText('BoxDeclInput_SelectNmimb'),
			contentWidth: "800px",
			contentHeight: "500px",
			beginButton: new sap.m.Button({
				text: oLng_Opr.getText("PacDm_Close"),
				press: function () {
					oDeclDialog.close();
				}
			}),
			afterClose: function() {
				oDeclDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");

		var oTable = new sap.m.Table({
			mode: sap.m.ListMode.SingleSelectMaster,
			noDataText: oLng_Opr.getText('BoxDeclInput_NoNmimb'),
			columns: [
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_Nmimb")
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_NmimbDescr")
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_NmimbMaterial")
					}),
					width: '170px'
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_NmimbQty")
					}),
					hAlign: sap.ui.core.TextAlign.End,
					width: '90px'
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_CodImb")
					}),
					hAlign: sap.ui.core.TextAlign.End,
					width: '90px'
				})
			],
			itemPress: function(oEvent) {

				var oSelectedContext = oEvent.getSource().getSelectedItem().getBindingContext();
				const sQty = oSelectedContext.getProperty("QTY");
				const sNmImb = oSelectedContext.getProperty("NMIMB");
				const sDescr = oSelectedContext.getProperty("DESCR");
				const sCodImb = oSelectedContext.getProperty("MATIMB");

				var oStdQtyCombo = oContext.combo;
				oStdQtyCombo.clearSelection();
				oStdQtyCombo.setValue(sQty + " (" + sNmImb + " " + sDescr + ")");
				oStdQtyCombo.data('NMIMB',{
					QTY: sQty,
					NMIMB: sNmImb
				});

				var oComboCodImb = oContext.comboCodImb;
				oComboCodImb.clearSelection();
				oComboCodImb.setValue(sCodImb);
				oComboCodImb.data('MATIMB', {
					MATIMB: sCodImb
				});

				oDeclDialog.close();
			}
		});
		oTable.bindAggregation("items", "/Rowset/Row", oTemplate);

		oDeclDialog.addContent(oTable);

		oDeclDialog.setSubHeader(new sap.m.Toolbar({
			content: new sap.m.SearchField({
				search: function (oEvent) {
					var sValue = oEvent.getParameter("query");
					var oNmImbFilter = new sap.ui.model.Filter("NMIMB", sap.ui.model.FilterOperator.Contains, sValue);
					var oDescrFilter = new sap.ui.model.Filter("DESCR", sap.ui.model.FilterOperator.Contains, sValue);
					var oMaterialFilter = new sap.ui.model.Filter("MATERIAL", sap.ui.model.FilterOperator.Contains, sValue);
					var oAllFilters = new sap.ui.model.Filter([oNmImbFilter,oDescrFilter,oMaterialFilter]);
					var oBinding = oTable.getBinding("items");
					oBinding.filter(oAllFilters);
				}
			})
		}));

		oView.addDependent(oDeclDialog);

		oDeclDialog.setModel(oStdQtyModel);
		oDeclDialog.open();
	},

	/***********************************************************************************************/
	// Conferma
	/***********************************************************************************************/

	onConfirm: function() {
		var oView = sap.ui.getCore().byId('boxDeclInput6View');
		var oController = oView.getController();
		var oModel = oView.getModel();
		const oArguments = oController._oArguments;
		const oFMBTotem = oController._oFMBTotem;
		const fieldMandatory = oController._fieldMandatory;

		var oBasketTTInput = $.UIbyID('boxDeclInputBasketTT6');
		var oDaterInput = $.UIbyID('boxDeclInputDater6');
		var oWorkTimeInput = $.UIbyID('boxDeclInputWorkTime6');
		var oQtyInput = $.UIbyID('boxDeclInputQty6');
		var oStdQtyCombo = $.UIbyID('boxDeclInputStdQty6');
		var note = $.UIbyID('boxDeclNote6').getValue();
		var codImb = $.UIbyID('boxDeclInputCodImb6').getSelectedKey() ? $.UIbyID('boxDeclInputCodImb6').getSelectedKey() : $.UIbyID('boxDeclInputCodImb6').getValue();

		oQtyInput.setValueState(sap.ui.core.ValueState.None);
		oQtyInput.setValueStateText();
		oBasketTTInput.setValueState(sap.ui.core.ValueState.None);
		oBasketTTInput.setValueStateText();
		oDaterInput.setValueState(sap.ui.core.ValueState.None);
		oDaterInput.setValueStateText();
		oWorkTimeInput.setValueState(sap.ui.core.ValueState.None);
		oWorkTimeInput.setValueStateText();
		oStdQtyCombo.setValueState(sap.ui.core.ValueState.None);
		oStdQtyCombo.setValueStateText();

		const sBasketTT = oBasketTTInput.getValue();
		const sDater = oDaterInput.getValue();
		const sQtyDecl = oQtyInput.getValue();
		const iQtyDecl = parseInt(sQtyDecl);
		const sWorkTime = oWorkTimeInput.getValue();
		const sNrPrnt = $.UIbyID('boxDeclInputNrPrnt6').getValue();

		/*var tollinp = sMovementPath + "ProdDecl/getTollinpFromLineSQ";
		tollinp += "&Param.1=" + oArguments.idline;
		var tolleranza = fnGetAjaxVal(tollinp, 'TOLLINP', false);

		var qtyProposta = parseInt($.UIbyID('QtyBuoni6').getText());
		var tollValueMin = Math.round(qtyProposta - (qtyProposta / 100 * tolleranza));
		var tollValueMax = Math.round(qtyProposta + (qtyProposta / 100 * tolleranza));
		*/

		var sNmImb = oStdQtyCombo.getSelectedKey();
		if(!sNmImb)
		{
			var oNmImbData = oStdQtyCombo.data('NMIMB');
			if(oNmImbData && oNmImbData.hasOwnProperty('NMIMB'))
				sNmImb = oNmImbData.NMIMB;
		}

		var iQtyRes = parseFloat(oModel.getProperty("/QTYRES"));
		if(isNaN(iQtyRes))
			iQtyRes = 0;

		var iMatPzUdc = parseFloat(oModel.getProperty("/MAT_PZUDC"));
		if(isNaN(iMatPzUdc))
			iMatPzUdc = 0;

		var sErrorMessage;
		var oErrorField;
		const sCurrentYear = parseInt(new Date().getFullYear().toString().substring(2, 4));

		if(sDater && (sDater.length != 4 || !sDater.match(/(0[1-9]|1[0-2])([0-9]{2})/))) // solo caratteri alfanumerici
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterErrorMsg");
			oErrorField = oDaterInput;
		} else if(sDater && (sDater.substring(2 ,4) < sCurrentYear-1 || sDater.substring(2, 4) > sCurrentYear + 1))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterYearErrorMsg");
			oErrorField = oDaterInput;
		} else if(sBasketTT && !sBasketTT.match(/^[0-9a-zA-Z ]+$/))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_BasketTTErrorMsg");
			oErrorField = oBasketTTInput;
		} else if(sWorkTime && sWorkTime <= 0)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_WorkTimeErrorMsg");
			oErrorField = oWorkTimeInput;
		} else if(sWorkTime && sWorkTime > 480)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_WorkTimeMaxErrorMsg") + " (480 Min)";
			oErrorField = oWorkTimeInput;
		} else if(iQtyDecl && iQtyDecl <= 0)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyErrorMsg");
			oErrorField = oQtyInput;
		} else if(iQtyDecl && iQtyDecl > 15000)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg") + " (15000)";
			oErrorField = oQtyInput;
		} else if(iMatPzUdc > 0 && (iQtyRes + iQtyDecl) > iMatPzUdc)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg");
			oErrorField = oQtyInput;
		} else if(fieldMandatory && fieldMandatory.length > 0) {
			$.each(fieldMandatory, function( i, row ) {
				let field = $.UIbyID(row);
				sErrorMessage = (!field.getValue() || field.getValue() === "0") ? "Campo obbligatorio" : "";
				oErrorField = field;
				if(sErrorMessage)
					return false;
			});
		}

		if(!sErrorMessage)
		{
			/*var supToll = false;
			if(!(iQtyDecl >= tollValueMin && iQtyDecl <= tollValueMax) && qtyProposta != 0) {
				supToll = true;
			}
			*/
			var continueFunc = function(sState) {

				if(iMatPzUdc && iMatPzUdc > 0 && (iQtyRes + iQtyDecl === iMatPzUdc)
					 && sState !== 'C')
				{
					var sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyFullErrorMsg");

					oQtyInput.setValueState(sap.ui.core.ValueState.Warning);
					oQtyInput.setValueStateText(sErrorMessage);
					oQtyInput.focus();

					sap.m.MessageBox.show(sErrorMessage, {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: oLng_Opr.getText("General_WarningText"),
						actions: sap.m.MessageBox.Action.OK
					});

					return;
				}

				const sFIUdC = (sState === 'C') ? 'C' : 'I';
				const sDatetime = getDatetimeString(new Date());

				let nextPhase = sap.ui.getCore().byId("boxDeclInputNextPhase6").getSelectedKey();

				var sSaveQuery = sMovementPath + "NextGen/saveDataTotemQR";
				sSaveQuery += "&Param.1=" + oFMBTotem.PLANT;
				sSaveQuery += "&Param.2=" + oFMBTotem.FLOWID;
				sSaveQuery += "&Param.3=" + (oController._currentRequestDate ? oController._currentRequestDate : oAppController._sDate);
				sSaveQuery += "&Param.4=" + (oController._currentShift ? oController._currentShift : oAppController._sShift);
				sSaveQuery += "&Param.5=" + oArguments.order;
				sSaveQuery += "&Param.6=" + oArguments.poper;
				sSaveQuery += "&Param.7=" + oFMBTotem.nextOrder;
				sSaveQuery += "&Param.8=" + (nextPhase ? nextPhase : "");
				sSaveQuery += "&Param.9=" + sQtyDecl;
				sSaveQuery += "&Param.10=";
				sSaveQuery += "&Param.11=" + oArguments.pernr;
				sSaveQuery += "&Param.12=" + note;
				sSaveQuery += "&Param.13=" + sNmImb;
				sSaveQuery += "&Param.14=" + sBasketTT;
				sSaveQuery += "&Param.15=" + sNrPrnt;
				sSaveQuery += "&Param.16=" + sDater;
				sSaveQuery += "&Param.17=" + sLanguage;
				sSaveQuery += "&Param.18=" + sFIUdC;
				sSaveQuery += "&Param.19=" + sWorkTime;
				sSaveQuery += "&Param.20=" + oController._sWsName;
				sSaveQuery += "&Param.21=" + oArguments.udc;
				sSaveQuery += "&Param.22=";
				sSaveQuery += "&Param.23=";
				sSaveQuery += "&Param.24=" + oArguments.idline;
				sSaveQuery += "&Param.25=";
				sSaveQuery += "&Param.26=" + codImb;

				var successFunction = function(oResults) {

					var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
					var oUdcObject = oSessionStorage.get('FUNCDATA');

					if(!oUdcObject)
						oUdcObject = {};

					if(oResults.hasOwnProperty('outputUdc') && oResults.outputUdc)
						oUdcObject.UDCNR = oResults.outputUdc;
					if(oResults.hasOwnProperty('pdfString') && oResults.pdfString)
					{
						oUdcObject.PDFSTRING = oResults.pdfString;
						if(parseInt(oResults.code) === 1 && oFMBTotem.UDCPRINT === "1") // WARNING = 1 : stampa non automatica, anteprima automatica
							oController.showPdfDialog(oResults.pdfString, true);
					}
					oUdcObject.PRINT_MODE = true;
					oUdcObject.QTYRES = iQtyRes + iQtyDecl;
					oUdcObject.QTYDECL = iQtyDecl;
					oUdcObject.WTDUR = sWorkTime;
					oUdcObject.NRPRNT = sNrPrnt;
					oUdcObject.NMIMB = sNmImb;
					oUdcObject.DATER = sDater;
					oUdcObject.BASKETTT = sBasketTT;
					oUdcObject.TIMEID = sDatetime;
					oUdcObject.LAST_OPEID = oArguments.pernr;
					oUdcObject.STATE = sState;

					const oLoginInfo = oSessionStorage.get('LOGIN_INFO');
					if(oLoginInfo.hasOwnProperty("opeName"))
						oUdcObject.LAST_OPENAME = oLoginInfo.opeName;

					oSessionStorage.put('FUNCDATA', oUdcObject);
					oController.displayInfo(oUdcObject, false);
				};

				oAppController.handleRequest(sSaveQuery, {
					onSuccess: successFunction,
					onWarning: successFunction,
					notShowWarning: [1],
					showSuccess: false,
					results: ['outputUdc','pdfString']
				});
			};

			/*if(supToll) {
					sap.m.MessageBox.confirm(
						oLng_Opr.getText("BoxDeclInput_QtyToleranceErrorMsg"),
						{
							actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.CANCEL],
							onClose: function(sAction) {
								if (sAction === sap.m.MessageBox.Action.YES) {
									oController.selectState({
										firstValue: 'O',
										secondValue: 'C',
										fmbTotem: oFMBTotem,
										onCallback: continueFunc
									});
								}
							}
						}
					);
				} else {*/
			oController.selectState({
				firstValue: 'O',
				secondValue: 'C',
				fmbTotem: oFMBTotem,
				onCallback: continueFunc
			})
			//}
		}
		else
		{
			if(oErrorField)
			{
				oErrorField.setValueState(sap.ui.core.ValueState.Warning);
				oErrorField.setValueStateText(sErrorMessage);
				oErrorField.focus();
			}

			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
	},

	/***********************************************************************************************/
	// Dialog per la selezione dello stato dell'UdC
	/***********************************************************************************************/

	selectState: function(oParams) {
		if(oParams.fmbTotem.hasOwnProperty("UDCFULL") && oParams.fmbTotem.UDCFULL == 0) {
			if(!oParams || typeof(oParams.onCallback) !== 'function')
				return;
			if(!oParams.hasOwnProperty('firstText'))
				oParams.firstText = oLng_Opr.getText("BoxDeclInput_Incomplete");
			if(!oParams.hasOwnProperty('firstColorCss'))
				oParams.firstColorCss = 'greyButton';
			if(!oParams.hasOwnProperty('secondText'))
				oParams.secondText = oLng_Opr.getText("BoxDeclInput_Complete");
			if(!oParams.hasOwnProperty('secondColorCss'))
				oParams.secondColorCss = 'greenButton';
			if(!oParams.hasOwnProperty('firstValue'))
				oParams.firstValue = false;
			if(!oParams.hasOwnProperty('secondValue'))
				oParams.secondValue = true;

			var oDialog = new sap.m.Dialog({
				title: oLng_Opr.getText("BoxDeclInput_SelectState"),
				contentWidth: "700px",
				contentHeight: "160px",
				content: [
					new sap.m.HBox({
						justifyContent: sap.m.FlexJustifyContent.Center,
						items: [
							new sap.m.Button({
								text: oParams.firstText,
								icon: "sap-icon://switch-views",
								press: function() {
									oDialog.close();
									oParams.onCallback(oParams.firstValue);
								}
							}).addStyleClass("bigButton " + oParams.firstColorCss),
							new sap.m.Button({
								text: oParams.secondText,
								icon: "sap-icon://complete",
								press: function() {
									oDialog.close();
									oParams.onCallback(oParams.secondValue);
								}
							}).addStyleClass("sapUiSmallMarginBegin bigButton " + oParams.secondColorCss)
						]
					}).addStyleClass("sapUiSmallMarginTop")
				],
				endButton: new sap.m.Button({
					text: oLng_Opr.getText("BoxDeclInput_Cancel"),
					icon: "sap-icon://decline",
					press: function() {
						oDialog.close();
					}
				}),
				afterClose: function() {
					oDialog.destroy();
				}
			});

			oDialog.open();
		} else {
			oParams.onCallback(oParams.secondValue);
		}
	},

	/***********************************************************************************************/
	// Stampa
	/***********************************************************************************************/

	onPrint: function() {
		var oView = sap.ui.getCore().byId('boxDeclInput6View');
		var oController = oView.getController();
		var oModel = oView.getModel();

		const sPdfString = oModel.getProperty("/PDFSTRING");

		oController.showPdfDialog(sPdfString, true);
	},

	showPdfDialog(sPrintUrl, bBase64, closeFunction) {

		if(bBase64)
			sPrintUrl = 'data:application/pdf;base64,' + sPrintUrl;

		var oHtml = new sap.ui.core.HTML();
		oHtml.setContent('<iframe width=100% height=100% src="' + sPrintUrl + '"></iframe>');

		var oPrintDialog = new sap.m.Dialog({
			//resizable: true,
			horizontalScrolling : false,
			verticalScrolling : false,
			contentWidth : '800px',
			contentHeight : '600px',
			customHeader: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://decline",
						press: function() {
							oHtml.destroy();
							oPrintDialog.close();
						}
					})
				]
			}),
			content: oHtml,
			afterClose: function() {
				oPrintDialog.destroy();
				if(typeof(closeFunction) === 'function')
					closeFunction();
			}
		}).addStyleClass('noPaddingDialog fullHeightScrollCont');

		oPrintDialog.open();
	},

	changeDateShift: function() {
		var oController = sap.ui.getCore().byId('boxDeclInput6View').getController();
		const oArguments = oController._oArguments;

		var oDatePicker = new sap.m.DatePicker({
			displayFormat: "dd/MM/yyyy",
			valueFormat: 'yyyy-MM-dd',
			maxDate: new Date(),
			width: "100%",
			layoutData : new sap.ui.layout.GridData({
				span: "L9 M9 S12"
			})
		});

		var oShiftTemplate = new sap.ui.core.Item({
			key : "{SHIFT}",
			text: "{SHNAME}"
		});

		var oShiftCombo = new sap.m.ComboBox({
			width: "100%",
			layoutData : new sap.ui.layout.GridData({
				span: "L9 M9 S12"
			})
		})
		oShiftCombo.bindAggregation("items","/Rowset/Row", oShiftTemplate);

		var oShiftModel = new sap.ui.model.xml.XMLModel();
		var sQuery = QService + sMasterDataPath + "Shifts/getShiftsByPlantSQ";
		sQuery += "&Param.1=" + oArguments.plant;
		oShiftModel.loadData(sQuery, false, false);
		oShiftCombo.setModel(oShiftModel);

		var oDateShiftDialog = new sap.m.Dialog({
			title: oLng_Opr.getText('ReprintUdc_SelectDateShift'),
			contentWidth: '500px',
			content: new sap.ui.layout.Grid({
				hSpacing: 1,
				vSpacing: 0.5,
				content: [
					new sap.m.Label({
						text: oLng_Opr.getText('ReprintUdc_Date') + ':',
						width: '100%',
						textAlign: sap.ui.core.TextAlign.Right,
						layoutData : new sap.ui.layout.GridData({
							span: "L3 M3 S12"
						})
					}).addStyleClass("inputLabel"),
					oDatePicker,
					new sap.m.Label({
						text: oLng_Opr.getText('ReprintUdc_Shift') + ':',
						width: '100%',
						textAlign: sap.ui.core.TextAlign.Right,
						layoutData : new sap.ui.layout.GridData({
							span: "L3 M3 S12",
							linebreak:true
						})
					}).addStyleClass("inputLabel"),
					oShiftCombo
				]
			}).addStyleClass("sapUiSmallMarginTopBottom"),
			beginButton: new sap.m.Button({
				text:  oLng_Opr.getText("ReprintUdc_Apply"),
				icon: "sap-icon://accept",
				press: function () {
					const sRequestDate = oDatePicker.getValue();
					var oShiftContext = oShiftCombo.getSelectedItem().getBindingContext();
					const sRequestShift = oShiftContext.getProperty("SHIFT");

					var sDate;
					var aDateParts = sRequestDate.split('-');
					if(aDateParts.length === 3) // da formato YYYY-MM-DD a DD/MM/YYYY
					{
						var sTmp = aDateParts[0];
						aDateParts[0] = aDateParts[2];
						aDateParts[2] = sTmp;
						sDate = aDateParts.join('/');
					}
					$.UIbyID('shiftDeclDate6').setText(sDate);
					$.UIbyID('shiftDeclShift6').setText(oShiftContext.getProperty("SHNAME"));

					oController._currentDate = sDate;
					oController._currentRequestDate = sRequestDate;
					oController._currentShift = sRequestShift;
					oDateShiftDialog.close();
				}
			}),
			endButton: new sap.m.Button({
				text: oLng_Opr.getText("ReprintUdc_Cancel"),
				icon: "sap-icon://decline",
				press: function() {
					oDateShiftDialog.close();
				}
			}),
			afterClose: function() {
				oDateShiftDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");

		oDatePicker.setValue(oAppController._sDate);
		oShiftCombo.setSelectedKey(oAppController._sShift);

		oDateShiftDialog.open();
	},

	restoreDateShift: function() {
		var oController = sap.ui.getCore().byId('boxDeclInput6View').getController();
		oController._currentDate = "";
		oController._currentShift = "";
		oController._currentRequestDate = "";

		$.UIbyID('shiftDeclDate6').setText("");
		$.UIbyID('shiftDeclShift6').setText("");
	},

	onNextPhaseChange: function(data) {
		var oController = sap.ui.getCore().byId('boxDeclInput6View').getController();
		const oArguments = oController._oArguments;
		const oFMBTotem = oController._oFMBTotem;

		let comboModel = data.oSource.getModel();
		let comboKey = data.oSource.getSelectedKey();
		const optional = getValueFromXMLModelByRowset(comboModel, "1", "POPER", comboKey, "OPTIONAL");
		let note = $.UIbyID('boxDeclNote6').getValue();

		if(optional == 1 && oFMBTotem.UDCNOTE != "") {
			$.UIbyID('boxDeclNote6').setValue(oFMBTotem.UDCNOTE);
		} else if (optional == 0 && note == oFMBTotem.UDCNOTE) {
			$.UIbyID('boxDeclNote6').setValue("");
		}

	}
});
