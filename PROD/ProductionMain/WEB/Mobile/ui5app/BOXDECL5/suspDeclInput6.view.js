// View Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.BOXDECL5.suspDeclInput6", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 */
	getControllerName: function () {
		return "ui5app.BOXDECL5.suspDeclInput6";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 */
	createContent: function (oController) {

		var oView = this;

		var idTextFormatter = function (sId, sTxt) {
			return sId + " " + sTxt;
		};

		var oStdQtyTemplate = new sap.ui.core.Item({
			key: "{MATIMB}",
			text: {
				parts: [
					{
						path: 'MATIMB'
					}
				],
				formatter: function (sMatImb) {
					return sMatImb;
				}
			}
		});

		var oCausalInput = new sap.m.Input({
			id: 'suspDeclInputCausale6',
			showValueHelp: true,
			valueHelpOnly: true,
			valueHelpRequest: function () {
				var oCausalsDialog = $.UIbyID('suspDeclCausalsDialog');
				oCausalsDialog['inputField'] = oCausalInput;
				oCausalsDialog.open();
			},
			layoutData: new sap.ui.layout.GridData({
				span: "L4 M4 S12"
			})
		});

		var oCodImbCombo = new sap.m.ComboBox({
			id: 'suspDeclInputCodImb6',
			width: '130%',
			maxWidth: '130%',
			layoutData: new sap.ui.layout.GridData({
				span: "L5 M5 S12"
			})
		}).bindAggregation("items", "/Rowset/Row", oStdQtyTemplate);

		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			content: [
				new sap.m.Label({
					id: "suspDeclOrder6",
					text: oLng_Opr.getText("BoxDeclInput_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}),
				new sap.m.Text({
					id: "suspDeclInputOrder6",
					text: '{/ORDER}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: "suspDeclPhase6",
					text: oLng_Opr.getText("General_CDL") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					id: "suspDeclInputPhase6",
					text: {
						parts: [
							{
								path: "/CDLID"
							},
							{
								path: "/CDLDESC"
							}
						],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
				new sap.ui.core.Icon({
					id: "suspDeclIconPhase6",
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowLineInfo(oView),
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: "suspDeclLine6",
					text: oLng_Opr.getText("General_Poper") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
				new sap.m.Text({
					id: "suspDeclInputLine6",
					text: {
						parts: [
							{
								path: "/POPER"
							},
							{
								path: "/CICTXT"
							}
						],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: "suspDeclMaterial6",
					text: oLng_Opr.getText("BoxDeclInput_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					id: "suspDeclInputMaterial6",
					text: {
						parts: [
							{
								path: "/MATERIAL"
							},
							{
								path: "/MAT_DESC"
							}
						],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
				new sap.ui.core.Icon({
					id: "suspDeclIconMaterial6",
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo, {
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclUDCLabel6',
					text: oLng_Opr.getText("BoxDeclInput_UDC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
				new sap.m.Text({
					id: 'suspDeclUDCText6',
					text: '{/UDCNR}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclDeclQtyLabel6',
					text: oLng_Opr.getText("BoxDeclInput_DeclQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'suspDeclDeclQtyText6',
					text: '{/QTYRES}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclOpLabel6',
					text: oLng_Opr.getText("BoxDeclInput_Operator") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
				new sap.m.Text({
					id: 'suspDeclOpText6',
					text: '{/LAST_OPENAME}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclTimeLabel6',
					text: oLng_Opr.getText("BoxDeclInput_Datetime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'suspDeclTimeText6',
					text: {
						parts: [
							{
								path: "/TIMEID"
							}
						],
						formatter: dateFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclDater6',
					text: oLng_Opr.getText("BoxDeclInput_Dater") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.MaskInput({
					id: 'suspDeclInputDater6',
					mask: "9999",
					placeholderSymbol: " ",
					placeholder: 'MMYY',
					width: '160px',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclBasketTT6',
					text: oLng_Opr.getText("BoxDeclInput_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'suspDeclInputBasketTT6',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclQty6',
					text: oLng_Opr.getText("BoxDeclInput_Qty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'suspDeclInputQty6',
					min: 0,
					width: '80px',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).addEventDelegate({
					onfocusin: function (e) {
						$("#boxDeclInputQty6-inner").select();
					}
				}),

				//Destinazione
				new sap.m.Label({
					id: 'suspDeclNextPhase6',
					text: oLng_Opr.getText("RepUdcInput_Destination") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}).addStyleClass("inputLabel"),

				new sap.m.ComboBox({
					id: 'suspDeclInputNextPhase6',
					width: "100%",
					layoutData: new sap.ui.layout.GridData({
						span: "L5 M5 S12"
					})
				}).bindAggregation("items", "/Rowset/1/Row",
					new sap.ui.core.Item({
						key: '{POPER}',
						text: {
							parts: [
								{
									path: 'POPER'
							},
								{
									path: 'CICTXT'
							},
								{
									path: 'COLOR'
							}
						],
							formatter: function (sIdRep, sDesc, sColor) {
								if (sColor) {
									var css = document.createElement("style");
									css.type = "text/css";
									css.innerHTML = "li[data-color='" + sColor + "'] { color: " + sColor + " !important; }";
									document.body.appendChild(css);
								}
								return sIdRep + ' - ' + sDesc;
							}
						},
						customData: [
						new sap.ui.core.CustomData({
								key: "color",
								value: "{COLOR}",
								writeToDom: true
							})
					]
					})
													),
				// Causale
				new sap.m.Label({
					id: 'suspDeclLabelCausale6',
					text: oLng_Opr.getText("SuspDeclReas_Causal") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				oCausalInput,
				// Codice imballo
				new sap.m.Label({
					id: 'suspDeclCodImb6',
					text: oLng_Opr.getText("BoxDeclInput_CodImb") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.HBox({
					id: 'suspDeclBoxCodImb6',
					justifyContent: sap.m.FlexJustifyContent.Start,
					width: "95%",
					items: [

						oCodImbCombo,
						new sap.ui.core.Icon({
							id: 'suspDeclIconCodImb6',
							src: "sap-icon://value-help",
							size: "18px",
							visible: false,
							press: [oController.onSelectNmimb, {
								view: oView,
								combo: oCodImbCombo
							}],
							tooltip: oLng_Opr.getText('BoxDeclInput_CodImb'),
							layoutData: new sap.ui.layout.GridData({
								span: "L1 M1 S12"
							})
						}).addStyleClass("inputLabel")
					],
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
				// NOTE
				new sap.m.Label({
					id: 'suspDeclLabelNote6',
					text: oLng_Opr.getText("SuspDeclReas_Note") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'suspDeclNote6',
					width: '100%',
					maxLength: 40,
					layoutData: new sap.ui.layout.GridData({
						span: "L10 M10 S12"
					})
				})
			]
		}).addStyleClass("sapUiTinyMarginTop");

		var oPanel = new sap.m.Panel({
			content: oGrid
		});

		var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL5.boxDeclInput6");

		var oPage = new sap.m.Page({
			id: "suspDeclInputPage6",
			title: oLng_Opr.getText('SuspDecl_Title'),
			enableScrolling: false,
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							id: 'suspDeclConfirmButton6',
							text: oLng_Opr.getText("BoxDeclInput_Confirm"),
							icon: "sap-icon://accept",
							press: oController.onConfirm
						}).addStyleClass("yellowButton bigButton"),
						new sap.m.Button({
							id: 'suspDeclPrintButton6',
							enabled: false,
							text: oLng_Opr.getText("BoxDeclInput_Print"),
							icon: "sap-icon://print",
							press: oController.onPrint
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
					]
				}).addStyleClass("sapUiTinyMarginTop")
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.Button({
						icon: "sap-icon://date-time",
						text: oLng_Opr.getText("ShiftDecl_ChangeDateShift"),
						press: oController.changeDateShift,
					}).addStyleClass("yellowButton noBorderButton"),
					new sap.m.Text({
						id: 'suspDeclDate6'
					}),
					new sap.m.Text({
						id: 'suspDeclShift6'
					}),
					new sap.m.ToolbarSpacer()
				]
			})
		});

		return oPage;
	}

});
