// View Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.BOXDECL5.closeUdcConfirm6", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 */
	getControllerName: function () {
		return "ui5app.BOXDECL5.closeUdcConfirm6";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 */
	createContent: function (oController) {

		var oView = this;

		var idTextFormatter = function (sId, sTxt) {
			return sId + " " + sTxt;
		};

		var oStdQtyTemplate = new sap.ui.core.Item({
			key: "{NMIMB}",
			text: {
				parts: [
					{
						path: 'NMIMB'
					},
					{
						path: 'DESCR'
					},
					{
						path: 'QTY'
					}
				],
				formatter: function (sNmimb, sDesc, sQty) {
					return sQty + ' (' + sNmimb + ' ' + sDesc + ')';
				}
			}
		});

		var oCausalInput = new sap.m.Input({
			id: 'closeUdcInputCausale6',
			showValueHelp: true,
			valueHelpOnly: true,
			valueHelpRequest: function () {
				var oCausalsDialog = $.UIbyID('closeUdcCausalsDialog');
				oCausalsDialog['inputField'] = oCausalInput;
				oCausalsDialog.open();
			},
			layoutData: new sap.ui.layout.GridData({
				span: "L4 M4 S12"
			})
		});

		var oCodImbCombo = new sap.m.ComboBox({
			id: 'closeUdcInputCodImb6',
			width: '100%',
			layoutData: new sap.ui.layout.GridData({
				span: "L3 M3 S12"
			})
		}).bindAggregation("items", "/Rowset/Row", oStdQtyTemplate);

		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			content: [
				new sap.m.Label({
					id: "closeUdcOrder6",
					text: oLng_Opr.getText("BoxDeclInput_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					id: "closeUdcInputOrder6",
					text: '{/ORDER}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: "closeUdcPhase6",
					text: oLng_Opr.getText("General_CDL") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					id: "closeUdcInputPhase6",
					text: {
						parts: [
							{
								path: "/CDLID"
							},
							{
								path: "/CDLDESC"
							}
						],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
				new sap.ui.core.Icon({
					id: "closeUdcIconPhase6",
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowLineInfo(oView),
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: "closeUdcLine6",
					text: oLng_Opr.getText("General_Poper") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
				new sap.m.Text({
					id: "closeUdcInputLine6",
					text: {
						parts: [
							{
								path: "/POPER"
							},
							{
								path: "/CICTXT"
							}
						],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: "closeUdcMaterial6",
					text: oLng_Opr.getText("BoxDeclInput_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					id: "closeUdcInputMaterial6",
					text: {
						parts: [
							{
								path: "/MATERIAL"
							},
							{
								path: "/MAT_DESC"
							}
						],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
				new sap.ui.core.Icon({
					id: "closeUdcIconMaterial6",
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo, {
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcUDCLabel6',
					text: oLng_Opr.getText("BoxDeclInput_UDC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
				new sap.m.Text({
					id: 'closeUdcUDCText6',
					text: '{/UDCNR}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcDeclQtyLabel6',
					text: oLng_Opr.getText("BoxDeclInput_DeclQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'closeUdcDeclQtyText6',
					text: '{/QTYRES}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcOpLabel6',
					text: oLng_Opr.getText("BoxDeclInput_Operator") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
				new sap.m.Text({
					id: 'closeUdcOpText6',
					text: '{/LAST_OPENAME}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcTimeLabel6',
					text: oLng_Opr.getText("BoxDeclInput_Datetime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'closeUdcTimeText6',
					text: {
						parts: [
							{
								path: "/TIMEID"
							}
						],
						formatter: dateFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcDater6',
					text: oLng_Opr.getText("BoxDeclInput_Dater") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.MaskInput({
					id: 'closeUdcInputDater6',
					mask: "9999",
					placeholderSymbol: " ",
					placeholder: 'MMYY',
					width: '160px',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcBasketTT6',
					text: oLng_Opr.getText("BoxDeclInput_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'closeUdcInputBasketTT6',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'closeUdcQty6',
					text: oLng_Opr.getText("BoxDeclInput_Qty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'closeUdcInputQty6',
					min: 0,
					width: '80px',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).addEventDelegate({
					onfocusin: function (e) {
						$("#boxDeclInputQty6-inner").select();
					}
				}),
				new sap.m.Label({
					id: 'closeUdcLabelCausale6',
					text: oLng_Opr.getText("SuspDeclReas_Causal") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}).addStyleClass("inputLabel"),
				oCausalInput,
				new sap.m.Label({
					id: 'closeUdcNextPhase6',
					text: oLng_Opr.getText("RepUdcInput_Destination") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				,
				new sap.m.ComboBox({
					id: 'closeUdcInputNextPhase6',
					width: "100%",
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					}),
					change: oController.onNextPhaseChange
				}).bindAggregation("items", "/Rowset/1/Row",
					new sap.ui.core.Item({
						key: '{POPER}',
						text: {
							parts: [
								{
									path: 'POPER'
								},
								{
									path: 'CICTXT'
								},
								{
									path: 'COLOR'
								}
			],
							formatter: function (sIdRep, sDesc, sColor) {
								if (sColor) {
									var css = document.createElement("style");
									css.type = "text/css";
									css.innerHTML = "li[data-color='" + sColor + "'] { color: " + sColor + " !important; }";
									document.body.appendChild(css);
								}
								return sIdRep + ' - ' + sDesc;
							}
						},
						customData: [
																			 new sap.ui.core.CustomData({
								key: "color",
								value: "{COLOR}",
								writeToDom: true
							})
		]
					})
	),
	new sap.m.HBox({
					id: 'closeUdcBoxCodImb6',
					justifyContent: sap.m.FlexJustifyContent.SpaceBetween,
					width: "95%",
					items: [
	new sap.m.Label({
							id: 'closeUdcCodImb6',
							text: oLng_Opr.getText("BoxDeclInput_CodImb") + ':',
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Right,
							layoutData: new sap.ui.layout.GridData({
								span: "L2 M2 S12"
							})
						}).addStyleClass("inputLabel"),
	oCodImbCombo,
	new sap.ui.core.Icon({
							id: 'closeUdcIconCodImb6',
							src: "sap-icon://value-help",
							size: "18px",
							press: [oController.onSelectNmimb, {
								view: oView,
								combo: oCodImbCombo
	}],
							tooltip: oLng_Opr.getText('BoxDeclInput_CodImb'),
							layoutData: new sap.ui.layout.GridData({
								span: "L1 M1 S12"
							})
						}).addStyleClass("inputLabel")
],
					layoutData: new sap.ui.layout.GridData({
						span: "L6 M6 S12"
					})
				}),
	new sap.m.Label({
					id: 'closeUdcLabelNote6',
					text: oLng_Opr.getText("SuspDeclReas_Note") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}).addStyleClass("inputLabel"),
		new sap.m.Input({
					id: 'closeUdcNote6',
					width: '100%',
					maxLength: 40,
					layoutData: new sap.ui.layout.GridData({
						span: "L9 M9 S12"
					})
				})
]
		}).addStyleClass("sapUiTinyMarginTop");

		var oPanel = new sap.m.Panel({
			content: oGrid
		});

		var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL5.boxDeclInput6");

		var oPage = new sap.m.Page({
			id: "closeUdcInputPage6",
			title: oLng_Opr.getText('BoxDecl5_Scrap'),
			enableScrolling: false,
			headerContent: [
		new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
	],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
		oPanel,
		new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
				new sap.m.Button({
							id: 'closeUdcConfirmButton6',
							text: oLng_Opr.getText("BoxDeclInput_Confirm"),
							icon: "sap-icon://accept",
							press: oController.onConfirm
						}).addStyleClass("redButton bigButton"),
				new sap.m.Button({
							id: 'closeUdcPrintButton6',
							enabled: false,
							text: oLng_Opr.getText("BoxDeclInput_Print"),
							icon: "sap-icon://print",
							press: oBoxDeclInputController.onPrint
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
			]
				}).addStyleClass("sapUiTinyMarginTop")
	],
			footer: new sap.m.Toolbar({
				content: [
			new sap.m.Button({
						icon: "sap-icon://date-time",
						text: oLng_Opr.getText("ShiftDecl_ChangeDateShift"),
						press: oController.changeDateShift,
					}).addStyleClass("yellowButton noBorderButton"),
			new sap.m.Text({
						id: 'closeDeclDate6'
					}),
			new sap.m.Text({
						id: 'closeDeclShift6'
					}),
			new sap.m.ToolbarSpacer()
		]
			})
		});

		return oPage;
	}

});
