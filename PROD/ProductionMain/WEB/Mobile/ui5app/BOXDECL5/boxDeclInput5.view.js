// View Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.BOXDECL5.boxDeclInput5", {

	/** Specifies the Controller belonging to this View.
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/
	getControllerName : function() {
		return "ui5app.BOXDECL5.boxDeclInput5";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	* Since the Controller is given to this method, its event handlers can be attached right away.
	*/
	createContent : function(oController) {

		var oAppController = sap.ui.getCore().byId('app').getController();

		var css = document.createElement("style");
		css.type = "text/css";
		css.innerHTML = ".greenTile .sapMStdIconMonitor { color: green !important; } .greenTile .sapMStdTileTitle { color: green !important;} "
		+ ".redTile .sapMStdIconMonitor { color: red !important; } .redTile .sapMStdTileTitle { color: red !important;}"
		+ ".orangeTile .sapMStdIconMonitor { color: orange !important; } .orangeTile .sapMStdTileTitle { color: orange !important;}";
		document.body.appendChild(css);
	
		var newMenuTileContainer = function() {
			return new sap.m.TileContainer("functionsList1", {
				width: "100%",
				height: "70%",
				tiles: [
					new sap.m.StandardTile({
						id: "tileBU",
						type : sap.m.StandardTileType.Monitor,
						icon :  "sap-icon://overlay",
						title : oLng_Opr.getText("BoxDecl5_Good"),
						removable: false,
						press: [oController.pressTile, {
							controller: oController,
							tile: "BU"
						}],
						visible: false
					}).addStyleClass('greenTile'),
					new sap.m.StandardTile({
						id: "tileSS",
						type : sap.m.StandardTileType.Monitor,
						icon :  "sap-icon://status-in-process",
						title : oLng_Opr.getText("SuspDecl_Title"),
						removable: false,
						press: [oController.pressTile, {
							controller: oController,
							tile: "SO"
						}],
						visible: false
					}).addStyleClass('orangeTile'),
					new sap.m.StandardTile({
						id: "tileSC",
						type : sap.m.StandardTileType.Monitor,
						icon :  "sap-icon://delete",
						title : oLng_Opr.getText("BoxDecl5_Scrap"),
						removable: false,
						press: [oController.pressTile, {
							controller: oController,
							tile: "SC"
						}],
						visible: false
					}).addStyleClass('redTile'),
					new sap.m.StandardTile({
						id: "tileSV",
						type : sap.m.StandardTileType.Monitor,
						icon :  "sap-icon://wrench",
						title : oLng_Opr.getText("ServiceDecl_Title"),
						removable: false,
						press: [oController.pressTile, {
							controller: oController,
							tile: "SV"
						}],
						visible: false
					})
				]
			});
		};
		
		var oPage = new sap.m.Page({
			id: "boxDeclInput5Page",
			title: oLng_Opr.getText("BoxDecl5_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: newMenuTileContainer(),
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});
		
	  	return oPage;
	}

});
