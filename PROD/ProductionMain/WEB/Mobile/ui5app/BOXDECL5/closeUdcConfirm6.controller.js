// +
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL5.closeUdcConfirm6", {

	_oArguments: undefined,
	_sWsName: '',
	_oFMBTotem: undefined,
	_sFunzione: 'SC',
	_fieldMandatory: [],
	_currentDate: '',
	_currentShift: '',
	_currentRequestDate: '',

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("closeUdcConfirm6").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArgParams = ['plant', 'pernr', 'order', 'poper', 'idline'];
		const oArguments = oAppController.getPatternArguments(oEvent, oArgParams);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		this._fieldMandatory = [];

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('closeUdcInputPage6'), sPlant, sPernr);

		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oWorkstInfo = oStorage.get('WORKST_INFO');
		if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
			this._sWsName = oWorkstInfo.WSNAME;

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		var funzioneTotem = oSessionStorage.get('FMBTOTEM');
		var that = this;
		$.each(funzioneTotem, function(i, row) {
			if(row.TPOPE == that._sFunzione) {
				that._oFMBTotem = row;
			}
		});

		var bDisabledMode = false;
		if(!oUdcObject || !oUdcObject.hasOwnProperty('UDCNR') ||
			(oUdcObject.UDCNR !== oArguments.udc && !(oUdcObject.PRINT_MODE && oArguments.udc === '')))
		{
			oUdcObject = {};
			bDisabledMode = true;
		}

		var oNextPhaseCombo = $.UIbyID("closeUdcInputNextPhase6");
		
		var oNextPhaseModel = oNextPhaseCombo.getModel();
		if(oNextPhaseModel)
			oNextPhaseModel.destroy();
		oNextPhaseModel = new sap.ui.model.xml.XMLModel();
		oNextPhaseCombo.setModel(oNextPhaseModel);

		var sNextPhaseQuery = QService + sMovementPath + "NextGen/getNextOrderPoperQR";
		sNextPhaseQuery += "&Param.1=" + sPlant;
		sNextPhaseQuery += "&Param.2=" + this._oFMBTotem.FLOWID;
		sNextPhaseQuery += "&Param.3=" + oArguments.order;
		sNextPhaseQuery += "&Param.4=" + oArguments.poper;
		oNextPhaseModel.loadData(sNextPhaseQuery, false, false);

		//let defaultOper = $(oNextPhaseModel.getData()).find('Rowset')[0].getElementsByTagName('POPER')[0].textContent;
		//let defaultOperDesc = $(oNextPhaseModel.getData()).find('Rowset')[0].getElementsByTagName('CICTXT')[0].textContent;
		//oNextPhaseCombo.setSelectedKey(defaultOper);

		$.UIbyID("closeUdcNote6").setValue("");
		$.UIbyID("closeUdcInputCausale6").setValue("");

		this._currentRequestDate = "";
		this._currentShift = "";
		this._currentDate = "";

		$.UIbyID('closeDeclDate6').setText("");
		$.UIbyID('closeDeclShift6').setText("");
		
		this.createCausalsDialog(oArguments.plant);
			
		this.displayInfo(oUdcObject, bDisabledMode);
	},

	createCausalsDialog: function(sPlant) {
		const sDialogId = 'closeUdcCausalsDialog';
		var oCausalsDialog = $.UIbyID(sDialogId);
		if(!oCausalsDialog) {
			
			var oScrapReasCombo = new sap.m.ComboBox({
				width : "100%",
				change: function () {
					const sScrapReas = oScrapReasCombo.getValue();
					var aScrapItems = oScrapReasCombo.getItems();
					for(var i=0; i<aScrapItems.length; i++)
					{
						var oCurrentItem = aScrapItems[i];
						if(oCurrentItem.getKey() === sScrapReas)
						{
							oScrapReasCombo.setSelectedItem(oCurrentItem);
							break;
						}
					}
				}
			});
			
			var oScrapReasTemplate = new sap.ui.core.Item({
				key : '{SREASID}'
			}).bindProperty("text", {
				parts: [
					{path: 'SREASID'},
					{path: 'REASTXT'}
				],		     
				formatter: function(sReasId,sReasTxt){
					return sReasId + ' - ' + sReasTxt;
				}
			});

			oScrapReasCombo.bindAggregation("items","/Rowset/Row", oScrapReasTemplate);
			
			var oGrscReasModel = new sap.ui.model.xml.XMLModel();
			var sGrscQuery = QService + sMasterDataPath + "ScrabReasons/getScrabReasonsGroupsByTypeSQ";
			sGrscQuery += "&Param.1=" + sPlant;
			sGrscQuery += "&Param.2=SC";
			oGrscReasModel.loadData(sGrscQuery, false, false);
				
			var oGrscReasCombo = new sap.m.ComboBox({
				width : "100%",
				change: function () {
					const sSctype = oGrscReasCombo.getValue();
					var aGrscItems = oGrscReasCombo.getItems();
					for(var i=0; i<aGrscItems.length; i++)
					{
						var oCurrentItem = aGrscItems[i];
						if(oCurrentItem.getKey() === sSctype)
						{
							oGrscReasCombo.setSelectedItem(oCurrentItem);
							break;
						}
					}
					oGrscReasCombo.fireSelectionChange();
				},
				selectionChange: function () {
					var sGrscReas  = oGrscReasCombo.getSelectedKey();
					if(sGrscReas)
					{
						oScrapReasCombo.clearSelection();
						oScrapReasCombo.setValue();
						var oScrapReasModel = oScrapReasCombo.getModel();
						var sScrapQuery = QService + sMasterDataPath + "ScrabReasons/getScrabReasonsByGroupSQ"
						sScrapQuery += "&Param.1=" + sPlant;
						sScrapQuery += "&Param.2=" + sGrscReas;
						oScrapReasModel.loadData(sScrapQuery,false,false);
						var aItems = oScrapReasCombo.getItems();
						if(aItems.length === 1)
							oScrapReasCombo.setSelectedItem(aItems[0]);
						
						var oController = sap.ui.getCore().byId('closeUdcConfirm6View').getController();
						var oGrscContext = oGrscReasCombo.getSelectedItem().getBindingContext();
						if(oController._sIdRep && oGrscContext && 
							oGrscContext.getProperty('ID_REP') != oController._sIdRep)
						{
							oGrscReasCombo.setValueStateText(oLng_Opr.getText("closeUdcReas_ReasTypeErrorMsg"));
							oGrscReasCombo.setValueState(sap.ui.core.ValueState.Error);
						}
						else
						{
							oGrscReasCombo.setValueStateText();
							oGrscReasCombo.setValueState(sap.ui.core.ValueState.None);
						}
					}
				}
			});
			oGrscReasCombo.setModel(oGrscReasModel);
			
			var oGrscReasTemplate = new sap.ui.core.Item({
				key : "{SCTYPE}"
			}).bindProperty("text", {
				parts: [
					{path: 'SCTYPE'},
					{path: 'REASTXT'}
				],		     
				formatter: function(sSctype,sReasTxt){
					return sSctype + ' - ' + sReasTxt;
				}
			});
			oGrscReasCombo.bindAggregation("items","/Rowset/Row", oGrscReasTemplate);
		
			oCausalsDialog = new sap.m.Dialog({
				id: sDialogId,
				title: oLng_Opr.getText("SuspDeclReas_Causals"),
				content: new sap.ui.layout.form.SimpleForm({
					editable: true,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					labelSpanL: 4,
					labelSpanM: 4,
					labelSpanS: 4,
					emptySpanL: 0,
					emptySpanM: 0,
					emptySpanS: 0,
					columnsL: 1,
					columnsM: 1,
					columnsS: 1,
					content: [
						new sap.m.Label({
							text: oLng_Opr.getText("SuspDeclReas_GrscReas"),
						}),
						oGrscReasCombo,
						new sap.m.Label({
							text: oLng_Opr.getText("SuspDeclReas_ScrapReas"),
						}),
						oScrapReasCombo
					],
				}),
				beginButton: new sap.m.Button({
					text:  oLng_Opr.getText("SuspDeclReas_Save"),
					icon: "sap-icon://save",
					press: function () {
						var oInputField = oCausalsDialog['inputField'];
						if(oInputField)
						{
							var oController = sap.ui.getCore().byId('closeUdcConfirm6View').getController();
							var oGrscContext = oGrscReasCombo.getSelectedItem().getBindingContext();
							if(oController._sIdRep && oGrscContext && 
								oGrscContext.getProperty('ID_REP') != oController._sIdRep)
							{
								sap.m.MessageBox.show(oLng_Opr.getText("SuspDeclReas_ReasTypeErrorMsg"), {
									icon: sap.m.MessageBox.Icon.ERROR,
									title: oLng_Opr.getText('General_ErrorText'),
									actions: [sap.m.MessageBox.Action.OK]
								});
							}
							else
							{
								//oInputField.setValue(oScrapReasCombo.getSelectedKey());
								oInputField.setValue(oScrapReasCombo.getSelectedItem().getText());
								oInputField.setValueStateText();
								oInputField.setValueState(sap.ui.core.ValueState.None);
								oCausalsDialog.close();
							}
						}
					}
				}),
				endButton: new sap.m.Button({
					text: oLng_Opr.getText("SuspDeclReas_Cancel"),
					icon: "sap-icon://decline",
					press: function() {
						oCausalsDialog.close();
					}
				}),
				beforeOpen: function() {
					oScrapReasCombo.clearSelection();
					oScrapReasCombo.setValue();
					var oScrapReasModel = oScrapReasCombo.getModel();
					if(oScrapReasModel)
						oScrapReasModel.destroy();
					oScrapReasCombo.setModel(new sap.ui.model.xml.XMLModel());
					oGrscReasCombo.clearSelection();
					oGrscReasCombo.setValue();
					oGrscReasCombo.setValueStateText();
					oGrscReasCombo.setValueState(sap.ui.core.ValueState.None);
				}
			}).addStyleClass("noPaddingDialog");
			
			this.getView().addDependent(oCausalsDialog);
		}
	},

/***********************************************************************************************/
// Visualizzazione delle informazioni
/***********************************************************************************************/

	displayInfo: function(oUdcObject, bDisabledMode) {

		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		var oView = sap.ui.getCore().byId('closeUdcConfirm6View');
		var oModel = oView.getModel();

		var oController = oView.getController();
		const oArguments = oController._oArguments;
		const oFMBTotem = oController._oFMBTotem;

		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oModel.forceNoCache(true);
			oModel.updateBindings(true);
			oView.setModel(oModel);
		} else {
			oModel.forceNoCache(true);
			oModel.updateBindings(true);
		}
		oModel.setData(oUdcObject);

		const sNewUdc = oUdcObject.UDCNR === '';

		var sCheckQuery = sMovementPath + "NextGen/getFieldsByFlowIdSQ";
		sCheckQuery += "&Param.1=" + oArguments.plant;
		sCheckQuery += "&Param.2=" + oFMBTotem.FLOWID;

		var oCheckResult = fnGetAjaxData(sCheckQuery, false);
		var rows = $(oCheckResult).find('Row');

		let fieldname;
		let fstate;
		let cond = false;
		$.each(rows, function( i, row ) {
			fieldname = row.getElementsByTagName('FIELDNAME')[0].textContent;
			fstate = row.getElementsByTagName('FSTATE')[0].textContent;
			if(fstate == 'M') {
				oController._fieldMandatory.push(fieldname);
			}
			cond = (fstate == 'V' || fstate == 'M' ? true : false);
			if($.UIbyID(fieldname))
				$.UIbyID(fieldname).setVisible(cond);
			else
				console.log("ID non trovato: " + fieldname);
		});

		$.each(oController._fieldMandatory, function( i, row ) {
			let field = $.UIbyID(row);
			field.setValueState(sap.ui.core.ValueState.None);
			field.setValueStateText();
		});
		
		if(oFMBTotem && oFMBTotem.UDCPRINT == 0) {
			$.UIbyID("closeUdcPrintButton6").setVisible(false);
		}

		/*if(oFMBTotem && oFMBTotem.UDCFULL == 1) {
			$.UIbyID("closeUdcUDCLabel6").setVisible(false);
			$.UIbyID("closeUdcUDCText6").setVisible(false);
			$.UIbyID("closeUdcDeclQtyLabel6").setVisible(false);
			$.UIbyID("closeUdcDeclQtyText6").setVisible(false);
			$.UIbyID("closeUdcOpLabel6").setVisible(false);
			$.UIbyID("closeUdcOpText6").setVisible(false);
			$.UIbyID("closeUdcTimeLabel6").setVisible(false);
			$.UIbyID("closeUdcTimeText6").setVisible(false);
		}*/

		var bPrintMode = false;
		if(oUdcObject.hasOwnProperty('PRINT_MODE'))
			bPrintMode = oUdcObject.PRINT_MODE;

		$.UIbyID('closeUdcInputBasketTT6').setValue(oUdcObject.BASKETTT);
		$.UIbyID('closeUdcInputDater6').setValue(oUdcObject.DATER);

		$.UIbyID('closeUdcInputQty6').setValue((bPrintMode) ? oUdcObject.QTYDECL : '');

		$.UIbyID('closeUdcConfirmButton6').setEnabled(!bDisabledMode && !bPrintMode);
		$.UIbyID('closeUdcPrintButton6').setEnabled(!bDisabledMode && bPrintMode);

		/*var oNextPhaseCombo = $.UIbyID("boxDeclInputNextPhase6");
		oNextPhaseCombo.setValue();
		oNextPhaseCombo.setSelectedKey();
		oNextPhaseCombo.setValueState();
		oNextPhaseCombo.setValueStateText();*/
	},

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('closeUdcConfirm6View').getController();

		oController._currentRequestDate = "";
		oController._currentShift = "";
		oController._currentDate = "";

		$.UIbyID('closeDeclDate6').setText("");
		$.UIbyID('closeDeclShift6').setText("");

		oAppRouter.navTo('boxDecl5',{
			query: oController._oArguments
		});
	},

/***********************************************************************************************/
// Conferma
/***********************************************************************************************/

	onConfirm: function() {
		var oView = sap.ui.getCore().byId('closeUdcConfirm6View');
		var oController = oView.getController();
		var oModel = oView.getModel();
		const oArguments = oController._oArguments;
		const oFMBTotem = oController._oFMBTotem;
		const fieldMandatory = oController._fieldMandatory;

		var oBasketTTInput = $.UIbyID('closeUdcInputBasketTT6');
		var oDaterInput = $.UIbyID('closeUdcInputDater6');
		var oQtyInput = $.UIbyID('closeUdcInputQty6');
		var note = $.UIbyID('closeUdcNote6').getValue();
		var causale = $.UIbyID('closeUdcInputCausale6').getValue();

		oQtyInput.setValueState(sap.ui.core.ValueState.None);
		oQtyInput.setValueStateText();
		oBasketTTInput.setValueState(sap.ui.core.ValueState.None);
		oBasketTTInput.setValueStateText();
		oDaterInput.setValueState(sap.ui.core.ValueState.None);
		oDaterInput.setValueStateText();

		const sBasketTT = oBasketTTInput.getValue();
		const sDater = oDaterInput.getValue();
		const sQtyDecl = oQtyInput.getValue();
		const iQtyDecl = parseInt(sQtyDecl);

		var iQtyRes = parseFloat(oModel.getProperty("/QTYRES"));
		if(isNaN(iQtyRes))
			iQtyRes = 0;

		var iMatPzUdc = parseFloat(oModel.getProperty("/MAT_PZUDC"));
		if(isNaN(iMatPzUdc))
			iMatPzUdc = 0;

		var sErrorMessage;
		var oErrorField;
		const sCurrentYear = parseInt(new Date().getFullYear().toString().substring(2, 4));
		
		if(sDater && (sDater.length != 4 || !sDater.match(/(0[1-9]|1[0-2])([0-9]{2})/))) // solo caratteri alfanumerici
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterErrorMsg");
			oErrorField = oDaterInput;
		} else if(sDater && (sDater.substring(2 ,4) < sCurrentYear-1 || sDater.substring(2, 4) > sCurrentYear + 1))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterYearErrorMsg");
			oErrorField = oDaterInput;
		} else if(sBasketTT && !sBasketTT.match(/^[0-9a-zA-Z ]+$/))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_BasketTTErrorMsg");
			oErrorField = oBasketTTInput;
		} else if(iQtyDecl && iQtyDecl <= 0)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyErrorMsg");
			oErrorField = oQtyInput;
		} else if(iQtyDecl && iQtyDecl > 15000)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg") + " (15000)";
			oErrorField = oQtyInput;
		} else if(iMatPzUdc > 0 && (iQtyRes + iQtyDecl) > iMatPzUdc)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg");
			oErrorField = oQtyInput;
		} else if(fieldMandatory && fieldMandatory.length > 0) {
			$.each(fieldMandatory, function( i, row ) {
				let field = $.UIbyID(row);
				sErrorMessage = !(field.getValue()) ? "Campo obbligatorio" : "";
				oErrorField = field;
				if(sErrorMessage)
					return false;
			});
		}

		if(!sErrorMessage)
		{
			var continueFunc = function(sState) {

					if(iMatPzUdc && iMatPzUdc > 0 && (iQtyRes + iQtyDecl === iMatPzUdc)
						&& sState !== 'C')
					{
						var sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyFullErrorMsg");

						oQtyInput.setValueState(sap.ui.core.ValueState.Warning);
						oQtyInput.setValueStateText(sErrorMessage);
						oQtyInput.focus();

						sap.m.MessageBox.show(sErrorMessage, {
							icon: sap.m.MessageBox.Icon.WARNING,
							title: oLng_Opr.getText("General_WarningText"),
							actions: sap.m.MessageBox.Action.OK
						});

						return;
					}

					const sFIUdC = (sState === 'C') ? 'C' : 'I';
					const sDatetime = getDatetimeString(new Date());

					let nextPhase = sap.ui.getCore().byId("closeUdcInputNextPhase6").getSelectedKey();

					var sSaveQuery = sMovementPath + "NextGen/saveDataTotemQR";
					sSaveQuery += "&Param.1=" + oFMBTotem.PLANT;
					sSaveQuery += "&Param.2=" + oFMBTotem.FLOWID;
					sSaveQuery += "&Param.3=" + (oController._currentRequestDate ? oController._currentRequestDate : oAppController._sDate);
					sSaveQuery += "&Param.4=" + (oController._currentShift ? oController._currentShift : oAppController._sShift);
					sSaveQuery += "&Param.5=" + oArguments.order;
					sSaveQuery += "&Param.6=" + oArguments.poper;
					sSaveQuery += "&Param.7=" + oArguments.order;
					sSaveQuery += "&Param.8=" + (nextPhase ? nextPhase : "");
					sSaveQuery += "&Param.9=" + sQtyDecl;
					sSaveQuery += "&Param.10=" + causale;
					sSaveQuery += "&Param.11=" + oArguments.pernr;
					sSaveQuery += "&Param.12=" + note;
					sSaveQuery += "&Param.13=";
					sSaveQuery += "&Param.14=" + sBasketTT;
					sSaveQuery += "&Param.15=";
					sSaveQuery += "&Param.16=" + sDater;
					sSaveQuery += "&Param.17=" + sLanguage;
					sSaveQuery += "&Param.18=" + sFIUdC;
					sSaveQuery += "&Param.19=";
					sSaveQuery += "&Param.20=" + oController._sWsName;
					sSaveQuery += "&Param.21=" + oArguments.udc;
					sSaveQuery += "&Param.22=";
					sSaveQuery += "&Param.23=";
					sSaveQuery += "&Param.24=" + oArguments.idline;
					sSaveQuery += "&Param.25=";

					var successFunction = function(oResults) {

						var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
						var oUdcObject = oSessionStorage.get('FUNCDATA');

						if(!oUdcObject)
							oUdcObject = {};

						if(oResults.hasOwnProperty('outputUdc') && oResults.outputUdc)
							oUdcObject.UDCNR = oResults.outputUdc;
						if(oResults.hasOwnProperty('pdfString') && oResults.pdfString)
						{
							oUdcObject.PDFSTRING = oResults.pdfString;
							if(parseInt(oResults.code) === 1) {
								var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL5.boxDeclInput6");
								oBoxDeclInputController.showPdfDialog(oResults.pdfString, true, function() {
									//oController.onNavBack(true);
									delete oController._oArguments.order;
									delete oController._oArguments.poper;
									delete oController._oArguments.idline;
									delete oController._oArguments.udc;

									oController._currentRequestDate = "";
									oController._currentShift = "";
									oController._currentDate = "";

									$.UIbyID('closeDeclDate6').setText("");
									$.UIbyID('closeDeclShift6').setText("");

									oAppRouter.navTo('boxDecl5',{
										query: oController._oArguments
									});
								});
							}
						} else {
							//oController.onNavBack(true);
							delete oController._oArguments.order;
							delete oController._oArguments.poper;
							delete oController._oArguments.idline;
							delete oController._oArguments.udc;

							oAppRouter.navTo('boxDecl5',{
								query: oController._oArguments
							});
						}
						oUdcObject.PRINT_MODE = true;
						oUdcObject.QTYRES = iQtyRes + iQtyDecl;
						oUdcObject.QTYDECL = iQtyDecl;
						oUdcObject.WTDUR = "";
						oUdcObject.NRPRNT = "";
						oUdcObject.NMIMB = "";
						oUdcObject.DATER = sDater;
						oUdcObject.BASKETTT = sBasketTT;
						oUdcObject.TIMEID = sDatetime;
						oUdcObject.LAST_OPEID = oArguments.pernr;
						oUdcObject.STATE = sState;

						const oLoginInfo = oSessionStorage.get('LOGIN_INFO');
						if(oLoginInfo.hasOwnProperty("opeName"))
							oUdcObject.LAST_OPENAME = oLoginInfo.opeName;

						oSessionStorage.put('FUNCDATA', oUdcObject);
						oController.displayInfo(oUdcObject, false);
					};

					oAppController.handleRequest(sSaveQuery, {
						onSuccess: successFunction,
						onWarning: successFunction,
						notShowWarning: [1],
						showSuccess: false,
						results: ['outputUdc','pdfString']
					});
				};

				var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL5.boxDeclInput6");
			
				oBoxDeclInputController.selectState({
					firstValue: 'O',
					secondValue: 'C',
					secondColorCss: 'redButton',
					fmbTotem: oFMBTotem,
					onCallback: continueFunc
				})
		}
		else
		{
			if(oErrorField)
			{
				oErrorField.setValueState(sap.ui.core.ValueState.Warning);
				oErrorField.setValueStateText(sErrorMessage);
				oErrorField.focus();
			}

			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
	},

	changeDateShift: function() {
		var oController = sap.ui.getCore().byId('closeUdcConfirm6View').getController();
		const oArguments = oController._oArguments;
		
		var oDatePicker = new sap.m.DatePicker({
			displayFormat: "dd/MM/yyyy",
			valueFormat: 'yyyy-MM-dd',
			maxDate: new Date(),
			width: "100%",
			layoutData : new sap.ui.layout.GridData({
				span: "L9 M9 S12"
			})
		});
		
		var oShiftTemplate = new sap.ui.core.Item({
			key : "{SHIFT}",
			text: "{SHNAME}"
		});
		
		var oShiftCombo = new sap.m.ComboBox({
			width: "100%",
			layoutData : new sap.ui.layout.GridData({
				span: "L9 M9 S12"
			})
		})
		oShiftCombo.bindAggregation("items","/Rowset/Row", oShiftTemplate);
		
		var oShiftModel = new sap.ui.model.xml.XMLModel();
		var sQuery = QService + sMasterDataPath + "Shifts/getShiftsByPlantSQ";
		sQuery += "&Param.1=" + oArguments.plant;
		oShiftModel.loadData(sQuery, false, false);
		oShiftCombo.setModel(oShiftModel);
		
		var oDateShiftDialog = new sap.m.Dialog({
			title: oLng_Opr.getText('ReprintUdc_SelectDateShift'),
			contentWidth: '500px',
			content: new sap.ui.layout.Grid({
				hSpacing: 1,
				vSpacing: 0.5,
				content: [
					new sap.m.Label({
						text: oLng_Opr.getText('ReprintUdc_Date') + ':',
						width: '100%',
						textAlign: sap.ui.core.TextAlign.Right,
						layoutData : new sap.ui.layout.GridData({
							span: "L3 M3 S12"
						})
					}).addStyleClass("inputLabel"),
					oDatePicker,
					new sap.m.Label({
						text: oLng_Opr.getText('ReprintUdc_Shift') + ':',
						width: '100%',
						textAlign: sap.ui.core.TextAlign.Right,
						layoutData : new sap.ui.layout.GridData({
							span: "L3 M3 S12",
							linebreak:true
						})
					}).addStyleClass("inputLabel"),
					oShiftCombo
				]
			}).addStyleClass("sapUiSmallMarginTopBottom"),
			beginButton: new sap.m.Button({
				text:  oLng_Opr.getText("ReprintUdc_Apply"),
				icon: "sap-icon://accept",
				press: function () {
					const sRequestDate = oDatePicker.getValue();
					var oShiftContext = oShiftCombo.getSelectedItem().getBindingContext();
					const sRequestShift = oShiftContext.getProperty("SHIFT");
					
					var sDate;
					var aDateParts = sRequestDate.split('-');
					if(aDateParts.length === 3) // da formato YYYY-MM-DD a DD/MM/YYYY
					{
						var sTmp = aDateParts[0];
						aDateParts[0] = aDateParts[2];
						aDateParts[2] = sTmp;
						sDate = aDateParts.join('/');
					}
					$.UIbyID('closeDeclDate6').setText(sDate);
					$.UIbyID('closeDeclShift6').setText(oShiftContext.getProperty("SHNAME"));

					oController._currentDate = sDate;
					oController._currentRequestDate = sRequestDate;
					oController._currentShift = sRequestShift;
					oDateShiftDialog.close();
				}
			}),
			endButton: new sap.m.Button({
				text: oLng_Opr.getText("ReprintUdc_Cancel"),
				icon: "sap-icon://decline",
				press: function() {
					oDateShiftDialog.close();
				}
			}),
			afterClose: function() {
				oDateShiftDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");
		
		oDatePicker.setValue(oAppController._sDate);
		oShiftCombo.setSelectedKey(oAppController._sShift);
		
		oDateShiftDialog.open();
	},

	onNextPhaseChange: function(data) {
		var oController = sap.ui.getCore().byId('closeUdcConfirm6View').getController();
		const oArguments = oController._oArguments;
		const oFMBTotem = oController._oFMBTotem;

		let comboModel = data.oSource.getModel();
		let comboKey = data.oSource.getSelectedKey();
		const optional = getValueFromXMLModelByRowset(comboModel, "1", "POPER", comboKey, "OPTIONAL");

		if(optional == 1) {
			$.UIbyID('closeUdcNote6').setValue(oFMBTotem.UDCNOTE);
		}

	}

});
