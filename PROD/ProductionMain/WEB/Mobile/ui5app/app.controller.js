/**
 * @file Controller Applicazione
 *
 * @author Alex Bonaffini <alex.bonaffini@it-link.it>
 * @version 0.1
 *
 * @namespace ui5app
 * @requires sap.m
 */

 
/**
 * @class app
 * @memberof ui5app
 */

var oAppRouter;
var oAppController;

sap.ui.controller("ui5app.app", {
	
	_sShift: undefined,
	_sDate: undefined,

	_bIsTimerEnabled: false,
	_sLogoutCallId: undefined,
	
	onInit: function() {

		oAppRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oAppController = this;

		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oConfig = oStorage.get('CONFIG_INFO');
		if(oConfig && oConfig.hasOwnProperty('CONNECTION_CHECK') && oConfig.CONNECTION_CHECK)	
			this.startConnectionCheck();
	},

//	onBeforeRendering: function() {
//
//	},

//	onAfterRendering: function() {
//
//	},

//	onExit: function() {
//
//	}

/**
 * Ritorna alla pagina di login
 * @function onLogout
 * @memberOf ui5app.app
 */
	onLogout: function () {	
		if(typeof(this.logoutFunc) === 'function')
			this.logoutFunc();
		
		if(oAppController._sLogoutCallId)
		{
			jQuery.sap.clearDelayedCall(oAppController._sLogoutCallId);
			oAppController._sLogoutCallId = undefined;
		}

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put("LOGIN_INFO",undefined);

		oAppRouter.navTo('login');
	},
	
	startLogoutTimer: function(fTimeoutDelay) {
		
		if(!fTimeoutDelay)
			fTimeoutDelay = 180;
		
		if(oAppController._sLogoutCallId) // se c'e già un timer avviato lo arresta
			jQuery.sap.clearDelayedCall(oAppController._sLogoutCallId);
			
		oAppController._sLogoutCallId = jQuery.sap.delayedCall(fTimeoutDelay*1000, null, function() {
							
			oAppController._sLogoutCallId = undefined;
			
			var iSecondCount = 30;
			var sCallId = jQuery.sap.delayedCall(iSecondCount * 1000, null, function() { // 10 secondi
				oAppController.onLogout();
			});
			
			var sTimeId;
			var oDialog = new sap.m.Dialog({
				icon: "sap-icon://message-warning",
				title: oLng_Opr.getText("General_LogoutWarningText") + " " + iSecondCount,
				type: sap.m.DialogType.Message,
				content: new sap.m.Text({
					text: oLng_Opr.getText("General_LogoutMessage")
				}),
				buttons: [
					new sap.m.Button({
						text: oLng_Opr.getText("General_Postpone"),
						press: function () {
							oAppController.startLogoutTimer(fTimeoutDelay);
							oDialog.close();
						}
					})/*,
					new sap.m.Button({
						text: oLng_Opr.getText("General_OkText"),
						press: function () {
							oAppController.onLogout();
							oDialog.close();
						}
					})*/
				],
				beforeOpen: function() {
					sTimeId = jQuery.sap.intervalCall(1000, null, function() {
						iSecondCount--;
						var sTitle = oLng_Opr.getText("General_LogoutWarningText");
						if(iSecondCount > 0)
							sTitle += " " + iSecondCount;
						oDialog.setTitle(sTitle);
					});
				},
				afterClose: function() {
					jQuery.sap.clearIntervalCall(sTimeId);
					jQuery.sap.clearDelayedCall(sCallId);
					oDialog.destroy();
				}
			});
			oDialog.addStyleClass('warningDialog');
			oDialog.open();
			
			/*
			const sPostponeAction = oLng_Opr.getText("General_Postpone");
			
			sap.m.MessageBox.show(oLng_Opr.getText("General_LogoutMessage"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText('General_WarningText'),
				actions: [sPostponeAction,sap.m.MessageBox.Action.OK],
				onClose: function(oAction) {
					jQuery.sap.clearDelayedCall(sCallId);
					
					if (oAction === sPostponeAction)
						timeoutFunc();
					else if(oAction === sap.m.MessageBox.Action.OK)
						oAppController.onLogout();
				}
			});
			*/
		});
		
		var aResetOnClick = function() {
			document.removeEventListener('click',aResetOnClick);
			if(oAppController._sLogoutCallId)
				oAppController.startLogoutTimer(fTimeoutDelay);
		};
			
		jQuery.sap.delayedCall(10000, null, function() {
			document.addEventListener('click', aResetOnClick);
		});
	},
	
/**
 * Recupero parametri passati per URL
 * @function getPatternArguments
 * @memberOf ui5app.app
 * @param {event} oEvent
 * @param {array} aParams - Array dei nomi dei parametri da recuperare dall'URL
 */
	getPatternArguments: function (oEvent, aParams) {
		
		var oPatternArguments = oEvent.getParameter("arguments");
		var oArguments;
		var sErrorMessage;
		if(oPatternArguments && oPatternArguments.hasOwnProperty('?query') && 
			oPatternArguments['?query'] !== undefined)
		{
			oArguments = oPatternArguments['?query'];
			
			for (var i=0; i < aParams.length; i++)
			{
				if(!oArguments.hasOwnProperty(aParams[i])) // || oArguments[aParams[i]]===''
				{
					sErrorMessage = oLng_Opr.getText('General_UrlMissedParamText') + ": " + aParams[i];
					break;
				}
			}
		}
		else
		{
			oArguments = {};
			sErrorMessage = oLng_Opr.getText('General_UrlErrorText') + ": " + aParams.join(", ");
		}
		
		if(sErrorMessage)
		{
			jQuery.sap.delayedCall(10, null, function() {
				sap.m.MessageBox.show(sErrorMessage, {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: oLng_Opr.getText('General_WarningText'),
					actions: sap.m.MessageBox.Action.OK
				});
			});
		}
		
		return oArguments;
	},

/**
 * Caricamento e visualizzazione dei dati nel custom subheader
 * @function loadSubHeaderInfo
 * @memberOf ui5app.app
 * @param {sap.m.Page} oPage - Pagina in cui inserire il subheader
 * @param {string} sPlant - Divisione
 * @param {string} sPernr - Identificativo operatore
 */
	loadSubHeaderInfo: function (oPage,sPlant,sPernr) {
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oLoginInfo = oSessionStorage.get("LOGIN_INFO");
		
		if(!oLoginInfo || !oLoginInfo.hasOwnProperty("plant") || oLoginInfo.plant !== sPlant) {
			// ripetere richiesta login per aggiornare LOGIN_INFO?
			return;
		}
		
		oAppController._sShift = oLoginInfo.shiftId;
		oAppController._sDate = oLoginInfo.date;
		
		const fTimeoutDelay = parseFloat(oLoginInfo.logoutTime);
		if(!oAppController._sLogoutCallId && !isNaN(fTimeoutDelay) && fTimeoutDelay > 0)
			oAppController.startLogoutTimer(fTimeoutDelay);
		
		if(oPage)
		{
			var sPlantTxt = '';
			var sDate = '';
			var sShiftTxt = '';
			var sOpeName = '';
				
			if(oLoginInfo.hasOwnProperty("plantDesc"))
				sPlantTxt = oLoginInfo.plantDesc;
			else
				sPlantTxt = oLoginInfo.plant;
			
			if(!oLoginInfo.hasOwnProperty("pernr") || oLoginInfo.pernr !== sPernr) {
				// ripetere richiesta login per aggiornare LOGIN_INFO?
			}
			
			if(oLoginInfo.hasOwnProperty("date") && oLoginInfo.date)
			{
				var aDateParts = oLoginInfo.date.split('-');
				if(aDateParts.length === 3) // da formato YYYY-MM-DD a DD/MM/YYYY
				{
					var sTmp = aDateParts[0];
					aDateParts[0] = aDateParts[2];
					aDateParts[2] = sTmp;
					sDate = aDateParts.join('/');
				}
			}
			
			if(oLoginInfo.hasOwnProperty("shiftDesc"))
				sShiftTxt = oLoginInfo.shiftDesc;
			else if(oLoginInfo.hasOwnProperty("shiftId"))
				sShiftTxt = oLoginInfo.shiftId;
			
			if(oLoginInfo.hasOwnProperty("opeName"))
				sOpeName = oLoginInfo.opeName;

			$.UIbyID('subHeaderPlantText').setText(sPlantTxt);
			$.UIbyID('subHeaderDateText').setText(sDate);
			$.UIbyID('subHeaderShiftText').setText(sShiftTxt);
			$.UIbyID('subHeaderOperatorText').setText(sOpeName);
			
			oPage.insertContent($.UIbyID('subHeaderPanel'),0);
		}
	},

/**
 * Visualizza un messaggio toast custom
 * @function showCustomToast
 * @memberOf ui5app.app
 * @param {object} oParams - parametri
 * @param {boolean} oParams.type - tipo di messaggio toast ('Default','Success','Warning') default: 'Default'
 * @param {string} oParams.message - messaggio visualizzato
 * @param {integer} oParams.duration - durata visualizzazione (millisecondi), default: 1000
 * @param {boolean} oParams.autoClose - chiusura automatica, default: true
 * @param {integer} oParams.animationDuration - durata animazione (millisecondi), default: 1000
 * @param {boolean} oParams.closeOnBrowserNavigation - chiusura del messaggio alla navigazione da una pagina a un'altra, default: false
 */
	showCustomToast: function(oParams) {
		
		if(!oParams.hasOwnProperty('type'))
			oParams.type = 'Default';
		if(!oParams.hasOwnProperty('duration'))
			oParams.duration = 6000;
		if(!oParams.hasOwnProperty('autoClose'))
			oParams.autoClose = true;
		if(!oParams.hasOwnProperty('animationDuration'))
			oParams.animationDuration = 1000;
		if(!oParams.hasOwnProperty('closeOnBrowserNavigation'))
			oParams.closeOnBrowserNavigation = false;

		sap.m.MessageToast.show(oParams.message, {
			duration: oParams.duration, 
			autoClose: oParams.autoClose,
			my: oParams.my,
			at: oParams.at,
			animationDuration: oParams.animationDuration,
			closeOnBrowserNavigation: oParams.closeOnBrowserNavigation
		});
		
		var oMessageToastDOM = $('#content').parent().find('.sapMMessageToast');
		switch(oParams.type)
		{
			case 'Success':
				oMessageToastDOM.css('background', 'rgba(0,128,0,1)');
				break;
			case 'Warning':
				oMessageToastDOM.css('background', 'rgba(240,171,0,1)');
				break;
			//default:
		}
		
		oMessageToastDOM.css('text-shadow', '0 0px 0 #ffffff');
		oMessageToastDOM.css('font-size', '130%');
	},
	
/**
 * Avvio del controllo di connessione
 * @function startConnectionCheck
 * @memberOf ui5app.app
 */
	startConnectionCheck: function() {
		
		var urlExists = function (url){
			var result;
		    $.ajax({
			        timeout: 5000,
			        type: 'GET',
			        dataType: 'jsonp',
			        jsonp: false,
			        contentType: "application/json; charset=utf-8;",
			        url: url,
			        cache: false,
			        "error":function(XMLHttpRequest,textStatus, errorThrown) {
			            if(errorThrown == "timeout") {
				        	console.debug('(test di connessione) Errore: ' + errorThrown);
			                result = {
			                	'code': 0,
				                'status': "Connessione debole o assente"
				            };
			            }
			            /*else
			            	console.debug('(test di connessione) Altro: ' + errorThrown);*/
			    },
			    complete: function(){

			    	//console.log("(test di connessione) Risposta: " + JSON.stringify(result));
			    	if(result.code === 0)
						oAppController.showCustomToast({
							type: 'Warning',
							message: result.status
						});
			    	
			    	if(oAppController._bIsTimerEnabled)
			    	{
				    	setTimeout(function(){
		            	   urlExists(url);
		       			}, 3000);
			    	}
			    },
			    statusCode: {
			        404:function(){
			        	result = {
			            	'code': 404,
			                'status': "Not found"
			        	};
			        },
			        0:function(){
			        	result = {
			            	'code': 0,
		                    'status': "Connessione debole o assente!!"
		                };
			        },
		            500:function(){
		            	result = {
		                	'code': 500,
		                    'status': "Internal server Error"
		               };
		            },
		            200:function(){
		            	result = {
		                    'code': 200,
		                    'status': "Ok"
		               };
		            }
			    }
		    });
		};
		
		if(!oAppController._bIsTimerEnabled)
		{
			oAppController._bIsTimerEnabled = true;
			urlExists('http://10.0.0.62:50000/XMII/CM/ProductionMain/Mobile/index.irpt');
		}
	},
	
/**
 * Arresto del controllo di connessione
 * @function stopConnectionCheck
 * @memberOf ui5app.app
 */
	stopConnectionCheck: function() {
		oAppController._bIsTimerEnabled = false;
	},
	
/**
 * Controllo validità ordine/fase
 * @function checkOrderPhase
 * @memberOf ui5app.app
 * @param {string} sOrderPhase - Ordine e fase
 * @return {string} Messaggio di errore o undefined
 */
	checkOrderPhase: function(sOrderPhase) {
		var sErrorMessage;
		
		if(sOrderPhase.length < 12)
			sErrorMessage = oLng_Opr.getText("General_OrderErrorMsg");
		else
		{
			var sOrder = sOrderPhase.substring(0,12);
			var sPhase = sOrderPhase.substring(12,sOrderPhase.length);
			
			if(sPhase.length !== 4)
				sErrorMessage = oLng_Opr.getText("General_PhaseErrorMsg");
		}
		
		return sErrorMessage;
	},

/**
 * Esegue una verifica ed esegue una funzione
 * @function handleRequest
 * @memberOf ui5app.app
 * @param {string} sRequest - Query / Transazione di verifica (con valori di risposta Code e Message)
 * @param {object} oParams - parametri aggiuntivi
 * @param {boolean} oParams.showSuccess - visualizzare il messaggio di conferma (default true)
 * @param {boolean} oParams.showSuccessToast - visualizzare il messaggio di conferma in un toast (default true)
 * @param {function} oParams.onSuccess - funzione da eseguire se ha esito positivo
 * @param {boolean / array} oParams.showWarning - boolean visualizzazione messaggio di warning o array di codici di warning abilitati alla visualizzazione
 * @param {boolean / array} oParams.notShowWarning - array di codici di warning da escludere dalla visualizzazione (solo se oParams.showWarning = true)
 * @param {boolean} oParams.oneButtonWarning - un solo bottone nel messaggio di warning (default false)
 * @param {string} oParams.confirmButtonWarning - testo bottone conferma messaggio di warning (default 'Ok')
 * @param {string} oParams.cancelButtonWarning - testo bottone annulla messaggio di warning (default 'Annulla')
 * @param {function} oParams.onWarning - funzione da eseguire a seguito del warning
 * @param {boolean} oParams.showError - visualizzare il messaggio di errore
 * @param {function} oParams.onError - funzione da eseguire se restituisce errore
 * @param {object} oParams.callbackParams - Parametri aggiuntivi da passare alla funzione di callback
 * @param {array/object} oParams.results - array o oggetto da passare a fnGetAjaxVal
 */
	handleRequest: function(sRequest, oParams) {

		if(!oParams)
			oParams = {};
		if(!oParams.hasOwnProperty('showSuccess'))
			oParams.showSuccess = true;
		if(!oParams.hasOwnProperty('showSuccessToast'))
			oParams.showSuccessToast = true;
		if(!oParams.hasOwnProperty('showWarning'))
			oParams.showWarning = true;
		if(!oParams.hasOwnProperty('confirmButtonWarning'))
			oParams.confirmButtonWarning = sap.m.MessageBox.Action.OK;
		if(!oParams.hasOwnProperty('cancelButtonWarning'))
			oParams.cancelButtonWarning = sap.m.MessageBox.Action.CANCEL;
		if(!oParams.hasOwnProperty('showError'))
			oParams.showError = true;
		
		const defaultProperties = ['code','message'];
		if(!oParams.hasOwnProperty('results') || typeof(oParams.results) !== 'object')
			oParams.results = defaultProperties;
		else
		{
			if(Array.isArray(oParams.results))
			{
				if(!oParams.results.includes('code'))
					oParams.results.push('code');
				if(!oParams.results.includes('message'))
					oParams.results.push('message');
			}
			else
			{
				if(!oParams.results.hasOwnProperty('returnProperties'))
					oParams.results.returnProperties = defaultProperties;
				else
				{
					if(!oParams.results.returnProperties.includes('code'))
						oParams.results.returnProperties.push('code');
					if(!oParams.results.returnProperties.includes('message'))
						oParams.results.returnProperties.push('message');
				}
			}
		}

		var oCheckResult = fnGetAjaxVal(sRequest,oParams.results,false);
		
		var sErrorMessage;
		if(oCheckResult) 
		{
			const iRetCode = parseInt(oCheckResult.code);
			if(isNaN(iRetCode))
			{
				sap.m.MessageBox.show(oLng_Opr.getText('General_RequestValErrorMsg'), {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: oLng_Opr.getText('General_WarningText'),
					actions: sap.m.MessageBox.Action.OK
				});
			}
			else if(iRetCode < 0)
			{
				if(oParams.showError)
				{
					sap.m.MessageBox.show(oCheckResult.message, {
						icon: sap.m.MessageBox.Icon.ERROR,
						title: oLng_Opr.getText('General_ErrorText'),
						actions: sap.m.MessageBox.Action.OK,
						onClose: function() {
							if(typeof(oParams.onError) === 'function')
								oParams.onError(oCheckResult,oParams.callbackParams);
						}
					});
				}
				else if(typeof(oParams.onError) === 'function')
					oParams.onError(oCheckResult,oParams.callbackParams);
			}
			else if(iRetCode > 0)
			{	
				var bShowMessage = false;
				if(typeof(oParams.showWarning) === 'boolean')
				{
					if(oParams.showWarning)
					{
						if(typeof(oParams.notShowWarning) !== 'object' || oParams.notShowWarning.constructor !== Array || 
							oParams.notShowWarning.indexOf(iRetCode) === -1)
							bShowMessage = true;
					}
				}
				else if(typeof(oParams.showWarning) === 'object' && oParams.showWarning.constructor === Array &&
					oParams.showWarning.indexOf(iRetCode) !== -1)
					bShowMessage = true;
					
				if(bShowMessage)
				{
					var aButtons = [oParams.confirmButtonWarning];
					if(!oParams.oneButtonWarning)
						aButtons.push(oParams.cancelButtonWarning);
					
					sap.m.MessageBox.show(oCheckResult.message, {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: oLng_Opr.getText('General_WarningText'),
						actions: aButtons,
						onClose: function(oAction) {
							if (oAction === oParams.confirmButtonWarning && typeof(oParams.onWarning) === 'function')
								oParams.onWarning(oCheckResult,oParams.callbackParams);
						}
					});
				}
				else if(typeof(oParams.onWarning) === 'function')
					oParams.onWarning(oCheckResult,oParams.callbackParams);
			}
			else if(iRetCode === 0)
			{
				if(oParams.showSuccess)
				{
					const sMessage = oCheckResult.message;
					if(oParams.showSuccessToast)
					{
						oAppController.showCustomToast({
							duration: 3000,
							type: 'Success',
							my: 'center center',
							at: 'center center',
							message: sMessage
						});
						
						if(typeof(oParams.onSuccess) === 'function')
							oParams.onSuccess(oCheckResult,oParams.callbackParams);
					}
					else
					{
						sap.m.MessageBox.show(sMessage, {
							icon: sap.m.MessageBox.Icon.SUCCESS,
							title: oLng_Opr.getText('General_SuccessText'),
							actions: sap.m.MessageBox.Action.OK,
							onClose: function() {
								if(typeof(oParams.onSuccess) === 'function')
									oParams.onSuccess(oCheckResult,oParams.callbackParams);
							}
						});
					}
				}
				else if(typeof(oParams.onSuccess) === 'function')
					oParams.onSuccess(oCheckResult,oParams.callbackParams);
			}
		}
		else
		{
			sap.m.MessageBox.show(oLng_Opr.getText('General_RequestErrorMsg'), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText('General_WarningText'),
				actions: sap.m.MessageBox.Action.OK
			});
		}
	},

/**
 * Selezione di una delle linee della postazione
 * @function selectFromDialog
 * @memberOf ui5app.app
 * @param {object} oParams - parametri
 * @param {function} oParams.callback - Funzione di callback da eseguire con primo parametro l'elemento selezionato
 * @param {object} oParams.callbackParams - Parametri aggiuntivi da passare alla funzione di callback
 * @param {string} oParams.title - Titolo della dialog
 * @param {string} oParams.noDataText - Testo in mancanza di elementi
 * @param {string} oParams.contentWidth - Larghezza in px del contenuto della dialog
 * @param {array} oParams.items - Array contenenti gli elementi della lista
 * @param {object} oParams.model - Model da utilizzare per il binding
 * @param {object} oParams.modelPath - Percorso del binding (default /Rowset/Row)
 * @param {string} oParams.modelTitle - Identificativo per il binding del title di ogni elemento (obbligatorio se si utilizza un model)
 * @param {string} oParams.modelDesc - Identificativo per il binding della descrizione di ogni elemento (opzionale)
 * @param {sap.m.ListItemBase} oParams.template - Classe erede di sap.m.ListItemBase (es. sap.m.CustomListItem), default sap.m.StandardListItem
 */
	selectFromDialog: function(oParams) {
		
		if(!oParams.hasOwnProperty('callback') || typeof(oParams.callback) !== 'function')
			oParams.callback = undefined;
		
		if(!oParams.hasOwnProperty('title'))
			oParams.title = oLng_Opr.getText("General_SelectDialogTitle");
		if(!oParams.hasOwnProperty('noDataText'))
			oParams.noDataText = oLng_Opr.getText("General_SelectDialogNoDataText");
		if(!oParams.hasOwnProperty('items') || !Array.isArray(oParams.items))
			oParams.items = [];
		
		var bBindingMode = 0; // 0 = nessun model, 1 = xml model, 2 = json model
		if(oParams.hasOwnProperty('model') && typeof(oParams.model) === 'object' &&
			typeof(oParams.model.getMetadata) === 'function' && oParams.hasOwnProperty('modelTitle'))
		{
			if(oParams.model.getMetadata().getName().indexOf('XMLModel') !== -1)
				bBindingMode = 1;
			else if(oParams.model.getMetadata().getName().indexOf('JSONModel') !== -1)
				bBindingMode = 2;
		}
		
		var oFilter = oParams.filter;
		
		var oSelectDialog = new sap.m.SelectDialog({
			title: oParams.title,
			noDataText: oParams.noDataText,
			contentWidth: oParams.contentWidth,
			search: function (oEvent) {
				var sValue = oEvent.getParameter("value");
				if(bBindingMode > 0)
				{
					var oTitleFilter = new sap.ui.model.Filter(oParams.modelTitle, sap.ui.model.FilterOperator.Contains, sValue);
					if(oFilter)
						oTitleFilter = new sap.ui.model.Filter([oFilter,oTitleFilter],true); // param.2 = logical and enabled
					var oBinding = oEvent.getSource().getBinding("items");
					oBinding.filter(oTitleFilter);
				}
			},
			confirm: function (oEvent) 
			{	
				var oSelectedItem = oEvent.getParameters().selectedItem;
				if(bBindingMode > 0)
				{
					var oObject = oSelectedItem.getBindingContext().getObject();
					if(bBindingMode === 1)
						oObject = getJsonObjectFromXMLObject(oObject);
					
					oParams.callback(oObject,oParams.callbackParams);
				}
				else
				{
					const sSelectedText = oSelectedItem.getTitle();
					if(oParams.callback)
						oParams.callback(sSelectedText,oParams.callbackParams);
				}
				
				oSelectDialog.destroy();
			},
			cancel: function () {
				oSelectDialog.destroy();
			}
		});
	
		if(bBindingMode > 0)
		{
			var sModelPath = "/";
			if(bBindingMode === 1)
				sModelPath = "/Rowset/Row";
		
			if(oParams.hasOwnProperty('modelPath') && typeof(oParams.modelPath) === 'string')
				sModelPath = oParams.modelPath;
				
			var oTemplate;
			if(oParams.hasOwnProperty('template') && oParams.template)
				oTemplate = oParams.template;
			else
			{
				var sDesc;
				if(oParams.hasOwnProperty('modelDesc'))
					sDesc = '{' + oParams.modelDesc + '}';
			
				oTemplate = new sap.m.StandardListItem({
					title: '{' + oParams.modelTitle + '}',
					description: sDesc
				});
			}
				
			oSelectDialog.bindAggregation('items',sModelPath,oTemplate);
			oSelectDialog.setModel(oParams.model);
			
			if(oFilter)
			{
				var oBinding = oSelectDialog.getBinding("items");
				oBinding.filter(oFilter);
			}
		}
		else
		{
			for(var i=0; i<oParams.items.length; i++)
			oSelectDialog.addItem(new sap.m.StandardListItem({
				title: oParams.items[i]
			}));
		}
		
		oSelectDialog.open();
	},
	
	
/**
 * Creazione sap.m.CustomListItem 
 * @function customListTemplate
 * @memberOf ui5app.app
 * @param {object} oParams - parametri
 * @param {string/object} oParams.titleText - stringa o oggetto binding per il titolo
 * @param {string/object} oParams.bottomLeftText - stringa o oggetto binding per il testo in basso a sinistra
 * @param {array} oParams.left - elementi da appendere nella colonna a sinistra
 * @param {string/object} oParams.topRightText - stringa o oggetto binding per il testo in alto a destra
 * @param {string/object} oParams.bottomRightText - stringa o oggetto binding per il testo in basso a destra
 * @param {array} oParams.right - elementi da appendere nella colonna a destra
 * @return {object} sap.m.CustomListItem creato
 */
	customListTemplate: function(oParams) {
		
		var oLeftLayout = new sap.ui.layout.VerticalLayout({
			width: "100%",
			content: [
				new sap.m.Text({
					text: oParams.titleText
				}).addStyleClass("standardText"),
				new sap.m.Label({
					text: oParams.bottomLeftText
				}).addStyleClass("smallLabel sapUiTinyMarginTop")
			],
			layoutData : new sap.ui.layout.GridData({
				span: "L6 M6 S6"
			})
		}).addStyleClass("sapUiTinyMarginTop");
		
		if(oParams.hasOwnProperty('left') && Array.isArray(oParams.left))
		{
			for(var i=0; i<oParams.left.length; i++)
				oLeftLayout.addContent(oParams.left[i]);
		}
		
		var oRightLayout = new sap.ui.layout.VerticalLayout({
			width: "100%",
			content: [
				new sap.m.Label({
					text: oParams.topRightText,
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right
				}).addStyleClass("smallLabel"),
				new sap.m.Label({
					text: oParams.bottomRightText,
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right
				}).addStyleClass("smallLabel sapUiTinyMarginTop"),
			],
			layoutData : new sap.ui.layout.GridData({
				span: "L6 M6 S6"
			})
		}).addStyleClass("sapUiTinyMarginTop");
		
		if(oParams.hasOwnProperty('right') && Array.isArray(oParams.right))
		{
			for(var i=0; i<oParams.right.length; i++)
				oRightLayout.addContent(oParams.right[i]);
		}
		
		var oCustomListItem = new sap.m.CustomListItem({
			content: [
				new sap.ui.layout.Grid({
					hSpacing: 0,
					vSpacing: 0,
					content: [
						oLeftLayout,
						oRightLayout,
					]
				}).addStyleClass("sapUiSmallMarginBeginEnd sapUiTinyMarginTopBottom")
			]
		});
		
		return oCustomListItem;
	},

/**
 * Apre una dialog che visualizza informazioni
 * @function onShowInfo
 * @memberOf ui5app.app
 * @param {string} this.viewName - Nome della view da cui recuperare il model
 * @param {string} this.title - Titolo della dialog
 * @param {string/array} this.description - Binding della dialog o array contenente i vari testi / binding da recuperare
 */
	onShowInfo: function() {
		if(this.view && this.title && this.description)
		{
			var sTitle;
			var sDescription;
			var oModel = this.view.getModel();
			
			var sTitle = oModel.getProperty(this.title);
			var sDescription = '';
			if(typeof(this.description) === 'string')
				sDescription = oModel.getProperty(this.description);
			else if(Array.isArray(this.description))
			{
				for(var i=0; i<this.description.length; i++)
				{
					var sCurrent = this.description[i];
					var aResult = sCurrent.match(/\{([^)]+)\}/); // testo tra {}
					if(aResult)
					{
						sCurrent = aResult[1];
						sDescription += oModel.getProperty(sCurrent);
					}
					else
						sDescription += sCurrent;
				}
			}
			
			sap.m.MessageBox.show(sDescription, {
				icon: sap.m.MessageBox.Icon.INFORMATION,
				title: sTitle,
				actions: sap.m.MessageBox.Action.OK
			});
		}
	},

/**
 * Apre una dialog che visualizza le informazioni della fase
 * @function onShowPoperInfo
 * @memberOf ui5app.app
 * @param {oggetto} oView - View da cui recuperare il model
 * @return {array} parametri da passare al gestore dell'evento di apertura
 */
	
	onShowPoperInfo: function(oView) {
		return [oAppController.onShowInfo, {
			view: oView,
			title: '/CICTXT',
			description: [
				oLng_Opr.getText('General_Poper') + ': ',
				'{/POPER}',
				'\n',
				'{/CICTXT}',
				'\n\n',
				oLng_Opr.getText('General_CDL') + ': ',
				'{/CDLID}',
				'\n',
				'{/CDLDESC}'
			]
		}]
	},

	onShowLineInfo: function(oView) {
		return [oAppController.onShowInfo, {
			view: oView,
			title: '/LINEID',
			description: [
				oLng_Opr.getText('BoxDeclInput_Line') + ': ',
				'{/LINEID}',
				'\n',
				'{/LINETXT}'				
			]
		}]
	}	
});

/***********************************************************************************************/
// Crea il controllo di inserimento numerico con bottoni di incremento e decremento
/***********************************************************************************************/

function NumericInput(oParams) {
	
	var oInput = new sap.m.Input({
		id: oParams.id,
		type: sap.m.InputType.Number,
		textAlign: sap.ui.core.TextAlign.Right,
		width: oParams.width
	});
	
	var oDecrButton = new sap.m.Button({
		id: oInput.getId() + 'Dec',
		icon: "sap-icon://less",
		press: function() {
			var iValue = parseInt(oInput.getValue());
			if(isNaN(iValue))
				iValue = 0;
			else if(!oParams.hasOwnProperty('min') || iValue >= oParams.min+1)
				iValue -= 1;
			oInput.setValue(iValue);
		}
	});
	
	var oIncButton = new sap.m.Button({
		id: oInput.getId() + 'Inc',
		icon: "sap-icon://add",
		press: function() {
			var iValue = parseInt(oInput.getValue());
			if(isNaN(iValue))
				iValue = 1;
			else if(!oParams.hasOwnProperty('max') || iValue <= oParams.max-1)
				iValue += 1;
			oInput.setValue(iValue);
		}
	})
	
	var oControl =  new sap.m.HBox({
		id: oInput.getId() + 'Box',
		visible: oParams.visible,
		items: [
			oDecrButton,
			oInput,
			oIncButton
		],
		layoutData: oParams.layoutData
	});
	
	return oControl;
}

/***********************************************************************************************/
// Converte un oggetto XML in un oggetto JSON
/***********************************************************************************************/

function getJsonObjectFromXMLObject(oXMLObject) {
	var oJsonObject;
	if(oXMLObject)
	{
		oJsonObject = {};
		var aChildNodes = oXMLObject.childNodes
		for(var j=0; j<aChildNodes.length; j++)
			oJsonObject[aChildNodes[j].nodeName] = aChildNodes[j].textContent;
	}
	return oJsonObject;
}

/***********************************************************************************************/
// Estrae tutti i dati della Row con valore sValue della proprietà sProperty da un XMLModel
/***********************************************************************************************/

function getJsonObjectFromXMLModel(oModel,sProperty,sValue) {
	var oJsonObject;
	if(oModel)
	{
		var sRowsetPath = "/Rowset/0";
		var oRowsetObject = oModel.getObject(sRowsetPath);
		if(oRowsetObject)
		{
			var aChildNodes = oRowsetObject.childNodes;
			var iRowCount = aChildNodes.length-1; // -1 perchè c'e Columns
			for(var i=0; i<iRowCount; i++)
			{
				var sRowPath = sRowsetPath + "/Row/" + i;
				if(oModel.getProperty(sRowPath + "/" + sProperty) === sValue)
				{
					oJsonObject = {};
					var oRowObject = oModel.getObject(sRowPath);
					var aChildProperties = oRowObject.childNodes
					for(var j=0; j<aChildProperties.length; j++)
						oJsonObject[aChildProperties[j].nodeName] = aChildProperties[j].textContent;
					break;
				}
			}
		}
	}
	return oJsonObject;
}

/***********************************************************************************************/
// Estrae il valore della propietà sRetProperty dell'XMLModel dalla Row dove sProperty ha valore sValue
/***********************************************************************************************/

function getValueFromXMLModel(oModel,sProperty,sValue,sRetProperty) {
	var sRetValue;
	if(oModel)
	{
		var sRowsetPath = "/Rowset/0";
		var oRowsetObject = oModel.getObject(sRowsetPath);
		if(oRowsetObject)
		{
			var aChildNodes = oRowsetObject.childNodes;
			var iRowCount = aChildNodes.length-1; // -1 perchè c'e Columns
			for(var i=0; i<iRowCount; i++)
			{
				const sRowPath = sRowsetPath + "/Row/" + i;
				if(oModel.getProperty(sRowPath + "/" + sProperty) === sValue)
				{
					sRetValue = oModel.getProperty(sRowPath + "/" + sRetProperty);
					break;
				}
			}
		}
		return sRetValue;
	}
}

function getValueFromXMLModelByRowset(oModel, rowset, sProperty, sValue, sRetProperty) {
	var sRetValue;
	if(oModel)
	{
		var sRowsetPath = "/Rowset/"+rowset;
		var oRowsetObject = oModel.getObject(sRowsetPath);
		if(oRowsetObject)
		{
			var aChildNodes = oRowsetObject.childNodes;
			var iRowCount = aChildNodes.length-1; // -1 perchè c'e Columns
			for(var i=0; i<iRowCount; i++)
			{
				const sRowPath = sRowsetPath + "/Row/" + i;
				if(oModel.getProperty(sRowPath + "/" + sProperty) === sValue)
				{
					sRetValue = oModel.getProperty(sRowPath + "/" + sRetProperty);
					break;
				}
			}
		}
		return sRetValue;
	}
}

/***********************************************************************************************/
// Restituisce una data nel formato 'yy-MM-dd hh:mm:ss'
/***********************************************************************************************/

function selectSingleObject (oParams) {	
	if(!oParams.hasOwnProperty('objects') || !Array.isArray(oParams.objects))
		oParams.objects = [];
		
	var oRetObject;
	for(var i=0; i<oParams.objects.length;i++)
	{
		const oCurrObject = oParams.objects[i];
		if(oCurrObject[oParams.propertyName] === oParams.propertyValue)
		{
			oRetObject = oCurrObject;
			break;
		}
	}

	return oRetObject;
};

/***********************************************************************************************/
// Restituisce una data nel formato 'yy-MM-dd hh:mm:ss'
/***********************************************************************************************/

function getDatetimeString(oDate, oParams) {
	
	if(!oParams)
		oParams = {}
	if(!oParams.hasOwnProperty('middleEndianFormat'))
		oParams.middleEndianFormat = false;
	if(!oParams.hasOwnProperty('dateSeparator'))
	{
		if(oParams.middleEndianFormat)
			oParams.dateSeparator = '/';
		else
			oParams.dateSeparator = '-';
	}
	
	var sDate = '';
	if(oDate && typeof(oDate.getFullYear) === 'function')
	{		
		var month = oDate.getMonth() + 1;
		if(month<10)
			month = '0' + month;
		
		var day = oDate.getDate();
		if(day<10)
			day = '0' + day;
		
		var aDate;
		if(oParams.middleEndianFormat)
			aDate = [day,month,oDate.getFullYear()];
		else
			aDate = [oDate.getFullYear(),month,day];
		
		sDate = aDate.join(oParams.dateSeparator);
		
		var hours = oDate.getHours();
		if(hours<10)
			hours = '0' + hours;
		sDate += " " + hours;
		
		var minutes = oDate.getMinutes();
		if(minutes<10)
			minutes = '0' + minutes;
		sDate += ":" + minutes;
		
		var seconds = oDate.getSeconds();
		if(seconds<10)
			seconds = '0' + seconds;
		sDate += ":" + seconds;
	}
	return sDate;
};

/***********************************************************************************************/
// Formatter per la visualizzazione di data e ora
/***********************************************************************************************/

function dateFormatter(sDate) {
	if(Date.parse(sDate))
		return getDatetimeString(new Date(sDate),{
			middleEndianFormat: true
		});
	else
		return '-';
};

/***********************************************************************************************/
// Formatter per la visualizzazione di id e descrizione
/***********************************************************************************************/

function idTextFormatter(sId,sTxt) {
	return sId + " " + sTxt;
};

/***********************************************************************************************/
// Sostituisce tutte le occorrenze in una stringa
/***********************************************************************************************/

String.prototype.replaceAll = function(str1, str2, ignore) {
	return this.replace(
		new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),
			(ignore?"gi":"g")
		),
		(typeof(str2)=="string") ? str2.replace(/\$/g,"$$$$") : str2
	);
};

/***********************************************************************************************/
// Restituisce incrementato un numero nel formato strigna
/***********************************************************************************************/

function incNumString(num){

if (Math.abs(parseInt(num,10)) == 0){return '1'};

	var numArray = num.split(''),
	answer   = [];
	var sign = 1;
	if (numArray[0] == '+' || numArray[0] == '-'){
	if (numArray[0] == '-'){sign = -1};
		numArray.splice(0,1);
	}
	var idx = numArray.length;
	var next_int = 0;
	var carry = 1; /* first add 1 */
	while(idx-- && carry != 0){
		next_int = parseInt(numArray[idx],10) + (sign * carry);
		if (sign > 0){
			answer[idx] = (next_int >= 10 ? next_int - 10 : next_int)
			carry = (next_int >= 10 ? 1 : 0)
		} else {
			answer[idx] = (next_int == -1 ? 9 : Math.abs(next_int));
			carry = (next_int == -1 ? 1 : 0)
		}
		if (carry == 0){
			while(idx--){
				answer[idx] = numArray[idx]
			}

		}

	}

	if (sign > 0){
		return (carry == 0 ? answer.join('') : carry + answer.join(''));
	} else {
		return ('-' + answer.join(''));
	}
};