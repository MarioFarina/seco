// Controller Chiusura UdC / Scarti / Lettura
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.CLOSEUDC.closeUdc", {
	
	_oArguments: undefined,
	_sViewType: '', // 'C' Chiusura , 'S' Scarti, 'R' Lettura UDC
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("closeUdc").attachPatternMatched(this.onCloseMatched, this);
		oAppRouter.getRoute("scrapUdc").attachPatternMatched(this.onScrapMatched, this);
		oAppRouter.getRoute("readUdc").attachPatternMatched(this.onReadMatched, this);

		var nextFunc = this.onNext;
		var oUdcInput = sap.ui.getCore().byId("closeUdcInput");
		
		oUdcInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onCloseMatched: function (oEvent) {
		this._sViewType = 'C';
		
		this.onObjectMatched(oEvent);
	},
	
	onScrapMatched: function (oEvent) {
		this._sViewType = 'S';
		
		this.onObjectMatched(oEvent);
	},
	
	onReadMatched: function (oEvent) {
		this._sViewType = 'R';
		
		this.onObjectMatched(oEvent);
	},

	onObjectMatched: function (oEvent) {
		
		const sViewType = this._sViewType;
		
		var oView = this.getView();
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData({VIEWTYPE: sViewType});
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']); 
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('closeUdcPage'),sPlant,sPernr);

		var oConfirmButton = $.UIbyID("closeUdcConfirmButton");
		if(sViewType === 'S')
		{
			oConfirmButton.removeStyleClass('greenButton');
			oConfirmButton.addStyleClass('redButton');
		}
		else // C e R
		{
			oConfirmButton.removeStyleClass('redButton');
			oConfirmButton.addStyleClass('greenButton');
		}
		
		var oUdcInput = sap.ui.getCore().byId("closeUdcInput");
		oUdcInput.setValueState();
		oUdcInput.setValueStateText();
		oUdcInput.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			oUdcInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('closeUdcView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
	onNext: function() {
		
		var oUdcInput = sap.ui.getCore().byId("closeUdcInput");
		
		oUdcInput.setValueState();
		oUdcInput.setValueStateText();
		
		const sUdcNr = oUdcInput.getValue();
		
		var sErrorMessage;
		if(!sUdcNr)
		{
			sErrorMessage = oLng_Opr.getText("CloseUdc_UdcErrorMessage");
			oUdcInput.setValueState(sap.ui.core.ValueState.Warning);
			oUdcInput.setValueStateText(sErrorMessage);
		}

		if(sErrorMessage) {
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{	
			var oController = sap.ui.getCore().byId('closeUdcView').getController();
			
			var sState = '';
			var sOrigin = '';
			
			switch(oController._sViewType)
			{
				case 'C':
					sState = 'O';
					break;
				case 'S':
					sState = 'C';
					sOrigin = 'C,S';
					break;
				/*case 'R':
					break;
				*/
			};
			
			var sCheckQuery = sMovementPath + "UDC/checkUDCStateAndOriginQR";
			sCheckQuery += "&Param.1=" + sUdcNr;
			sCheckQuery += "&Param.2=" + sState;
			sCheckQuery += "&Param.3=" + sOrigin;
			sCheckQuery += "&Param.4=1";
			sCheckQuery += "&Param.5=1";
			sCheckQuery += "&Param.6=1"
			sCheckQuery += "&Param.7=1";
			sCheckQuery += "&Param.8=" + sLanguage;
			
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oController.onCheckSuccess,
				results: {
					returnXMLDoc: true
				}
			});
		}
	},
	
	onCheckSuccess: function(oResults,oCallbackParams) {
		
		var xmlData = oResults.data.childNodes[0]; // Rowsets
		xmlData = xmlData.childNodes[1]; // Rowset 1
		xmlData = xmlData.childNodes[1]; // Row 1 (Columns 0)
		
		var oUdcObject = getJsonObjectFromXMLObject(xmlData);

		var oController = sap.ui.getCore().byId('closeUdcView').getController();
		const sViewType = oController._sViewType;
		
		var sNavName;
		switch(sViewType)
		{
			case 'C':
				sNavName = 'closeUdcConfirm';
				break;
			case 'S':
				sNavName = 'scrapUdcInput';
				break;
			case 'R':
				sNavName = 'readUdcInput';
				break;
		};
		
		oUdcObject.VIEWTYPE = sViewType;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);
		
		var oArguments = oController._oArguments;
		oArguments['udcnr'] = oUdcObject.UDCNR;

		oAppRouter.navTo(sNavName,{
			query: oArguments
		});
	}

});