// Controller Check commessa/fase
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.SERIALNOCONN.checkOrderPoper", {
	
	_oArguments: undefined,
	_sViewType: '', // 'N' = Seriali macchina non collegata, 'SG' Dichiarazione di scarto da buoni
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute('serialNoConn').attachPatternMatched(this.onSerialMatched, this);
		oAppRouter.getRoute('scrapGoods').attachPatternMatched(this.onScrapGoodsMatched, this);
		
		var oOrderInput = $.UIbyID('checkOrderPoperInput');
		
		var nextFunc = this.onNext;
		oOrderInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onSerialMatched: function (oEvent) {
		this._sViewType = 'N';
		
		this.onObjectMatched(oEvent);
	},
	
	onScrapGoodsMatched: function (oEvent) {
		this._sViewType = 'SG';
		
		this.onObjectMatched(oEvent);
	},
	
	onObjectMatched: function (oEvent) {
		
		const sViewType = this._sViewType;
		
		var oView = this.getView();
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData({VIEWTYPE: sViewType});
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		
		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('checkOrderPoperPage'),sPlant,sPernr);

		var oOrderInput = sap.ui.getCore().byId('checkOrderPoperInput');
		oOrderInput.setValue();
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();

		var oConfirmButton = $.UIbyID('checkOrderPoperConfirmButton');
		oConfirmButton.removeStyleClass('greenButton');
		oConfirmButton.removeStyleClass('redButton');
		var sStyleClass;
		switch(sViewType)
		{
			case 'N':
				sStyleClass = 'greenButton';
				break;
			case 'SG':
				sStyleClass = 'redButton';
				break;
		}
		oConfirmButton.addStyleClass(sStyleClass);
		
		jQuery.sap.delayedCall(500, null, function() {
			oOrderInput.focus();
		});
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('checkOrderPoperView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},

/***********************************************************************************************/
// Avanzamento alla pagina successiva
/***********************************************************************************************/

	onNext: function() {

		var oOrderInput = $.UIbyID('checkOrderPoperInput');
		const sOrderPhase = oOrderInput.getValue();
		
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();
		
		const sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage) {
			oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderInput.setValueStateText(sErrorMessage);
			
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else // Controlla commessa e fase, selezione linea e udc nuovo o incompleto
		{			
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			var aLinesObjects = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
			{
				aLinesObjects = oSettings.LINES;
				for(var i=0; i<aLinesObjects.length; i++)
					aLines.push(aLinesObjects[i].lineid);
			}
			
			const aSpecialWarningCodes = [2,3];
			
			var oController = sap.ui.getCore().byId('checkOrderPoperView').getController();
			var oArguments = oController._oArguments;
			
			var sCheckQuery = sMovementPath + "ProdDecl/checkOrderAndPoperQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + sOrderPhase;
			sCheckQuery += "&Param.3=" + aLines.join();
			sCheckQuery += "&Param.6=" + sLanguage;
			
			var navFunction = function(oLineInfo,oCallbackParams) // presenza udc incompleti
			{
				var oFuncData = {
					ORDER: oCallbackParams.order,
					POPER: oCallbackParams.poper,
					CICTXT: oCallbackParams.returnedData.cicTxt,
					LINEID: oLineInfo.lineid,
					LINETXT: oLineInfo.linetxt,
					MATERIAL: oCallbackParams.returnedData.material,
					MAT_DESC: oCallbackParams.returnedData.matDesc,
					VIEWTYPE: oCallbackParams.viewType
				};
		
				var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
				oSessionStorage.put('FUNCDATA',oFuncData);
		
				oArguments['order'] = oCallbackParams.order;
				oArguments['poper'] = oCallbackParams.poper;
				oArguments['idline'] = oLineInfo.lineid;
				
				var sNavName;
				switch(oCallbackParams.viewType) {
					case 'N':
						sNavName = 'serialCheck';
						break;
					case 'SG':
						sNavName = 'scrapGoodsInput';
						break;
				}

				oAppRouter.navTo(sNavName,{
					query: oArguments
				});
			}
			
			var oBoxDeclController = sap.ui.controller("ui5app.BOXDECL.boxDecl");
	
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oBoxDeclController.onLineSuccess,
				showWarning: aSpecialWarningCodes, 						// array codici di errore per cui visualizza il messaggio
				onWarning: oBoxDeclController.onLineWarning,
				callbackParams: {
					warningCodes: aSpecialWarningCodes,					// come showWarning per recuperarlo in onWarning
					linesObjects: aLinesObjects,						// linee di postazione con linetxt corrispondente (match in onSuccess e onWarning)
					oneLineCallback: oBoxDeclController.onLineSuccess,	// callback per onWarning quando c'e una sola linea
					lineCallback: navFunction,							// callback a seguito della selezione della linea
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					plant: oArguments.plant,							// serve per la selezione delle linee in onLineSuccess/onLineWarning
					viewType: oController._sViewType
				},
				results: {
					returnProperties: ['outputLines','cicTxt','material','matDesc'],
					returnXMLDoc: true
				}
			});
		}
	}
	
});
