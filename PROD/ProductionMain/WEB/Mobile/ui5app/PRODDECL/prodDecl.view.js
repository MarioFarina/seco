// View Dichiarazioni di Produzione
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PRODDECL.prodDecl", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PRODDECL.prodDecl";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oForm = new sap.ui.layout.form.Form({
			editable: true,
			layout: new sap.ui.layout.form.GridLayout(),
			formContainers: [
				new sap.ui.layout.form.FormContainer({
					formElements: [
					    new sap.ui.layout.form.FormElement({
					    	label: new sap.m.Label({
					    		text: oLng_Opr.getText("ProdDecl_Line")
				            }).addStyleClass("sapUiMediumMarginTop"),
					   	  	fields: [
								new sap.m.ComboBox({
									id: 'prodDeclLineCombo',
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "6"
									}),
								}).addStyleClass("sapUiMediumMarginTop")
					   	  	]
					    }),
						new sap.ui.layout.form.FormElement({
					    	label: new sap.m.Label({
					    		text: oLng_Opr.getText("ProdDecl_Order")
				            }),
					   	  	fields: [
								new sap.m.ComboBox({
									id: 'prodDeclOrderCombo',
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "6"
									}),
									selectionChange: function(oControlEvent) {
										var oSelectedItem = oControlEvent.getSource().getSelectedItem();
										var iPZUDC = oSelectedItem.getBindingContext().getProperty("PZUDC");
										$.UIbyID('prodDeclQuantity').setValue(iPZUDC);
									}
								})
					   	  	]
					    }),
						new sap.ui.layout.form.FormElement({
					    	label: new sap.m.Label({
					    		text: oLng_Opr.getText("ProdDecl_Phase")
				            }),
					   	  	fields: [
								new sap.m.Input({
									id: 'prodDeclPhaseInput',
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "6"
									}),
									maxLength: 4
								})
					   	  	]
					    }),
						new sap.ui.layout.form.FormElement({
					    	label: new sap.m.Label({
					    		text: oLng_Opr.getText("ProdDecl_Quantity")
				            }),
					   	  	fields: [
								new sap.m.HBox({
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "3"
									}),
									items: [
										new sap.m.Button({
											icon: "sap-icon://less",
											press:  [oAppController.onChangeQuantity,{
												add:false,
												id: 'prodDeclQuantity'
											}]
										}),
										new sap.m.Input({
											id: "prodDeclQuantity",
											type: sap.m.InputType.Number,
											textAlign: sap.ui.core.TextAlign.Right,
										}),
										new sap.m.Button({
											icon: "sap-icon://add",
											press: [oAppController.onChangeQuantity,{
												add:true,
												id: 'prodDeclQuantity'
											}]
										})
									]
								}).addStyleClass("sapUiSmallMarginBottom")
					   	  	]
					    }),
					]
				})
			]
        });
						  
		var oPage = new sap.m.Page({
			id: "prodDeclPage",
			enableScrolling: false,
			title: oLng_Opr.getText("ProdDecl_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: [oController.onNavBack,{confirm:false}],
			content: [
				oForm,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("ProdDecl_Good"),
							icon: "sap-icon://message-success",
							press: [oController.onProcess,{isGood:true}]
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton"),
						new sap.m.Button({
							text: oLng_Opr.getText("ProdDecl_Suspended"),
							icon: "sap-icon://message-error",
							press: [oController.onProcess,{isGood:false}]
						}).addStyleClass("sapUiSmallMarginBegin yellowButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar({
				active: true,
				press: oController.onShowDeclDialog,
				content: [
					new sap.m.Label({
						design: sap.m.LabelDesign.Bold,
						text: oLng_Opr.getText("ProdDecl_GoodCount") + ":"
					}).addStyleClass("sapUiSmallMarginBegin greenLabel mediumLabelSize"),
					new sap.m.Label({
						id: "goodCountLabel",
						design: sap.m.LabelDesign.Bold,
						text: "0"
					}).addStyleClass("sapUiTinyMarginBegin greenLabel mediumLabelSize"),
					new sap.m.Label({
						design: sap.m.LabelDesign.Bold,
						text: oLng_Opr.getText("ProdDecl_SuspCount") + ":"
					}).addStyleClass("sapUiSmallMarginBegin yellowLabel mediumLabelSize"),
					new sap.m.Label({
						id: "suspCountLabel",
						design: sap.m.LabelDesign.Bold,
						text: "0"
					}).addStyleClass("sapUiTinyMarginBegin sapUiSmallMarginEnd yellowLabel mediumLabelSize"),
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						text: oLng_Opr.getText("ProdDecl_Confirm"),
						icon: "sap-icon://accept",
						press: [oController.onNavBack,{confirm:true}]
					}).addStyleClass("sapUiSmallMarginBeginEnd"),
				]
			})
		});
		
	  	return oPage;
	}

});