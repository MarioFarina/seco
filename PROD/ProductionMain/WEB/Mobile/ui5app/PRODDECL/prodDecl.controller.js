// Controller Dichiarazioni di Produzione
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.PRODDECL.prodDecl", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("prodDecl").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		var oView = this.getView();
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		
		oAppController.loadSubHeaderInfo($.UIbyID('prodDeclPage'),sPlant,sPernr);

		// Combo per la selezione della linea						
		var oLineComboTemplate = new sap.ui.core.Item({
            text: "{LINETXT}",
            key: "{IDLINE}"
        }); 
		  
		var oLineCombo = $.UIbyID('prodDeclLineCombo');
		oLineCombo.bindAggregation("items", "/Rowset/Row", oLineComboTemplate);
		
		var oLinesModel = new sap.ui.model.xml.XMLModel();
		oLinesModel.loadData(QService + sMasterDataPath + "Lines/getLinesbyPlantSQ&Param.1=" + sPlant,false,false);
		oLineCombo.setModel(oLinesModel);
		
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oSettings = oStorage.get("WORKST_INFO");
		var sSelectedLine;
		if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
		{
			if(oSettings.LINES.length === 1)
			{
				oLineCombo.setEnabled(false);
				sSelectedLine = oSettings.LINES[0];
			}
			else if(oSettings.LINES.length > 1)
			{
				oLineCombo.setEnabled(true);
				var aAllFilters = [];
				for(var i=0; i<oSettings.LINES.length; i++)
				{
					var oFilter = new sap.ui.model.Filter("IDLINE", sap.ui.model.FilterOperator.EQ, oSettings.LINES[i]);
					aAllFilters.push(oFilter);
				}
				var oAllFilters = new sap.ui.model.Filter(aAllFilters); 
				var oBinding = oLineCombo.getBinding("items");
				oBinding.filter(oAllFilters);
			}
		}
		else
		{
			oLineCombo.setEnabled(true);
			oLineCombo.setValue();
		}
		oLineCombo.setSelectedKey(sSelectedLine);
		
		//Combo per la selezione della commessa
		var oOrderComboTemplate = new sap.ui.core.Item({
            text: "{ORDER}",
            key: "{ORDER}"
        });
		
		var oOrderCombo = $.UIbyID('prodDeclOrderCombo');
		oOrderCombo.bindAggregation("items", "/Rowset/Row", oOrderComboTemplate);
		
		var oOrderModel = new sap.ui.model.xml.XMLModel();
		oOrderModel.loadData(QService + sMasterDataPath + "Orders/getOrdersSQ",false,false);
		oOrderCombo.setModel(oOrderModel);
		oOrderCombo.setValue();
		oOrderCombo.setSelectedKey();
		
		$.UIbyID('prodDeclPhaseInput').setValue();
		$.UIbyID('prodDeclQuantity').setValue(0);
		
		// Imposta il model iniziale vuoto
		var oViewModel = new sap.ui.model.json.JSONModel();
		oViewModel.setData({data:[]});
		oView.setModel(oViewModel);
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {	
		var oView = sap.ui.getCore().byId('prodDeclView');
		var oViewModel = oView.getModel();
		oViewModel.setData({data:[]});
		
		$.UIbyID('goodCountLabel').setText("0");
		$.UIbyID('suspCountLabel').setText("0");
		
		if(this.confirm)
		{
			sap.m.MessageToast.show(oLng_Opr.getText("ProdDecl_Completed"), {
				duration: 6000,
				animationDuration: 3000,
				closeOnBrowserNavigation: false
			});
		}
		
		var oController = oView.getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Modifica della quantità con i bottoni
/***********************************************************************************************/
	
	onProcess: function() {
		var sLine = $.UIbyID('prodDeclLineCombo').getSelectedKey();
		var sOrder = $.UIbyID('prodDeclOrderCombo').getSelectedKey();
		var sPhase = $.UIbyID('prodDeclPhaseInput').getValue();
		var sQuantity = $.UIbyID('prodDeclQuantity').getValue();
		
		const bIsGood = this.isGood;
		
		if(!sLine)
		{
			sap.m.MessageBox.show(oLng_Opr.getText("ProdDecl_LineErrorMsg"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Attenzione",
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else if(!sOrder)
		{
			sap.m.MessageBox.show(oLng_Opr.getText("ProdDecl_OrderErrorMsg"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Attenzione",
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else if(!sPhase)
		{
			sap.m.MessageBox.show(oLng_Opr.getText("ProdDecl_PhaseErrorMsg"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Attenzione",
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else if(parseFloat(sQuantity) <= 0)
		{
			sap.m.MessageBox.show(oLng_Opr.getText("ProdDecl_QtyErrorMsg"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Attenzione",
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oController = sap.ui.getCore().byId('prodDeclView').getController();
			
			var sCausal = "";
			
			var fInsertFunction = function() {

				var sQuery = sConfirmPath + "ConfProd/addConfProdSQ";
				sQuery += "&Param.1=" + sLine;
				sQuery += "&Param.2=" + oAppController._sDate;
				sQuery += "&Param.3=" + oAppController._sShift;
				sQuery += "&Param.4=" + oController._oArguments.pernr;
				sQuery += "&Param.5=" + sOrder;
				sQuery += "&Param.6=" + sPhase;
				sQuery += "&Param.7=" + (bIsGood?'G':'S');
				sQuery += "&Param.8=" + sCausal; // Causale di scarto
				sQuery += "&Param.9=" + ""; // Identificativo UDC
				sQuery += "&Param.10=" + sQuantity;
				sQuery += "&Param.11=" + "0"; // Quantità proposta
				sQuery += "&Param.12=" + oController._oArguments.plant;
				sQuery += "&Param.13=" + "0"; // Confermato in sap	
				sQuery += "&Param.14=" + "0"; // Conferma stornata ed eliminata	
				sQuery += "&Param.15=" + "it-link"; // Utente MII che ha effettuato la modifica	
				sQuery += "&Param.16=" + ""; // Tipo di modifica
				
				// aggiungere param mancanti
				var bSuccess = fnExeQuerym(sQuery);
				if(bSuccess)
					oController.updateViewModel(bIsGood,sLine,sOrder,sQuantity);
				
				sap.ui.core.BusyIndicator.hide();
			};
			
			if(!bIsGood)
			{
				const sDialogId = 'causalDialog';
				var oCausalDialog = sap.ui.getCore().byId(sDialogId);
				if(!oCausalDialog) 
				{
					var oCausalInput = new sap.m.Input({
						maxLength: 10
					});
					
					var oOkButton = new sap.m.Button({
						text:  oLng_Opr.getText("ProdDecl_OkDlgButton"),
						icon: "sap-icon://accept",
						press: function () {
							var sCausalValue = oCausalInput.getValue();
							if(!sCausalValue)
							{
								oCausalInput.setValueState(sap.ui.core.ValueState.Warning);
								oCausalInput.setValueStateText(oLng_Opr.getText("ProdDecl_CausalErrorMsg"));
							}
							else
							{
								sap.ui.core.BusyIndicator.show(1000);
								var sQuery = sScrabReasonsPath +  "checkScrabReasonSQ";
								sQuery += "&Param.1=" + oController._oArguments.plant;
								sQuery += "&Param.2=" + sCausalValue;
								var iCheckResult = parseInt(fnGetAjaxVal(sQuery,["Answer"],false));
								if(iCheckResult)
								{
									sCausal = sCausalValue;
									oCausalDialog.close();
									fInsertFunction();
								}
								else
								{
									sap.ui.core.BusyIndicator.hide();
									oCausalInput.setValueState(sap.ui.core.ValueState.Warning);
									oCausalInput.setValueStateText(oLng_Opr.getText("ProdDecl_CausalErrorMsg"));
								}
							}
						}
					});
					
					oCausalInput.onsapenter = function (oEvent)
					{
						oOkButton.firePress();
					}
					
					oCausalDialog = new sap.m.Dialog({
						id: sDialogId,
						title: oLng_Opr.getText("ProdDecl_CausalTitle"),
						content: [
							new sap.m.VBox({
								items : [
									new sap.m.Label({
										text: oLng_Opr.getText("ProdDecl_Causal"),
									}),
									oCausalInput
								]
							})
						],
						beginButton: oOkButton,
						endButton: new sap.m.Button({
							text:  oLng_Opr.getText("ProdDecl_CancelDlgButton"),
							icon: "sap-icon://decline",
							press: function () {
								oCausalDialog.close();
							}
						}),
						beforeOpen: function() {
							oCausalInput.setValue();
							oCausalInput.setValueState(sap.ui.core.ValueState.None);
							oCausalInput.setValueStateText();
						},
						afterOpen: function() {
							oCausalInput.focus();
						}
					});
				}
				
				oCausalDialog.open();
			}
			else
			{
				sap.ui.core.BusyIndicator.show(1000);
				fInsertFunction();
			}
		}
	},
	
	updateViewModel: function(bIsGood,sLine,sOrder,sQuantity) {
		var oView = sap.ui.getCore().byId('prodDeclView');
		var oViewModel = oView.getModel();
		var oJsonData = oViewModel.getData();
		
		var oCountLabel;
		if(bIsGood)
			oCountLabel = $.UIbyID('goodCountLabel');
		else
			oCountLabel = $.UIbyID('suspCountLabel');
		
		var iCount = parseInt(oCountLabel.getText());
		iCount += parseInt(sQuantity);
		oCountLabel.setText(iCount);
		
		oArrayData = oJsonData.data;
		
		oArrayData.push({
			isgood: bIsGood,
			line: sLine,
			order: sOrder,
			quantity: sQuantity
		})
		
		oJsonData.data = oArrayData;
		oViewModel.setData(oJsonData);
		//console.log(JSON.stringify(oJsonData));
		
		sap.ui.getCore().byId('prodDeclQuantity').setValue(0);
	},
	
	onShowDeclDialog: function() {
		var oView = sap.ui.getCore().byId('prodDeclView');
		const sDialogId = 'prodDeclDialog';
		var oSelectDialog = $.UIbyID(sDialogId);
		if(!oSelectDialog) {
			oSelectDialog = new sap.m.TableSelectDialog({
				id: sDialogId,
				title: oLng_Opr.getText("ProdDecl_Declarations"),
				noDataText: oLng_Opr.getText("ProdDecl_NoDecl"),
				columns: [
					new sap.m.Column({
						header: new sap.m.Text({}),
						width: '24px'
					}),
					new sap.m.Column({
						header: new sap.m.Text({text: oLng_Opr.getText("ProdDecl_Order")})
					}),
					new sap.m.Column({
						hAlign: sap.ui.core.TextAlign.Right,
						header: new sap.m.Text({text: oLng_Opr.getText("ProdDecl_Quantity")})
					})
				]
			});
			
			var oItemTemplate = new sap.m.ColumnListItem({
				cells: [
					new sap.ui.core.Icon({
						src: {
							parts : [
								{path:"isgood"}
							],
							formatter : function(sIsgood) {
								if(sIsgood)
									return "sap-icon://sys-enter";
								else
									return "sap-icon://notification";
							}
						},
						color: {
							parts : [
								{path:"isgood"}
							],
							formatter : function(sIsgood) {
								if(sIsgood)
									return "#008000";
								else
									return "#C08800";
							}
						}
					}),
					new sap.m.Text({
						text: "{order}"
					}),
					new sap.m.Text({
						text: "{quantity}"
					})
				]
			})

			oSelectDialog.addStyleClass("noPaddingDialog");
			oSelectDialog.setContentWidth("400px");
			oSelectDialog.bindAggregation("items", "/data", oItemTemplate);
			
			oView.addDependent(oSelectDialog);
		}
		oSelectDialog.setModel(oView.getModel());
		
		oSelectDialog.open();
	}

});