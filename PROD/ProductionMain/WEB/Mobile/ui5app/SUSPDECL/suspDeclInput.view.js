// View Dichiarazione sospesi input
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SUSPDECL.suspDeclInput", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SUSPDECL.suspDeclInput";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oView = this;
		
		var idTextFormatter = function(sId,sTxt) {
			return sId + " " + sTxt;
		};
		
		var udcVisibleBindFunc = function() {
			return {
				parts: [
					{path: "/UDCNR" }
				],		     
				formatter: function(sUdcNr) {
					var bVisible = sUdcNr !== '';
					return bVisible;
				}
			}
		};
		
		var oQtyLabel = new sap.m.Label({
			id: 'suspDeclInputQtyLabel',
			text: oLng_Opr.getText("SuspDeclInput_Qty") + ':',
			width: '100%',
			textAlign: sap.ui.core.TextAlign.Right,
			layoutData : new sap.ui.layout.GridData({
				span: "L2 M3 S12",
				linebreak:true
			})
		}).addStyleClass("inputLabel");
		oView.addDependent(oQtyLabel);
		
		var oQtyInput = new NumericInput({
			id: 'suspDeclInputQty',
			min: 0,
			width: '80px',
			layoutData : new sap.ui.layout.GridData({
				span: "L3 M3 S12"
			})
		});
		oView.addDependent(oQtyInput);
		
		var oDMLabel = new sap.m.Label({
			id: 'suspDeclInputDMLabel',
			text: oLng_Opr.getText("SuspDeclInput_DataMatrix") + ':',
			width: '100%',
			textAlign: sap.ui.core.TextAlign.Right,
			layoutData : new sap.ui.layout.GridData({
				span: "L2 M3 S12",
				linebreak:true
			})
		}).addStyleClass("inputLabel");
		oView.addDependent(oDMLabel);
		
		var oDMInput = new sap.m.Input({
			id: 'suspDeclInputDM',
			layoutData : new sap.ui.layout.GridData({
				span: "L5 M5 S12"
			})
		});
		oView.addDependent(oDMInput);
		
		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					text: '{/ORDER}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_Phase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/POPER" },
							{path: "/CICTXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowPoperInfo(oView),
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_Line") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/LINEID" },
							{path: "/LINETXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/LINEID',
						description: '/LINETXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/MATERIAL" },
							{path: "/MAT_DESC"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclUDCLabel',
					visible: udcVisibleBindFunc(),
					text: oLng_Opr.getText("SuspDeclInput_UDC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					id: 'suspDeclUDCText',
					visible: udcVisibleBindFunc(),
					text: '{/UDCNR}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclQtyLabel',
					visible: udcVisibleBindFunc(),
					text: oLng_Opr.getText("SuspDeclInput_CurrentQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'suspDeclQtyText',
					visible: udcVisibleBindFunc(),
					text: '{/PZUDC}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclOpLabel',
					visible: udcVisibleBindFunc(),
					text: oLng_Opr.getText("SuspDeclInput_Operator") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					id: 'suspDeclOpText',
					visible: udcVisibleBindFunc(),
					text: '{/LAST_OPENAME}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					id: 'suspDeclTimeLabel',
					visible: udcVisibleBindFunc(),
					text: oLng_Opr.getText("SuspDeclInput_Datetime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					id: 'suspDeclTimeText',
					visible: udcVisibleBindFunc(),
					text: {
						parts: [
							{path: "/TIMEID"}
						],
						formatter: dateFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				/*
				new sap.m.Label({
					id: 'suspDeclInputDMLabel',
					text: oLng_Opr.getText("SuspDeclInput_DataMatrix") + ':',
					visible: {
						parts: [
							{path: "/DMREQ"}
						],
						formatter: function(sDmReq) {
							return (sDmReq === "1");
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'suspDeclInputDM',
					visible: {
						parts: [
							{path: "/DMREQ"}
						],
						formatter: function(sDmReq) {
							return (sDmReq === "1");
						}
					},
					layoutData : new sap.ui.layout.GridData({
						span: "L5 M5 S12"
					})
				}),
				*/
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_Dater") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.MaskInput({
					id: 'suspDeclInputDater',
					mask: "9999",
					placeholderSymbol: " ",
					placeholder: 'MMYY',
					width: '160px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'suspDeclInputBasketTT',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclInput_WorkTime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'suspDeclInputWorkTime',
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				/*new sap.m.Label({
					id: 'suspDeclInputQtyLabel',
					text: oLng_Opr.getText("SuspDeclInput_Qty") + ':',
					visible: {
						parts: [
							{path: "/DMREQ"}
						],
						formatter: function(sDmReq) {
							return !(sDmReq === "1");
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'suspDeclInputQty',
					visible: {
						parts: [
							{path: "/DMREQ"}
						],
						formatter: function(sDmReq) {
							return !(sDmReq === "1");
						}
					},
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				})*/
			]
		}).addStyleClass("sapUiTinyMarginTop");
		
		this.attachModelContextChange(function () {
			var oModel = this.getModel();
			if(oModel)
			{
				const sDmReq = oModel.getProperty("/DMREQ");
				if(sDmReq === "1")
				{
					oGrid.addContent(oDMLabel);
					oGrid.addContent(oDMInput);
					oGrid.removeContent(oQtyLabel);
					oGrid.removeContent(oQtyInput);
				}
				else
				{
					oGrid.addContent(oQtyLabel);
					oGrid.addContent(oQtyInput);
					oGrid.removeContent(oDMLabel);
					oGrid.removeContent(oDMInput);
				}
			}
		});
		
		var oPanel = new sap.m.Panel({
			content: oGrid
		});
		
		var oPage = new sap.m.Page({
			id: "suspDeclInputPage",
			enableScrolling: false,
			title: oLng_Opr.getText("SuspDeclInput_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							id: 'suspDeclNextButton',
							text: oLng_Opr.getText("SuspDeclInput_Next"),
							icon: "sap-icon://navigation-right-arrow",
							press: oController.onNext
						}).addStyleClass("sapUiSmallMarginBegin yellowButton bigButton"),
						new sap.m.Button({
							id: 'suspDeclPrintButton',
							enabled: false,
							text: oLng_Opr.getText("SuspDeclInput_Print"),
							icon: "sap-icon://print",
							press: oController.onPrint
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
					]
				}).addStyleClass("sapUiTinyMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});