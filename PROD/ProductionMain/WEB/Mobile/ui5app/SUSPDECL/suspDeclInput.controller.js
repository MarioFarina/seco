// Controller Dichiarazione sospesi input
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.SUSPDECL.suspDeclInput", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("suspDeclInput").attachPatternMatched(this.onObjectMatched, this);

		var oDMInput = sap.ui.getCore().byId("suspDeclInputDM");
		
		var nextFunc = this.onNext;
		oDMInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const aArgNames = ['plant','pernr','order','poper','idline','udcnr'];
		const oArguments = oAppController.getPatternArguments(oEvent,aArgNames);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('suspDeclInputPage'),sPlant,sPernr);
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		
		var bDisabledMode = false;
		if(!oUdcObject || !oUdcObject.hasOwnProperty('UDCNR') || 
			(oUdcObject.UDCNR !== oArguments.udcnr && !(oUdcObject.PRINT_MODE && oArguments.udcnr === '')))
		{
			oUdcObject = {};
			bDisabledMode = true;
		}
		
		this.displayInfo(oUdcObject, bDisabledMode);
		
		const bDMreq = ((oUdcObject && oUdcObject.hasOwnProperty('DMREQ')) ? (oUdcObject.DMREQ === '1') : false);
		if(bDMreq)
		{
			jQuery.sap.delayedCall(500, null, function() {
				oDMInput.focus();
			});
		}
	},

/***********************************************************************************************/
// Visualizzazione delle informazioni
/***********************************************************************************************/
	
	displayInfo: function(oUdcObject, bDisabledMode) {
		
		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		
		var oView = sap.ui.getCore().byId('suspDeclInputView');
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oUdcObject);
		oView.fireModelContextChange();

		var bPrintMode = false;
		if(oUdcObject.hasOwnProperty('PRINT_MODE'))
			bPrintMode = oUdcObject.PRINT_MODE;
		
		var oWorkTimeInput = $.UIbyID('suspDeclInputWorkTime');
		oWorkTimeInput.setValue(oUdcObject.WTDUR);		
		oWorkTimeInput.setValueState(sap.ui.core.ValueState.None);
		oWorkTimeInput.setValueStateText();
		
		var oBasketTT = $.UIbyID('suspDeclInputBasketTT');
		oBasketTT.setValue(oUdcObject.BASKETTT);		
		oBasketTT.setValueState(sap.ui.core.ValueState.None);
		oBasketTT.setValueStateText();
		
		var oDater = $.UIbyID('suspDeclInputDater');
		oDater.setValue(oUdcObject.DATER);		
		oDater.setValueState(sap.ui.core.ValueState.None);
		oDater.setValueStateText();
		
		var oDMInput = $.UIbyID('suspDeclInputDM');
		oDMInput.setValue();
		
		var oQtyInput = $.UIbyID('suspDeclInputQty');
		oQtyInput.setValue(oUdcObject.QTYDECL);
		oQtyInput.setValueState(sap.ui.core.ValueState.None);
		oQtyInput.setValueStateText();
		
		$.UIbyID('suspDeclNextButton').setEnabled(!bDisabledMode && !bPrintMode);
		$.UIbyID('suspDeclPrintButton').setEnabled(!bDisabledMode && bPrintMode);
	},
	
/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('suspDeclInputView').getController();
		var oArguments = oController._oArguments;
		
		if(oArguments.hasOwnProperty('order'))
			delete oArguments.order;
		if(oArguments.hasOwnProperty('poper'))
			delete oArguments.poper;
		if(oArguments.hasOwnProperty('idline'))
			delete oArguments.idline;
		if(oArguments.hasOwnProperty('udcnr'))
			delete oArguments.udcnr;
		
		oAppRouter.navTo('suspDecl',{
			query: oArguments
		});
	},
	
/***********************************************************************************************/
// Passa alla videata di inserimento delle causali
/***********************************************************************************************/

	onNext: function() {
		var oView = sap.ui.getCore().byId('suspDeclInputView');
		var oController = oView.getController();
		
		var oBasketTT = $.UIbyID('suspDeclInputBasketTT');
		var oDater = $.UIbyID('suspDeclInputDater');
		var oWorkTimeInput = $.UIbyID('suspDeclInputWorkTime');
		
		oBasketTT.setValueState(sap.ui.core.ValueState.None);
		oBasketTT.setValueStateText();
		oDater.setValueState(sap.ui.core.ValueState.None);
		oDater.setValueStateText();
		oWorkTimeInput.setValueState(sap.ui.core.ValueState.None);
		oWorkTimeInput.setValueStateText();
		
		const sBasketTT = oBasketTT.getValue();
		const sDater = oDater.getValue();
		const sWorkTime = oWorkTimeInput.getValue();
		
		var navFunction = function(sDataMatrix,sQuantity) {
			var oArguments = oController._oArguments;
			oArguments['dater'] = sDater;
			oArguments['baskettt'] = sBasketTT;
			oArguments['wtdur'] = sWorkTime;
			oArguments['serial'] = sDataMatrix;
			oArguments['quantity'] = sQuantity;
			
			oAppRouter.navTo('suspDeclReas',{
				query: oArguments
			});
		}
		
		var sErrorMessage;
		var oErrorField;
		const sCurrentYear = parseInt(new Date().getFullYear().toString().substring(2,4));
		if(sDater && (!sDater.match(/(0[1-9]|1[0-2])([0-9]{2})/) || sDater.length != 4)) // solo caratteri numerici
		{
			sErrorMessage = oLng_Opr.getText("SuspDeclInput_DaterErrorMsg");
			oErrorField = oDater;
		}
		else if(sDater && (sDater.substring(2,4) < sCurrentYear-1 || sDater.substring(2,4) > sCurrentYear+1))
		{
			sErrorMessage = oLng_Opr.getText("SuspDeclInput_DaterYearErrorMsg");
			oErrorField = oDater;
		}
		else if(sBasketTT && !sBasketTT.match(/^[0-9a-zA-Z ]+$/)) // solo caratteri alfanumerici
		{
			sErrorMessage = oLng_Opr.getText("SuspDeclInput_BasketTTErrorMsg");
			oErrorField = oBasketTT;
		}
		else if(isNaN(sWorkTime) || sWorkTime<=0)
		{
			sErrorMessage = oLng_Opr.getText("SuspDeclInput_WorkTimeErrorMsg");
			oErrorField = oWorkTimeInput;
		}
		else if(sWorkTime > 480)
		{
			sErrorMessage = oLng_Opr.getText("SuspDeclInput_WorkTimeMaxErrorMsg") + " (480 Min)";
			oErrorField = oWorkTimeInput;
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
			oErrorField.setValueState(sap.ui.core.ValueState.Warning);
			oErrorField.setValueStateText(sErrorMessage);
		}
		else
		{
			var oModel = oView.getModel();
			if(oModel.getProperty("/DMREQ") === "1")
			{	
				const sDataMatrix = $.UIbyID('suspDeclInputDM').getValue();
				
				var sCheckQuery = sMovementPath + "PAC/checkAvailableSerialsSQ";
				sCheckQuery += "&Param.1=" + sDataMatrix;
				sCheckQuery += "&Param.2=";
				sCheckQuery += "&Param.3=1";
				oAppController.handleRequest(sCheckQuery,{
					showSuccess: false,
					onSuccess: function () {
						navFunction(sDataMatrix,'1');
					}
				});
			}
			else
			{
				var oQtyInput = $.UIbyID('suspDeclInputQty');
				oQtyInput.setValueState(sap.ui.core.ValueState.None);
				oQtyInput.setValueStateText();
				
				const sQuantity = oQtyInput.getValue();
				const iQuantity = parseInt(sQuantity);
				if(isNaN(iQuantity) || iQuantity<=0)
					sErrorMessage = oLng_Opr.getText("SuspDeclInput_QtyErrorMsg");
				else if(iQuantity > 15000)
					sErrorMessage = oLng_Opr.getText("SuspDeclInput_QtyMaxErrorMsg") + " (15000)";
				
				if(sErrorMessage)
				{
					sap.m.MessageBox.show(sErrorMessage, {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: oLng_Opr.getText("General_WarningText"),
						actions: sap.m.MessageBox.Action.OK
					});
					oQtyInput.setValueState(sap.ui.core.ValueState.Warning);
					oQtyInput.setValueStateText(sErrorMessage);
				}
				else
					navFunction('',iQuantity.toString());
			}
		}
	},
	
/***********************************************************************************************/
// Ristampa dell'etichetta
/***********************************************************************************************/

	onPrint: function() {
		var oView = sap.ui.getCore().byId('suspDeclInputView');
		var oModel = oView.getModel();
		const sPdfString = oModel.getProperty("/PDFSTRING");
		
		var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
		oBoxDeclInputController.showPdfDialog(sPdfString,true);
			
	}
	
});