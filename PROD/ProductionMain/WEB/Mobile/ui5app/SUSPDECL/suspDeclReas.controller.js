// Controller Dichiarazioni Sospesi - Causali
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.SUSPDECL.suspDeclReas", {
	
	_oArguments: undefined,
	_sViewTipe: '', // 'S' Sospesi , 'R' Riparazioni / Rilavorazioni
	_sIdRep: '',
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("suspDeclReas").attachPatternMatched(this.onSuspMatched, this);
		oAppRouter.getRoute("repUdcReas").attachPatternMatched(this.onRepMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onSuspMatched: function (oEvent) {
		this._sViewTipe = 'S';
		
		this.onObjectMatched(oEvent);
	},
	
	onRepMatched: function (oEvent) {
		this._sViewTipe = 'R';
		
		this.onObjectMatched(oEvent);
	},

	onObjectMatched: function (oEvent) {
		
		var aArgNames = ['plant','pernr','order','poper','idline'];
		switch(this._sViewTipe) {
			case 'S':
				aArgNames.concat(['udcnr','dater','baskettt','wtdur','serial','quantity']);
				break;
			case 'R':
				aArgNames.concat(['udcfrom','udcto']);
				break;
		};
		
		const oArguments = oAppController.getPatternArguments(oEvent,aArgNames);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		//const sDatamatrix = oArguments.serial;
		//const sLine = oArguments.idline;
		
		//$.UIbyID('suspDeclReasDMText').setText(sDatamatrix);

		oAppController.loadSubHeaderInfo($.UIbyID('suspDeclReasPage'),sPlant,sPernr);
		
		const sLineId = oArguments.idline;
		var sUdcNr;
		switch(this._sViewTipe) {
			case 'S':
				sUdcNr = oArguments.udcnr;
				break;
			case 'R':
				sUdcNr = oArguments.udcto;
				break;
		};
		var sIdRep = '';
		if(sUdcNr && sLineId)
		{
			var sRequest = sMovementPath + "Scrap/getIdrepFromDlineScrapSQ";
			sRequest += "&Param.1=" + sLineId;
			sRequest += "&Param.2=" + sUdcNr;
			var oResult = fnGetAjaxVal(sRequest,['ID_REP'],false);
			if(oResult)
				sIdRep = oResult.ID_REP;
		}
		this._sIdRep = sIdRep;

		var oSuspTable = $.UIbyID('suspDeclReasTable');
		oSuspTable.destroyItems();
		oSuspTable['idcount'] = 0;
		oSuspTable.addItem(
			new sap.m.ColumnListItem({
				type : sap.m.ListType.Active,
				press: this.insertSuspendedItem,
				cells: [
					new sap.m.HBox({
						items: [
							new sap.ui.core.Icon({
								src: "sap-icon://add"
							}),
							new sap.m.Text({
								text: oLng_Opr.getText("SuspDeclReas_Add")
							}).addStyleClass("sapUiTinyMarginBegin")
						]
					}),
					new sap.m.Text(),
					new sap.m.Text()
				]
			})
		);
		
		this.createCausalsDialog(oArguments.plant);
			
		this.insertSuspendedItem();
	},
	
/***********************************************************************************************/
// Crea la dialog di selezione delle causali
/***********************************************************************************************/

	createCausalsDialog: function(sPlant) {
		const sDialogId = 'suspDeclCausalsDialog';
		var oCausalsDialog = $.UIbyID(sDialogId);
		if(!oCausalsDialog) {
			
			var oScrapReasCombo = new sap.m.ComboBox({
				width : "100%",
				change: function () {
					const sScrapReas = oScrapReasCombo.getValue();
					var aScrapItems = oScrapReasCombo.getItems();
					for(var i=0; i<aScrapItems.length; i++)
					{
						var oCurrentItem = aScrapItems[i];
						if(oCurrentItem.getKey() === sScrapReas)
						{
							oScrapReasCombo.setSelectedItem(oCurrentItem);
							break;
						}
					}
				}
			});
			
			var oScrapReasTemplate = new sap.ui.core.Item({
				key : '{SREASID}'
			}).bindProperty("text", {
				parts: [
					{path: 'SREASID'},
					{path: 'REASTXT'}
				],		     
				formatter: function(sReasId,sReasTxt){
					return sReasId + ' - ' + sReasTxt;
				}
			});
			oScrapReasCombo.bindAggregation("items","/Rowset/Row",oScrapReasTemplate);
			
			var oGrscReasModel = new sap.ui.model.xml.XMLModel();
			var sGrscQuery = QService + sMasterDataPath + "ScrabReasons/getScrabReasonsGroupsByTypeSQ";
			sGrscQuery += "&Param.1=" + sPlant;
			sGrscQuery += "&Param.2=SO";
			oGrscReasModel.loadData(sGrscQuery,false,false);
				
			var oGrscReasCombo = new sap.m.ComboBox({
				width : "100%",
				change: function () {
					const sSctype = oGrscReasCombo.getValue();
					var aGrscItems = oGrscReasCombo.getItems();
					for(var i=0; i<aGrscItems.length; i++)
					{
						var oCurrentItem = aGrscItems[i];
						if(oCurrentItem.getKey() === sSctype)
						{
							oGrscReasCombo.setSelectedItem(oCurrentItem);
							break;
						}
					}
					oGrscReasCombo.fireSelectionChange();
				},
				selectionChange: function () {
					var sGrscReas  = oGrscReasCombo.getSelectedKey();
					if(sGrscReas)
					{
						oScrapReasCombo.clearSelection();
						oScrapReasCombo.setValue();
						var oScrapReasModel = oScrapReasCombo.getModel();
						var sScrapQuery = QService + sMasterDataPath + "ScrabReasons/getScrabReasonsByGroupSQ"
						sScrapQuery += "&Param.1=" + sPlant;
						sScrapQuery += "&Param.2=" + sGrscReas;
						oScrapReasModel.loadData(sScrapQuery,false,false);
						var aItems = oScrapReasCombo.getItems();
						if(aItems.length === 1)
							oScrapReasCombo.setSelectedItem(aItems[0]);
						
						var oController = sap.ui.getCore().byId('suspDeclReasView').getController();
						var oGrscContext = oGrscReasCombo.getSelectedItem().getBindingContext();
						if(oController._sIdRep && oGrscContext && 
							oGrscContext.getProperty('ID_REP') != oController._sIdRep)
						{
							oGrscReasCombo.setValueStateText(oLng_Opr.getText("SuspDeclReas_ReasTypeErrorMsg"));
							oGrscReasCombo.setValueState(sap.ui.core.ValueState.Error);
						}
						else
						{
							oGrscReasCombo.setValueStateText();
							oGrscReasCombo.setValueState(sap.ui.core.ValueState.None);
						}
					}
				}
			});
			oGrscReasCombo.setModel(oGrscReasModel);
			
			var oGrscReasTemplate = new sap.ui.core.Item({
				key : "{SCTYPE}"
			}).bindProperty("text", {
				parts: [
					{path: 'SCTYPE'},
					{path: 'REASTXT'}
				],		     
				formatter: function(sSctype,sReasTxt){
					return sSctype + ' - ' + sReasTxt;
				}
			});
			oGrscReasCombo.bindAggregation("items","/Rowset/Row",oGrscReasTemplate);
		
			oCausalsDialog = new sap.m.Dialog({
				id: sDialogId,
				title: oLng_Opr.getText("SuspDeclReas_Causals"),
				content: new sap.ui.layout.form.SimpleForm({
					editable: true,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					labelSpanL: 4,
					labelSpanM: 4,
					labelSpanS: 4,
					emptySpanL: 0,
					emptySpanM: 0,
					emptySpanS: 0,
					columnsL: 1,
					columnsM: 1,
					columnsS: 1,
					content: [
						new sap.m.Label({
							text: oLng_Opr.getText("SuspDeclReas_GrscReas"),
						}),
						oGrscReasCombo,
						new sap.m.Label({
							text: oLng_Opr.getText("SuspDeclReas_ScrapReas"),
						}),
						oScrapReasCombo
					],
				}),
				beginButton: new sap.m.Button({
					text:  oLng_Opr.getText("SuspDeclReas_Save"),
					icon: "sap-icon://save",
					press: function () {
						var oInputField = oCausalsDialog['inputField'];
						if(oInputField)
						{
							var oController = sap.ui.getCore().byId('suspDeclReasView').getController();
							var oGrscContext = oGrscReasCombo.getSelectedItem().getBindingContext();
							if(oController._sIdRep && oGrscContext && 
								oGrscContext.getProperty('ID_REP') != oController._sIdRep)
							{
								sap.m.MessageBox.show(oLng_Opr.getText("SuspDeclReas_ReasTypeErrorMsg"), {
									icon: sap.m.MessageBox.Icon.ERROR,
									title: oLng_Opr.getText('General_ErrorText'),
									actions: [sap.m.MessageBox.Action.OK]
								});
							}
							else
							{
								oInputField.setValue(oScrapReasCombo.getSelectedKey());
								oInputField.setValueStateText();
								oInputField.setValueState(sap.ui.core.ValueState.None);
								oCausalsDialog.close();
							}
						}
					}
				}),
				endButton: new sap.m.Button({
					text: oLng_Opr.getText("SuspDeclReas_Cancel"),
					icon: "sap-icon://decline",
					press: function() {
						oCausalsDialog.close();
					}
				}),
				beforeOpen: function() {
					oScrapReasCombo.clearSelection();
					oScrapReasCombo.setValue();
					var oScrapReasModel = oScrapReasCombo.getModel();
					if(oScrapReasModel)
						oScrapReasModel.destroy();
					oScrapReasCombo.setModel(new sap.ui.model.xml.XMLModel());
					oGrscReasCombo.clearSelection();
					oGrscReasCombo.setValue();
					oGrscReasCombo.setValueStateText();
					oGrscReasCombo.setValueState(sap.ui.core.ValueState.None);
				}/*,
				afterClose: function() {
					oCausalsDialog.destroy();
				}*/
			}).addStyleClass("noPaddingDialog");
			
			this.getView().addDependent(oCausalsDialog);
		}
	},
	
/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna a Dichiarazione Sospesi
/***********************************************************************************************/

	onNavBack: function(bCompleted) {	
	
		var backFunc = function(/*bToRoot*/) {
			var oController = sap.ui.getCore().byId('suspDeclReasView').getController();
			var oArguments = oController._oArguments;
			
			var sRouteName;
			switch(oController._sViewTipe) {
				case 'S':
					sRouteName = 'suspDeclInput';
					if(oArguments.hasOwnProperty('dater'))
						delete oArguments.dater;
					if(oArguments.hasOwnProperty('baskettt'))
						delete oArguments.baskettt;
					if(oArguments.hasOwnProperty('wtdur'))
						delete oArguments.wtdur;
					if(oArguments.hasOwnProperty('serial'))
						delete oArguments.serial;
					if(oArguments.hasOwnProperty('quantity'))
						delete oArguments.quantity;
					break;
				case 'R':
					sRouteName = 'repUdcInput';
					if(oArguments.hasOwnProperty('udcto'))
						delete oArguments.udcto;
					if(oArguments.hasOwnProperty('idline'))
						delete oArguments.idline;
					break;
			};
			
			/*if(bToRoot)
			{
				sRouteName = 'suspDecl';
				if(oArguments.hasOwnProperty('order'))
					delete oArguments.order;
				if(oArguments.hasOwnProperty('poper'))
					delete oArguments.poper;
				if(oArguments.hasOwnProperty('idline'))
					delete oArguments.idline;
			}
			else
				sRouteName = 'suspDeclInput';*/
			
			oAppRouter.navTo(sRouteName,{
				query: oArguments
			});
		};
		
		var oReasTable = $.UIbyID('suspDeclReasTable');
		var iItemsCount = oReasTable.getItems().length - 1;
		
		if((typeof(bCompleted) === 'boolean' && bCompleted) || iItemsCount <= 1)
			backFunc();
		else
		{
			sap.m.MessageBox.show(oLng_Opr.getText("SuspDeclReas_NavBackMsg"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: [sap.m.MessageBox.Action.YES,sap.m.MessageBox.Action.CANCEL],
				onClose: function(oAction) {
					if (oAction === sap.m.MessageBox.Action.YES)
						backFunc();
				}
			});
		}
	},
	
/***********************************************************************************************/
// Inserimento di un nuovo Item nella tabella
/***********************************************************************************************/
	
	insertSuspendedItem: function() {
		
		var oReasTable = $.UIbyID('suspDeclReasTable');
		var iItemsCount = oReasTable.getItems().length - 1;		
		var oCausalInput = new sap.m.Input({
			showValueHelp: true,
			valueHelpRequest: function () {
				var oCausalsDialog = $.UIbyID('suspDeclCausalsDialog');
				oCausalsDialog['inputField'] = oCausalInput;
				oCausalsDialog.open();
			}
		});
		
		var oSuspColumnListItem = new sap.m.ColumnListItem({
			//type : sap.m.ListType.Active,
			cells: [
				oCausalInput,
				new sap.m.Input(),
				(iItemsCount === 0) ? 
					new sap.m.Text() :
					new sap.m.Button({
						icon: "sap-icon://delete",
						press: function() {	
							oSuspColumnListItem.destroy();
						}
					})
			]
		});
		
		oReasTable.insertItem(oSuspColumnListItem,iItemsCount);
		oReasTable['idcount'] += 1;
		
		jQuery.sap.delayedCall(1000, null, function() {
			oCausalInput.focus();
		});
	},
	
/***********************************************************************************************/
// Verifica dei dati inseriti e completa l'operazione
/***********************************************************************************************/
	
	onComplete: function() {
		var oController = sap.ui.getCore().byId('suspDeclReasView').getController();
		const oArguments = oController._oArguments;
		
		var oReasTable = $.UIbyID('suspDeclReasTable');
		var aReasTableItems = oReasTable.getItems();
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		const oUdcObject = oSessionStorage.get('FUNCDATA');
		const sMaterial = ((oUdcObject && oUdcObject.MATERIAL) ? oUdcObject.MATERIAL : '');
		
		var sErrorMessage,oCausalInput;
		var sCausalNoteMap = '';
		var sReasId;
		for(var i=0; i<aReasTableItems.length-1; i++)
		{
			var aCells = aReasTableItems[i].getCells();
			oCausalInput = aCells[0];
			var sCausalValue = oCausalInput.getValue();
			var sNoteValue = aCells[1].getValue();
			
			if(!sCausalValue)
			{
				sErrorMessage = oLng_Opr.getText("SuspDeclReas_CausalErrorMsg");
				break;
			}
			
			// Controllo validità causale
			var sCheckQuery = sMasterDataPath + "ScrabReasons/checkScrabReasonSQ";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + sCausalValue;
			var iCheckResult = parseInt(fnGetAjaxVal(sCheckQuery,["checked"],false).checked);
			if(!iCheckResult)
			{
				sErrorMessage = oLng_Opr.getText("SuspDeclReas_NotExistErrorMsg");
				break;
			}
			
			oCausalInput.setValueState(sap.ui.core.ValueState.None);
			
			if(!sReasId)
				sReasId = sCausalValue;
			
			if(sCausalNoteMap)
					sCausalNoteMap += ',';
			sCausalNoteMap += sCausalValue + '=' + sNoteValue;
		}
		sCausalNoteMap = '{' + sCausalNoteMap + '}';
		
		if(sErrorMessage)
		{
			oCausalInput.setValueStateText(sErrorMessage);
			oCausalInput.setValueState(sap.ui.core.ValueState.Warning);
				
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText('General_WarningText'),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{					
			if(oController._sViewTipe === 'S')
			{
				var continueFunc = function (sState) 
				{
					var sWsName = '';
					var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
					var oWorkstInfo = oStorage.get('WORKST_INFO');
					if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
						sWsName = oWorkstInfo.WSNAME;
					
					const sDatetime = getDatetimeString(new Date());
				
					var sSaveQuery = sMovementPath + "Scrap/saveScrapAndPrintQR";
					sSaveQuery += "&Param.1=" + oArguments.plant;
					sSaveQuery += "&Param.2=" + oArguments.idline;
					sSaveQuery += "&Param.3=" + oAppController._sShift;
					sSaveQuery += "&Param.4=" + oAppController._sDate;
					sSaveQuery += "&Param.5=" + oArguments.pernr;
					sSaveQuery += "&Param.6=" + oArguments.order;
					sSaveQuery += "&Param.7=" + oArguments.poper;
					sSaveQuery += "&Param.8=" + sMaterial;
					sSaveQuery += "&Param.9=" + oArguments.udcnr;
					sSaveQuery += "&Param.10=" + sState;
					sSaveQuery += "&Param.11=" + '';
					sSaveQuery += "&Param.12=" + '';
					sSaveQuery += "&Param.13=" + '';
					sSaveQuery += "&Param.14=" + oArguments.serial;
					sSaveQuery += "&Param.15=" + sReasId;
					sSaveQuery += "&Param.16=" + oArguments.quantity;
					sSaveQuery += "&Param.17=" + oArguments.wtdur;
					sSaveQuery += "&Param.18=" + sCausalNoteMap;
					sSaveQuery += "&Param.19=" + oArguments.dater;
					sSaveQuery += "&Param.20=" + oArguments.baskettt;
					sSaveQuery += "&Param.21=" + sWsName;
					sSaveQuery += "&Param.22=" + sDatetime;
					sSaveQuery += "&Param.23=" + '';
					sSaveQuery += "&Param.24=" + sLanguage;
					
					var successFunction = function(oResults) {
						
						var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
						var oUdcObject = oSessionStorage.get('FUNCDATA');
						
						if(!oUdcObject)
							oUdcObject = {};
						
						var iQtyRes = parseInt(oUdcObject.QTYRES);
						if(isNaN(iQtyRes))
							iQtyRes = 0;
						
						if(oResults.hasOwnProperty('outputUdc') && oResults.outputUdc)
							oUdcObject.UDCNR = oResults.outputUdc;
						if(oResults.hasOwnProperty('pdfString') && oResults.pdfString)
						{
							oUdcObject.PDFSTRING = oResults.pdfString;
							if(parseInt(oResults.code) === 1) // WARNING = 1 : stampa non automatica, anteprima automatica
							{
								var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
								oBoxDeclInputController.showPdfDialog(oResults.pdfString,true,function() {
									oController.onNavBack(true);
								});
							}
						}
						oUdcObject.PRINT_MODE = true;
						oUdcObject.PZUDC = iQtyRes + parseInt(oArguments.quantity);
						oUdcObject.QTYDECL = oArguments.quantity;
						oUdcObject.WTDUR = oArguments.wtdur;
						oUdcObject.DATER = oArguments.dater;
						oUdcObject.BASKETTT = oArguments.baskettt;
						oUdcObject.TIMEID = sDatetime;
						oUdcObject.LAST_OPEID = oArguments.pernr;
						
						const oLoginInfo = oSessionStorage.get('LOGIN_INFO');
						if(oLoginInfo.hasOwnProperty("opeName"))
							oUdcObject.LAST_OPENAME = oLoginInfo.opeName;
						
						oSessionStorage.put('FUNCDATA',oUdcObject);
						
						if(parseInt(oResults.code) !== 1)
							oController.onNavBack(true);
					};
					
					oAppController.handleRequest(sSaveQuery,{
						onSuccess: successFunction,
						onWarning: successFunction,
						notShowWarning: [1],
						results: ['outputUdc','pdfString']
					});
				};
				
				var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
			
				oBoxDeclInputController.selectState({
					firstValue: 'O',
					secondValue: 'C',
					secondColorCss: 'yellowButton',
					onCallback: continueFunc
				})
			}
			else if(oController._sViewTipe === 'R')
			{
				var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
				var oFuncData = oSessionStorage.get('FUNCDATA');
				
				if(oFuncData && oFuncData.hasOwnProperty('CALLBACKPARAMS') && oFuncData.CALLBACKPARAMS)
					oFuncData.CALLBACKPARAMS.reasid = sReasId;
					
				var oRepUdcInputController = sap.ui.controller("ui5app.REPUDC.repUdcInput");
				oRepUdcInputController.onRework({UDCNR: oArguments.udcto},oFuncData.CALLBACKPARAMS);
			}
		}
	}

});