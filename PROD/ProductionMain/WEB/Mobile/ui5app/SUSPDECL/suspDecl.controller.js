// Controller Dichiarazione sospesi
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.SUSPDECL.suspDecl", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("suspDecl").attachPatternMatched(this.onObjectMatched, this);
		
		var oOrderInput = $.UIbyID('suspDeclOrderInput');
		
		var nextFunc = this.onNext;
		oOrderInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('suspDeclPage'),sPlant,sPernr);
		
		var oOrderInput = $.UIbyID('suspDeclOrderInput');
		oOrderInput.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			oOrderInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('suspDeclView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Passa alla videata di inserimento delle causali
/***********************************************************************************************/

	onNext: function() {
		var oOrderInput = $.UIbyID('suspDeclOrderInput');
		const sOrderPhase = oOrderInput.getValue();
		
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		
		const sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage) {
			oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderInput.setValueStateText(sErrorMessage);
			
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else // Controlla commessa e fase e se sono validi esegue l'avanzamento	
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			var aLinesObjects = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
			{
				aLinesObjects = oSettings.LINES;
				for(var i=0; i<aLinesObjects.length; i++)
					aLines.push(aLinesObjects[i].lineid);
			}
			
			const aSpecialWarningCodes = [2,3];
			
			var oController = sap.ui.getCore().byId('suspDeclView').getController();
			var oArguments = oController._oArguments;
			
			var sCheckQuery = sMovementPath + "UDC/checkUDCOutPartLineQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + sOrderPhase;
			sCheckQuery += "&Param.3=" + aLines.join();
			sCheckQuery += "&Param.4=C";
			sCheckQuery += "&Param.5=" + sLanguage;
			
			var navFunction = function(oCallbackParams)
			{			
				oArguments['order'] = oCallbackParams.order;
				oArguments['poper'] = oCallbackParams.poper;
				oArguments['idline'] = oCallbackParams.lineData.lineid;
				oArguments['udcnr'] = oCallbackParams.udcnr;
		
				oAppRouter.navTo('suspDeclInput',{
					query: oArguments
				});
			};
			
			var oBoxDeclController = sap.ui.controller("ui5app.BOXDECL.boxDecl");
		
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oBoxDeclController.onLineSuccess,
				showWarning: aSpecialWarningCodes,
				onWarning: oBoxDeclController.onLineWarning,
				callbackParams: {
					warningCodes: aSpecialWarningCodes,
					linesObjects: aLinesObjects,
					oneLineCallback: oBoxDeclController.onLineSuccess,
					lineCallback: oBoxDeclController.onUdcSelection,
					leftUdcData: [
						new sap.m.Label({
							text: "{REASTXT}",
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Left
						}).addStyleClass("smallLabel sapUiTinyMarginTop")
					],
					udcCallback: oBoxDeclController.onNav,
					navCallback: navFunction,
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					plant: oArguments.plant
				},
				results: {
					returnProperties: ['outputLines','cicTxt','material','matDesc','cdlId','cdlDesc'],
					returnXMLDoc: true
				}
			});
		}
	},

});