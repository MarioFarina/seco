// View Dichiarazioni Sospesi - Causali
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SUSPDECL.suspDeclReas", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SUSPDECL.suspDeclReas";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		/*var oFormText = new sap.ui.layout.form.SimpleForm({
			editable: false,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 4,
			labelSpanM: 4,
			emptySpanL: 3,
			emptySpanM: 3,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDeclReas_DataMatrix")
				}),
				new sap.m.Text({
					id: 'suspDeclReasDMText'
				})
			]
		});*/
		
		var oTable = new sap.m.Table({
			id: 'suspDeclReasTable',
			columns: [
		          new sap.m.Column({
		        	  header: new sap.m.Text({text: oLng_Opr.getText("SuspDeclReas_Causal")}),
		        	  width: "150px"
		          }),
		          new sap.m.Column({
		        	  header: new sap.m.Text({text: oLng_Opr.getText("SuspDeclReas_Note")})
		          }),
				  new sap.m.Column({
		        	  width: "48px"
		          })
    		]
		});
		
		var oPage = new sap.m.Page({
			id: "suspDeclReasPage",
			enableScrolling: true,
			title: oLng_Opr.getText("SuspDeclReas_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				//oFormText,
				oTable
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						text: oLng_Opr.getText("SuspDeclReas_Complete"),
						icon: "sap-icon://accept",
						press: oController.onComplete
					}).addStyleClass("yellowButton noBorderButton")
				]
			})
		});
		
	  	return oPage;
	}

});