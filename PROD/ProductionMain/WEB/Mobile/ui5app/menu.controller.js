// Controller Menu Operatore

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.menu", {

	_oArguments: undefined,
	_aParents:[],
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("menu").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put("FUNC_INFO",false);
		
		var oAppController = sap.ui.getCore().byId('app').getController();
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		// richiamo la funzione unicamente per il controllo del timer
		oAppController.loadSubHeaderInfo(false,sPlant,sPernr);
		
		var oFunctionsModel = new sap.ui.model.xml.XMLModel();
		var sLoadQuery = QService + sMasterDataPath + "Functions/getFunctionsbyPernrSQ";
		sLoadQuery += "&Param.1=" + sPlant;
		sLoadQuery += "&Param.2=" + sPernr;
        sLoadQuery += "&Param.3=" + sLanguage;
		oFunctionsModel.loadData(sLoadQuery);
		this.getView().setModel(oFunctionsModel);
		
		var sIdParent = '';
		var sFuncName;
		if(this._aParents)
		{
			const iParentsCount = this._aParents.length;
			if(iParentsCount > 0)
			{
				const oParent = this._aParents[iParentsCount-1];
				sIdParent = oParent.id;
				sFuncName = oParent.text;
			}
		}
		
		this.setViewFilter(sIdParent);
		
		sap.ui.getCore().byId("menuPage").setTitle(sFuncName ? sFuncName : oLng_Opr.getText("Menu_Menu"));
	},
	
	setViewFilter: function(sValue) {
		var sAggregation;
		if(this._bListView)
			sAggregation = "items";
		else
			sAggregation = "tiles";
		
		var oFilter = new sap.ui.model.Filter("Parent", sap.ui.model.FilterOperator.EQ, sValue);
		var oBinding = $.UIbyID('functionsList').getBinding(sAggregation);		
		oBinding.filter(oFilter);
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Ritorna al livello precedente della gerarchia o torna a login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('menuView').getController();
		var sPrevParent = oController._aParents.pop();
		if(sPrevParent)
		{
			var sIdParent = '';
			var sFuncName = oLng_Opr.getText("Menu_Menu");
			var iDepth = oController._aParents.length;
			if(iDepth > 0)
			{
				var oParent = oController._aParents[iDepth-1];
				sIdParent = oParent.id;
				sFuncName = oParent.text;
			}

			oController.setViewFilter(sIdParent);
			
			sap.ui.getCore().byId("menuPage").setTitle(sFuncName);
		}
		else
			oAppController.onLogout();
	},
	
/***********************************************************************************************/
// Logout
/***********************************************************************************************/
	
	onLogout: function () {
		var oController = sap.ui.getCore().byId('menuView').getController();
		oController._aParents = [];
	},

/***********************************************************************************************/
// Funzione per l'ingresso nel livello successivo della gerarchia o nel link
/***********************************************************************************************/

	onFunctionSelected: function(oEvent) {
		var oController = sap.ui.getCore().byId("menuView").getController();
		var oContext = oEvent.getSource().getBindingContext();
		const sLink = oContext.getProperty("FuncLink");
		const sIdFunc = oContext.getProperty("IdFunc");
		const sFuncName = oContext.getProperty("FuncName");
		
		if(sLink)
		{
			//var oArguments = oController._oArguments;
			const sNavName = oContext.getProperty("NavName");
			if(sNavName)
			{
				/*oArguments.params = {funcName: sFuncName};*/
				
				var sWsName;
				var oLocalStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				var oSettings = oLocalStorage.get("WORKST_INFO");
				if(oSettings)
				{
					if(oSettings.hasOwnProperty('WSNAME'))
						sWsName = oSettings.WSNAME;
				}
				
				var sRequest = sMovementPath + "Swsope/addSwsopeQR";
				sRequest += "&Param.1=" + oController._oArguments.plant;
				sRequest += "&Param.2=" + sWsName;
				sRequest += "&Param.3=" + oController._oArguments.pernr;
				sRequest += "&Param.4=" + sIdFunc;
				sRequest += "&Param.5=";
				var bSuccess = fnExeQuerym(sRequest);
				if(bSuccess)
				{
					oAppRouter.navTo(sNavName, {
						query: oController._oArguments
					}, true);
				}
			}
			else
			{}
		}
		else
		{
			oController._aParents.push({
				id: sIdFunc,
				text: sFuncName
			});
			
			oController.setViewFilter(sIdFunc);
			
			sap.ui.getCore().byId("menuPage").setTitle(sFuncName);
		}
	}

});