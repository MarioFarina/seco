// View Applicazione
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// Percorsi
var QService = "/XMII/Illuminator?";
var QHeader = "Content-Type=text/XML&QueryTemplate=";
var sBasicComponentsPath = QHeader + "Common/BasicComponents/";
var sConfirmPath = QHeader + "ProductionMain/Confirm/";
var sMasterDataPath = QHeader + "ProductionMain/MasterData/";
var sTimeShiftPath = QHeader + "ProductionMain/TimeShift/";
var sMonitorPath = QHeader + "ProductionMain/Monitor/";
var sMovementPath = QHeader + "ProductionMain/Movement/";
var sProductionPath = QHeader + "ProductionMain/Production/";
var sMobilePath = QHeader + "ProductionMain/Mobile/";
var sDataLoadPath = QHeader + "ProductionMain/DataLoad/";

// Lingua locale
var sCurrentLang = sap.ui.getCore().getConfiguration().getLanguage();

var oLng_Opr = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Mobile/i18n/mobile.i18n.properties",
	locale: sCurrentLang
});

sap.ui.jsview("ui5app.app", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5routingtest.App
	*/ 
	getControllerName : function() {
		return "ui5app.app";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5routingtest.App
	*/ 
	createContent : function(oController) {
		this.setDisplayBlock(true);
		
		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			position: "Center",
			defaultSpan: "L3 M3 S3",
			defaultIndent: "L0 M0 S0",
			content: [
				new sap.m.ObjectIdentifier({
					id: 'subHeaderPlantText',
					title: oLng_Opr.getText("Header_Plant")
				}),
				new sap.m.ObjectIdentifier({
					id: 'subHeaderDateText',
					title: oLng_Opr.getText("Header_Date")
				}),
				new sap.m.ObjectIdentifier({
					id: 'subHeaderShiftText',
					title: oLng_Opr.getText("Header_Shift")
				}),
				new sap.m.ObjectIdentifier({
					id: 'subHeaderOperatorText',
					title: oLng_Opr.getText("Header_Operator")
				})
			]
		});
	
		new sap.m.Panel({
			id: 'subHeaderPanel',
			backgroundDesign: sap.m.BackgroundDesign.Solid,
			content: oGrid
		});
		
 		return new sap.m.App("appContainer");
	}

});