//**************************************************************************************
//Title:  Analisi conferme produzione v.2
//Author: Bruno Rosati
//Date:   04/10/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Analisi conferme produzione v.2

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var QPrefix = "Content-Type=text/XML&QueryTemplate=";
var dataConfProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/Confirm/ConfProd/";
var icon16 = "/XMII/CM/Common/icons/16x16/";
var dateSelFrom = "";
var dateSelTo = "";
var dtFormat = "";
var dtSelect = "";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Analysis = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + "PivotAnalysis/" + "res/Analysis_res.i18n.properties",
	locale: sLanguage
});

jQuery.sap.require("sap.ui.core.format.DateFormat");
/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        //"/XMII/CM/ProductionMain/MasterData/Operators_fn/fn_operatorTools",
        //"/XMII/CM/ProductionMain/MasterData/Material_fn/fn_materialTools",
        //"/XMII/CM/ProductionMain/MasterData/Material_fn/fn_mcicdTools"
    ],
    function () {
        $(document).ready(function () {
            /*************************************************************/
            // Creo la tool bar
            /*************************************************************/
            dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: "yyyyMMdd"
            });
            dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: "yyyy-MM-dd"
            });
            
            // Crea la ComboBox per le divisioni
            var oModel = new sap.ui.model.xml.XMLModel();
            oModel.loadData(QService + dataCommon + "GetPlantsQR");
            
            console.log("path: " +QService + dataCommon + "GetPlantsQR")
            
            /**
            oCmbPlant = new sap.ui.commons.ComboBox("cmbPlants");
            oCmbPlant.setModel(oModel);
            //oCmbPlant.bindRows("/Rowset/Row");
            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("text", "NAME1");
            oItemPlant.bindProperty("key", "PLANT");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            
            oCmbPlant.attachChange(function() {
                $("#Plant").val( $.UIbyID("cmbPlants").getSelectedKey());
                //window.oCmbRep.getModel().loadData(QService + dataRep + "getDepartmentsByPlant&Param.1=" + $("#Plant").val());
                //refreshTabCons();
            });
            */
            
            // combo linee @todo
            
            // creazione delle due DtPicker
            var yesterday = new Date();
            yesterday.setDate( yesterday.getDate() - 1 );
            var ToDay = new Date();
            
            var  year = yesterday.getUTCFullYear();
            var  month = yesterday.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            var  day = yesterday.getUTCDate();
            if( day.toString().length == 1 ) day = "0" + day;
            dateSelFrom = year + "-" + month+ "-" + day;
            dOrigDateSelFtrom = dateSelFrom;
            
            var fromday = new Date(yesterday.getFullYear(), yesterday.getMonth() + 1, 0);
            year = ToDay.getUTCFullYear();
            month = ToDay.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            day = ToDay.getUTCDate();            
            dateSelTo = year + "-" + month+ "-" + day;
            dOrigDateSelTo = dateSelTo;
            
            odtFrom = new sap.ui.commons.DatePicker('dtFrom');
            odtFrom.setYyyymmdd( dateSelFrom );
            odtFrom.setLocale("it"); // Try with "de" or "fr" instead!
            odtFrom.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelFrom = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                }
            );
            
            odtTo = new sap.ui.commons.DatePicker('dtTo');
            odtTo.setYyyymmdd( dateSelTo );
            odtTo.setLocale("it"); // Try with "de" or "fr" instead!
            odtTo.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelTo = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                    //refreshTabAnalys();
                }
            );
            
            //Filtro Buoni/Sospesi
            var oCmbTypeConfFilter = new /*sap.ui.commons.ComboBox*/sap.m.MultiComboBox("filtTypeConf", {
                //layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                width: "215px",
                change: function (oEvent) {
                    //$.UIbyID("delFilter").setEnabled(true);
                }
            }).addStyleClass('customMComboBox');
            var oItemTypeConf = new sap.ui.core.ListItem();
            oItemTypeConf.bindProperty("key", "MII_TYPECONF");
            oItemTypeConf.bindProperty("text", "EXTENDED_TEXT");
            oCmbTypeConfFilter.bindItems("/Rowset/Row", oItemTypeConf);
            var oTypeConfModel = new sap.ui.model.xml.XMLModel();
            oTypeConfModel.loadData(QService + dataConfProd + "getMtypeconfSQ&Param.1=" + sLanguage);
            oCmbTypeConfFilter.setModel(oTypeConfModel);
            
            var oToolbar1 = new sap.ui.commons.Toolbar("tb1",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
                items: [
                    /**
                    new sap.ui.commons.Label("lPlant",{
                        text : oLng_Analysis.getText("Analysis_Plant"), //"Divisione",
                        tooltip : oLng_Analysis.getText("Analysis_SelectPlant") //"Seleziona la divisione"
                    }),
                    oCmbPlant,
                    */
                    new sap.ui.commons.Label("lFrom",{
                        text : oLng_Analysis.getText("Analysis_FromDate"), //"Da data:",
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") //"Seleziona l'intervallo di date"
                    }),
                    odtFrom,
                    new sap.ui.commons.Label("lTo",{
                        text : oLng_Analysis.getText("Analysis_ToDate"), //"a data:",
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") //"Seleziona l'intervallo di date"
                    }),
                    odtTo,
                    /**
                    new sap.ui.commons.RadioButton("rdbDateShift",{
                        text: oLng_Analysis.getText("Analysis_DateShift"), //"Data turno"
                        selected: true
                    }),
                    new sap.ui.commons.RadioButton("rdbDateReg",{
                        text: oLng_Analysis.getText("Analysis_DateReg"), //"Data registrazione"
                        selected: false
                    })
                    */
                ],
                rightItems: [
                    new sap.ui.commons.Button("btnToogle",{
                        icon: "sap-icon://resize",
                        //		 text : "PNG",
                        tooltip : oLng_Analysis.getText("Analysis_Show_HideToolbar"), //"Mostra/Nascondi barra strumenti",
                        press: function() {
                            $(".pvtAxisContainer, .pvtVals").toggle();
                        }
                    }),
                    new sap.ui.commons.Button("btnXls",{
                        icon: "/XMII/CM/Common/icons/document/xls_16.png",
                        //text : "XLS",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToExcel"), //"Esporta in excel",
                        press: function() {
                            exportTo("xls");
                        }
                    }),
                    new sap.ui.commons.Button("btnDoc",{
                        icon: "/XMII/CM/Common/icons/document/word_16.png",
                        //	text : "CSV",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToWord"), //"Esporta in Word",
                        press: function() {
                            exportTo("doc");
                        }
                    }),
                    new sap.ui.commons.Button("pdf",{
                        icon: "/XMII/CM/Common/icons/document/pdf_16.png",
                        //		 text : "PNG",
                        tooltip : oLng_Analysis.getText("Analysis_PrintPdf"), //"Stampa in PDF",
                        press: function() {
                            exportTo("pdf");
                        }
                    })
                ]
            });
            
            
            /*
            var oToolbar2 = new sap.ui.commons.Toolbar("tb2",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_Analysis.getText("Analysis_Operator"), //"Operatore",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    new  sap.ui.commons.TextField("txtOpeFiter", {
                        value: "",
                        editable: false,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                    }),
                    new sap.ui.commons.Button("btnOpeFilter", {
                        icon: "sap-icon://arrow-down",
                        tooltip: oLng_Analysis.getText("Analysis_SelectOperator"), //"Seleziona operatore"
                        enabled: true,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                        press: function (){
                            fnGetOperatorMultipleSelectTableDialog($.UIbyID("cmbPlants").getSelectedKey(),"",setSelectedOperator);
                        }
                    }),
                    new sap.ui.commons.Label({
                        text: oLng_Analysis.getText("Analysis_Material"), //"Materiale",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    new  sap.ui.commons.TextField("txtMatFiter", {
                        value: "",
                        editable: false,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                    }),
                    new sap.ui.commons.Button("btnMatFilter", {
                        icon: "sap-icon://arrow-down",
                        tooltip: oLng_Analysis.getText("Analysis_SelectMaterial"), //"Seleziona materiale"
                        enabled: true,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                        press: function (){
                            fnGetMaterialTableDialog($.UIbyID("cmbPlants").getSelectedKey(),"",setSelectedMaterial,false);
                        }
                    }),
                    new sap.ui.commons.Label({
                        text: oLng_Analysis.getText("Analysis_Draw"), //"Disegno",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    new  sap.ui.commons.TextField("txtDrawFiter", {
                        value: "",
                        editable: false,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                    }),
                    new sap.ui.commons.Button("btnDrawFilter", {
                        icon: "sap-icon://arrow-down",
                        tooltip: oLng_Analysis.getText("Analysis_SelectDraw"), //"Seleziona disegno"
                        enabled: true,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                        press: function (){
                            fnGetMaterialTableDialog($.UIbyID("cmbPlants").getSelectedKey(),"",setSelectedDraw,true);
                        }
                    }),
                    new sap.ui.commons.Label({
                        text: oLng_Analysis.getText("Analysis_Declared") //"Dichiar."
                    }),
                    oCmbTypeConfFilter
                ],
                rightItems: []
            });
            */
            
            /**
            var oToolbar3 = new sap.ui.commons.Toolbar("tb3",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_Analysis.getText("Analysis_Operation"), //"Fase",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    new  sap.ui.commons.TextField("txtPoperFiter", {
                        value: "",
                        editable: false,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                    }),
                    new sap.ui.commons.Button("btnPoperFilter", {
                        icon: "sap-icon://arrow-down",
                        tooltip: oLng_Analysis.getText("Analysis_SelectOperation"), //"Seleziona fase"
                        enabled: true,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                        press: function (){
                            fnGetMcicdTableDialog($.UIbyID("cmbPlants").getSelectedKey(),"",setSelectedOperation,true);
                        }
                    }),
                    new sap.ui.commons.Label({
                        text: oLng_Analysis.getText("Analysis_Cdl"), //"Centro di lavoro",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    new  sap.ui.commons.TextField("txtCdlFiter", {
                        value: "",
                        editable: false,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                    }),
                    new sap.ui.commons.Button("btnCdlFilter", {
                        icon: "sap-icon://arrow-down",
                        tooltip: oLng_Analysis.getText("Analysis_SelectCdl"), //"Seleziona centro di lavoro"
                        enabled: true,
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                        press: function (){
                            fnGetMcicdTableDialog($.UIbyID("cmbPlants").getSelectedKey(),"",setSelectedCdl,false);
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button("delFilter",{
                        icon: "sap-icon://filter",
                        enabled: true,
                        text : oLng_Analysis.getText("Analysis_RemoveFilters"), //"Rimuovi filtri"
                        tooltip: oLng_Analysis.getText("Analysis_RemoveFilters"), //"Rimuovi filtri"
                        press: function () {
                            $.UIbyID("cmbPlants").setSelectedKey("");
                            odtFrom.setYyyymmdd(dOrigDateSelFtrom);
                            odtTo.setYyyymmdd(dOrigDateSelTo);
                            $.UIbyID("txtOpeFiter").setValue("").data("SelectedKey", "");
                            $.UIbyID("txtMatFiter").setValue("").data("SelectedKey", "");
                            $.UIbyID("txtDrawFiter").setValue("").data("SelectedKey", "");
                            $.UIbyID("filtTypeConf").setSelectedKeys([]);
                            $.UIbyID("txtPoperFiter").setValue("").data("SelectedKey", "");
                            $.UIbyID("txtCdlFiter").setValue("").data("SelectedKey", "");
                        }
                    }),
                    new sap.ui.commons.Button("btnRefresh",{
                        icon: "sap-icon://refresh",
                        text : oLng_Analysis.getText("Analysis_Refresh"), //"Aggiorna",
                        tooltip : oLng_Analysis.getText("Analysis_ResultRefresh"), //"Aggiorna risultato",
                        press: function() {
                            renderTable();
                        }
                    })
                ]
            });
            */
            
            var oVLayout = new sap.ui.commons.layout.VerticalLayout({
                width: "100%",
                content: [
                    oToolbar1,
                    //oToolbar2,
                    //oToolbar3
                ]
            });
            
            $("#splash-screen").hide();
            oVLayout.placeAt("Toolbar");
            
            renderTable();
        });
    }
)

function renderTable(){
	var dateFormat = $.pivotUtilities.derivers.dateFormat;
	var sortAs =           $.pivotUtilities.sortAs;

	$("#splash-screen").show();
	var P1 = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	var P2 = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
    //var sSelectedOpe = ($.UIbyID("txtOpeFiter").data("SelectedKey") == null)?"":$.UIbyID("txtOpeFiter").data("SelectedKey");
    //var sSelectedMat = ($.UIbyID("txtMatFiter").data("SelectedKey") == null)?"":$.UIbyID("txtMatFiter").data("SelectedKey");
    //var sSelectedDraw = ($.UIbyID("txtDrawFiter").data("SelectedKey") == null)?"":$.UIbyID("txtDrawFiter").data("SelectedKey");
    //var sSelectedCictxt = ($.UIbyID("txtPoperFiter").data("SelectedKey") == null)?"":$.UIbyID("txtPoperFiter").data("SelectedKey");
    //var sSelectedCdltxt = ($.UIbyID("txtCdlFiter").data("SelectedKey") == null)?"":$.UIbyID("txtCdlFiter").data("SelectedKey");
    //var iDateShiftSelected = $.UIbyID("rdbDateShift").getSelected()?1:0;
    
    var sFiltTypeConf = "[";
    var aSelectedTypeConf = $.UIbyID("filtTypeConf").getSelectedKeys();
    for(var i = 0; i < aSelectedTypeConf.length; i++){
        sFiltTypeConf = sFiltTypeConf + aSelectedTypeConf[i] + "___";
    }
    sFiltTypeConf = sFiltTypeConf.substring(0, sFiltTypeConf.length - 3);
    sFiltTypeConf = (sFiltTypeConf === "")?sFiltTypeConf:(sFiltTypeConf + "]");
    
    console.log(sFiltTypeConf);
    
    /*costruzione del jSon per le colonne opzionali della tabella*/
    var itm = {};
    
    const sMandante = oLng_Analysis.getText("Mandante"); //"Operatore"
    itm[sMandante] = function(record) {return record.MANDT;};
    /**
    const sOperatorLabel = oLng_Analysis.getText("Analysis_Operator"); //"Operatore"
    itm[sOperatorLabel] = function(record) {return record.OPENAME;};
    const sDeclarationLabel = oLng_Analysis.getText("Analysis_Declaration"); //"Dichiarazione"
    itm[sDeclarationLabel] = function(record) {return record.EXTENDED_TEXT;};
    const sConfQtyLabel = oLng_Analysis.getText("Analysis_ConfQty"); //"Qtà Conf."
    itm[sConfQtyLabel] = function(record) {return record.QTY;};
    */
    
    /**
    const sLineLabel = oLng_Analysis.getText("Analysis_Line"); //"Linea"
    itm[sLineLabel] = function(record) {return record.IDLINE;};
    const sShiftLabel = oLng_Analysis.getText("Analysis_Shift"); //"Turno"
    itm[sShiftLabel] = function(record) {return record.SHIFT;};
    const sMaterialLabel = oLng_Analysis.getText("Analysis_Material"); //"Materiale"
    itm[sMaterialLabel] = function(record) {return record.MATERIAL;};
    const sMaterialDescLabel = oLng_Analysis.getText("Analysis_MaterialDesc"); //"Desc.Materiale"
    itm[sMaterialDescLabel] = function(record) {return record.DESC;};
    const sOrderLabel = oLng_Analysis.getText("Analysis_Order"); //"Commessa"
    itm[sOrderLabel] = function(record) {return Number(record.ORDER);};
    const sPoperLabel = oLng_Analysis.getText("Analysis_Operation"); //"Fase"
    itm[sPoperLabel] = function(record) {return record.POPER;};
    const sPoperDescLabel = oLng_Analysis.getText("Analysis_OperationDesc"); //"Desc.Fase"
    itm[sPoperDescLabel] = function(record) {return record.CICTXT;};
    const sCdlLabel = oLng_Analysis.getText("Analysis_CdlShort"); //"CdL"
    itm[sCdlLabel] = function(record) {return record.CDLID;};
    const sCdlDescLabel = oLng_Analysis.getText("Analysis_CdlShortDesc"); //"Desc. CdL"
    itm[sCdlDescLabel] = function(record) {return record.CDLDESC;};
    const sBasketTTLabel = oLng_Analysis.getText("Analysis_BasketTT"); //"CestaTT"
    itm[sBasketTTLabel] = function(record) {return record.CESTA_TT;};
    const sTimeMinsLabel = oLng_Analysis.getText("Analysis_TimeMins"); //"Tempo (Mins)"
    itm[sTimeMinsLabel] = function(record) {return record.WTDUR;};
    const sNPrintLabel = oLng_Analysis.getText("Analysis_NPrint"); //"N.Stampo"
    itm[sNPrintLabel] = function(record) {return record.NRPRNT;};
    const sDataryLabel = oLng_Analysis.getText("Analysis_Datary"); //"Datario"
    itm[sDataryLabel] = function(record) {return record.DATARIO;};
    const sBadgeLabel = oLng_Analysis.getText("Analysis_Badge"); //"Matricola"
    itm[sBadgeLabel] = function(record) {return record.OPEID;};
    const sOperatorLabel = oLng_Analysis.getText("Analysis_Operator"); //"Operatore"
    itm[sOperatorLabel] = function(record) {return record.OPENAME;};
    const sDrawLabel = oLng_Analysis.getText("Analysis_Draw"); //"Disegno"
    itm[sDrawLabel] = function(record) {return record.DISEGNO;};
    const sUdcScrapLabel = oLng_Analysis.getText("Analysis_UdcScrap"); //"Stato UdC"
    itm[sUdcScrapLabel] = function(record) {return record.UDC_STATE;};
    const sStopScrapCauseLabel = oLng_Analysis.getText("Analysis_StopScrapCause"); //"Caus.Sosp.Scarto"
    itm[sStopScrapCauseLabel] = function(record) {return record.SREASTXT;};
    const sStopScrapCodeLabel = oLng_Analysis.getText("Analysis_StopScrapCode"); //"Cod.Sosp.Scarto"
    itm[sStopScrapCodeLabel] = function(record) {return record.SREASID;};
    const sUdcStatusLabel = oLng_Analysis.getText("Analysis_UdcStatus"); //"Stato UdC"
    itm[sUdcStatusLabel] = function(record) {return record.UDC_STATE;};
    const sUdcNumberLabel = oLng_Analysis.getText("Analysis_UdcNumber"); //"N.UdC"
    itm[sUdcNumberLabel] = function(record) {return record.UDCNR;};
    const sDeclarationLabel = oLng_Analysis.getText("Analysis_Declaration"); //"Dichiarazione"
    itm[sDeclarationLabel] = function(record) {return record.EXTENDED_TEXT;};
    const sConfQtyLabel = oLng_Analysis.getText("Analysis_ConfQty"); //"Qtà Conf."
    itm[sConfQtyLabel] = function(record) {return record.QTY;};
    const sUdcQtyLabel = oLng_Analysis.getText("Analysis_UdcQty"); //"Qtà UdC"
    itm[sUdcQtyLabel] = function(record) {return record.QTYUDC;};
    const sDateShiftYearLabel = oLng_Analysis.getText("Analysis_DateShiftYear"); //"Anno (data turno)"
    itm[sDateShiftYearLabel] = dateFormat("DATE_SHIFT", "%y", true);
    const sDateShiftMonthLabel = oLng_Analysis.getText("Analysis_DateShiftMonth"); //"Mese (data turno)"
    itm[sDateShiftMonthLabel] = dateFormat("DATE_SHIFT", "%m", true);
    const sDateShiftCompleteDateLabel = oLng_Analysis.getText("Analysis_DateShiftCompleteDate"); //"Data completa (data turno)"
    itm[sDateShiftCompleteDateLabel] = dateFormat("DATE_SHIFT", "%d/%m/%y", true);
    const sDateShiftDayLabel = oLng_Analysis.getText("Analysis_DateShiftDay"); //"Giorno (data turno)"
    itm[sDateShiftDayLabel] = dateFormat("DATE_SHIFT", "%d", true);
    const sDateShiftMonthNameLabel = oLng_Analysis.getText("Analysis_DateShiftMonthName"); //"Nome mese (data turno)"
    */
    
    //itm[sDateShiftMonthNameLabel] = dateFormat(
    //    "DATE_SHIFT", "%n", true,[
    //        oLng_Analysis.getText("Analysis_JanuaryShort")/*"Gen"*/,oLng_Analysis.getText("Analysis_FebruaryShort")/*"Feb"*/,
    //        oLng_Analysis.getText("Analysis_MarchShort")/*"Mar"*/,oLng_Analysis.getText("Analysis_AprilShort")/*"Apr"*/,
    //        oLng_Analysis.getText("Analysis_MayShort")/*"Mag"*/,oLng_Analysis.getText("Analysis_JuneShort")/*"Giu"*/,
    //        oLng_Analysis.getText("Analysis_JulyShort")/*"Lug"*/,oLng_Analysis.getText("Analysis_AugustShort")/*"Ago"*/,
    //        oLng_Analysis.getText("Analysis_SeptemberShort")/*"Set"*/,oLng_Analysis.getText("Analysis_OctoberShort")/*"Ott"*/,
    //        oLng_Analysis.getText("Analysis_NovemberShort")/*"Nov"*/,oLng_Analysis.getText("Analysis_DecemberShort")/*"Dic"*/
    //    ]
    //);
    //
    //const sDateShiftExtMonthNameLabel = oLng_Analysis.getText("Analysis_DateShiftExtMonthName"); //"Nome mese est. (data turno)"
    //itm[sDateShiftExtMonthNameLabel] = dateFormat(
    //    "DATE_SHIFT", "%n", true,[
    //        oLng_Analysis.getText("Analysis_January")/*"Gennaio"*/,oLng_Analysis.getText("Analysis_February")/*"Febbraio"*/,
    //        oLng_Analysis.getText("Analysis_March")/*"Marzo"*/,oLng_Analysis.getText("Analysis_April")/*"Aprile"*/,
    //        oLng_Analysis.getText("Analysis_May")/*"Maggio"*/,oLng_Analysis.getText("Analysis_June")/*"Giugno"*/,
    //        oLng_Analysis.getText("Analysis_July")/*"Luglio"*/,oLng_Analysis.getText("Analysis_August")/*"Agosto"*/,
    //        oLng_Analysis.getText("Analysis_September")/*"Settembre"*/,oLng_Analysis.getText("Analysis_October")/*"Ottobre"*/,
    //        oLng_Analysis.getText("Analysis_November")/*"Novembre"*/,oLng_Analysis.getText("Analysis_December")/*"Dicembre"*/
    //    ]
    //);
    //const sDateShiftDayNameLabel = oLng_Analysis.getText("Analysis_DateShiftDayName"); //"Nome giorno (data turno)"
    //itm[sDateShiftDayNameLabel] = dateFormat(
    //    "DATE_SHIFT", "%w", true,[
    //        oLng_Analysis.getText("Analysis_MondayShort")/*"Lun"*/,oLng_Analysis.getText("Analysis_TuesdayShort")/*"Mar"*/,
    //        oLng_Analysis.getText("Analysis_WednesdayShort")/*"Mer"*/,oLng_Analysis.getText("Analysis_ThursdayShort")/*"Gio"*/,
    //        oLng_Analysis.getText("Analysis_FridayShort")/*"Ven"*/,oLng_Analysis.getText("Analysis_SaturdayShort")/*"Sab"*/,
    //        oLng_Analysis.getText("Analysis_SundayShort")/*"Dom"*/
    //    ]
    //);
    //const sDateShiftWeekLabel = oLng_Analysis.getText("Analysis_DateShiftWeek"); //"Settimana (data turno)"
    //itm[sDateShiftWeekLabel] = dateFormat("DATE_SHIFT", "%k", true);
    //const sDateShiftYearWeekLabel = oLng_Analysis.getText("Analysis_DateShiftYearWeek"); //"Settimana-Anno (data turno)"    
    //itm[sDateShiftYearWeekLabel] = dateFormat("DATE_SHIFT", "%K", true);
    //
    //const sDateRegYearLabel = oLng_Analysis.getText("Analysis_DateRegYear"); //"Anno (data registrazione)"
    //itm[sDateRegYearLabel] = dateFormat("DATA_REG", "%y", true);
    //const sDateRegMonthLabel = oLng_Analysis.getText("Analysis_DateRegMonth"); //"Mese (data registrazione)"
    //itm[sDateRegMonthLabel] = dateFormat("DATA_REG", "%m", true);
    //const sDateRegCompleteDateLabel = oLng_Analysis.getText("Analysis_DateRegCompleteDate"); //"Data completa (data registrazione)"
    //itm[sDateRegCompleteDateLabel] = dateFormat("DATA_REG", "%d/%m/%y", true);
    //const sDateRegDayLabel = oLng_Analysis.getText("Analysis_DateRegDay"); //"Giorno (data registrazione)"
    //itm[sDateRegDayLabel] = dateFormat("DATA_REG", "%d", true);
    //const sDateRegMonthNameLabel = oLng_Analysis.getText("Analysis_DateRegMonthName"); //"Nome mese (data registrazione)"
    //itm[sDateRegMonthNameLabel] = dateFormat(
    //    "DATA_REG", "%n", true,[
    //        oLng_Analysis.getText("Analysis_JanuaryShort")/*"Gen"*/,oLng_Analysis.getText("Analysis_FebruaryShort")/*"Feb"*/,
    //        oLng_Analysis.getText("Analysis_MarchShort")/*"Mar"*/,oLng_Analysis.getText("Analysis_AprilShort")/*"Apr"*/,
    //        oLng_Analysis.getText("Analysis_MayShort")/*"Mag"*/,oLng_Analysis.getText("Analysis_JuneShort")/*"Giu"*/,
    //        oLng_Analysis.getText("Analysis_JulyShort")/*"Lug"*/,oLng_Analysis.getText("Analysis_AugustShort")/*"Ago"*/,
    //        oLng_Analysis.getText("Analysis_SeptemberShort")/*"Set"*/,oLng_Analysis.getText("Analysis_OctoberShort")/*"Ott"*/,
    //        oLng_Analysis.getText("Analysis_NovemberShort")/*"Nov"*/,oLng_Analysis.getText("Analysis_DecemberShort")/*"Dic"*/
    //    ]
    //);
    //const sDateRegExtMonthNameLabel = oLng_Analysis.getText("Analysis_DateRegExtMonthName"); //"Nome mese est. (data registrazione)"
    //itm[sDateRegExtMonthNameLabel] = dateFormat(
    //    "DATA_REG", "%n", true,[
    //        oLng_Analysis.getText("Analysis_January")/*"Gennaio"*/,oLng_Analysis.getText("Analysis_February")/*"Febbraio"*/,
    //        oLng_Analysis.getText("Analysis_March")/*"Marzo"*/,oLng_Analysis.getText("Analysis_April")/*"Aprile"*/,
    //        oLng_Analysis.getText("Analysis_May")/*"Maggio"*/,oLng_Analysis.getText("Analysis_June")/*"Giugno"*/,
    //        oLng_Analysis.getText("Analysis_July")/*"Luglio"*/,oLng_Analysis.getText("Analysis_August")/*"Agosto"*/,
    //        oLng_Analysis.getText("Analysis_September")/*"Settembre"*/,oLng_Analysis.getText("Analysis_October")/*"Ottobre"*/,
    //        oLng_Analysis.getText("Analysis_November")/*"Novembre"*/,oLng_Analysis.getText("Analysis_December")/*"Dicembre"*/
    //    ]
    //);
    //const sDateRegDayNameLabel = oLng_Analysis.getText("Analysis_DateRegDayName"); //"Nome giorno (data registrazione)"
    //itm[sDateRegDayNameLabel] = dateFormat(
    //    "DATA_REG", "%w", true,[
    //        oLng_Analysis.getText("Analysis_MondayShort")/*"Lun"*/,oLng_Analysis.getText("Analysis_TuesdayShort")/*"Mar"*/,
    //        oLng_Analysis.getText("Analysis_WednesdayShort")/*"Mer"*/,oLng_Analysis.getText("Analysis_ThursdayShort")/*"Gio"*/,
    //        oLng_Analysis.getText("Analysis_FridayShort")/*"Ven"*/,oLng_Analysis.getText("Analysis_SaturdayShort")/*"Sab"*/,
    //        oLng_Analysis.getText("Analysis_SundayShort")/*"Dom"*/
    //    ]
    //);
    //const sDateRegWeekLabel = oLng_Analysis.getText("Analysis_DateRegWeek"); //"Settimana (data registrazione)"
    //itm[sDateRegWeekLabel] = dateFormat("DATA_REG", "%k", true);
    //const sDateRegYearWeekLabel = oLng_Analysis.getText("Analysis_DateRegYearWeek"); //"Settimana-Anno (data registrazione)"    
    //itm[sDateRegYearWeekLabel] = dateFormat("DATA_REG", "%K", true);
    //
    var aColumns = [];
    /**
    if(iDateShiftSelected){
        aColumns.push(sDateShiftDayLabel);
        aColumns.push(sShiftLabel);
    }
    else{
        aColumns.push(sDateRegDayLabel);
    }
    */
    
	var PivotPar = {
		PlaceAt: "#Analisys",
		Query: {
			Name: "getProductionEventsFromMIIQR",
			Path: QPrefix + "ProductionMain/Report/Pivot/",
            
            Params: "&Param.1=" + P1 + "&Param.2=" + P2 
                    //"&Param.7=" + sSelectedOpe + 
                    //"&Param.8=" + sSelectedMat +
                    //"&Param.9=" + sSelectedDraw + 
                    //"&Param.10=" + sSelectedCictxt +
                    //"&Param.11=" + sSelectedCdltxt + 
                    //"&Param.12=" + iDateShiftSelected +
                    //"&Param.13=" + sLanguage + "&RowCount=50000"
                
		},
		Pivot: {
			Rows: [], // [sOperatorLabel, sDeclarationLabel],
			Cols: aColumns,
			Vals: [], //[sConfQtyLabel],
			//HiddenAttributes:["DATE_SHIFT","IDLINE","SHIFT","MATERIAL","MAKTX","DESC","ORDER","POPER","CICTXT","CDLID","CDLDESC","CESTA_TT","WTDUR","NRPNT",
//												"DATARIO","OPENAME","DISEGNO","UDCNR","QTY","OPEID","UDCNR","EXTENDED_TEXT","TYPECONF","SREASID","QTY_PRO","PLANT","PZUDC",
//												"DELETED","CONFIRMED","USERMOD","TYPEMOD","NRPRNT","UMDUR","MII_GUID","SAP_GUID","MII_GUID_ST","NOTEMOD","EXTENDED_TEXT",
//												"TIME_ID_CALCULATED","UDC_STATE","EXID","QTYUDC","SREASID","LINETXT","TIME_ID","DATE_UPD","SREASTXT","ERROR","CURRENT_QTY","DATE_UPD","DATA_REG"],
            
            HiddenAttributes:[
                 "MANDT"
                ,"ZZDATETIME"
                ,"ZZLINEIDENTITY"
                ,"ZZORDER"
                ,"ZZTIMETO"
                ,"ZZDURATION"
                ,"ZZTIMEUNIT"
                ,"ZZEVENTTYPE"
                ,"ZZALARMID"
                ,"ZZOPERATORID"
                ,"ZZGRP1"
                ,"ZZGRP2"
                ,"ZZGRP3" 
            ],
            
			DerivedAttributes: itm /*{
				sLineLabel: function(record) {return record.IDLINE;},
				sShiftLabel: function(record) {return record.SHIFT;},
				sMaterialLabel: function(record) {return record.MATERIAL;},
				sMaterialDescLabel: function(record) {return record.DESC;},
				//"Divisione": function(record) {return record.NAME1;},
				sOrderLabel: function(record) {return Number(record.ORDER);},
				sPoperLabel: function(record) {return record.POPER;},
				sPoperDescLabel: function(record) {return record.CICTXT;},
				sCdlLabel: function(record) {return record.CDLID;},
				sCdlDescLabel: function(record) {return record.CDLDESC;},
				sBasketTTLabel: function(record) {return record.CESTA_TT;},
				sTimeMinsLabel: function(record) {return record.WTDUR;},
				sNPrintLabel: function(record) {return record.NRPRNT;},
				sDataryLabel: function(record) {return record.DATARIO;},
				sBadgeLabel: function(record) {return record.OPEID;},
				sOperatorLabel: function(record) {return record.OPENAME;},
				sDrawLabel: function(record) {return record.DISEGNO;},
				sUdcScrapLabel: function(record) {return record.UDC_STATE;},
				sStopScrapCauseLabel: function(record) {return record.SREASTXT;},
				sStopScrapCodeLabel: function(record) {return record.SREASID;},
				sUdcStatusLabel: function(record) {return record.UDC_STATE;},
				sUdcNumberLabel: function(record) {return record.UDCNR;},
				sDeclarationLabel: function(record) {return record.EXTENDED_TEXT;},
				sConfQtyLabel: function(record) {return record.QTY;},
				sUdcQtyLabel: function(record) {return record.QTYUDC;},
				sDateShiftYearLabel:    dateFormat("DATE_SHIFT", "%y", true),
				sDateShiftMonthLabel:      dateFormat("DATE_SHIFT", "%m", true),
				sDateShiftCompleteDateLabel: dateFormat("DATE_SHIFT", "%d/%m/%y", true),
				sDateShiftDayLabel:        dateFormat("DATE_SHIFT", "%d", true),
				sDateShiftMonthNameLabel: dateFormat("DATE_SHIFT", "%n", true,["Gen","Feb","Mar","Apr", "Mag","Giu","Lug","Ago","Set","Ott","Nov","Dic"]),
				sDateShiftExtMonthNameLabel: dateFormat("DATE_SHIFT", "%n", true,["Gennaio","Febbraio","Marzo","Aprile", "Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"]),
				sDateShiftDayNameLabel:   dateFormat("DATE_SHIFT", "%w", true,["Lun","Mar","Mer", "Gio","Ven","Sab","Dom"]),
				sDateShiftWeekLabel:        dateFormat("DATE_SHIFT", "%k", true),
				sDateShiftYearWeekLabel:        dateFormat("DATE_SHIFT", "%K", true)
			}*/
		}
	};
    
    console.log(PivotPar.Query);

	renderTablePivot(PivotPar);

	$("#splash-screen").hide();

};

function setSelectedOperator(oControlEvent){
	/* SINGLE SELECT
    var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	var sBadge = oControlEvent.getSource().getModel().getProperty("PERNR", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);*/
    
	/* MULTIPLE SELECT */
    var oSelContext =  oControlEvent.getParameter("selectedContexts");
    var sBadge = "[";
	var sOpeName = "";
    
    for(var i = 0; i < oSelContext.length; i++){
        sBadge = sBadge + oControlEvent.getSource().getModel().getProperty("PERNR", oSelContext[i]) + "___";
        sOpeName = sOpeName + oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext[i]) + "; ";
    }
    
    sBadge = sBadge.substring(0, sBadge.length - 3);
    sBadge = sBadge + "]";
    sOpeName = sOpeName.substring(0, sOpeName.length - 2);
    
	$.UIbyID("txtOpeFiter").setValue(sOpeName).data("SelectedKey", sBadge);
	$.UIbyID("dlgOperatorTableMultipleSelect").destroy();
	//$.UIbyID("delFilter").setEnabled(true);
}

function setSelectedMaterial(oControlEvent){
	/* SINGLE SELECT
    var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];

	var sMaterial = oControlEvent.getSource().getModel().getProperty("MATERIAL", oSelContext);
	var sDesc = oControlEvent.getSource().getModel().getProperty("DESC", oSelContext);
    if(sDesc == "" || sDesc == null)
        sDesc = sMaterial;*/
    
    /* MULTIPLE SELECT */
    var oSelContext =  oControlEvent.getParameter("selectedContexts");
    var sMaterial = "[";
    var sDesc = "";
    var sDescTMP = "";
    
    for(var i = 0; i < oSelContext.length; i++){
        sMaterial = sMaterial + oControlEvent.getSource().getModel().getProperty("MATERIAL", oSelContext[i]) + "___";
        sDescTMP = oControlEvent.getSource().getModel().getProperty("DESC", oSelContext[i]);
        if(sDescTMP == "" || sDescTMP == null)
            sDescTMP = oControlEvent.getSource().getModel().getProperty("MATERIAL", oSelContext[i]);
        sDesc = sDesc + sDescTMP + "; ";
    }
    
    sMaterial = sMaterial.substring(0, sMaterial.length - 3);
    sMaterial = sMaterial + "]";
    sDesc = sDesc.substring(0, sDesc.length - 2);

	$.UIbyID("txtMatFiter").setValue(sDesc).data("SelectedKey", sMaterial);
	$.UIbyID("dlgMaterialTableSelect").destroy();
	//$.UIbyID("delFilter").setEnabled(true);
}

function setSelectedDraw(oControlEvent){
    /* SINGLE SELECT
	var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	var sDraw = oControlEvent.getSource().getModel().getProperty("DISEGNO", oSelContext);*/
    
    /* MULTIPLE SELECT */
    var oSelContext =  oControlEvent.getParameter("selectedContexts");
    var sDraw = "[";
    var sDrawDesc = "";
    for(var i = 0; i < oSelContext.length; i++){
        sDraw = sDraw + oControlEvent.getSource().getModel().getProperty("DISEGNO", oSelContext[i]) + "___";
        sDrawDesc = sDrawDesc + oControlEvent.getSource().getModel().getProperty("DISEGNO", oSelContext[i]) + "; ";
    }
    
    sDraw = sDraw.substring(0, sDraw.length - 3);
    sDraw = sDraw + "]";
    sDrawDesc = sDrawDesc.substring(0, sDraw.length - 2);
    
	$.UIbyID("txtDrawFiter").setValue(sDrawDesc).data("SelectedKey", sDraw);
	$.UIbyID("dlgMaterialTableSelect").destroy();
	//$.UIbyID("delFilter").setEnabled(true);
}

function setSelectedOperation(oControlEvent){
    /* SINGLE SELECT
	var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	var sCicTxt = oControlEvent.getSource().getModel().getProperty("CICTXT", oSelContext);*/
    
    /* MULTIPLE SELECT */
    var oSelContext =  oControlEvent.getParameter("selectedContexts");
    var sCicTxt = "[";
    var sCicTxtDesc = "";
    for(var i = 0; i < oSelContext.length; i++){
        sCicTxt = sCicTxt + oControlEvent.getSource().getModel().getProperty("POPER", oSelContext[i]) + "___";
        sCicTxtDesc = sCicTxtDesc + oControlEvent.getSource().getModel().getProperty("CICTXT", oSelContext[i]) + "; ";
    }
    
    sCicTxt = sCicTxt.substring(0, sCicTxt.length - 3);
    sCicTxt = sCicTxt + "]";
    sCicTxtDesc = sCicTxtDesc.substring(0, sCicTxtDesc.length - 2);
    console.log("poper: " + sCicTxt + " - cictxt: " + sCicTxtDesc);
    
	$.UIbyID("txtPoperFiter").setValue(sCicTxtDesc).data("SelectedKey", sCicTxt);
	$.UIbyID("dlgMcicdTableSelect").destroy();
	//$.UIbyID("delFilter").setEnabled(true);
}

function setSelectedCdl(oControlEvent){
    /* SINGLE SELECT
	var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	var sCdl = oControlEvent.getSource().getModel().getProperty("CDLDESC", oSelContext); */
    
    /* MULTIPLE SELECT */
    var oSelContext =  oControlEvent.getParameter("selectedContexts");
    var sCdl = "[";
    var sCdlDesc = "";
    for(var i = 0; i < oSelContext.length; i++){
        sCdl = sCdl + oControlEvent.getSource().getModel().getProperty("CDLID", oSelContext[i]) + "___";
        console.log("cdldesc: " + oControlEvent.getSource().getModel().getProperty("CDLDESC", oSelContext[i]));
        sCdlDesc = sCdlDesc + oControlEvent.getSource().getModel().getProperty("CDLDESC", oSelContext[i]) + "; ";
    }
    
    sCdl = sCdl.substring(0, sCdl.length - 3);
    sCdl = sCdl + "]";
    sCdlDesc = sCdlDesc.substring(0, sCdlDesc.length - 2);
    console.log("cdl: " + sCdl + " - cdldesc: " + sCdlDesc);
    
	$.UIbyID("txtCdlFiter").setValue(sCdlDesc).data("SelectedKey", sCdl);;
	$.UIbyID("dlgMcicdTableSelect").destroy();
	//$.UIbyID("delFilter").setEnabled(true);
}