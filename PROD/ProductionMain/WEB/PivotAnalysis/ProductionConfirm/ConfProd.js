//**************************************************************************************
//Title:  Analisi conferme produzione v.2
//Author: Antonio Masseretti
//Date:   03/06/2019
//Vers:   1.0
//**************************************************************************************
// Script per pagina Analisi conferme produzione v.2

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var QPrefix = "Content-Type=text/XML&QueryTemplate=";
var dataConfProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/Report/Pivot/";
var icon16 = "/XMII/CM/Common/icons/16x16/";
var dateSelFrom = "";
var dateSelTo = "";
var dtFormat = "";
var dtSelect = "";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Analysis = jQuery.sap.resources({
url: "/XMII/CM/ProductionMain/" + "PivotAnalysis/" + "res/Analysis_res.i18n.properties",
locale: sLanguage
});

jQuery.sap.require("sap.ui.core.format.DateFormat");
/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
Libraries.load(
[
       "/XMII/CM/Common/MII_core/UI5_utils",
       //"/XMII/CM/ProductionMain/MasterData/Operators_fn/fn_operatorTools",
       //"/XMII/CM/ProductionMain/MasterData/Material_fn/fn_materialTools",
       //"/XMII/CM/ProductionMain/MasterData/Material_fn/fn_mcicdTools"
   ],
   
        
    function () {
        $(document).ready(function () {
            /*************************************************************/
            // Creo la tool bar
            /*************************************************************/
            dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: "yyyyMMdd"
            });
            dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: "yyyy-MM-dd"
            });
            
    
            
      
            
            // creazione delle due DtPicker
            var yesterday = new Date();
            yesterday.setDate( yesterday.getDate() - 1 );
            var ToDay = new Date();
            
            var  year = yesterday.getUTCFullYear();
            var  month = yesterday.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            var  day = yesterday.getUTCDate();
            if( day.toString().length == 1 ) day = "0" + day;
            dateSelFrom = year + "-" + month+ "-" + day;
            dOrigDateSelFtrom = dateSelFrom;
            
            var fromday = new Date(yesterday.getFullYear(), yesterday.getMonth() + 1, 0);
            year = ToDay.getUTCFullYear();
            month = ToDay.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            day = ToDay.getUTCDate();            
            dateSelTo = year + "-" + month+ "-" + day;
            dOrigDateSelTo = dateSelTo;
            
            odtFrom = new sap.ui.commons.DatePicker('dtFrom');
            odtFrom.setYyyymmdd( dateSelFrom );
            odtFrom.setLocale("it"); // Try with "de" or "fr" instead!
            odtFrom.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelFrom = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                }
            );
            
            odtTo = new sap.ui.commons.DatePicker('dtTo');
            odtTo.setYyyymmdd( dateSelTo );
            odtTo.setLocale("it"); // Try with "de" or "fr" instead!
            odtTo.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelTo = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                    //refreshTabAnalys();
                }
            );
            
            
            var oToolbar1 = new sap.ui.commons.Toolbar("tb1",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
                items: [
                  
                    new sap.ui.commons.Label("lFrom",{
                        text : oLng_Analysis.getText("Analysis_FromDate"), //"Da data:",
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") //"Seleziona l'intervallo di date"
                    }),
                    odtFrom,
                    new sap.ui.commons.Label("lTo",{
                        text : oLng_Analysis.getText("Analysis_ToDate"), //"a data:",
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") //"Seleziona l'intervallo di date"
                    }),
                    odtTo
                
                 
                ],
                rightItems: [
                    new sap.ui.commons.Button("btnToogle",{
                        icon: "sap-icon://resize",
                        //		 text : "PNG",
                        tooltip : oLng_Analysis.getText("Analysis_Show_HideToolbar"), //"Mostra/Nascondi barra strumenti",
                        press: function() {
                            $(".pvtAxisContainer, .pvtVals").toggle();
                        }
                    }),
                    new sap.ui.commons.Button("btnXls",{
                        icon: "/XMII/CM/Common/icons/document/xls_16.png",
                        //text : "XLS",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToExcel"), //"Esporta in excel",
                        press: function() {
                            exportTo("xls");
                        }
                    }),
                    new sap.ui.commons.Button("btnDoc",{
                        icon: "/XMII/CM/Common/icons/document/word_16.png",
                        //	text : "CSV",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToWord"), //"Esporta in Word",
                        press: function() {
                            exportTo("doc");
                        }
                    }),
                    new sap.ui.commons.Button("pdf",{
                        icon: "/XMII/CM/Common/icons/document/pdf_16.png",
                        //		 text : "PNG",
                        tooltip : oLng_Analysis.getText("Analysis_PrintPdf"), //"Stampa in PDF",
                        press: function() {
                            exportTo("pdf");
                        }
                    })
                ]
            });
            
          
            
            var oToolbar3 = new sap.ui.commons.Toolbar("tb3",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
               
                rightItems: [
                    new sap.ui.commons.Button("delFilter",{
                        icon: "sap-icon://filter",
                        enabled: true,
                        text : oLng_Analysis.getText("Analysis_RemoveFilters"), //"Rimuovi filtri"
                        tooltip: oLng_Analysis.getText("Analysis_RemoveFilters"), //"Rimuovi filtri"
                        press: function () {
                            $.UIbyID("cmbPlants").setSelectedKey("");
                            odtFrom.setYyyymmdd(dOrigDateSelFtrom);
                            odtTo.setYyyymmdd(dOrigDateSelTo);
                                                  }
                    }),
                    new sap.ui.commons.Button("btnRefresh",{
                        icon: "sap-icon://refresh",
                        text : oLng_Analysis.getText("Analysis_Refresh"), //"Aggiorna",
                        tooltip : oLng_Analysis.getText("Analysis_ResultRefresh"), //"Aggiorna risultato",
                        press: function() {
                            renderTable();
                        }
                    })
                ]
            });
            
            var oVLayout = new sap.ui.commons.layout.VerticalLayout({
                width: "100%",
                content: [
                    oToolbar1,
                     oToolbar3
                ]
            });
            
            $("#splash-screen").hide();
            oVLayout.placeAt("Toolbar");
            
            renderTable();
        });
    }
)

function renderTable(){
	var dateFormat = $.pivotUtilities.derivers.dateFormat;
	var sortAs =           $.pivotUtilities.sortAs;

	$("#splash-screen").show();
	var P1 = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	var P2 = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));

    
   
    
    /*costruzione del jSon per le colonne opzionali della tabella*/
    var itm = {};
    const sLineLabel = oLng_Analysis.getText("Analysis_Line"); //"Linea"
	
	console.log(sLineLabel);
	
	console.log(function(record) {return record.ZZLINEIDENTITY});
	
    itm[sLineLabel] = function(record) {return record.ZZLINEIDENTITY};
    const sMaterialLabel = oLng_Analysis.getText("Analysis_Material"); //"Materiale"
    itm[sMaterialLabel] = function(record) {return record.ZZMATERIAL;};
    const sMaterialDescLabel = oLng_Analysis.getText("Analysis_MaterialDesc");  //"Descrizione Materiale"
    itm[sMaterialDescLabel] = function(record) {return record.ZZMATDESCRIPTION;};
    const sOrderLabel = oLng_Analysis.getText("Analysis_Order"); //"Ordine"
    itm[sOrderLabel] = function(record) {return Number(record.ZZORDER);};
    const sPoperLabel = oLng_Analysis.getText("Analysis_Operation"); //"Fase"
    itm[sPoperLabel] = function(record) {return record.ZZOPERATION;};


    const sOperatorLabel = oLng_Analysis.getText("Analysis_Operator"); //"Operatore"
    itm[sOperatorLabel] = function(record) {return record.ZZOPERATORID;};

    const sQtyGoodLabel = oLng_Analysis.getText("Analysis_QtyGood"); //"QtyGood"
    itm[ sQtyGoodLabel ] = function(record) {return record.ZZGOODQTY;};

  
    
    var aColumns = [];

    
	var PivotPar = {
		PlaceAt: "#Analisys",
		Query: {
			Name: "getProductionConfirmFromMIIQR",
			Path: QPrefix + "ProductionMain/Report/Pivot/",
			Params: "&Param.2=" + P1 + "&Param.3=" + P2 + 
          	        "&RowCount=50000"
		},
		Pivot: {
			Rows: [sQtyGoodLabel],
			Cols: aColumns,
			Vals:[sQtyGoodLabel],
			HiddenAttributes:[ "ZZDATETIMESTR","ZZLINEIDENTITY","ZZMATERIAL","ZZMATDESCRIPTION","ZZORDER","ZZOPERATION","ZZDATETIMELST","ZZORDERQTY","ZZGOODQTY","ZZSCRAPQTY","ZZLOADUNIT",
						"ZZOPERATORID","ZZFIRST","ZZNRCOLLAUDO","ZZTEMPOCICLO","DATA_START","DATA_ULTIMO_VERSAMENTO"
					],
			DerivedAttributes:itm,

			
		}
	};
    
    console.log(PivotPar.Query);

	renderTablePivot(PivotPar);

	$("#splash-screen").hide();

};


