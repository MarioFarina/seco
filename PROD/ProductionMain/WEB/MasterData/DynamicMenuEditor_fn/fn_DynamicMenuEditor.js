var oPlantFOperator;
var oCmbCID;

//**************************************************************************************
//Title:  Creazione dinamica del Menù custom
//Author: Bruno Rosati
//Date:   03/05/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina 'Creazione dinamica del Menù custom'


// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabDynamicMenuEditor() {
    
    /*
            Questo è stato abbandonato perché ora abbiamo i testi nella tabella MTEXTS
    var jType = {root:[]};
    jType.root.push({
        key: 'M',
        text: 'Mobile'
    });
    jType.root.push({
        key: 'D',
        text: 'Desktop'
    });
  
    var oTypeModel = new sap.ui.model.json.JSONModel();
    oTypeModel.setData(jType);
    console.log(jType);
    */
    
    var oTypeModel = new sap.ui.model.xml.XMLModel();
	oTypeModel.loadData(QService + dataProdMD + "Functions/getFunctionsTypeSQ&Param.1=" + sLanguage);
        
    // Crea la ComboBox per selezionare il tipo delle funzioni da visualizzare
    var oCmbTypesFilter = new sap.ui.commons.ComboBox({
        id: "CmbTypesFilter", 
        selectedKey : 'D',
        change: function() {
            $.UIbyID("FunctionsTab").setSelectedIndex(-1);
            refreshTabDynamicMenuEditor();
        }
    });
    oCmbTypesFilter.setModel(oTypeModel);
    var oItemTypesFilter = new sap.ui.core.ListItem();
    oItemTypesFilter.bindProperty("text", "EXTENDED_TEXT");
	oItemTypesFilter.bindProperty("key", "ORIG_VALUE");
    //oCmbTypesFilter.bindItems("/root", oItemTypesFilter); //valido per JSONModel
	oCmbTypesFilter.bindItems("/Rowset/Row", oItemTypesFilter);
    
    //Crea L'oggetto Tabella Funzione
    var oTable = UI5Utils.init_UI5_Table({
        id: "FunctionsTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_MenuVoices"), //"Voci Menù",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("FunctionsTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModFunctions").setEnabled(false);
        			$.UIbyID("btnDelFunctions").setEnabled(false);
                    $.UIbyID("btnEditOpTypes").setEnabled(false);                    
                }
                else {
                    $.UIbyID("btnModFunctions").setEnabled(true);
                    $.UIbyID("btnDelFunctions").setEnabled(true);
                    $.UIbyID("btnEditOpTypes").setEnabled(true);
                }
            },
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_MasterData.getText("MasterData_Type") + ": ", //"Tipo",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oCmbTypesFilter
                ]
            })
        },
        exportButton: true,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddOperator",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openFunctionEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModFunctions",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openFunctionEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelFunctions",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelFunction();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_QualifiedOperators"), //"Operatori abilitati"
                id:   "btnEditOpTypes",
                icon: 'sap-icon://detail-view',         
                enabled: false,
                press: function (){
                    openOptypeEdit(fnGetCellValue("FunctionsTab", "IDFUNC"), fnGetCellValue("FunctionsTab", "NAME"), false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabDynamicMenuEditor();
                }
            })
        ],
        columns: [
			{
				Field: "IDFUNC",
				label: oLng_MasterData.getText("MasterData_FunctionID"), //"ID Funzione",
				properties: {
					width: "50px"
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "NAME",
                label: oLng_MasterData.getText("MasterData_FunctionName"), //"Nome funzione",
				properties: {
					width: "100px",
                    flexible : false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "TYPE", 
				//label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "50px",
                    visible: false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "TYPE_DESC", 
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "50px",
                    flexible : false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "LINK",
				label: oLng_MasterData.getText("MasterData_Link"), //"Link",
				properties: {
					width: "190px",
                    flexible : false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "NAV_NAME", 
				label: oLng_MasterData.getText("MasterData_NavigationName"), //"Nome navigazione",
				properties: {
					width: "80px",
                    flexible : false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "SORT", 
				label: oLng_MasterData.getText("MasterData_Sort"), //"Ordinamento",
				properties: {
					width: "60px",
                    flexible : false
				}
            },
            {
				Field: "PARENT", 
				label: oLng_MasterData.getText("MasterData_Parent"), //"Genitore",
				properties: {
					width: "60px",
                    flexible : false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "ICON", 
				label: oLng_MasterData.getText("MasterData_Icon"), //"Icona",
				properties: {
					width: "70px",
                    flexible : false
				}
            },
            {
				Field: "COLOR", 
				label: oLng_MasterData.getText("MasterData_Color"), //"Colore",
				properties: {
					width: "70px",
                    flexible : false
				}
            }
        ],
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabDynamicMenuEditor();
    return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabDynamicMenuEditor() {
    if ($.UIbyID("CmbTypesFilter").getSelectedKey() === ""){
        $.UIbyID("FunctionsTab").getModel().loadData(
            QService + dataProdMD + 
            "Functions/getFunctionsSQ&Param.1=D" + "&Param.2=" + sLanguage
        );
    }
    else{
        $.UIbyID("FunctionsTab").getModel().loadData(
            QService + dataProdMD + 
            "Functions/getFunctionsSQ&Param.1=" + $.UIbyID("CmbTypesFilter").getSelectedKey()  + "&Param.2=" + sLanguage
        );
    }
    
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Funzioni
function openFunctionEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    if(!bEdit){
        $.UIbyID("FunctionsTab").setSelectedIndex(-1);
    }
    
    var oModelSelectTypes = new sap.ui.model.xml.XMLModel();
	oModelSelectTypes.loadData(QService + dataProdMD + "Functions/getFunctionsTypeSQ" + "&Param.1=" + sLanguage);
        
    var ocmbSelectTypes = new sap.ui.commons.ComboBox({
        id: "cmbSelectTypes",
        selectedKey : bEdit?fnGetCellValue("FunctionsTab", "TYPE"):$.UIbyID("CmbTypesFilter").getSelectedKey(),
        enabled: false
    });
    ocmbSelectTypes.setModel(oModelSelectTypes);
    var oItemSelectTypes = new sap.ui.core.ListItem();
    oItemSelectTypes.bindProperty("text", "EXTENDED_TEXT");
	oItemSelectTypes.bindProperty("key", "ORIG_VALUE");	
	ocmbSelectTypes.bindItems("/Rowset/Row", oItemSelectTypes);
    
    var oModelSelectParent = new sap.ui.model.xml.XMLModel();
    var sFunctionCalling = "Functions/getFunctionParentsQR";
    if(bEdit){
        sFunctionCalling = sFunctionCalling + "&Param.1=" + fnGetCellValue("FunctionsTab", "IDFUNC");
    }
    if ($.UIbyID("CmbTypesFilter").getSelectedKey() === ""){
        sFunctionCalling = sFunctionCalling + "&Param.2=D";
    }
    else{
        sFunctionCalling = sFunctionCalling + "&Param.2=" + $.UIbyID("CmbTypesFilter").getSelectedKey();
    }
	oModelSelectParent.loadData(QService + dataProdMD + sFunctionCalling);

	// Crea la ComboBox per determinare il genitore della funzione
	var ocmbSelectParent = new sap.ui.commons.ComboBox({
        id: "cmbSelectParent",
        //layoutData: [new sap.m.FlexItemData({growFactor: 1})],
        selectedKey : bEdit?fnGetCellValue("FunctionsTab", "PARENT"):""
    });
	ocmbSelectParent.setModel(oModelSelectParent);		
	var oItemSelectParent = new sap.ui.core.ListItem();
    // oItemSelectParent.bindProperty("text", "PLNAME");
	oItemSelectParent.bindProperty("text", "NAME");
	oItemSelectParent.bindProperty("key", "IDFUNC");	
	ocmbSelectParent.bindItems("/Rowset/Row", oItemSelectParent);
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgFunction",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("FunctionsTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyFunction"):oLng_MasterData.getText("MasterData_NewFunction"),
            /*"Modifica Funzione":"Nuova Funzione",*/
            //icon: "/images/address.gif",
            /*tooltip: */
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_FunctionID"), //"ID Funzione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("FunctionsTab", "IDFUNC"):"",
                                editable: !bEdit,
                                maxLength: 15,
                                id: "IDFUNC"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_FunctionName"), //"Nome funzione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("FunctionsTab", "NAME"):"",
                                maxLength: 70,
                                id: "NAME"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            /*new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("FunctionsTab", "TYPE"):"",
                                maxLength: 5,
                                id: "TYPE"
                            })*/
                            ocmbSelectTypes
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Link"), //"Link",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("FunctionsTab", "LINK"):"",
                                maxLength: 255,
                                id: "LINK"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_NavigationName"), //"Nome navigazione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("FunctionsTab", "NAV_NAME"):"",
                                maxLength: 255,
                                id: "NAV_NAME"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Sort"), //"Ordinamento",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                value: bEdit?fnGetCellValue("FunctionsTab", "SORT"):"",
                                editable: true,
                                maxLength: 5,
                                id: "SORT",
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Parent"), //"Genitore",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            ocmbSelectParent,
                            new sap.ui.commons.Button({
                                icon: "sap-icon://filter",
                                id: "delFilter",
                                press: function () {
                                    ocmbSelectParent.setValue("");
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Icon"), //"Icona",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                value: bEdit?fnGetCellValue("FunctionsTab", "ICON"):"",
                                enabled: ($.UIbyID("CmbTypesFilter").getSelectedKey() === 'M')?true:false,
                                maxLength: 30,
                                id: "ICON"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Color"), //"Colore",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                value: bEdit?fnGetCellValue("FunctionsTab", "COLOR"):"",
                                enabled: ($.UIbyID("CmbTypesFilter").getSelectedKey() === 'M')?true:false,
                                maxLength: 15,
                                id: "COLOR"
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveFunction(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            ocmbSelectParent.destroy();
            ocmbSelectTypes.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Function per creare la finestra di dialogo Funzioni
function openOptypeEdit(idFunc, functionName, mode) {
    
    var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgAddOptypes",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    var aMOptypesXML = [];
    var aMOptypesDesc = [];
    var oMOptypesModel = new sap.ui.model.xml.XMLModel();
    oMOptypesModel.loadData(QService + dataProdMD + "Functions/EnabledFunctions/getMOptypesByIdSQ&Param.1=" + idFunc, false, false);
    aMOptypesXML = getValueFromXMLModel(oMOptypesModel, "OPTYPE");
    aMOptypesDesc = getValueFromXMLModel(oMOptypesModel, "OPDESC");
    
    var aMenabledFuncXML = [];
    var oMenabledFuncModel = new sap.ui.model.xml.XMLModel();
    oMenabledFuncModel.loadData(QService + dataProdMD + "Functions/EnabledFunctions/getMenabledFuncByIdSQ&Param.1=" + idFunc, false, false);
    aMenabledFuncXML = getValueFromXMLModel(oMenabledFuncModel, "OPTYPE");
    
    var oFormContainer = new sap.ui.layout.form.FormContainer();
    
    if(mode){
        // MasterData_FunctionSaved
        oFormContainer.addFormElement(
            new sap.ui.layout.form.FormElement({
                fields: [
                    new sap.ui.commons.Label({
                        text: oLng_MasterData.getText("MasterData_FunctionSaved"), //"Funzione salvata correttamente",
                        //layoutData: new sap.ui.layout.form.GridElementData({hCells: "6"})
                    })
                ]
            })
        );
    }
    
    oFormContainer.addFormElement(
        new sap.ui.layout.form.FormElement({
            fields: [
                new sap.ui.commons.Label({
                    text: oLng_MasterData.getText("MasterData_MoptypesFunctionAdding") + " '" + functionName + "'", //"Abilitazione operatori alla funzione",
                    //layoutData: new sap.ui.layout.form.GridElementData({hCells: "6"})
                })
            ]
        }) 
    );
    
    for(var i = 0; i < aMOptypesXML.length; i++){
        var isEnabled = false;
        if(aMenabledFuncXML.indexOf(aMOptypesXML[i]) >= 0){
            isEnabled = true;
        }
        
        var oCB = new sap.ui.commons.CheckBox({
            id: aMOptypesXML[i],
            text: aMOptypesDesc[i],
            tooltip: aMOptypesDesc[i],
            checked: isEnabled
        });
        oFormContainer.addFormElement(
            new sap.ui.layout.form.FormElement({
                fields: [
                    oCB
                ]
            }) 
        );
    }
    
   //(fnGetCellValue("FunctionsTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: oLng_MasterData.getText("MasterData_MoptypesAdding") //Abilitazione operatori
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [oFormContainer]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            saveMoptypesForFunction(idFunc, aMOptypesXML);
        }
    }));
    
    if(!mode){
        oEdtDlg.addButton(new sap.ui.commons.Button({
            text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
            press:function(){
                oEdtDlg.close();
                oEdtDlg.destroy();
            }
        }));	
    }
    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveFunction(bEdit) 
{
    var idFunc = $.UIbyID("IDFUNC").getValue(); 
    var name = $.UIbyID("NAME").getValue();
    var type = "";
    if($.UIbyID("cmbSelectTypes").getValue() != "" && $.UIbyID("cmbSelectTypes").getValue() != null){
        type = $.UIbyID("cmbSelectTypes").getSelectedKey();
    }
    var link = $.UIbyID("LINK").getValue();
    var nav_name = $.UIbyID("NAV_NAME").getValue();
    var sort = 0;
    if($.UIbyID("SORT").getValue() != "" && $.UIbyID("SORT").getValue() != null){
        sort = $.UIbyID("SORT").getValue();
    }
    var parent = "";
    if($.UIbyID("cmbSelectParent").getValue() != "" && $.UIbyID("cmbSelectParent").getValue() != null){
        parent = $.UIbyID("cmbSelectParent").getSelectedKey();
    }
    var icon = $.UIbyID("ICON").getValue();
    var color = $.UIbyID("COLOR").getValue();
    
    var qexe = "";
    
    if(bEdit){
        qexe =  dataProdMD +
            "Functions/modFunctionSQ&Param.1=" + idFunc +
            "&Param.2=" + name +
            "&Param.3=" + type +
            "&Param.4=" + link +
            "&Param.5=" + nav_name +
            "&Param.6=" + sort +
            "&Param.7=" + parent +
            "&Param.8=" + icon +
            "&Param.9=" + color;
    }
    else{
        qexe =  dataProdMD +
            "Functions/addFunctionSQ&Param.1=" + idFunc +
            "&Param.2=" + name +
            "&Param.3=" + type +
            "&Param.4=" + link +
            "&Param.5=" + nav_name +
            "&Param.6=" + sort +
            "&Param.7=" + parent +
            "&Param.8=" + icon +
            "&Param.9=" + color;
    }
    
    var ret = fnExeQuery(qexe, "",
                         oLng_MasterData.getText("MasterData_FunctionSavedError"), //"Errore salvataggio funzione"
                         false, false);
    
    /* oLng_MasterData.getText("MasterData_FunctionSaved") //Funzione salvata correttamente */
    if (ret){
        $.UIbyID("dlgFunction").close();
        $.UIbyID("dlgFunction").destroy();
        $.UIbyID("FunctionsTab").setSelectedIndex(-1);
        $.UIbyID("btnModFunctions").setEnabled(false);
        $.UIbyID("btnDelFunctions").setEnabled(false);
        $.UIbyID("btnEditOpTypes").setEnabled(false);        
        refreshTabDynamicMenuEditor();
        openOptypeEdit(idFunc, name, true);
    }
}

function fnDelFunction(){
    
        //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        title: oLng_MasterData.getText("MasterData_FunctionDeletingWithName") + " '" + fnGetCellValue("FunctionsTab", "NAME") + "'?", //"Eliminare la funzione ***?",
        id: "dlgDeleteFunction",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("FunctionsTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        fields: [
                            new sap.ui.commons.RadioButton("rdbDeleteTree", {
                                text: oLng_MasterData.getText("MasterData_DeleteFunctionTree"), //"Eliminare la funzione selezionata e relativa discendenza"
                                selected: true,
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "6"})
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        fields: [
                            new sap.ui.commons.RadioButton("rdbDeleteNode", {
                                text: oLng_MasterData.getText("MasterData_DeleteFunctionNode"), //"Eliminare solo la funzione selezionata
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "6"})
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(new sap.ui.commons.Label({
        text: oLng_MasterData.getText("MasterData_DeletingModalitySelection"), //"Selezionare la modalità di eliminazione:",
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"})
    }));
    oEdtDlg.addContent(oForm1);
    
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina",
        press:function(){
            sap.ui.commons.MessageBox.show(
                oLng_MasterData.getText("MasterData_FunctionDeleting"), //"Eliminare la funzione selezionata?",
                sap.ui.commons.MessageBox.Icon.WARNING,
                oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
                [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
                function(sResult){
                    if (sResult == 'YES'){
                        var qexe = dataProdMD + "Functions/delFunctionQR" +
                            "&Param.1=" + fnGetCellValue("FunctionsTab", "IDFUNC");
                        
                        if($.UIbyID("rdbDeleteTree").getSelected()){
                            qexe = qexe + "&Param.2=" + $.UIbyID("rdbDeleteTree").getSelected();
                        }
                        
                        if($.UIbyID("rdbDeleteNode").getSelected()){
                            qexe = qexe + "&Param.2=" + !($.UIbyID("rdbDeleteNode").getSelected());
                        }
                        console.log(qexe);
                        var ret = fnSQLQuery(
                            qexe,
                            oLng_MasterData.getText("MasterData_FunctionDeleted"), //"Funzione eliminata correttamente",
                            oLng_MasterData.getText("MasterData_FunctionDeletedError"), //"Errore in eliminazione funzione",
                            false
                        );
                        refreshTabDynamicMenuEditor();
                        $.UIbyID("FunctionsTab").setSelectedIndex(-1);
                        $.UIbyID("btnModFunctions").setEnabled(false);
                        $.UIbyID("btnDelFunctions").setEnabled(false);
                        $.UIbyID("btnEditOpTypes").setEnabled(false);                        
                        oEdtDlg.close();
                        oEdtDlg.destroy();
                    }
                },
                sap.ui.commons.MessageBox.Action.YES
            );            
        }
    }));
    
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));    
	oEdtDlg.open();
}

function saveMoptypesForFunction(idFunc, allMoptypes){
    
    var sEnabledMoptypes = '';
    for(var i = 0; i < allMoptypes.length; i++){
        if($.UIbyID(allMoptypes[i]).getChecked()){
            if(sEnabledMoptypes)
                sEnabledMoptypes += ',';
            sEnabledMoptypes += allMoptypes[i];
        }
    }
    
    sEnabledMoptypes = "[" + sEnabledMoptypes + "]";
    
    var qexe =  dataProdMD +
                "Functions/EnabledFunctions/setMoptypesForMfunctionsQR&Param.1=" + idFunc +
                "&Param.2=" + sEnabledMoptypes;
    var ret = fnExeQuery(
        qexe,
        oLng_MasterData.getText("MasterData_MoptypesFunctionSaved"), //Operatori aggiunti correttamente alla funzione
        oLng_MasterData.getText("MasterData_MoptypesFunctionSavedError"), //"Errore in aggiunta operatori alla funzione"
        false, false
    );
    
    /* oLng_MasterData.getText("MasterData_FunctionSaved") //Funzione salvata correttamente */
    if (ret){
        $.UIbyID("dlgAddOptypes").close();
        $.UIbyID("dlgAddOptypes").destroy();        
    }
}

function getValueFromXMLModel(oModel,sProperty) {
	var aRetValue = [];
	if(oModel)
	{
		var sRowsetPath = "/Rowset/0";
		var oRowsetObject = oModel.getObject(sRowsetPath);
		if(oRowsetObject)
		{
			var aChildNodes = oRowsetObject.childNodes;
			var iRowCount = aChildNodes.length-1; // -1 perchè c'e Columns
			for(var i=0; i<iRowCount; i++)
			{
				const sRowPath = sRowsetPath + "/Row/" + i;
				aRetValue.push(oModel.getProperty(sRowPath + "/" + sProperty));
			}
		}
		return aRetValue;
	}
}
