//**************************************************************************************
//Title:  Traduzione delle voci del Menù custom
//Author: Bruno Rosati
//Date:   19/03/2018
//Vers:   1.0
//**************************************************************************************
// Script per pagina 'Traduzione delle voci del Menù custom'


// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabDynamicMenuTranslate() {
    
    var oTypeModel = new sap.ui.model.xml.XMLModel();
	oTypeModel.loadData(QService + dataProdMD + "Functions/getFunctionsTypeSQ&Param.1=" + sLanguage);
        
    // Crea la ComboBox per selezionare il tipo delle funzioni da visualizzare
    var oCmbTypesFilter = new sap.ui.commons.ComboBox({
        id: "CmbTypesFilter", 
        selectedKey : 'D',
        change: function() {
            $.UIbyID("FunctionsIntTab").setSelectedIndex(-1);
            refreshTabDynamicMenuTranslate();
        }
    });
    oCmbTypesFilter.setModel(oTypeModel);
    var oItemTypesFilter = new sap.ui.core.ListItem();
    oItemTypesFilter.bindProperty("text", "EXTENDED_TEXT");
	oItemTypesFilter.bindProperty("key", "ORIG_VALUE");
    oCmbTypesFilter.bindItems("/Rowset/Row", oItemTypesFilter);
    
    var oLanguageModel = new sap.ui.model.xml.XMLModel();
    oLanguageModel.loadData(QService + dataProd + "Translate/getLanguagesListQR");
    // Crea la ComboBox per selezionare la lingua delle funzioni da visualizzare
    var oCmbLanguageFilter = new sap.ui.commons.ComboBox({
        id: "CmbLanguageFilter", 
        selectedKey : sLanguage,
        change: function() {
            $.UIbyID("FunctionsIntTab").setSelectedIndex(-1);
            refreshTabDynamicMenuTranslate();
        }
    });
    oCmbLanguageFilter.setModel(oLanguageModel);
    var oItemLanguaegFilter = new sap.ui.core.ListItem();
    oItemLanguaegFilter.bindProperty("text", "value");
	oItemLanguaegFilter.bindProperty("key", "name");
    oCmbLanguageFilter.bindItems("/Rowset/Row", oItemLanguaegFilter);
    
    //Crea L'oggetto Tabella Funzione
    var oTable = UI5Utils.init_UI5_Table({
        id: "FunctionsIntTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_MenuVoices"), //"Voci Menù",
            visibleRowCount: 15,
            width: "100%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){}
        },
        exportButton: true,
		toolbarItems:[
            new sap.ui.commons.Label({
                text: oLng_MasterData.getText("MasterData_Type") + ": ", //"Tipo",
                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
            }),
            oCmbTypesFilter,
            new sap.ui.commons.Label({
                text: oLng_MasterData.getText("MasterData_Type") + ": ", //"Tipo",
                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
            }),
            oCmbLanguageFilter,
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabDynamicMenuTranslate();
                }
            })
        ],
        columns: [
			{
				Field: "IDFUNC",
				label: oLng_MasterData.getText("MasterData_FunctionID"), //"ID Funzione",
				properties: {
					width: "120px",
                    flexible : false
				}
            },
            {
				Field: "LANGUAGE",
				properties: {
					width: "80px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "NAME",
                label: oLng_MasterData.getText("MasterData_FunctionName"), //"Nome funzione",
				properties: {
					width: "300px",
                    flexible : false
				},
				template: {
					type: "textInput",
					enabled: true,
                    change: function (oEvent) {
                        var sIdFunc = oEvent.getSource().getBindingContext().getProperty("IDFUNC");
                        var sName = oEvent.getSource().getBindingContext().getProperty("NAME");
                        //var checked = oEvent.getParameter('checked');
                        var that = this;
                        sap.ui.commons.MessageBox.show(
                            oLng_MasterData.getText("MasterData_ModifyFunctionName") + " " + sIdFunc + "?",
                            sap.ui.commons.MessageBox.Icon.WARNING,
                            oLng_MasterData.getText("MasterData_Translate"),
                            [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
                            function(sResult) {
                                if (sResult == 'YES') {
                                    var sQuery = (
                                        sName === "" ? 
                                        "Functions/DelFunctionNameTranslateSQ" : 
                                        "Functions/AddUpdFunctionNameTranslateSQ"
                                    );
                                    var qexe = dataProdMD + sQuery +
                                        "&Param.1=" + sIdFunc +
                                        "&Param.2=" + (
                                            $.UIbyID("CmbLanguageFilter").getSelectedKey() === "" ? 
                                            sLanguage : $.UIbyID("CmbLanguageFilter").getSelectedKey()
                                        ) +
                                        "&Param.3=" + sName;
                                    
                                    var ret = fnSQLQuery(
                                        qexe,
                                        oLng_MasterData.getText("MasterData_FunctionNameModified"),
                                        oLng_MasterData.getText("MasterData_FunctionNameModifiedError") + " " + sIdFunc,
                                        false
                                    );
                                    refreshTabDynamicMenuTranslate();                                    
                                } else {
                                    refreshTabDynamicMenuTranslate();
                                }
                            },
                            sap.ui.commons.MessageBox.Action.YES
                        );
                    }
                }
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabDynamicMenuTranslate();
    return oTable;
}

// Aggiorna la tabella FunctionsIntTab
function refreshTabDynamicMenuTranslate() {
    
    var sType = $.UIbyID("CmbTypesFilter").getSelectedKey() === "" ? "D" : $.UIbyID("CmbTypesFilter").getSelectedKey();
    var sSelectedLanguage = $.UIbyID("CmbLanguageFilter").getSelectedKey() === "" ? sLanguage : $.UIbyID("CmbLanguageFilter").getSelectedKey();
    
    $.UIbyID("FunctionsIntTab").getModel().loadData(
        QService + dataProdMD +
        "Functions/getFunctionsTranlateSQ&Param.1=" + sType  + "&Param.2=" + sSelectedLanguage
    );
    
    $.UIbyID("FunctionsIntTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}