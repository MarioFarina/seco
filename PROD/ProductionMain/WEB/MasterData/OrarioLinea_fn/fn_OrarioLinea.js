/***********************************************************************************************/
// Funzioni ed oggetti per Reparti
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

function createGrid() {
	let vertical = new sap.ui.layout.VerticalLayout({
		id: "verticalLayout",
		width: "100%",
		content: [
			new sap.m.Panel({
				id: "panel",
				content: [
					new sap.m.FlexBox({
						height: "100px",
						alignItems: "Start",
						justifyContent: "Center",
						items: [
							new sap.ui.commons.Label().setText("Inizio").addStyleClass("labelPadding"),
							new sap.m.TimePicker({
								id: "TP1",
								valueFormat: "HH:mm",
								displayFormat: "HH:mm"
							}),
							new sap.ui.commons.Button({
								text: "Inizia Ora",
								enabled: true,
								press: function () {
									$.UIbyID("TP1").setDateValue(new Date());
								}
							}).addStyleClass("buttonStyle")
						]
					}).addStyleClass("columns"),
					new sap.m.FlexBox({
						height: "100px",
						alignItems: "Start",
						justifyContent: "Center",
						items: [
							new sap.ui.commons.Label().setText("Fine").addStyleClass("labelPadding"),
							new sap.m.TimePicker({
								id: "TP2",
								valueFormat: "HH:mm",
								displayFormat: "HH:mm"
							}),
							new sap.ui.commons.Button({
								text: "Fine Ora",
								enabled: true,
								press: function () {
									$.UIbyID("TP2").setDateValue(new Date());
								}
							}).addStyleClass("buttonStyle")
						]
					}).addStyleClass("columns"),
					new sap.m.FlexBox({
						height: "100px",
						alignItems: "Start",
						justifyContent:"End",
						items: [
							new sap.ui.commons.Button({
								text: "Salva",
								enabled: true,
								press: function () {
									saveData();
								}
							}).addStyleClass("buttonStyle")
						]
					})
				]
			})
		]
	}).addStyleClass("sapUiContentPadding equalColumns");
	//oModel = new sap.ui.model.xml.XMLModel();
    //$.UIbyID("panel").setModel(oModel);
    //$.UIbyID("panel").bindElement("/Rowset/0/Row");
	return vertical;
}

function refreshTimePickers() {
	var ret = fnGetAjaxData(
		QService + dataProdRep +
		"First/GETWORKDAYSTR"
	);

	console.log(ret);
}

function saveData() {
	let inizio = $.UIbyID("TP1").getDateValue();
	let fine = $.UIbyID("TP2").getDateValue();
	let linea = $.UIbyID("filtPlant").getSelectedKey();

	if(inizio == undefined || inizio == null) {
		sap.ui.commons.MessageBox.alert("Ora Inizio non valorizzata");
	} else if(fine == undefined || fine == null) {
		sap.ui.commons.MessageBox.alert("Ora Fine non valorizzata");
	} else {
		let oraInizio = inizio.toTimeString().split(' ')[0];
		let oraFine = fine.toTimeString().split(' ')[0];

		console.log(oraInizio);
		console.log(oraFine);
		console.log(linea);

		if(linea == undefined || linea == "") {
			sap.ui.commons.MessageBox.alert("Linea non selezionata");
		} else {
			/*var qexe =  dataProdMD +"LineAvail/upsLineAvailSQ";
			qexe += "&Param.1=120" +
					"&Param.2=" + linea +
					"&Param.3=" + REASTXT +
					"&Param.4=" + DESCR +
					"&Param.5=" + DESCR +
					"&Param.6=" + active;
			var ret = fnExeQuery(qexe, "Ora salvata correttamente", "Errore in salvataggio ora", false);*/
		}				
	}	 
}