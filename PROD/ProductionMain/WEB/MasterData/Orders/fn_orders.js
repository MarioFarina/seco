/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Anagrafica commesse - funzioni
//Author: Bruno Rosati
//Date:   20/06/2017
//Vers:   1.1
//**************************************************************************************

//Creo la tabella
function createOrdersTab() {
    var oTable = UI5Utils.init_UI5_Table({
        id: "OrdersTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_OrdersList"),
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive
        },
        exportButton: true,
        toolbarItems: [
        new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"),
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function () {
                    refreshOrdersTab();
                }
            })
        ],
        columns: [
            {
                Field: "PLANT",
                properties: {
                    width: "50px",
                    visible: false //campo nascosto
                }
            },
            {
                Field: "NAME1",
                label: oLng_MasterData.getText("MasterData_Plant"),
                properties: {
                    width: "50px"
                }
            },
            {
                Field: "ORDER",
                label: oLng_MasterData.getText("MasterData_Order"),
                properties: {
                    width: "50px"
                }
            },
            {
                Field: "MATERIAL",
                label: oLng_MasterData.getText("MasterData_MaterialCode"),
                properties: {
                    width: "70px"
                }
            },
            {
                Field: "DESC",
                label: oLng_MasterData.getText("MasterData_MaterialDescr"),
                properties: {
                    width: "120px"
                }
            },
            {
                Field: "VERSION",
                label: oLng_MasterData.getText("MasterData_Version"),
                properties: {
                    width: "60px"
                }
            },
            {
                Field: "TARGET",
                label: oLng_MasterData.getText("MasterData_Target"),
                properties: {
                    width: "50px"
                }
            }

        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    return oTable;
}

//Pulsante Aggiorna
function refreshOrdersTab() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("OrdersTab").getModel().loadData(
            QService + dataProdMD +
            "Orders/getOrdersWithKeySQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("OrdersTab").getModel().loadData(
            QService + dataProdMD +
            "Orders/getOrdersWithKeySQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("OrdersTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}