// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataProdTR = "Content-Type=text/XML&QueryTemplate=ProductionMain/Translate/";
var dataProdRep = "Content-Type=text/XML&QueryTemplate=ProductionMain/Report/";
var i = 0;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
    url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
    locale: sCurrentLocale
});

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
jQuery.ajaxSetup({
    cache: false
});

//jQuery.sap.includeScript("/XMII/CM/Common/js/gauge.js", "gauge");

Libraries.load(
    ["/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/MasterData/ReportQuality/fn_ReportQuality"],
    function () {
        $(document).ready(function () {
            var oLblPlant = new sap.ui.commons.Label().setText("Linea");
            var labelDateFrom = new sap.ui.commons.Label().setText("Da data");
            var labelDateTo = new sap.ui.commons.Label().setText("A data");
            var labelMaterial = new sap.ui.commons.Label().setText("Materiale");
            var labelOrder = new sap.ui.commons.Label().setText("Ordine");

            var dateFrom = new sap.m.DatePicker({
                valueFormat: "yyyy-MM-dd",
                change: function (oEvent) {
                    refreshTabReportFirst();
                }
            }).setWidth("auto");
            var dateTo = new sap.m.DatePicker({
                valueFormat: "yyyy-MM-dd",
                change: function (oEvent) {
                    refreshTabReportFirst();
                }
            }).setWidth("auto");

            //Escape per gestione parametro plant da GET/POST
            var qexe = dataProdMD + "Lines/getLinesSQ";
            var sUserPlant = fnGetAjaxVal(qexe, ["plant"], false);
            if ($('#plant').val() === '{plant}') {
                $('#plant').val(sUserPlant.plant);
            }

            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
                selectedKey: $('#plant').val(),
                tooltip: "Linea", //Divisione
                change: function (oEvent) {
                    refreshTabReportFirst();
                }
            });

            var oInputMaterial = new sap.m.Input({
                tooltip: "Materiale",
                width: "10%",
                change: function (oEvent) {
                    refreshTabReportFirst();
                }
            });

            var oInputOrdine = new sap.m.Input({
                tooltip: "Ordine", //Divisione
                width: "10%",
                change: function (oEvent) {
                    refreshTabReportFirst();
                }
            });

            //Setto il default come chiave selezionata
            if ($.UIbyID("filtPlant").getSelectedKey() === '{plant}') {
                $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            }

            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("key", "IDLINE");
            oItemPlant.bindProperty("text", "LINETXT");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            var oPlantsModel = new sap.ui.model.xml.XMLModel();
            oPlantsModel.loadData(QService + dataProdMD + "Lines/getLinesSQ"); //getPlantsSQ
            oCmbPlant.setModel(oPlantsModel);

            var oSeparator = new sap.ui.commons.Toolbar({
                width: "auto",
                items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    labelDateFrom,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    dateFrom,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    labelDateTo,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    dateTo,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    labelMaterial,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oInputMaterial,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    labelOrder,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oInputOrdine
                ]
            }).addStyleClass("sapUiSizeCompact");

            var oHorizontalLayout = new sap.ui.layout.Grid({
                // width: "auto",
                content: [
                    createGraph(),
                    createNumericContent()
                ],
                hSpacing: 2
            });
            //}).addStyleClass("horizontalLayout");
            var oVerticalLayout = new sap.ui.layout.VerticalLayout({
                id: "VerticalLayout",
                content: [
                    oSeparator,
                    createTabReportFirst(),
                    oHorizontalLayout
                ],
                width: "100%" // fixme  va bene solo così, ma non va bene
            });

            oVerticalLayout.placeAt("MasterCont");
            $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            refreshTabReportFirst();
            //refreshGraph();

            $("#splash-screen").hide(); //nasconde l'icona di loading
        })
    }
);
