// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/MasterData/Grstreas_fn/fn_grstreas"
    ],
    function () {
        $(document).ready(function () {
            var oLblPlant = new sap.ui.commons.Label().setText(oLng_MasterData.getText("MasterData_Plant")); //Divisione

            //Escape per gestione parametro plant da GET/POST
            var qexe = dataProdComm + "getUserPlantQR";
            var sUserPlant = fnGetAjaxVal(qexe,["plant"],false);
            if ($('#plant').val() === '{plant}') {
                $('#plant').val(sUserPlant.plant);
            }
            
            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
                selectedKey: $('#plant').val(),
                tooltip: oLng_MasterData.getText("MasterData_Plant"), //Divisione
                change: function (oEvent) {                    
                    refreshTabGrstreas();
                }
            });
			
			//Setto il default come chiave selezionata
            if ($.UIbyID("filtPlant").getSelectedKey() === '{plant}' ) {
                $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            }
            
            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("key", "PLANT");
            oItemPlant.bindProperty("text", "NAME1");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            var oPlantsModel = new sap.ui.model.xml.XMLModel();
            oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsByUserQR"); //getPlantsSQ
            oCmbPlant.setModel(oPlantsModel);
            
            var oSeparator = new sap.ui.commons.Toolbar({
                items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbPlant
                ]
            });
            
            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
                content: [
                    oSeparator,
                    createTabGrstreas()
				],
                width: "100%"
            });
            
            oVerticalLayout.placeAt("MasterCont");
            $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            refreshTabGrstreas();
            $("#splash-screen").hide(); //nasconde l'icona di loading
        });
    }
);