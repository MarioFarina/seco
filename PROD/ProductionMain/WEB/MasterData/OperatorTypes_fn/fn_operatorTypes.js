var oPlantFOperator;
var oCmbCID;

//**************************************************************************************
//Title:  Gestione Tipi Operatore
//Author: Bruno Rosati
//Date:   03/05/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina 'Gestione Tipi Operatore'

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabOperator() {
    
    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "OperatorTypesTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_OperatorTypesList"), //"Lista tipi operatore",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("OperatorTypesTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModOperatorType").setEnabled(false);
        			$.UIbyID("btnDelOperatorType").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModOperatorType").setEnabled(true);
                    $.UIbyID("btnDelOperatorType").setEnabled(true);			
                }
            }
        },
        exportButton: true,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddOperatorType",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openOperatorEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModOperatorType",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openOperatorEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelOperatorType",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelOperatorType();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabOperatorType();
                }
            })
        ],
        columns: [
			{
				Field: "TYPE",
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
                    visible: false,
					width: "50px"
				}
            },
            {
				Field: "TYPE_DESC",
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "OPTYPE", 
				label: oLng_MasterData.getText("MasterData_OperatorType"), //"Tipo operatore",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "OPDESC",
				label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
				properties: {
					width: "80px"
				}
            }
        ],
        /*
        extension: new sap.ui.commons.Toolbar({
            items: []
        })
        */
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabOperatorType();
    return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabOperatorType() {
  $.UIbyID("OperatorTypesTab").getModel().loadData(QService + dataProdMD + "OperatorTypes/getAllOperatorTypesSQ" );
  $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Operatori
function openOperatorEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var jType = {root:[]};
    jType.root.push({
        key: 'D',
        text: 'Desktop'
    });
    jType.root.push({
        key: 'M',
        text: 'Mobile'
    });
  
    var oTypeModel = new sap.ui.model.json.JSONModel();
    oTypeModel.setData(jType);
    console.log(jType);
        
    var oCmbType = new sap.ui.commons.ComboBox({
        id: "cmbType", 
        selectedKey : bEdit?fnGetCellValue("OperatorTypesTab", "TYPE"):'D',
        change: function (oEvent) {
            if($.UIbyID("cmbType").getSelectedKey() === 'D'){
                $.UIbyID("OPTYPE").setMaxLength(50);
                $.UIbyID("OPTYPE").setTooltip("");
            }
            else{               
                $.UIbyID("OPTYPE").setMaxLength(2);
                $.UIbyID("OPTYPE").setTooltip(oLng_MasterData.getText("MasterData_Max2Characters")); //"Massimo due caratteri"
            }
            $.UIbyID("OPTYPE").setValue("");
        }
    });
    oCmbType.setModel(oTypeModel);
    var oItemType = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemType.bindProperty("text", "text");
	oItemType.bindProperty("key", "key");	
	oCmbType.bindItems("/root", oItemType);

	// contenitore dati lista divisioni
	var oModel = new sap.ui.model.xml.XMLModel();
	oModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	//  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgOperatorType",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyOperatorType"):oLng_MasterData.getText("MasterData_NewOperatorType"),
            /*"Modifica Tipo Operatore":"Nuovo Tipo Operatore",*/
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbType.setEnabled(!bEdit)
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_OperatorType"), //"Tipo Operatore",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTypesTab", "OPTYPE"):"",
                                editable: !bEdit,
                                tooltip: "" /*oLng_MasterData.getText("MasterData_Max2Characters")*/, //"Massimo due caratteri"
                                maxLength: 50,
                                id: "OPTYPE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTypesTab", "OPDESC"):"",
                                id: "OPDESC"
                            })
                        ]
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveOperatorType(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche al tipo operatore o inserirlo
function fnSaveOperatorType(bEdit) 
{
    var type = $.UIbyID("cmbType").getSelectedKey();
    var opType = $.UIbyID("OPTYPE").getValue();
    var opDesc = $.UIbyID("OPDESC").getValue();
    
    if ( type === "" || opType === "" || opDesc === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe = "";
    
    if(bEdit){
        qexe =  dataProdMD +
            "OperatorTypes/updOperatorTypeSQ" +
            "&Param.1=" + type +
            "&Param.2=" + opType +
            "&Param.3=" + opDesc;
    }
    else{
        qexe =  dataProdMD +
            "OperatorTypes/addOperatorTypeSQ" +
            "&Param.1=" + type +
            "&Param.2=" + opType +
            "&Param.3=" + opDesc;
    }
    
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_OperatorTypeSaved"),
                         oLng_MasterData.getText("MasterData_OperatorTypeSavedError")
                         /*"Tipo Operatore salvato correttamente", "Errore in salvataggio tipo operatore"*/,
                         false);
    if (ret){
        $.UIbyID("dlgOperatorType").close();
        $.UIbyID("dlgOperatorType").destroy();
        refreshTabOperatorType();
    }
}

function fnDelOperatorType(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_OperatorTypeDeleting"), //"Eliminare il tipo operatore selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "OperatorTypes/delOperatorTypeQR" +
                            "&Param.1=" + fnGetCellValue("OperatorTypesTab", "TYPE") +
                            "&Param.2=" + fnGetCellValue("OperatorTypesTab", "OPTYPE");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_OperatorTypeDeleted"), //"Tipo operatore eliminato correttamente",
                    oLng_MasterData.getText("MasterData_OperatorTypeDeletedError"), //"Errore in eliminazione tipo operatore",
                    false
                );
                refreshTabOperatorType();
                $.UIbyID("btnModOperatorType").setEnabled(false);
                $.UIbyID("btnDelOperatorType").setEnabled(false);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}