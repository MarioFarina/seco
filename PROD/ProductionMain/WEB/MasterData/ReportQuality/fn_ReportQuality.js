/***********************************************************************************************/
// Funzioni ed oggetti per Reparti
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
    url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
    locale: sCurrentLocale
});

// Functione che crea la tabella reparti e ritorna l'oggetto oTable
// function createTabReportFirst() {
//     var oTable = new sap.ui.table.Table({
//         id: "RepTab",
//         showNoData: true,
//         width: "auto"
//         // columnHeaderHeight : 10,
//         // visibleRowCount: 7
//     });
//
//     // colonne
//     var columns = [
//         {
//             Field: "ZZDATETIME",
//             label: oLng_MasterData.getText("MasterData_DepartID"), //"ID Reparto",
//             properties: {
//                 // width: "100px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZLINEIDENTITY",
//             label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
//             properties: {
//                 // width: "100px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZMATERIAL",
//             label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
//             properties: {
//                 // width: "100px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZMATDESCRIPTION",
//             label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
//             properties: {
//                 // width: "100px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZORDER",
//             label: oLng_MasterData.getText("MasterData_Slowtime"), //"SLOWTIME (soglia rallentamento/fermo) in minuti",
//             properties: {
//                 // width: "285px",
//                 // flexible: true
//             },
//             template: {
//                 type: "Numeric",
//                 textAlign: "Right"
//             }
//         },
//         {
//             Field: "ZZOPERATION",
//             label: oLng_MasterData.getText("MasterData_Stoptrim"), //"STOPTRIM (tolleranza) in secondi",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             },
//             template: {
//                 type: "Numeric",
//                 textAlign: "Right"
//             }
//         },
//         {
//             Field: "ZZGOODQTY",
//             label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZSCRAPQTY",
//             label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZSERIAL",
//             label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZLOADUNIT",
//             label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZOPERATIONID",
//             label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             }
//         },
//         {
//             Field: "ZZFIRST",
//             label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
//             properties: {
//                 // width: "200px",
//                 // flexible: true
//             }
//         }
//     ]
//
//
//     oModel = new sap.ui.model.xml.XMLModel();
//     // var oModel = new sap.ui.model.json.JSONModel();
//     oModel.setData({
//         columns: columns
//     });
//     oTable.setModel(oModel);
//     // oTable.bindColumns("/columns", function (sId, oContext) {
//     //     var sColumnId = oContext.getObject().columnId;
//     //     return new sap.ui.table.Column({
//     //         id: sColumnId,
//     //         label: sColumnId,
//     //         template: sColumnId,
//     //         sortProperty: sColumnId,
//     //         filterProperty: sColumnId
//     //     });
//     // });
//     oTable.bindRows("/Rowset/0/Row");
//     refreshTabReportFirst();
//     return oTable;
// }

function createTabReportFirst() {
    //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "RepTab",
        properties: {
            visibleRowCount: 15,
            width: "auto",
            firstVisibleRow: 0
            // selectionMode: sap.ui.table.SelectionMode.Single
            // navigationMode: sap.ui.table.NavigationMode.Scrollbar
        },
        toolbarItems: [
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function () {
                    refreshTabReportFirst();
                }
            })
        ],
        columns: [
            {
                Field: "ZZDATETIME",
                label: oLng_MasterData.getText("MasterData_DepartID"), //"ID Reparto",
                properties: {
                    // width: "100px",
                    // flexible: true
                }
            },
            {
                Field: "ZZLINEIDENTITY",
                label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
                properties: {
                    // width: "100px",
                    // flexible: true
                }
            },
            {
                Field: "ZZMATERIAL",
                label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                properties: {
                    // width: "100px",
                    // flexible: true
                }
            },
            {
                Field: "ZZMATDESCRIPTION",
                label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                properties: {
                    // width: "100px",
                    // flexible: true
                }
            },
            {
                Field: "ZZORDER",
                label: oLng_MasterData.getText("MasterData_Slowtime"), //"SLOWTIME (soglia rallentamento/fermo) in minuti",
                properties: {
                    // width: "285px",
                    // flexible: true
                },
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            },
            {
                Field: "ZZOPERATION",
                label: oLng_MasterData.getText("MasterData_Stoptrim"), //"STOPTRIM (tolleranza) in secondi",
                properties: {
                    // width: "200px",
                    // flexible: true
                },
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            },
            {
                Field: "ZZGOODQTY",
                label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                properties: {
                    // width: "200px",
                    // flexible: true
                }
            },
            {
                Field: "ZZSCRAPQTY",
                label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                properties: {
                    // width: "200px",
                    // flexible: true
                }
            },
            {
                Field: "ZZSERIAL",
                label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                properties: {
                    // width: "200px",
                    // flexible: true
                }
            },
            {
                Field: "ZZLOADUNIT",
                label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                properties: {
                    // width: "200px",
                    // flexible: true
                }
            },
            {
                Field: "ZZOPERATIONID",
                label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                properties: {
                    // width: "200px",
                    // flexible: true
                }
            },
            {
                Field: "ZZFIRST",
                label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                properties: {
                    // width: "200px",
                    // flexible: true
                }
            }
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/0/Row");
    refreshTabReportFirst();
    return oTable;
}

function createGraph() {
    var model = new sap.ui.model.json.JSONModel({
        width: "60%"
    });
    var charts = new sap.viz.ui5.controls.VizFrame({
        id: "chart",
        vizType: 'line',
        uiConfig: {'applicationSet': 'fiori'},
        width: "100%",
        layoutData: new sap.ui.layout.GridData({
            span: "L8 M8 S8"    // fixme    TENERE D'OCCHIO
        }),
        vizProperties: {
            title: {
                text: 'ROWSET3'
            },
            plotArea: {
                dataLabel: {
                    visible: true
                },
                colorPalette: ['#d32030', '#e17b24', '#61a656', '#848f94']
            }
        },
        selectData: function (oControlEvent) {
            //alert("ciao!");
        },
        dataset: new sap.viz.ui5.data.FlattenedDataset({
            data: "{/Row}",
            dimensions: [
                new sap.viz.ui5.data.DimensionDefinition({
                    name: "DATA",
                    value: "{DATA}"
                })
            ],
            measures: [
                new sap.viz.ui5.data.MeasureDefinition({
                    name: "First",
                    value: "{VALORE}"
                }),
                new sap.viz.ui5.data.MeasureDefinition({
                    name: "Totale prodotto",
                    value: "{TOTALE}"
                })
            ]
        }),
        feeds: [
            new sap.viz.ui5.controls.common.feeds.FeedItem({
                id: 'valueAxisFeed',
                uid: "valueAxis",
                type: "Measure",
                values: ["First"]
            }),
            new sap.viz.ui5.controls.common.feeds.FeedItem({
                id: 'valueAxisFeed1',
                uid: "valueAxis",
                type: "Measure",
                values: ["Totale prodotto"]
            }),
            new sap.viz.ui5.controls.common.feeds.FeedItem({
                id: 'categoryAxisFeed',
                uid: "categoryAxis",
                type: "Dimension",
                values: ["DATA"]
            })
        ]
    });

    var test = new sap.ui.model.xml.XMLModel();
    test.setXML('<?xml version="1.0" encoding="UTF-8"?><Rowsets CachedTime="" DateCreated="2019-04-19T17:48:45" EndDate="2019-04-19T17:48:45" StartDate="2019-04-19T16:48:45" Version="15.2 SP3 Patch 7 (Feb 14, 2019)"><Rowset></Rowset><Rowset></Rowset><Rowset><Columns><Column Description="VALORE" MaxRange="1" MinRange="0" Name="VALORE" SQLDataType="3" SourceColumn="VALORE"/><Column Description="TOTALE" MaxRange="1" MinRange="0" Name="TOTALE" SQLDataType="3" SourceColumn="TOTALE"/><Column Description="INDICE" MaxRange="1" MinRange="0" Name="INDICE" SQLDataType="3" SourceColumn="INDICE"/><Column Description="DATA" MaxRange="1" MinRange="0" Name="DATA" SQLDataType="91" SourceColumn="DATA"/></Columns><Row><VALORE>4</VALORE><TOTALE>10</TOTALE><INDICE>0.4</INDICE><DATA>2019-04-11T00:00:00</DATA></Row><Row><VALORE>15</VALORE><TOTALE>25</TOTALE><INDICE>0.6</INDICE><DATA>2019-04-05T00:00:00</DATA></Row></Rowset></Rowsets>');
    var oData = test.getXML();
    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(oData, "text/xml");
    var myObject = {};
    myObject.Row = [];
    var obj = {};

    for (var i = 0; i < xmlDoc.getElementsByTagName("Rowset")[2].childElementCount; i++) {
        if (xmlDoc.getElementsByTagName("Rowset")[2].childNodes[i].nodeName != 'Columns') {
            var childNode = xmlDoc.getElementsByTagName("Rowset")[2].childNodes[i].childNodes;
            for (var k = 0; k < childNode.length; k++) {
                var etichetta = childNode[k].tagName;
                var valore = childNode[k].textContent;
                if (etichetta == 'DATA') {
                    var today = new Date(valore);
                    valore = today.toLocaleDateString("it-IT");
                }
                obj[etichetta] = valore;
            }
            myObject.Row.push(obj);
            obj = {};
        }
    }
    myObject.Row.sort(function (a, b) {
        var d1 = new Date(a.DATA);
        var d2 = new Date(b.DATA);
        if (d1.getTime() < d2.getTime()) {
            return -1;
        }
        if (d1.getTime() > d2.getTime()) {
            return 1;
        }
        // names must be equal
        return 0;
    });
    model.setData(myObject)
    charts.setModel(model);
    //refreshGraph();
    return charts;
}

function createNumericContent() {
    /*var numericContent = new sap.m.NumericContent({
        scale: "%",
        class: "sapUiSmallMargin",
        withMargin: false,
        value: "{VALORE}",
        valueColor: sap.m.ValueColor.Critical,
        layoutData: new sap.ui.layout.GridData({
            span: "L2 M2 S2"
        })
    });*/

    var oModel = new sap.ui.model.xml.XMLModel();
    oModel.setXML('<?xml version="1.0" encoding="UTF-8"?><Rowsets CachedTime="" DateCreated="2019-04-19T17:48:45" EndDate="2019-04-19T17:48:45" StartDate="2019-04-19T16:48:45" Version="15.2 SP3 Patch 7 (Feb 14, 2019)"><Rowset></Rowset><Rowset><Columns><Column Description="VALORE" MaxRange="1" MinRange="0" Name="VALORE" SQLDataType="3" SourceColumn="VALORE"/><Column Description="TOTALE" MaxRange="1" MinRange="0" Name="TOTALE" SQLDataType="3" SourceColumn="TOTALE"/><Column Description="INDICE" MaxRange="1" MinRange="0" Name="INDICE" SQLDataType="3" SourceColumn="INDICE"/><Column Description="DATA" MaxRange="1" MinRange="0" Name="DATA" SQLDataType="91" SourceColumn="DATA"/></Columns><Row><VALORE>40</VALORE><TOTALE>10</TOTALE><INDICE>0.4</INDICE><DATA>2019-04-11T00:00:00</DATA></Row></Rowset><Rowset><Columns><Column Description="VALORE" MaxRange="1" MinRange="0" Name="VALORE" SQLDataType="3" SourceColumn="VALORE"/><Column Description="TOTALE" MaxRange="1" MinRange="0" Name="TOTALE" SQLDataType="3" SourceColumn="TOTALE"/><Column Description="INDICE" MaxRange="1" MinRange="0" Name="INDICE" SQLDataType="3" SourceColumn="INDICE"/><Column Description="DATA" MaxRange="1" MinRange="0" Name="DATA" SQLDataType="91" SourceColumn="DATA"/></Columns><Row><VALORE>4</VALORE><TOTALE>10</TOTALE><INDICE>0.4</INDICE><DATA>2019-04-11T00:00:00</DATA></Row><Row><VALORE>15</VALORE><TOTALE>25</TOTALE><INDICE>0.6</INDICE><DATA>2019-04-05T00:00:00</DATA></Row></Rowset></Rowsets>');

    var oData = oModel.getXML();
    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(oData, "text/xml");
    var value = 0;

    for (var i = 0; i < xmlDoc.getElementsByTagName("Rowset")[1].childElementCount; i++) {
        if (xmlDoc.getElementsByTagName("Rowset")[1].childNodes[i].nodeName != 'Columns') {
            var childNode = xmlDoc.getElementsByTagName("Rowset")[1].childNodes[i].childNodes;
            for (var k = 0; k < childNode.length; k++) {
                var etichetta = childNode[k].tagName;
                var valore = childNode[k].textContent;
                if (etichetta == 'VALORE') {
                    value = valore;
                    break;
                }
            }
        }
    }

    //numericContent.setModel(oModel);
    //numericContent.bindElement("/Rowset/1/Row/0");

    /*numericContent = new sap.ui.core.HTML({
        content: '<span id="memoryGaugeContainer" style="width: 250px; height: 250px;"></span>'
    });

    this.bFirst = true;
    var that = this;

    numericContent.addEventDelegate({
        onAfterRendering : function(oEvent) {
            if (that.bFirst === true) {
                that.bFirst = false;
                var name = 'memoryGaugeContainer';
                var min = 0;
                var max = 100;
                var label = "";
                var config = {
                    size: 125,
                    label: label,
                    min: undefined != min ? min : 0,
                    max: undefined != max ? max : 100,
                    minorTicks: 5
                };

                var range = config.max - config.min;
                //config.greenZones = [{ from: config.min, to: config.min + range*0.75 }];
                //config.yellowZones = [{ from: config.min + range*0.75, to: config.min + range*0.9 }];
                //config.redZones = [{ from: config.min + range*0.9, to: config.max }];
                config.greenZones = [{ from: config.min, to: config.max }];

                var g = new Gauge(name, config);
                g.render();
                g.redraw(95);
            }
        }
    });*/

    var numericContent = new sap.suite.ui.microchart.RadialMicroChart({
        size: sap.m.Size.Responsive,
        //percentage: 55,
        percentage: parseInt(value),
        // class: "sapUiSmallMargin",
        class: "sapUiResponsiveMargin",
        width: "30%",
        valueColor: sap.m.ValueColor.Critical,
        layoutData: new sap.ui.layout.GridData({
            span: "L2 M2 S2"
        }),
        tooltip: "radial chart"
        // }).addStyleClass("");
    }).addStyleClass("radialMicroChart");

    numericContent.setModel(oModel);
    numericContent.bindElement("/Rowset/1/Row/0");

    return numericContent;
}

// Aggiorna la tabella reparti
function refreshTabReportFirst() {
    if ($.UIbyID("filtPlant").getSelectedKey() === "") {
        $.UIbyID("RepTab").getModel().loadData(
            QService + dataProdRep +
            "First/getFirstQR&Param.1=false&Param.2=2019-04-01&Param.3=&Param.4=2019-04-30&Param.5=P3-08-C1&Param.6="
        );
    } else {
        $.UIbyID("RepTab").getModel().loadData(
            QService + dataProdRep +
            "First/getFirstQR&Param.1=false&Param.2=2019-04-01&Param.3=&Param.4=2019-04-30&Param.5=P3-08-C1&Param.6="
        );
    }
    $.UIbyID("RepTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

/*function refreshGraph() {
	var test = new sap.ui.model.xml.XMLModel();
	test.loadData(QService + dataProdRep +
		"First/getFirstQR&Param.1=false&Param.2=2019-04-01&Param.3=&Param.4=2019-04-30&Param.5=P3-08-C1&Param.6=");
	test.attachRequestCompleted(function(oEvent) {
		test.setXML('<?xml version="1.0" encoding="UTF-8"?><Rowsets CachedTime="" DateCreated="2019-04-19T17:48:45" EndDate="2019-04-19T17:48:45" StartDate="2019-04-19T16:48:45" Version="15.2 SP3 Patch 7 (Feb 14, 2019)"><Rowset></Rowset><Rowset></Rowset><Rowset><Columns><Column Description="VALORE" MaxRange="1" MinRange="0" Name="VALORE" SQLDataType="3" SourceColumn="VALORE"/><Column Description="TOTALE" MaxRange="1" MinRange="0" Name="TOTALE" SQLDataType="3" SourceColumn="TOTALE"/><Column Description="INDICE" MaxRange="1" MinRange="0" Name="INDICE" SQLDataType="3" SourceColumn="INDICE"/><Column Description="DATA" MaxRange="1" MinRange="0" Name="DATA" SQLDataType="91" SourceColumn="DATA"/></Columns><Row><VALORE>4</VALORE><TOTALE>10</TOTALE><INDICE>0.4</INDICE><DATA>2019-04-11T00:00:00</DATA></Row><Row><VALORE>15</VALORE><TOTALE>25</TOTALE><INDICE>0.6</INDICE><DATA>2019-04-05T00:00:00</DATA></Row></Rowset></Rowsets>');
		var oData = test.getXML();
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString(oData,"text/xml");
		var myObject = {};
		myObject.Row = [];
		var obj = {};

		for(var i=0;i<xmlDoc.getElementsByTagName("Rowset")[2].childElementCount;i++) {
			if(xmlDoc.getElementsByTagName("Rowset")[2].childNodes[i].nodeName != 'Columns') {
				var childNode = xmlDoc.getElementsByTagName("Rowset")[2].childNodes[i].childNodes;
				for(var k=0;k<childNode.length; k++) {
					var etichetta = childNode[k].tagName;
					var valore = childNode[k].textContent;
					obj[etichetta] = valore;
				}
				myObject.Row.push(obj);
			}
		}
		/*var model = new sap.ui.model.json.JSONModel();
		model.setData(myObject);
		$.UIbyID("chart").setModel(model);
		$.UIbyID("chart").getModel().refresh();
		$.UIbyID("chart").getModel().setProperty("/", myObject);
	});

	/*if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		$.UIbyID("chart").getModel().loadData(
            QService + dataProdRep +
            "First/getFirstQR&Param.1=false&Param.2=2019-04-01&Param.3=&Param.4=2019-04-30&Param.5=P3-08-C1&Param.6="
        );
    } else {
		$.UIbyID("chart").getModel().loadData(
            QService + dataProdRep +
            "First/getFirstQR&Param.1=false&Param.2=2019-04-01&Param.3=&Param.4=2019-04-30&Param.5=P3-08-C1&Param.6="
        );
    }
    $("#splash-screen").hide();
}*/