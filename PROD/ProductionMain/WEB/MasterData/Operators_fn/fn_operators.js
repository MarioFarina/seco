var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Operatori
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabOperator() {
    
    // contenitore dati lista CID
	/*
    var oModel2 = new sap.ui.model.xml.XMLModel()
	oModel2.loadData(QService + dataProdMD + "GetAllCIDQR");

	// Crea la ComboBox per i CID

	oCmbCID = new sap.ui.commons.ComboBox("cmbCID", {selectedKey : ""});
	oCmbCID.setModel(oModel2);		
	var oItemCID = new sap.ui.core.ListItem();
	oItemCID.bindProperty("text", "EMNAM");
	oItemCID.bindProperty("key", "PERNR");	
	oCmbCID.bindItems("/Rowset/Row", oItemCID);
    */
    
    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "OperatorTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_OperatorsList"), //"Lista operatori",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("OperatorTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModOperator").setEnabled(false);
        			$.UIbyID("btnDelOperator").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModOperator").setEnabled(true);
                    $.UIbyID("btnDelOperator").setEnabled(true);			
                }
            }
        },
        exportButton: true,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddOperator",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openOperatorEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModOperator",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openOperatorEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelOperator",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelOperator();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabOperator();
                }
            })
           

        
        ],
        columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "NAME1", // in Sigit è "PLNAME",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "80px",
                    visible: false
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "PERNR",
				label: oLng_MasterData.getText("MasterData_Register"), //"PERNR",
				properties: {
					width: "50px"
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "OPENAME", 
				label: oLng_MasterData.getText("MasterData_OperatorName"), //"Nome Operatore",
				properties: {
					width: "80px"
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "BADGE", 
				label: oLng_MasterData.getText("MasterData_Badge"), //"Badge",
				properties: {
					width: "80px"
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "ENABLED",// in Sigit è "ACTIVE",
				label: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "TYPE", 
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "60px",
                    visible: false
				}
            },
            {
				Field: "OPDESC", 
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "120px"
				}
            },
            {
				Field: "GROUP_ID",
				label: oLng_MasterData.getText("MasterData_Group"), //"Gruppo",
				properties: {
					width: "90px"
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            },
            {
				Field: "NOTES", 
				label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
				properties: {
					width: "160px"
				}/*,
				template: {
					type: "DateTime",
					textAlign: "Center"
                }*/
            }
        ],
        /*
        extension: new sap.ui.commons.Toolbar({
            items: [
                oCmbPlant,
                new sap.ui.commons.Button({
                    text: oLng_Gen.getText("ConfPge_BtnResetCalendarText"),
                    id: "btnResetOperator",
                    icon: icon16 + "application-export.png",
                    enabled: false,
                    press: function(){
                        $.UIbyID("OperatorTab").filter(oPlantFOperator,"");
                        $.UIbyID("btnResetOperator").setEnabled(false);
                        // $.UIbyID("btnCopySAP").setEnabled(false);
                        $.UIbyID("filtPlant").setSelectedKey("");
                    }
                })
            ]
        })
        */
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabOperator();
    return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabOperator() {

  if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("OperatorTab").getModel().loadData(
            QService + dataProdMD +
            "Operators/getAllCIDbyPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("OperatorTab").getModel().loadData(
            QService + dataProdMD +
            "Operators/getAllCIDbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }

    $.UIbyID("OperatorTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Operatori
function openOperatorEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oModelPlant = new sap.ui.model.xml.XMLModel();
	oModelPlant.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        enabled: false,
        selectedKey : bEdit?fnGetCellValue("OperatorTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey()
    });
	oCmbPlantInput.setModel(oModelPlant);		
	var oItemPlantInput = new sap.ui.core.ListItem();
    oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    var oModelOPType = new sap.ui.model.xml.XMLModel();
	oModelOPType.loadData(QService + dataProdMD + "OperatorTypes/getMOperatorTypesSQ");

	// Crea la ComboBox per il tipo operatore (quindi solo Mobile)
	var oCmbOPType = new sap.ui.commons.ComboBox({
        id: "cmbOPType",
        selectedKey : bEdit?fnGetCellValue("OperatorTab", "TYPE"):""
    });
	oCmbOPType.setModel(oModelOPType);		
	var oItemOPType = new sap.ui.core.ListItem();
    oItemOPType.bindProperty("text", "OPDESC");
	oItemOPType.bindProperty("key", "OPTYPE");	
	oCmbOPType.bindItems("/Rowset/Row", oItemOPType);
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgOperator",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("OperatorTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyOperator"):oLng_MasterData.getText("MasterData_NewOperator"),
            /*"Modifica Operatore":"Nuovo Operatore",*/
            //icon: "/images/address.gif",
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Register"), //"Matricola",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTab", "PERNR"):"",
                                editable: !bEdit,
                                maxLength: 12,
                                id: "PERNR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_OperatorName"), //"Nome Operatore",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTab", "OPENAME" /*"EMNAM"*/):"",
                                editable: true,
                                maxLength: 45,
                                id: "OPENAME" /*"EMNAM"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Badge"), //"Badge",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTab", "BADGE" /*"EMNAM"*/):"",
                                editable: true,
                                maxLength: 15,
                                id: "BADGE" /*"EMNAM"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTab", "NOTES" /*"EMNAM"*/):"",
                                maxLength: 255,
                                id: "NOTES" /*"EMNAM"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("OperatorTab", "ENABLED") == "0" ? false : true ) : true,
                                /*bEdit?(fnGetCellValue("OperatorTab", "ACTIVE") == "0" ? false : true ) : false,*/
                                id: "ENABLED" /*"ACTIVE"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbOPType
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Group"), //"Gruppo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("OperatorTab", "GROUP_ID"):"",
                                id: "GROUP_ID",
                                tooltip: oLng_MasterData.getText("MasterData_Max4Characters"), //"Massimo quattro caratteri"
                            })
                        ]
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveOperator(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbOPType.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche all'operatore o inserirlo
function fnSaveOperator(bEdit) 
{
    var cid = $.UIbyID("PERNR").getValue(); 
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var opeName = $.UIbyID("OPENAME").getValue();
    var badge = $.UIbyID("BADGE").getValue();
    var notes = $.UIbyID("NOTES").getValue();
    var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";
    var type = $.UIbyID("cmbOPType").getSelectedKey();
    var group_id = $.UIbyID("GROUP_ID").getValue();
    
    if ( cid === "" || plant === "" || opeName === "" || type === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe = dataProdMD + (bEdit?"Operators/updOperatorsSQ":"Operators/addOperatorsSQ");
    qexe += "&Param.1=" + plant +
            "&Param.2=" + opeName +
            "&Param.3=" + badge +
            "&Param.4=" + notes +
            "&Param.5=" + active +
            "&Param.6=" + type +
            "&Param.7=" + group_id +
            "&Param.8=" + cid;
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_OperatorSaved"),
                         oLng_MasterData.getText("MasterData_OperatorSavedError")
                         /*"Operatore salvato", "Errore salvataggio operatore"*/,
                         false);
    if (ret){
        $.UIbyID("dlgOperator").close();
        $.UIbyID("dlgOperator").destroy();
        refreshTabOperator();
    }
}

function fnDelOperator(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_OperatorDeleting"), //"Eliminare l'operatore selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe =  dataProdMD + "Operators/delOperatorsSQ" +
                            "&Param.1=" + fnGetCellValue("OperatorTab", "PERNR") +
                            "&Param.2=" + fnGetCellValue("OperatorTab", "PLANT");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_OperatorDeleted"), //"Operatore eliminato correttamente",
                    oLng_MasterData.getText("MasterData_OperatorDeletedError"), //"Errore in eliminazione operatore",
                    false
                );
                refreshTabOperator();
                $.UIbyID("btnModOperator").setEnabled(false);
                $.UIbyID("btnDelOperator").setEnabled(false);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}


    /**
     * Crea una combobox con valori da sorgente dati
     * @param id : ID univoco
     * @param key : chiave
     * @param text : testo 
     * @param source : sorgente dati
     * @returns: oggetto sap.ui.commons.ComboBox
     */
//function comboBoxQuery( id, key, text, query, def="", edit=true )
/*
function comboBoxQuery( id, key, text, query, def, edit )
{
    if ( def === undefined ) def = "";
    if ( edit === undefined ) edit = true;

    // contenitore dati lista
    var oModel = new sap.ui.model.xml.XMLModel();
    oModel.loadData( QService + dataProdMD + query );

    var oComboBox = new sap.ui.commons.ComboBox(id,  {selectedKey : ""});
    oComboBox.setEditable(edit);
    if( def !== "" ) {
        oComboBox.setValue(def);
    }
    oComboBox.setValue(def);
    oComboBox.setWidth("300px"); 
    oComboBox.setModel(oModel);
    var oItem = new sap.ui.core.ListItem();
    oItem.bindProperty("text", text);
    oItem.bindProperty("key", key);
    oComboBox.bindItems("/Rowset/Row", oItem); 
    return oComboBox;

}  // function comboBoxQuery( id, key, text, query, def, edit )
*/

/*
function copyFromSAP()
{
    sap.ui.commons.MessageBox.show( oLng_Gen.getText("PrPage_Msg_CopyOperatorsDataFromSAPQuestion"),
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Gen.getText("PrPage_Msg_CopyOperatorsFromSAP"),
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO ],
			function(sResult) {
			    if (sResult == 'YES') {
                                            execCopyFromSAP();
			    }
			},
        sap.ui.commons.MessageBox.Action.YES
    );

}    // function copyFromSAP()

function execCopyFromSAP() 
{
     var qexe = dataProdMD + "CopyAllCIDfromSapQR";
  
    var ret = fnExeQuery(qexe, oLng_Gen.getText("PrPage_retMsg_OperatorsCopied"),oLng_Gen.getText("PrPage_retMsg_OperatorsCopiedError"), false);
    if (ret) {
	     refreshTabOperator();
	 }

}
*/

