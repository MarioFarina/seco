var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Alarm
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Alarm e ritorna l'oggetto oTable
function createTabAlarm() {
    
    //Crea L'oggetto Tabella Alarm
    var oTable = UI5Utils.init_UI5_Table({
        id: "AlarmTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_AlarmList"), //"Lista allarmi",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("AlarmTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModAlarm").setEnabled(false);
//        			$.UIbyID("btnDelAlarm").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModAlarm").setEnabled(true);
//                    $.UIbyID("btnDelAlarm").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddAlarm",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openAlarmEdit(false, "INSERT");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModAlarm",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openAlarmEdit(true, "UPDATE");
                }
            }),
/*
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelAlarm",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    fnDelAlarm();
                }
            }),
*/
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabAlarm();
                }
            })
        ],
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "70px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "NAME1", // in Sigit è "PLNAME",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "70px"
				}
            },
            {
				Field: "IDLINE",
				label: oLng_MasterData.getText("MasterData_LineID"), //"Linea ID",
				properties: {
					width: "50px",
					visible: false
				}
            },
            {
				Field: "LINETXT",
				label: oLng_MasterData.getText("MasterData_Line"), //"Linea",
				properties: {
					width: "70px"
				}
            },
            {
				Field: "ALMID",
				label: oLng_MasterData.getText("MasterData_AlarmID"), //"ID allarme",
				properties: {
					width: "70px"
				}
            },
            {
				Field: "ALMTXT",
				label: oLng_MasterData.getText("MasterData_AlarmText"), //"Allarme",
				properties: {
					width: "120px"
				}
            },
            {
				Field: "ALMNOTE",
				label: oLng_MasterData.getText("MasterData_AlarmNote"), //"STOPID",
				properties: {
					width: "120px"
				}
            },
            {
				Field: "STOPID",
				label: oLng_MasterData.getText("MasterData_StopReason"), //"Codice causale fermo",
				properties: {
					width: "50px",
					visible: false
				}
            },
            {
				Field: "STOPID_TXT",
				label: oLng_MasterData.getText("MasterData_StopReason"), //"Codice causale fermo",
				properties: {
					width: "100px"
				}
            },
            {
				Field: "ALGRID",
				label: oLng_MasterData.getText("MasterData_AlarmGroup"), //"Gruppo allarmi",
				properties: {
					width: "100px",
					visible: false
				}
            },
            {
				Field: "ALGRID_TXT",
				label: oLng_MasterData.getText("MasterData_AlarmGroup"), //"STOPID",
				properties: {
					width: "100px"
				}
            },
            {
				Field: "RELEVANT",
				label: oLng_MasterData.getText("MasterData_Relevant"), //"Rilevante",
				properties: {
					width: "70px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            }
        ],
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabAlarm();
    return oTable;
}

// Aggiorna la tabella Alarm
function refreshTabAlarm() {
  if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("AlarmTab").getModel().loadData(
            QService + dataProdMD +
            "Alarm/getAlarmSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("AlarmTab").getModel().loadData(
            QService + dataProdMD +
            "Alarm/getAlarmSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("AlarmTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Alarm
function openAlarmEdit(bEdit, mode) {
    if (bEdit === undefined) bEdit = false;
    if (mode === undefined) mode = "";
  
    var oCmbPlantInput = fnCreateCombo( bEdit, "oCmbPlantInput", "PLANT", "NAME1",  QService + dataProdMD + "Plants/getPlantsSQ");
    oCmbPlantInput.setSelectedKey($.UIbyID("filtPlant").getSelectedKey());
    oCmbPlantInput.setEnabled(false);
    var oCmbStopReason = fnCreateCombo( bEdit, "oCmbStopReason", "STOPID", "REASTXT",  QService + dataProdMD + "Stopreas/getStopreasSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() );
    var oCmbLinesInput = fnCreateCombo( bEdit, "oCmbLinesInput", "IDLINE", "LINETXT",  QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() );
    var oCmbAlarmGroupInput = fnCreateCombo( bEdit, "oCmbAlarmGroupInput", "ALGRID", "REASTXT",  QService + dataProdMD + "Alarm/getGralarmByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() );

    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgStopreas",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("AlarmTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyAlarm"):oLng_MasterData.getText("MasterData_NewAlarm"),
            /*"Modifica Stoprease":"Nuovo Stoprease",*/
            //icon: "/images/address.gif",
            tooltip: "Info Stoprease"//oLng_Gen.getText("PrPage_MsgTT_StopreasInfo")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_AlarmID"), //"ID allarme",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("AlarmTab", "ALMID"):"",
                                editable: !bEdit,
                                maxLength: 15,
                                id: "ALMID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Line"), //"Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbLinesInput.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_AlarmText"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("AlarmTab", "ALMTXT"):"",
                                editable: true,
                                maxLength: 30,
                                id: "ALMTXT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_AlarmNote"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("AlarmTab", "ALMNOTE"):"",
                                editable: true,
                                maxLength: 200,
                                id: "ALMNOTE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_StopReason"), //"Codice causale fermo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbStopReason
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_AlarmGroup"), //"Codice causale fermo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbAlarmGroupInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Relevant"), //"Rilevante",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("AlarmTab", "RELEVANT") == "0" ? false : true ) : true,
                                /*bEdit?(fnGetCellValue("AlarmTab", "ACTIVE") == "0" ? false : true ) : false,*/
                                id: "RELEVANT" /*"ACTIVE"*/
                            })
                        ]
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveStopreas(mode);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
            oCmbLinesInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveStopreas(mode ) 
{
    if ( mode === 'undefined' ) mode = "";

    var plant = $.UIbyID("oCmbPlantInput").getSelectedKey();
    var IDLINE = $.UIbyID("oCmbLinesInput").getSelectedKey();
    var ALMID = $.UIbyID("ALMID").getValue();
    var ALMTXT = $.UIbyID("ALMTXT").getValue();
    var ALMNOTE = $.UIbyID("ALMNOTE").getValue();
    var STOPID = $.UIbyID("oCmbStopReason").getSelectedKey();
    var ALGRID = $.UIbyID("oCmbAlarmGroupInput").getSelectedKey();
    var relevant = $.UIbyID("RELEVANT"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";

    if ( IDLINE === "" || plant === "" || ALMID === "" || STOPID === "" || ALGRID === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    var qexe = "";
    switch( mode ) {
        case "INSERT":
            qexe =dataProdMD +"Alarm/addAlarmSQ";
            break;
        case "UPDATE":
            qexe =dataProdMD +"Alarm/updAlarmSQ";
            break;
    };

    qexe += "&Param.1=" + plant +
                "&Param.2=" + IDLINE +
                "&Param.3=" + ALMID +
                "&Param.4=" + ALMTXT +
                "&Param.5=" + ALMNOTE +
                "&Param.6=" + STOPID +
                "&Param.7=" + ALGRID +
                "&Param.8=" + relevant;
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_AlarmSaved"),
                         oLng_MasterData.getText("MasterData_AlarmSavedError")
                         /*"Stoprease salvato", "Errore salvataggio Stoprease"*/,
                         /*CONTROLLARE A COSA SERVE L'ULTIMO PARAMETRO, PASSATO FALSE*/
                         false);
    if (ret){
        $.UIbyID("dlgStopreas").close();
        $.UIbyID("dlgStopreas").destroy();
        refreshTabAlarm();
    }
}

function fnDelAlarm(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_AlarmDeleting"), //"Eliminare l'Stoprease selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "Stopreas/delStopreasSQ" +
                            "&Param.1=" + fnGetCellValue("AlarmTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("AlarmTab", "STOPID");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_AlarmDeleted"), //"Stoprease eliminato correttamente",
                    oLng_MasterData.getText("MasterData_AlarmDeletedError"), //"Errore in eliminazione Stoprease",
                    false
                );
                refreshTabAlarm();
                $.UIbyID("btnModAlarm").setEnabled(false);
                $.UIbyID("btnDelAlarm").setEnabled(false);
                 $.UIbyID("AlarmTab").setSelectedIndex(-1);
           }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnCreateCombo( bEdit, id, key, text, query ) {
    var oModel4 = new sap.ui.model.xml.XMLModel();
	oModel4.loadData(query);

	// Crea la ComboBox per le divisioni in input
	var oComboBox = new sap.ui.commons.ComboBox({
        id: id, 
        selectedKey : bEdit?fnGetCellValue("AlarmTab", key):""
    });
	oComboBox.setModel(oModel4);		
	var oItemScrabReasonInput = new sap.ui.core.ListItem();
	oItemScrabReasonInput.bindProperty("text", text);
	oItemScrabReasonInput.bindProperty("key", key);	
	oComboBox.bindItems("/Rowset/Row", oItemScrabReasonInput);
	
	return oComboBox;
}


