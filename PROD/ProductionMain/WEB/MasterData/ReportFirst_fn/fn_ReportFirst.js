/***********************************************************************************************/
// Funzioni ed oggetti per Reparti
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella reparti e ritorna l'oggetto oTable
function createTabReportFirst() {

    //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "RepTab",
        properties: {
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Paginator
        },
        toolbarItems: [
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function () {
                    refreshTabReportFirst();
                }
            })
        ],
        columns: [
            {
				Field: "ZZDATETIMELST",
				label: "ZZDATETIMELST",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "ZZLINEIDENTITY",
				label: "ZZLINEIDENTITY",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "ZZMATERIAL",
				label: "ZZMATERIAL",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "ZZMATDESCRIPTION",
				label: "ZZMATDESCRIPTION",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "ZZORDER",
				label: "ZZORDER",
				properties: {
					width: "285px",
					flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
			},
            {
				Field: "ZZOPERATION",
				label: "ZZOPERATION",
				properties: {
					width: "200px",
					flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
			},
            {
				Field: "ZZGOODQTY",
				label: "ZZGOODQTY",
				properties: {
					width: "200px",
					flexible : false
				}
			},
            {
				Field: "ZZSCRAPQTY",
				label: "ZZSCRAPQTY",
				properties: {
					width: "200px",
					flexible : false
				}
			},
            {
				Field: "ZZSERIAL",
				label: "ZZSERIAL",
				properties: {
					width: "200px",
					flexible : false
				}
			},
            {
				Field: "ZZLOADUNIT",
				label: "ZZLOADUNIT",
				properties: {
					width: "200px",
					flexible : false
				}
			},
            {
				Field: "ZZOPERATIONID",
				label: "ZZOPERATIONID",
				properties: {
					width: "200px",
					flexible : false
				}
			},
            {
				Field: "ZZFIRST",
				label: "ZZFIRST",
				properties: {
					width: "200px",
					flexible : false
				}
			},
			{
				Field: "ZZCOLOR",
				label: "ZZCOLOR",
				properties: {
					width: "200px",
					flexible : false
				}
			}
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/0/Row");
    refreshTabReportFirst();
    return oTable;
}

function createGraph() {

	var model = new sap.ui.model.json.JSONModel();
    var charts = new sap.viz.ui5.controls.VizFrame({
		id: "chart",
		vizType: 'line',
		uiConfig : { 'applicationSet': 'fiori' },
		width: "100%",
		layoutData: new sap.ui.layout.GridData({
			span: "L8 M8 S8"
		}),
		vizProperties: {
			title: {
				text: 'Prova titolo'
			},
			plotArea: {
				dataLabel: {
				  visible: true
				},
				colorPalette:  ['#d32030', '#e17b24', '#61a656', '#848f94']
			}
		},
		selectData: function(oControlEvent) {
			//alert("ciao!");
		},
		dataset: new sap.viz.ui5.data.FlattenedDataset({
				 	data: "{/Row}",
					dimensions: [
						new sap.viz.ui5.data.DimensionDefinition({
							name: "DATA",
							value: "{DATA}"
						})
					],
					measures: [
						new sap.viz.ui5.data.MeasureDefinition({
							name: "First",
							value: "{TOTALEFIRST}"
						}),
						new sap.viz.ui5.data.MeasureDefinition({
							name: "Totale prodotto",
							value: "{TOTALEBUONI}"
						})
					]
		}),
		feeds: [
			new sap.viz.ui5.controls.common.feeds.FeedItem({
				id: 'valueAxisFeed',
				uid: "valueAxis",
				type: "Measure",
                values: ["First"]
			}),
			new sap.viz.ui5.controls.common.feeds.FeedItem({
				id: 'valueAxisFeed1',
				uid: "valueAxis",
				type: "Measure",
                values: ["Totale prodotto"]
			}),
			new sap.viz.ui5.controls.common.feeds.FeedItem({
				id: 'categoryAxisFeed',
				uid: "categoryAxis",
				type: "Dimension",
                values: ["DATA"]
			})
		]
	});
	
	refreshGraph();
    return charts;
}

function createNumericContent() {
	let percentuale = 0;
	var numericContent = new sap.suite.ui.microchart.RadialMicroChart({
		id: "RadialMicroChart",
		size: sap.m.Size.Responsive,
		percentage: percentuale,
		class: "sapUiSmallMargin",
		valueColor: percentuale < 35 ? sap.m.ValueColor.Good : (percentuale >= 35 && percentuale < 70 ? sap.m.ValueColor.Critical : sap.m.ValueColor.Error),
		layoutData: new sap.ui.layout.GridData({
			span: "L2 M2 S2"
		}),
		tooltip: "radial chart"
	}).addStyleClass("radialMicroChart");

	refreshNumericContent();
	return numericContent;
}

// Aggiorna la tabella reparti
function refreshTabReportFirst() {
	let dateFrom = $.UIbyID("dateFrom").getValue();
	let dateTo = $.UIbyID("dateTo").getValue();
	let material = $.UIbyID("material").getValue();
	let ordine = $.UIbyID("ordine").getValue();
    if ($.UIbyID("filtPlant").getSelectedKey() === "") {
        $.UIbyID("RepTab").getModel().loadData(
            QService + dataProdRep +
            "First/getFirstQR&Param.1=false&Param.2="+dateFrom+"&Param.3="+material+"&Param.4="+dateTo+"&Param.5=&Param.6="+ordine
		);
    }
    else{
        $.UIbyID("RepTab").getModel().loadData(
            QService + dataProdRep +
            "First/getFirstQR&Param.1=false&Param.2="+dateFrom+"&Param.3="+material+"&Param.4="+dateTo+"&Param.5="+$.UIbyID("filtPlant").getSelectedKey()+"&Param.6="+ordine
		);
    }
    $.UIbyID("RepTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function refreshGraph() {
	let dateFrom = $.UIbyID("dateFrom").getValue();
	let dateTo = $.UIbyID("dateTo").getValue();
	let material = $.UIbyID("material").getValue();
	let ordine = $.UIbyID("ordine").getValue();
	var model = new sap.ui.model.json.JSONModel();
	var test = new sap.ui.model.xml.XMLModel();
	test.loadData(QService + dataProdRep +
		"First/getFirstQR&Param.1=false&Param.2="+dateFrom+"&Param.3="+material+"&Param.4="+dateTo+"&Param.5="+$.UIbyID("filtPlant").getSelectedKey()+"&Param.6="+ordine);
	test.attachRequestCompleted(function() {
		var oData = test.getXML();
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString(oData,"text/xml");
		var myObject = {};
		myObject.Row = [];
		var obj = {};
		
		for(var i=0;i<xmlDoc.getElementsByTagName("Rowset")[2].childElementCount;i++) {
			if(xmlDoc.getElementsByTagName("Rowset")[2].childNodes[i].nodeName != 'Columns') {
				var childNode = xmlDoc.getElementsByTagName("Rowset")[2].childNodes[i].childNodes;
				for(var k=0;k<childNode.length; k++) {
					var etichetta = childNode[k].tagName;
					var valore = childNode[k].textContent;
					if(etichetta == 'DATA') {
						var today  = new Date(valore);
						valore = today.toLocaleDateString("it-IT");
					}
					obj[etichetta] = valore;
				}
				myObject.Row.push(obj);
				obj = {};
			}
		}
		myObject.Row.sort(function(a, b) {
			var d1 = new Date(a.DATA);
			var d2 = new Date(b.DATA);
			if (d1.getTime() < d2.getTime()) {
				return -1;
			}
			if (d1.getTime() > d2.getTime()) {
				return 1;
			}
			// names must be equal
			return 0;
		});
		model.setData(myObject)
		$.UIbyID("chart").setModel(model);
		$("#splash-screen").hide();
	});
}

function refreshNumericContent() {
	let dateFrom = $.UIbyID("dateFrom").getValue();
	let dateTo = $.UIbyID("dateTo").getValue();
	let material = $.UIbyID("material").getValue();
	let ordine = $.UIbyID("ordine").getValue();

	var oModel = new sap.ui.model.xml.XMLModel();
	//oModel.setXML('<?xml version="1.0" encoding="UTF-8"?><Rowsets CachedTime="" DateCreated="2019-04-19T17:48:45" EndDate="2019-04-19T17:48:45" StartDate="2019-04-19T16:48:45" Version="15.2 SP3 Patch 7 (Feb 14, 2019)"><Rowset></Rowset><Rowset><Columns><Column Description="VALORE" MaxRange="1" MinRange="0" Name="VALORE" SQLDataType="3" SourceColumn="VALORE"/><Column Description="TOTALE" MaxRange="1" MinRange="0" Name="TOTALE" SQLDataType="3" SourceColumn="TOTALE"/><Column Description="INDICE" MaxRange="1" MinRange="0" Name="INDICE" SQLDataType="3" SourceColumn="INDICE"/><Column Description="DATA" MaxRange="1" MinRange="0" Name="DATA" SQLDataType="91" SourceColumn="DATA"/></Columns><Row><VALORE>40</VALORE><TOTALE>10</TOTALE><INDICE>0.4</INDICE><DATA>2019-04-11T00:00:00</DATA></Row></Rowset><Rowset><Columns><Column Description="VALORE" MaxRange="1" MinRange="0" Name="VALORE" SQLDataType="3" SourceColumn="VALORE"/><Column Description="TOTALE" MaxRange="1" MinRange="0" Name="TOTALE" SQLDataType="3" SourceColumn="TOTALE"/><Column Description="INDICE" MaxRange="1" MinRange="0" Name="INDICE" SQLDataType="3" SourceColumn="INDICE"/><Column Description="DATA" MaxRange="1" MinRange="0" Name="DATA" SQLDataType="91" SourceColumn="DATA"/></Columns><Row><VALORE>4</VALORE><TOTALE>10</TOTALE><INDICE>0.4</INDICE><DATA>2019-04-11T00:00:00</DATA></Row><Row><VALORE>15</VALORE><TOTALE>25</TOTALE><INDICE>0.6</INDICE><DATA>2019-04-05T00:00:00</DATA></Row></Rowset></Rowsets>');
	oModel.loadData(QService + dataProdRep +
		"First/getFirstQR&Param.1=false&Param.2="+dateFrom+"&Param.3="+material+"&Param.4="+dateTo+"&Param.5="+$.UIbyID("filtPlant").getSelectedKey()+"&Param.6="+ordine);

	oModel.attachRequestCompleted(function() {
		var oData = oModel.getXML();
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString(oData,"text/xml");
		var value = 0;
		var totale = 0;

		for(var i=0;i<xmlDoc.getElementsByTagName("Rowset")[1].childElementCount;i++) {
			if(xmlDoc.getElementsByTagName("Rowset")[1].childNodes[i].nodeName != 'Columns') {
				var childNode = xmlDoc.getElementsByTagName("Rowset")[1].childNodes[i].childNodes;
				for(var k=0;k<childNode.length; k++) {
					var etichetta = childNode[k].tagName;
					var valore = childNode[k].textContent;
					if(etichetta == 'TOTALEFIRST') {
						value = valore;
					} else if(etichetta == 'TOTALEBUONI') {
						totale = valore;
					}
				}
			}
		}

		let percentuale = (parseInt(totale) / parseInt(value)) * 100;
		$.UIbyID("RadialMicroChart").setPercentage(Math.round(percentuale));
	});
}