// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataProdTR = "Content-Type=text/XML&QueryTemplate=ProductionMain/Translate/";
var dataProdRep = "Content-Type=text/XML&QueryTemplate=ProductionMain/Report/";
var i = 0;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
jQuery.ajaxSetup({
	cache: false
});

//jQuery.sap.includeScript("/XMII/CM/Common/js/gauge.js", "gauge");

Libraries.load(
    ["/XMII/CM/Common/MII_core/UI5_utils",
	 "/XMII/CM/ProductionMain/MasterData/OrarioLinea_fn/fn_OrarioLinea"],
    function() {
        $(document).ready(function() {
            var oLblPlant = new sap.ui.commons.Label().setText("Linea");
            var labelDate = new sap.ui.commons.Label().setText("Data");           

            var selDate = new sap.m.DatePicker({
                id: "selDate",
                valueFormat: "yyyyMMdd",
                value: formatDate(new Date()),
                change: function (oEvent) {
                    refreshTimePickers();
                }
            }).setWidth("auto");

            //Escape per gestione parametro plant da GET/POST
            var qexe =  dataProdMD + "Lines/getLinesSQ";
            var sUserPlant = fnGetAjaxVal(qexe,["plant"],false);
            if ($('#plant').val() === '{plant}') {
                $('#plant').val(sUserPlant.plant);
            }

            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
                selectedKey: $('#plant').val(),
                tooltip: "Linea", //Divisione
                change: function (oEvent) {
                    //refreshTabReportFirst();
                    //refreshGraph();
                    //refreshNumericContent();
                }
            });

            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("key", "IDLINE");
            oItemPlant.bindProperty("text", "LINETXT");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            var oPlantsModel = new sap.ui.model.xml.XMLModel();
            oPlantsModel.loadData(QService + dataProdMD + "Lines/getLinesSQ"); //getPlantsSQ
            oCmbPlant.setModel(oPlantsModel);

            var oSeparator = new sap.ui.commons.Toolbar({
                items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    labelDate,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    selDate
                ]
            }).addStyleClass("sapUiSizeCompact");
            
            var oVerticalLayout = new sap.ui.layout.VerticalLayout({
                id: "VerticalLayout",
                content: [
                    oSeparator,
                    createGrid()
				],
                width: "100%"
            });

            oVerticalLayout.placeAt("MasterCont");
            $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            //refreshTabReportFirst();
            //refreshGraph();
            //refreshNumericContent();
            $("#splash-screen").hide(); //nasconde l'icona di loading
        })
    }
);

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('');
}
