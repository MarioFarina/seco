"use strict";

var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Conferme produzione
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella conferme di produzione e ritorna l'oggetto oTable
function createTabConfdashboard() {
    
    // contenitore dati lista causali fermo
    
    //Crea L'oggetto Tabella conferme di produzione
    var oTable = UI5Utils.init_UI5_Table({
        id: "ConfdashboardTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_ConfProdList"), //"Lista causali fermo",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("ConfdashboardTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModConfProd").setEnabled(false);
        			$.UIbyID("btnDelConfProd").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModConfProd").setEnabled(true);
                    $.UIbyID("btnDelConfProd").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_ConfProdAdd"), //"Aggiungi dichiarazione",
                id:   "btnAddConfProd",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openConfProdEdit(false, "INSERT");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_ConfProdModify"), //"Modifica quantità dichiarata",
                id:   "btnModConfProd",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openConfProdEdit(true, "UPDATE");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_ConfProdUndo"), //"Annulla dichiarazione"
                id:   "btnDelConfProd",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    fnUndoConfProd();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabConfDashboard();
                }
            })
        ],
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "0px",
                   				 //campo nascosto
                   				 visible: false
				}
            },
            {
				Field: "NAME1", // in Sigit è "PLNAME",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "IDLINE",
				properties: {
					width: "0px",
					visible: false,
				}
            },
            {
				Field: "LINETXT",
				label: oLng_MasterData.getText("MasterData_Line"), //"Linea",
				properties: {
					width: "60px"
				}
            },
            {
				Field: "DATE_SHIFT",
				label: oLng_MasterData.getText("MasterData_DateShift"), //"Abilitato",
				properties: {
					width: "90px"
				},
				template: {
					type: "Date",
					textAlign: "Center"
                			}
            },
            {
				Field: "SHIFT", 
				properties: {
					width: "0px",
                    				visible: false
				}
            },
            {
				Field: "SHNAME", 
				label: oLng_MasterData.getText("MasterData_Shift"), //"Turno",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "OPENAME", 
				label: oLng_MasterData.getText("MasterData_OperatorName"), //"Operatore",
				properties: {
					width: "110px"
				}
            },
            {
				Field: "PERNR", 
				properties: {
					width: "0px",
                    				visible: false
				}
            },
            {
				Field: "ORDER", 
				label: oLng_MasterData.getText("MasterData_Order"), //"Testo esteso",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "POPER", 
				label: oLng_MasterData.getText("MasterData_Operation"), //"STTYPE",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "TYPECONF", 
				label: oLng_MasterData.getText("MasterData_TypeConf"), //"STTYPE",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "SREASID", 
				properties: {
					width: "0px",
                    				visible: false
				}
            },
            {
				Field: "REASTXT", 
				label: oLng_MasterData.getText("MasterData_ScrabReason"), //"STTYPE",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "UDCNR", 
				label: oLng_MasterData.getText("MasterData_UdC"), //"UdC",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "TIME_ID",
				label: oLng_MasterData.getText("MasterData_TimeID"), //"Data acquisizione",
				properties: {
					width: "120px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
               			}
            },
            {
				Field: "QTY", 
				label: oLng_MasterData.getText("MasterData_ConfQty"), //"Q.tà dichiarata",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "QTY_PRO", 
				label: oLng_MasterData.getText("MasterData_ConfQtypro"), //"Q.tà proposta",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "DATE_UPD",
				label: oLng_MasterData.getText("MasterData_UpdateDate"), //"Data aggiornamento",
				properties: {
					width: "120px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                			}
            },
            {
				Field: "CONFIRMED",
				label: oLng_MasterData.getText("MasterData_Confirmed"), //"Confermato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "DELETED",// 
				label: oLng_MasterData.getText("MasterData_Deleted"), //"Cancellato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
               			 }
            },
            {
				Field: "USERMOD", 
				label: oLng_MasterData.getText("MasterData_UserMod"), //"Utente modifica",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "TYPEMOD", 
				label: oLng_MasterData.getText("MasterData_TypeMod"), //"Tipo modifica",
				properties: {
					width: "80px"
				}
            }
        ]/*,
        extension: new sap.ui.commons.Toolbar({
            items: [
//                fnCreateCombo( false, "oCmbLinesFilter", "IDLINE", "LINETXT",  QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlantConfProd").getSelectedKey() )
		new sap.ui.commons.Button({text: "Button in the Extension Area", press: function() { alert("Button pressed!"); }})
            ]
        })
*/
    });
    
    var oModel = new sap.ui.model.xml.XMLModel();

    if ( $.UIbyID("filtPlantConfProd").getSelectedKey() === "" ) $.UIbyID("filtPlantConfProd").setSelectedKey("2101");
    var plant = $.UIbyID("filtPlantConfProd").getSelectedKey() === "" ? encodeURI("2101") : $.UIbyID("filtPlantConfProd").getSelectedKey();

    var oCmbShiftsFilter = fnCreateCombo( false, "oCmbShiftsFilter", "SHIFT", "SHNAME",  QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + plant );
    var oCmbLinesFilter = fnCreateCombo( false, "oCmbLinesFilter", "IDLINE", "LINETXT",  QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + plant )

    var oCmbTypeConfFilter = new sap.ui.commons.ComboBox("oCmbTypeConfFilter");
    var oItem = new sap.ui.core.ListItem("Buoni").setKey("G").setText("Buoni")
    oCmbTypeConfFilter.addItem(oItem);
    oItem = new sap.ui.core.ListItem("Sospesi").setKey("S").setText("Sospesi");
    oCmbTypeConfFilter.addItem(oItem);

    var oDateFrom = new sap.ui.commons.DatePicker('oDateFrom').setYyyymmdd( (new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 6)).toISOString().slice(0,10) );
	oDateFrom.attachChange(
		function(oEvent){
			refreshTabConfDashboard();
		}
	);
	var oDateTo = new sap.ui.commons.DatePicker('oDateTo').setYyyymmdd( new Date().toISOString().slice(0,10) );
	oDateTo.attachChange(
		function(oEvent){
			refreshTabConfDashboard();
		}
	);
	oCmbLinesFilter.attachChange(
		function(oEvent){
			refreshTabConfDashboard();
		}
	);
	oCmbShiftsFilter.attachChange(
		function(oEvent){
			refreshTabConfDashboard();
		}
	)
	oCmbTypeConfFilter.attachChange(
		function(oEvent){
			refreshTabConfDashboard();
		}
	)

    var oExtToolbar = new sap.ui.commons.Toolbar({
             items: [
		new sap.ui.commons.Label().setText( oLng_MaserData.getText("MasterData_DateFrom") ),
		oDateFrom,		
		new sap.ui.commons.Label().setText( oLng_MaserData.getText("MasterData_DateTo") ),
		oDateTo,
		new sap.ui.commons.Label().setText( oLng_MaserData.getText("MasterData_Shift") ),
		oCmbShiftsFilter,
		new sap.ui.commons.Label().setText( oLng_MaserData.getText("MasterData_Line") ),
		oCmbLinesFilter,
		new sap.ui.commons.Label().setText( oLng_MaserData.getText("MasterData_TypeConf") ),
		oCmbTypeConfFilter
            ]
   });


    oTable.setModel(oModel);
    oTable.addExtension(oExtToolbar);
    oTable.bindRows("/Rowset/Row");
    refreshTabConfDashboard();
    return oTable;
}

// Aggiorna la tabella DCONFPROD
function refreshTabConfDashboard() {

//	$.UIbyID("ConfdashboardTab").getModel().loadData(QService + dataProdMD + "ConfProd/getConfProdSQ" );
	var IDLINE = $.UIbyID("oCmbLinesFilter").getSelectedKey() === "" ? encodeURI("%") : $.UIbyID("oCmbLinesFilter").getSelectedKey();
	var SHIFT = $.UIbyID("oCmbShiftsFilter").getSelectedKey() === "" ? encodeURI("%") : $.UIbyID("oCmbShiftsFilter").getSelectedKey();
	var TYPECONF = $.UIbyID("oCmbTypeConfFilter").getSelectedKey() === "" ? encodeURI("%") : $.UIbyID("oCmbTypeConfFilter").getSelectedKey();
	var qexe = QService + dataConfProd + "ConfProd/getConfProdByPlantSQ" +
		"&Param.1=" + "2101"/*$.UIbyID("filtPlantConfProd").getSelectedKey() */ +
		"&Param.2=" + $.UIbyID("oDateFrom").getYyyymmdd() +
		"&Param.3=" + $.UIbyID("oDateTo").getYyyymmdd() +
	            "&Param.4=" + IDLINE +
                        "&Param.5=" + SHIFT +
                        "&Param.6=" + TYPECONF;

	$.UIbyID("ConfdashboardTab").getModel().loadData( qexe );
	$("#splash-screen").hide();
}


// Function per creare la finestra di dialogo 
function openConfProdEdit(bEdit, mode) {
    if (bEdit === undefined) bEdit = false;
    if (mode === undefined) mode = "";

    var oCmbPlantInput = fnCreateCombo( bEdit, "oCmbPlantInput", "PLANT", "NAME1",  QService + dataProdMD + "Plants/getPlantsSQ");
    var oCmbShiftInput = fnCreateCombo( bEdit, "oCmbShiftInput", "SHIFT", "SHNAME",  QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlantConfProd").getSelectedKey() );
    var oCmbLinesInput = fnCreateCombo( bEdit, "oCmbLinesInput", "IDLINE", "LINETXT",  QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlantConfProd").getSelectedKey() );
    var oCmbOperatorInput = fnCreateCombo( bEdit, "oCmbOperatorInput", "PERNR", "OPENAME",  QService + dataProdMD + "Operators/getAllCIDbyPlantSQ&Param.1=" + $.UIbyID("filtPlantConfProd").getSelectedKey() );
    var oCmbScrabReasInput = fnCreateCombo( bEdit, "oCmbScrabReasInput", "SREASID", "REASTXT",  QService + dataProdMD + "ScrabReasons/getScrabReasonsSQ&Param.1=" + $.UIbyID("filtPlantConfProd").getSelectedKey() );
    
    var oCmbTypeConf = new sap.ui.commons.ComboBox("oCmbTypeConf");
    var oItem = new sap.ui.core.ListItem("Buoni").setKey("G").setText("Buoni")
    oCmbTypeConf.addItem(oItem);
    oItem = new sap.ui.core.ListItem("Sospesi").setKey("S").setText("Sospesi");
    oCmbTypeConf.addItem(oItem);
    oCmbTypeConf.setSelectedKey(bEdit ? fnGetCellValue("ConfdashboardTab", "TYPECONF") : "");

    var dateShift = bEdit ? fnGetCellValue("ConfdashboardTab", "DATE_SHIFT").slice(0,10) : new Date().toISOString().slice(0,10);
    var oDateShift = new sap.ui.commons.DatePicker('oDateShift').setYyyymmdd( dateShift );;

    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgConfDashboard",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("ConfdashboardTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ConfProdModify"):oLng_MasterData.getText("MasterData_ConfProdAdd"),
            /*"Modifica materiale":"Nuovo materiale",*/
            //icon: "/images/address.gif",
 //           tooltip: "Info Materiale"//oLng_Gen.getText("PrPage_MsgTT_MaterialInfo")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_LineID"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbLinesInput.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_DateShift"), //"Data turno",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oDateShift.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbPlantInput.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Shift"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbShiftInput.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_OperatorName"), //"Operatore",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbOperatorInput.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Order"), //"Commessa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "ORDER"):"",
                                editable: !bEdit,
                                maxLength: 12,
                                id: "ORDER"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Operation"), //"Fase",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "POPER"):"",
                                editable: !bEdit,
                                maxLength: 4,
                                id: "POPER"
                            })
                        ]
                    }),
                    /*
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_TypeConf"), //"Buoni o sospesi",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "TYPECONF"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "TYPECONF",
                            })
                        ]
                    }),
                    */
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_TypeConf"), //"Tipo conferma",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbTypeConf.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ScrabReason"), //"Data turno",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !bEdit,
                        fields: oCmbScrabReasInput.setEnabled(!bEdit)
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_UdC"), //"UdC",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "UDCNR"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "UDCNR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_TimeID"), //"Data aggiornamento",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnConvertDate(fnGetCellValue("ConfdashboardTab", "TIME_ID")):"",
                                editable: false,
                                maxLength: 19,
                                id: "TIME_ID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ConfQty"), //"Quantità dichiarata",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "QTY"):"",
                                editable: true,
                                maxLength: 10,
                                id: "QTY"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ConfQtypro"), //"Quantità proposta",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "QTY_PRO"):"",
                                editable: false,
                                maxLength: 10,
                                id: "QTY_PRO"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_UpdateDate"), //"Data aggiornamento",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnConvertDate(fnGetCellValue("ConfdashboardTab", "DATE_UPD")):"",
                                editable: false,
                                maxLength: 19,
                                id: "DATE_UPD"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Confirmed"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("ConfdashboardTab", "CONFIRMED") == "0" ? false : true ) : false,
                                editable: false,
                                id: "CONFIRMED" /*"ACTIVE"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Deleted"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("ConfdashboardTab", "DELETED") == "0" ? false : true ) : false,
                                editable: !bEdit,
                                id: "DELETED" /*"ACTIVE"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_UserMod"), //"Quantità dichiarata",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "USERMOD"):"",
                                editable: false,
                                maxLength: 10,
                                id: "USERMOD"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_TypeMod"), //"Quantità dichiarata",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ConfdashboardTab", "TYPEMOD"):"",
                                editable: false,
                                maxLength: 10,
                                id: "TYPEMOD"
                            })
                        ]
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveConfProd(mode);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveConfProd( /*bEdit*/mode ) 
{
    if ( mode === 'undefined' ) mode = "";

    var IDLINE = $.UIbyID("oCmbLinesInput").getSelectedKey(); 
    var DATE_SHIFT = $.UIbyID("oDateShift").getYyyymmdd();
    var SHIFT = $.UIbyID("oCmbShiftInput").getSelectedKey(); 
    var OPEID = $.UIbyID("oCmbOperatorInput").getSelectedKey(); 
    var ORDER = $.UIbyID("ORDER").getValue();
    var POPER = $.UIbyID("POPER").getValue();
    var TYPECONF = $.UIbyID("oCmbTypeConf").getSelectedKey(); 
    var SREASID = $.UIbyID("oCmbScrabReasInput").getSelectedKey();          
    var UDCNR = $.UIbyID("UDCNR").getValue();
    var QTY = $.UIbyID("QTY").getValue() === "" ? 0 : parseFloat($.UIbyID("QTY").getValue());
    var QTY_PRO = $.UIbyID("QTY_PRO").getValue() === "" ? 0 : parseFloat($.UIbyID("QTY_PRO").getValue());
    var plant = $.UIbyID("oCmbPlantInput").getSelectedKey();     
    var CONFIRMED = $.UIbyID("CONFIRMED"/*"ACTIVE"*/).getChecked() === true ? 1 : 0;
    var DELETED = $.UIbyID("DELETED"/*"ACTIVE"*/).getChecked() === true ? 1 : 0;
    var USERMOD = $.UIbyID("USERMOD").getValue();
    var TYPEMOD = $.UIbyID("TYPEMOD").getValue();

    // check chiavi compilate
    if ( IDLINE === "" || DATE_SHIFT === "" || SHIFT === "" || OPEID === "" || ORDER === "" || POPER === "" || TYPECONF === "" /*|| SREASID === ""*/ || UDCNR === "" || plant === "" || QTY === 0 ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError"),
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    /*
    // check input CYCLE_TIME non superiore a 3 decimali
    if ( CYCLE_TIME.substr( CYCLE_TIME.indexOf(".")) > 0 && CYCLE_TIME.substr(CYCLE_TIME.indexOf(".")+1).length > 3 ) {
        sap.ui.commons.MessageBox.show( (oLng_MasterData.getText("MasterData_MaxDecimals") + " 3" ),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_CycleTime"),
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
	return;
    }
;
    // check  PZUDC intero
    if ( PZUDC.indexOf(".") > 0 ) {
        sap.ui.commons.MessageBox.show( (oLng_MasterData.getText("MasterData_UnsignedInput") ),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_NoPcUdC"),
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
	return;
    }
    */
    
    var qexe = "";
    switch( mode ) {
        case "INSERT":
            qexe =dataProdMD +"ConfProd/addConfProdSQ";
            qexe += "&Param.1=" + IDLINE;
            qexe += "&Param.2=" + DATE_SHIFT;
            qexe += "&Param.3=" + SHIFT;
            qexe += "&Param.4=" + OPEID;
            qexe += "&Param.5=" + ORDER;
            qexe += "&Param.6=" + POPER;
            qexe += "&Param.7=" + TYPECONF;
            qexe += "&Param.8=" + SREASID; // Causale di scarto
            qexe += "&Param.9=" + UDCNR; // Identificativo UDC
            qexe += "&Param.10=" + QTY;
            qexe += "&Param.11=" + QTY_PRO; // Quantità proposta
            qexe += "&Param.12=" + plant;
            qexe += "&Param.13=" + CONFIRMED; // Confermato in sap    
            qexe += "&Param.14=" + DELETED; // Conferma stornata ed eliminata    
            qexe += "&Param.15=" + USERMOD; // Utente MII che ha effettuato la modifica    
            qexe += "&Param.16=" + TYPEMOD; // Tipo di modifica
            break;
        case "UPDATE":
/*
	if ( !fnDataChanged( "ConfdashboardTab" ) {
		alert("dati non modificati");
		return;
	}
*/
            qexe =dataProdMD +"ConfProd/updConfProdQtySQ";
            qexe += "&Param.1=" + IDLINE;
            qexe += "&Param.2=" + DATE_SHIFT;
            qexe += "&Param.3=" + SHIFT;
            qexe += "&Param.4=" + OPEID;
            qexe += "&Param.5=" + ORDER;
            qexe += "&Param.6=" + POPER;
            qexe += "&Param.7=" + TYPECONF;
            qexe += "&Param.8=" + SREASID; // Causale di scarto
            qexe += "&Param.9=" + UDCNR; // Identificativo UDC
            qexe += "&Param.10=" + QTY;
            qexe += "&Param.11=" + USERMOD; // Utente MII che ha effettuato la modifica    
            qexe += "&Param.12=" + TYPEMOD; // Tipo di modifica
            break;
    };
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_ConfProdSaved"),
                         oLng_MasterData.getText("MasterData_ConfProdSavedError")
                         /*"Materiale salvato", "Errore salvataggio Materiale"*/,
                         /*CONTROLLARE A COSA SERVE L'ULTIMO PARAMETRO, PASSATO FALSE*/
                         false);
    if (ret){
        $.UIbyID("dlgConfDashboard").close();
        $.UIbyID("dlgConfDashboard").destroy();
        refreshTabConfDashboard();
    }
}

function fnUndoConfProd(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_ConfProdUndoing"), //"Eliminare l'Materiale selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_ConfProdUndoingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var IDLINE = fnGetCellValue("ConfdashboardTab", "IDLINE"); 
                var DATE_SHIFT = fnGetCellValue("ConfdashboardTab", "DATE_SHIFT").slice(0,10);
                var SHIFT = fnGetCellValue("ConfdashboardTab", "SHIFT"); 
                var OPEID = fnGetCellValue("ConfdashboardTab", "OPEID"); 
                var ORDER = fnGetCellValue("ConfdashboardTab", "ORDER");
                var POPER = fnGetCellValue("ConfdashboardTab", "POPER");
                var TYPECONF = fnGetCellValue("ConfdashboardTab", "TYPECONF"); 
                var SREASID = fnGetCellValue("ConfdashboardTab", "SREASID");          
                var UDCNR = fnGetCellValue("ConfdashboardTab", "UDCNR");
                var USERMOD = "";
                var TYPEMOD = "";
                
                var qexe =dataProdMD +"ConfProd/undoConfProdSQ";
                qexe += "&Param.1=" + IDLINE;
                qexe += "&Param.2=" + DATE_SHIFT;
                qexe += "&Param.3=" + SHIFT;
                qexe += "&Param.4=" + OPEID;
                qexe += "&Param.5=" + ORDER;
                qexe += "&Param.6=" + POPER;
                qexe += "&Param.7=" + TYPECONF;
                qexe += "&Param.8=" + SREASID; // Causale di scarto
                qexe += "&Param.9=" + UDCNR; // Identificativo UDC
                qexe += "&Param.10=" + 1;
                qexe += "&Param.11=" + USERMOD; // Utente MII che ha effettuato la modifica    
                qexe += "&Param.12=" + TYPEMOD; // Tipo di modifica
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_ConfProdDeleted"), //"Materiale eliminato correttamente",
                    oLng_MasterData.getText("MasterData_ConfProdDeletedError"), //"Errore in eliminazione Materiale",
                    false
                );
                refreshTabConfDashboard();
                $.UIbyID("btnModConfProd").setEnabled(false);
                $.UIbyID("btnDelConfProd").setEnabled(false);
                $.UIbyID("ConfdashboardTab").setSelectedIndex(-1);
           }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnConvertDate( date ) {
	return date.substr(8,2) + "/" + date.substr(5,2) + "/" + date.substr(0,4) + " " + date.substr(11,8);
}

function fnCreateCombo( bEdit, id, key, text, query, value ) {
	if ( value === 'undefined' ) value = "";
	var oModel4 = new sap.ui.model.xml.XMLModel();
	oModel4.loadData(query);

	// Crea la ComboBox per le divisioni in input
	var oComboBox = new sap.ui.commons.ComboBox({
		id: id, 
		selectedKey : bEdit?fnGetCellValue("ConfdashboardTab", key):value
	});
	oComboBox.setModel(oModel4);		
	var oItemScrabReasonInput = new sap.ui.core.ListItem();
	oItemScrabReasonInput.bindProperty("text", text);
	oItemScrabReasonInput.bindProperty("key", key);	
	oComboBox.bindItems("/Rowset/Row", oItemScrabReasonInput);
	
	return oComboBox;
}

function fnGetSelectedRow( idTable ) {

	var index = $.UIbyID(idTable).getSelectedIndex();
	var obj = [];
	var rowset = $.UIbyID('ConfdashboardTab').getModel().getObject('/Rowset');
	var row = rowset.getElementsByTagName("Row")
	var itm = {}
	var children = row[index].children;
	for (var j = 0; j < children.length; j++) {
		itm[children[j].nodeName] = children[j].textContent;
	}
	obj.push(itm);
	return obj;
}

function fnDataChanged( idTable ) {
	var oldData =getSelectedRow( "ConfdashboardTab" );
	for( var i=0; i<oldData.length; i++ ) {
		
	}
}