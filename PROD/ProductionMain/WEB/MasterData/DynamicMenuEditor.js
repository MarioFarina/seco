// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var icon16 = "/XMII/CM/Common/icons/16x16/";

jQuery.ajaxSetup({
    cache: false
});

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

Libraries.load(
	["/XMII/CM/Common/MII_core/UI5_utils",
     "/XMII/CM/ProductionMain/MasterData/DynamicMenuEditor_fn/fn_DynamicMenuEditor"
    ],
    function () {

        $(document).ready(function () {
            $("#splash-screen").hide();
        });

        // Crea la TabStrip principale
        var oTabMaster = new sap.ui.commons.TabStrip("TabMaster");
        oTabMaster.attachClose(function (oEvent) {
            var oTabStrip = oEvent.oSource;
            oTabStrip.closeTab(oEvent.getParameter("index"));
        });


        // 1. tab: Login Numbers
        var oTable1 = createTabDynamicMenuEditor();
        var oLayout1 = new sap.ui.commons.layout.MatrixLayout("DynamicMenuEditorTab", {
            columns: 1
        });
        oLayout1.createRow(oTable1);
        oTabMaster.createTab(
            oLng_MasterData.getText("MasterData_MenuVoices"), //"Voci Menù",
            oLayout1
        );

        oTabMaster.placeAt("MasterCont");

    }
);