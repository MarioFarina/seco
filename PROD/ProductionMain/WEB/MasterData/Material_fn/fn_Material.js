var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Material
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Material e ritorna l'oggetto oTable
function createTabMaterial() {
    
    // contenitore dati lista causali fermo
    
    //Crea L'oggetto Tabella Material
    var oTable = UI5Utils.init_UI5_Table({
        id: "MaterialTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_MaterialList"), //"Lista causali fermo",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("MaterialTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModMaterial").setEnabled(false);
//        			$.UIbyID("btnDelMaterial").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModMaterial").setEnabled(true);
//                    $.UIbyID("btnDelMaterial").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddMaterial",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openMaterialEdit(false, "INSERT");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModMaterial",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openMaterialEdit(true, "UPDATE");
                }
            }),
/*
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelMaterial",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    fnDelMaterial();
                }
            }),
*/
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabMaterial();
                }
            })
        ],
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "NAME1", // in Sigit è "PLNAME",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "MATERIAL",
				label: oLng_MasterData.getText("MasterData_MaterialCode"), //"Materiale",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "DESC", 
				label: oLng_MasterData.getText("MasterData_MaterialDescr"), //"Descrizione breve",
				properties: {
					width: "170px"
				}
            },
            {
				Field: "ORDER", 
				label: oLng_MasterData.getText("MasterData_Order"), //"Testo esteso",
				properties: {
					width: "80px",
                    visible: false
				}
            },
            {
				Field: "CUSTOMER", 
				label: oLng_MasterData.getText("MasterData_Customer"), //"STTYPE",
				properties: {
					width: "80px",
                    visible: false
				}
            },
            {
				Field: "CYCLE_TIME", 
				label: oLng_MasterData.getText("MasterData_CycleTime"), //"STTYPE",
				properties: {
					width: "80px",
					visible: false
				}
            },
            {
				Field: "cycle_time", 
				label: oLng_MasterData.getText("MasterData_CycleTime"), //"STTYPE",
				properties: {
					width: "80px",
					visible: false
				},
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            },
            {
				Field: "DATEUPD",
				label: oLng_MasterData.getText("MasterData_UpdateDate"), //"Abilitato",
				properties: {
					width: "120px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "PZUDC", 
				label: oLng_MasterData.getText("MasterData_NoPcUdC"), //"N. pezzi UdC",
				properties: {
					width: "80px",
					visible: false
				},
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            },
            {
				Field: "DISEGNO", 
				label: oLng_MasterData.getText("MasterData_Draw"), //"Disegno",
				properties: {
					width: "120px"
				}
            },
            {
				Field: "EXID", 
				label: oLng_MasterData.getText("MasterData_ExternarlID"),
				properties: {
					width: "80px"
				}
            }
        ],
        /*
        extension: new sap.ui.commons.Toolbar({
            items: [
                oCmbPlant,
                new sap.ui.commons.Button({
                    text: oLng_Gen.getText("ConfPge_BtnResetCalendarText"),
                    id: "btnResetMaterial",
                    icon: icon16 + "application-export.png",
                    enabled: false,
                    press: function(){
                        $.UIbyID("MaterialTab").filter(oPlantFMaterial,"");
                        $.UIbyID("btnResetMaterial").setEnabled(false);
                        // $.UIbyID("btnCopySAP").setEnabled(false);
                        $.UIbyID("filtPlant").setSelectedKey("");
                    }
                })
            ]
        })
        */
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabMaterial();
    return oTable;
}

// Aggiorna la tabella Material
function refreshTabMaterial() {
    
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("MaterialTab").getModel().loadData(
            QService + dataProdMD +
            "Material/getMaterialByPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("MaterialTab").getModel().loadData(
            QService + dataProdMD +
            "Material/getMaterialByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("MaterialTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Material
function openMaterialEdit(bEdit, mode) {
    if (bEdit === undefined) bEdit = false;
    if (mode === undefined) mode = "";
    
   // contenitore dati lista divisioni
	var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        enabled: false,
        selectedKey : bEdit?fnGetCellValue("MaterialTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey()
    });
	oCmbPlantInput.setModel(oModel3);		
	var oItemPlantInput = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    var oModel4 = new sap.ui.model.xml.XMLModel();
	oModel4.loadData(
        QService + dataProdMD +
        "Grstreas/getGrstreasSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
    );

/*
	// Crea la ComboBox per le divisioni in input
	var oCmbPlantMaterial = new sap.ui.commons.ComboBox({
        id: "oCmbPlantMaterial", 
        selectedKey : bEdit?fnGetCellValue("MaterialTab", "STTYPE"):""
    });
	oCmbPlantMaterial.setModel(oModel4);		
	var oItemScrabReasonInput = new sap.ui.core.ListItem();
    // oItemScrabReasonInput.bindProperty("text", "PLNAME");
	oItemScrabReasonInput.bindProperty("text", "REASTXT");
	oItemScrabReasonInput.bindProperty("key", "STTYPE");	
	oCmbPlantMaterial.bindItems("/Rowset/Row", oItemScrabReasonInput);
*/    

    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgMaterial",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("MaterialTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyMaterial"):oLng_MasterData.getText("MasterData_NewMaterial"),
            /*"Modifica materiale":"Nuovo materiale",*/
            //icon: "/images/address.gif",
 //           tooltip: "Info Materiale"//oLng_Gen.getText("PrPage_MsgTT_MaterialInfo")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MaterialCode"), //"Materiale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "MATERIAL"):"",
                                editable: !bEdit,
                                maxLength: 40,
                                id: "MATERIAL"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MaterialDescr"), //"Descrizione materiale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "DESC"):"",
                                editable: true,
                                maxLength: 40,
                                id: "DESC"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Order"), //"Commessa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "ORDER"):"",
                                editable: true,
                                maxLength: 12,
                               id: "ORDER"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Customer"), //"Cliente",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "CUSTOMER"):"",
                                editable: !bEdit,
                                maxLength: 10,
                               id: "CUSTOMER"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_CycleTime"), //"Tempo ciclo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?parseFloat(fnGetCellValue("MaterialTab", "cycle_time")).toFixed(3):"0",
                                editable: true,
                                maxLength: 10,
                                id: "cycle_time",
                                liveChange: function(ev){
                                    /* VERSIONE DI ALEX PER LA GESTIONE DEL CAMPO SOLO NUMERICO CON DECIMALI*/
                                    var oldVal = this['oldValue'];
                                    var val = ev.getParameter('liveValue');
                                    var oldFocus = this['oldFocus'];
                                    var newval = val.replace(/[^.\d]/g, '');
                                    if(newval.split(".").length - 1 > 1)
                                        newval = oldVal;
                                    this.setValue(newval);
                                    this['oldValue'] = newval;
                                    //console.log(JSON.stringify(oldFocus));
                                    if(newval === oldVal) {
                                        //console.log(JSON.stringify(newval));
                                        this.applyFocusInfo({cursorPos:oldFocus.cursorPos});
                                    }
                                    else
                                        this['oldFocus'] = this.getFocusInfo();
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_UpdateDate"), //"Data aggiornamento",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnConvertDate(fnGetCellValue("MaterialTab", "DATEUPD")):"",
                                editable: false,
                                maxLength: 19,
                               id: "DATEUPD"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_NoPcUdC"), //"N. pezzi UdC",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "PZUDC"):"",
                                editable: true,
                                maxLength: 10,
                                id: "PZUDC",
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Draw"), //"Disegno",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "DISEGNO"):"",
                                editable: true,
                                maxLength: 10,
                               id: "DISEGNO"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExternarlID"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MaterialTab", "EXID"):"",
                                editable: true,
                                maxLength: 20,
                               id: "txtEXID"
                            })
                        ]
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveMaterial(mode);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveMaterial( /*bEdit*/mode ) 
{
    if ( mode === 'undefined' ) mode = "";

    var MATERIAL = $.UIbyID("MATERIAL").getValue(); 
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var DESC = $.UIbyID("DESC").getValue();
    var ORDER = $.UIbyID("ORDER").getValue();
    var CUSTOMER = $.UIbyID("CUSTOMER").getValue();
    var CYCLE_TIME = parseFloat( $.UIbyID("cycle_time").getValue()==="" ? "0" : $.UIbyID("cycle_time").getValue() ).toString();
    var DATEUPD = new Date().toISOString().slice(0,19).replace("T", " ");
    var PZUDC = $.UIbyID("PZUDC").getValue() === "" ? "0" :$.UIbyID("PZUDC").getValue() ;
    var DISEGNO = $.UIbyID("DISEGNO").getValue();
    var sExId = $.UIbyID("txtEXID").getValue();

    // check chiavi compilate
    if ( MATERIAL === "" || plant === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError"),
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    // check input CYCLE_TIME non superiore a 3 decimali
    if ( CYCLE_TIME.substr( CYCLE_TIME.indexOf(".")) > 0 && CYCLE_TIME.substr(CYCLE_TIME.indexOf(".")+1).length > 3 ) {
        sap.ui.commons.MessageBox.show( (oLng_MasterData.getText("MasterData_MaxDecimals") + " 3" ),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_CycleTime"),
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
	return;
    }
;
    // check  PZUDC intero
    if ( PZUDC.indexOf(".") > 0 ) {
        sap.ui.commons.MessageBox.show( (oLng_MasterData.getText("MasterData_UnsignedInput") ),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_NoPcUdC"),
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
	return;
    }
;
    var qexe = "";
    switch( mode ) {
        case "INSERT":
            qexe =dataProdMD +"Material/addMaterialSQ";
            break;
        case "UPDATE":
            qexe =dataProdMD +"Material/updMaterialSQ";
            break;
    };

    qexe += "&Param.1=" + plant +
                "&Param.2=" + MATERIAL +
                "&Param.3=" + DESC +
                "&Param.4=" + ORDER +
                "&Param.5=" + CUSTOMER +
                "&Param.6=" + CYCLE_TIME +
                "&Param.7=" + PZUDC +
                "&Param.8=" + DISEGNO +
                "&Param.9=" + sExId;
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_MaterialSaved"),
                         oLng_MasterData.getText("MasterData_MaterialSavedError")
                         /*"Materiale salvato", "Errore salvataggio Materiale"*/,
                         /*CONTROLLARE A COSA SERVE L'ULTIMO PARAMETRO, PASSATO FALSE*/
                         false);
    if (ret){
        $.UIbyID("dlgMaterial").close();
        $.UIbyID("dlgMaterial").destroy();
        refreshTabMaterial();
    }
}

function fnDelMaterial(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_MaterialDeleting"), //"Eliminare l'Materiale selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "Material/delMaterialSQ" +
                            "&Param.1=" + fnGetCellValue("MaterialTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("MaterialTab", "STOPID");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_MaterialDeleted"), //"Materiale eliminato correttamente",
                    oLng_MasterData.getText("MasterData_MaterialDeletedError"), //"Errore in eliminazione Materiale",
                    false
                );
                refreshTabMaterial();
                $.UIbyID("btnModMaterial").setEnabled(false);
                $.UIbyID("btnDelMaterial").setEnabled(false);
                 $.UIbyID("MaterialTab").setSelectedIndex(-1);
           }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnConvertDate( date ) {
	return date.substr(8,2) + "/" + date.substr(5,2) + "/" + date.substr(0,4) + " " + date.substr(11,8);
}

