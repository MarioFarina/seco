
/***********************************************************************************************/
// Funzioni ed oggetti per Macchine
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Macchine e ritorna l'oggetto oTable
function createTabMachine(sIdLine) {

   var iTabVisibleRowCount = 15;
	if(typeof(sIdLine) === 'undefined')
        sIdLine = '';
    else
        iTabVisibleRowCount = 3;

   //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "MachTab" + sIdLine,
        properties: {
            title: oLng_MasterData.getText("MasterData_MachinesList"), //"Lista Macchine",
            visibleRowCount: iTabVisibleRowCount,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("MachTab" + sIdLine).getSelectedIndex() == -1){
                    $.UIbyID("btnModMach" + sIdLine).setEnabled(false);
                    $.UIbyID("btnDelMach" + sIdLine).setEnabled(false);
                }
                else{
                    $.UIbyID("btnModMach" + sIdLine).setEnabled(true);
                    $.UIbyID("btnDelMach" + sIdLine).setEnabled(true);
                }
            }
        },
        toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                icon: 'sap-icon://add',
                press: function(){
                    openMachEdit(false,sIdLine);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id: "btnModMach" + sIdLine,
                icon: 'sap-icon://edit',
                enabled: false,
                press: function(){
                    openMachEdit(true,sIdLine);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina",
                id: "btnDelMach" + sIdLine,
                icon: 'sap-icon://delete',
                enabled: false,
                press: function(){
                    fnDelMach(sIdLine);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function(){
                    refreshTabMacchine(sIdLine);
                }
            })
        ],
        columns: [
			{
				Field: "IDMACH",
				label: oLng_MasterData.getText("MasterData_MachineID"), //"ID Macchina",
				properties: {
					width: "120px",
					flexible : false
				}
			},
            {
                Field: "MACHTXT",
				label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
				properties: {
					width: "150px",
					flexible : false
				}
            },
            {
                Field: "EXID",
				label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
				properties: {
					width: "110px",
					flexible : false
				}
            },
            {
                Field: "ENABLED",
				label: oLng_MasterData.getText("MasterData_Active"), //"Attivo",
				properties: {
					width: "50px",
					flexible : false
				},
                template: {
                    type: "checked"
                }
            },
            {
                Field: "TYPE",
				label: oLng_MasterData.getText("MasterData_Type"), //"Lettura",
				properties: {
					width: "110px",
					flexible : false,
					tooltip: oLng_MasterData.getText("MasterData_TypeTooltip")
				}
            },
            {
				Field: "STOPTRIM",
				label: oLng_MasterData.getText("MasterData_Stoptrim"), //"STOPTRIM (tolleranza) in secondi",
				properties: {
					width: "110px",
					flexible : false,
					tooltip: oLng_MasterData.getText("MasterData_StoptrimTooltip")
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
			},
			{
                Field: "PPMODE",
                label: oLng_MasterData.getText("MasterData_PpMode"),
                properties: {
                    width: "110px",
                    flexible : false
                }
			},
			{
                Field: "DRADR",
                properties: {
                    width: "110px",
                    visible: false,
                    flexible : false
                }
			},
			{
                Field: "DRPWD",
                properties: {
                    width: "110px",
                    visible: false,
                    flexible : false
                }
			},
			{
                Field: "EXID2",
                label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                properties: {
                    width: "110px",
                    visible : false
                }
            },
            {
                Field: "EXID3",
                label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                properties: {
                    width: "110px",
                    visible : false
                }
            },
            {
                Field: "IDFILE",
                label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                properties: {
                    width: "110px",
                    visible : false
                }
            },
            {
                Field: "PPADR",
                label: oLng_MasterData.getText("MasterData_PPAdr"), //"ID Esterno",
                properties: {
                    width: "110px",
                    visible : false
                }
            },
            {
                Field: "PPUSR",
                label: oLng_MasterData.getText("MasterData_PPUsr"), //"ID Esterno",
                properties: {
                    width: "110px",
                    visible : false
                }
            },
            {
                Field: "PPPWD",
                label: oLng_MasterData.getText("MasterData_PPPwd"), //"ID Esterno"
                properties: {
                    width: "110px",
                    visible : false
                }
            }
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    refreshTabMacchine(sIdLine);

	return oTable;
}

// Aggiorna la tabella Macchine
function refreshTabMacchine(sIdLine) {
    var qexe = QService + dataProdMD;
	if(sIdLine){
        qexe += "Lines/getMachinesFromIDLineSQ&Param.1=" + sIdLine;
        var qexe2 = QService + dataProdMD + "Machines/getMachinesSQ";
        $.UIbyID("MachTab" + sIdLine).getModel().loadData(qexe);
        $.UIbyID("MachTab" + sIdLine).setSelectedIndex(-1);
        if ($.UIbyID("filtPlant").getSelectedKey() === ""){
            qexe2 += "&Param.1=" + $('#plant').val();
        }
        else{
            qexe2 += "&Param.1=" + $.UIbyID("filtPlant").getSelectedKey();
        }
        //$.UIbyID("MachTab").getModel().loadData(qexe2);
    }
	else{
        qexe += "Machines/getMachinesSQ";
        if ($.UIbyID("filtPlant").getSelectedKey() === ""){
            //$.UIbyID("MachTab").getModel().loadData(
            //    qexe + "&Param.1=" + $('#plant').val()
            //);
        }
        else{
            //$.UIbyID("MachTab").getModel().loadData(
            //    qexe + "&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
            //);
        }
    }
    //$.UIbyID("MachTab").setSelectedIndex(-1);
    //$("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Macchine
function openMachEdit(bEdit,sIdLine) {

    if (bEdit == undefined)
        bEdit = false;

//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
        title: oLng_MasterData.getText("MasterData_MachinesRegistry"), //"Anagrafica Macchine",
        modal:  true,
        resizable: true,
        id: "dlgAddMach" + sIdLine,
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });

    var oLineField;
	// contenitore dati lista linee
	var oModel = new sap.ui.model.xml.XMLModel()
	oModel.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
	// Crea la ComboBox per le divisioni
    oLineField = new sap.ui.commons.ComboBox({
        id : "lineField" + sIdLine,
        selectedKey : bEdit?fnGetCellValue("MachTab" + sIdLine, "IDLINE"):""
    });

	oLineField.setModel(oModel);
    if(sIdLine){
        oLineField.setSelectedKey(sIdLine);
        oLineField.setEnabled(false);
    }
	var oItemLine = new sap.ui.core.ListItem();
	oItemLine.bindProperty("text", "LINETXT");
	oItemLine.bindProperty("key", "IDLINE");
	oLineField.bindItems("/Rowset/Row", oItemLine);
	//oEdtDlg.setTitle("Aggiungi Macchina");

     // contenitore dati lista Tipi macchine
	var oModel1 = new sap.ui.model.xml.XMLModel()
	oModel1.loadData(QService + dataProdMD + "Machines/GetMachTypeListQR");
	// Crea la ComboBox per lista tipi macchine
	var ocmbMType= new sap.ui.commons.ComboBox({
		id : "cmbMType" + sIdLine,
		selectedKey : bEdit?fnGetCellValue("MachTab" + sIdLine, "TYPE"):""
	});
	ocmbMType.setModel(oModel1);
	var oItemMon = new sap.ui.core.ListItem();
	oItemMon.bindProperty("text", "value");
	oItemMon.bindProperty("key", "name");

	ocmbMType.bindItems("/Rowset/Row", oItemMon);

	var oModel2 = new sap.ui.model.xml.XMLModel()
	oModel2.loadData(QService + dataProdMD + "Machines/GetPPTypeListQR");

	var ocmbPPMode= new sap.ui.commons.ComboBox({
		id : "ocmbPPMode" + sIdLine,
		selectedKey : bEdit?fnGetCellValue("MachTab" + sIdLine, "PPMODE"):""
	});
	ocmbPPMode.setModel(oModel2);
	var oItemPPMode = new sap.ui.core.ListItem();
	oItemPPMode.bindProperty("text", "value");
	oItemPPMode.bindProperty("key", "name");

	ocmbPPMode.bindItems("/Rowset/Row", oItemPPMode);

	if (bEdit) {
		// pulizia campo OrdID
		var lordId = fnGetCellValue("MachTab" + sIdLine, "ORDID");
		if (Number(lordId) <= 0 )
			lordId = 1;
	}

    var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});

    var oForm1 = new sap.ui.layout.form.Form({
        //title: new sap.ui.core.Title({text: "Anagrafica Macchine", icon: 'sap-icon://edit', tooltip: "Info Macchina"}),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ID") + ":", //"ID:",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "IDMACH"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "txtMachId" + sIdLine,
                                change: function() {
                                    UI5Utils.validationTextField(this, /^[A-Za-z0-9-]+$/, "saveMachineButton");
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "MACHTXT"):"",
                                maxLength: 50,
                                id: "txtMachTxt" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Line"), //"Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: oLineField
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "NOTES"):"",
                                maxLength: 255,
                                id: "txtNote" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Stoptrim"), //"STOPTRIM",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit ? fnGetCellValue("MachTab" + sIdLine, "STOPTRIM") : "120",
                                maxLength: 3,
                                id: "txtSTOPTRIM" + sIdLine,
																tooltip: oLng_MasterData.getText("MasterData_StoptrimTooltip"),
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),                    
                    new sap.ui.layout.form.FormElement({
                        fields: [
                            new sap.ui.commons.Label({
                                tooltip : oLng_MasterData.getText("MasterData_EnabledMachine"),
                                text: oLng_MasterData.getText("MasterData_Active"), //"Attivo",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                tooltip : oLng_MasterData.getText("MasterData_EnabledMachine"), //'Macchina attiva',
                                id : 'chkFix' + sIdLine,
                                checked : (fnGetCellValue("MachTab" + sIdLine, "ENABLED")=='1')?true : false
                            }),
                            new sap.ui.commons.Label({
                                tooltip : oLng_MasterData.getText("MasterData_In"),
                                text: oLng_MasterData.getText("MasterData_In"), //"in Dashboard",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                tooltip : oLng_MasterData.getText("MasterData_In"), //'Macchina attiva',
                                id : 'chkIn' + sIdLine,
                                checked : (fnGetCellValue("MachTab" + sIdLine, "Q_IN")=='1')?true : false
                            }),
                            new sap.ui.commons.Label({
                                tooltip : oLng_MasterData.getText("MasterData_Out"),
                                text: oLng_MasterData.getText("MasterData_Out"), //"in Dashboard",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                tooltip : oLng_MasterData.getText("MasterData_Out"), //'Macchina attiva',
                                id : 'chkOut' + sIdLine,
                                checked : (fnGetCellValue("MachTab" + sIdLine, "Q_OUT")=='1')?true : false
                            }),
                            new sap.ui.commons.Label({
                                tooltip : oLng_MasterData.getText("MasterData_PeriodicStCheTooltip"),
                                text: oLng_MasterData.getText("MasterData_PeriodicStChe"), //"in Dashboard",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                tooltip : oLng_MasterData.getText("MasterData_PeriodicStCheTooltip"), //'Macchina attiva',
                                id : 'chkJob' + sIdLine,
                                checked : (fnGetCellValue("MachTab" + sIdLine, "PERIODIC_ST_CHECK")=='1')?true : false
                            })
                        ]
                    })
                ]
            }),
            new sap.ui.layout.form.FormContainer({
                title: new sap.ui.core.Title({text: oLng_MasterData.getText("MasterData_TitleForm")}),
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ReadingType"), //"Tipo Lettura",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: ocmbMType
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "EXID"):"",
                                maxLength: 15,
                                id: "txtEXID" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExternarlID2"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "EXID2"):"",
                                maxLength: 15,
                                id: "txtEXID2" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExternarlID3"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "EXID3"):"",
                                maxLength: 15,
                                id: "txtEXID3" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_DRADR"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "DRADR"):"",
                                maxLength: 255,
                                id: "txtDRADR" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PPPwd"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "DRPWD"):"",
                                maxLength: 255,
                                id: "txtDRPWD" + sIdLine
                            })
                        ]
                    })
                ]
            }),
            new sap.ui.layout.form.FormContainer({
                title: new sap.ui.core.Title({text: oLng_MasterData.getText("MasterData_TitleForm1")}),
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_SendingType"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: ocmbPPMode
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PPAdr"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "PPADR"):"",
                                maxLength: 15,
                                id: "txtPPADR" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_DRADR"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "PPUSR"):"",
                                maxLength: 15,
                                id: "txtPPUSR" + sIdLine
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PPPwd"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.PasswordField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "PPPWD"):"",
                                maxLength: 15,
                                id: "txtPPPWD" + sIdLine
                            })
                        ]
                    })
                ]
            }),
            new sap.ui.layout.form.FormContainer({
                title: new sap.ui.core.Title({text: oLng_MasterData.getText("MasterData_IDFILE")}),
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PCoConfiguration"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("MachTab" + sIdLine, "IDFILE"):"",
                                maxLength: 250,
                                id: "txtIDFILE" + sIdLine
                            })
                        ]
                    })
                ]
            })
        ]
    });

	oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(
        new sap.ui.commons.Button({
            text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
						id: "saveMachineButton",
            press:function(){
                if (bEdit)
                    fnSaveMach(sIdLine);
                else fnAddMach(sIdLine)
            }
        })
    );
	oEdtDlg.addButton(
        new sap.ui.commons.Button({
            text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
            press:function(){
                oEdtDlg.close();
                oEdtDlg.destroy()
            }
        })
    );
	oEdtDlg.open();
}

// Funzione per salvare l'inserimento di un macchina
function fnAddMach(sIdLine) {
    var sMachId = $.UIbyID("txtMachId" + sIdLine).getValue();

    if( sMachId === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    var iStopTrim = ($.UIbyID("txtSTOPTRIM" + sIdLine).getValue() === ""?"120":$.UIbyID("txtSTOPTRIM" + sIdLine).getValue());
    iStopTrim = parseInt(iStopTrim);

    var qexe = dataProdMD+"Machines/addMachinesSQ&Param.1=" + sMachId;
    qexe += "&Param.2=" + $.UIbyID("txtMachTxt" + sIdLine).getValue();
    qexe += "&Param.3=" + $.UIbyID("txtEXID" + sIdLine).getValue();
    qexe += "&Param.4=" + $.UIbyID("lineField" + sIdLine).getSelectedKey();
    qexe += "&Param.5=" + $.UIbyID("txtNote" + sIdLine).getValue();
    qexe += "&Param.6=0";
    qexe += "&Param.7=" + $.UIbyID("chkFix" + sIdLine).getChecked();
    qexe += "&Param.8=" + $.UIbyID("chkIn" + sIdLine).getChecked();
    qexe += "&Param.9=" + $.UIbyID("chkOut" + sIdLine).getChecked();
    qexe += "&Param.10=" + $.UIbyID("cmbMType" + sIdLine).getSelectedKey();
    qexe += "&Param.11=" + iStopTrim;
		qexe += "&Param.12=" + $.UIbyID("chkJob" + sIdLine).getChecked();
		qexe += "&Param.13=" + $.UIbyID("txtEXID2" + sIdLine).getValue();
		qexe += "&Param.14=" + $.UIbyID("txtEXID3" + sIdLine).getValue();
		qexe += "&Param.15=" + $.UIbyID("txtIDFILE" + sIdLine).getValue();
		qexe += "&Param.16=" + $.UIbyID("ocmbPPMode" + sIdLine).getSelectedKey();
		qexe += "&Param.17=" + $.UIbyID("txtPPADR" + sIdLine).getValue();
		qexe += "&Param.18=" + $.UIbyID("txtPPUSR" + sIdLine).getValue();
		qexe += "&Param.19=" + $.UIbyID("txtPPPWD" + sIdLine).getValue();
		qexe += "&Param.20=" + $.UIbyID("txtDRADR" + sIdLine).getValue();
		qexe += "&Param.21=" + $.UIbyID("txtDRPWD" + sIdLine).getValue();

    var ret = fnSQLQuery(
        qexe,
        oLng_MasterData.getText("MasterData_MachineAdded"), //"Macchina aggiunta correttamente",
        oLng_MasterData.getText("MasterData_MachineAddedError"), //"Errore in aggiunta macchina",
        false
    );

    if (ret) {
        $.UIbyID("dlgAddMach" + sIdLine).close();
        $.UIbyID("dlgAddMach" + sIdLine).destroy();
        refreshTabMacchine(sIdLine);
    }
}

// Funzione per salvare le modifiche al macchina
function fnSaveMach (sIdLine){
    var sMachId = $.UIbyID("txtMachId" + sIdLine).getValue();

    if( sMachId === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    var iStopTrim = ($.UIbyID("txtSTOPTRIM" + sIdLine).getValue() === ""?"120":$.UIbyID("txtSTOPTRIM" + sIdLine).getValue());
    iStopTrim = parseInt(iStopTrim);

    var qexe = dataProdMD+"Machines/saveMachinesSQ&Param.1=" + sMachId;
    qexe += "&Param.2=" + $.UIbyID("txtMachTxt" + sIdLine).getValue();
    qexe += "&Param.3=" + $.UIbyID("txtEXID" + sIdLine).getValue();
    qexe += "&Param.4=" + $.UIbyID("lineField" + sIdLine).getSelectedKey();
    qexe += "&Param.5=" + $.UIbyID("txtNote" + sIdLine).getValue();
    qexe += "&Param.6=0";
    qexe += "&Param.7=" + $.UIbyID("chkFix" + sIdLine).getChecked();
    qexe += "&Param.8=" + $.UIbyID("chkIn" + sIdLine).getChecked();
    qexe += "&Param.9=" + $.UIbyID("chkOut" + sIdLine).getChecked();
    qexe += "&Param.10=" + $.UIbyID("cmbMType" + sIdLine).getSelectedKey();
    qexe += "&Param.11=" + iStopTrim;
		qexe += "&Param.12=" + $.UIbyID("chkJob" + sIdLine).getChecked();
		qexe += "&Param.13=" + $.UIbyID("txtEXID2" + sIdLine).getValue();
		qexe += "&Param.14=" + $.UIbyID("txtEXID3" + sIdLine).getValue();
		qexe += "&Param.15=" + $.UIbyID("txtIDFILE" + sIdLine).getValue();
		qexe += "&Param.16=" + $.UIbyID("ocmbPPMode" + sIdLine).getSelectedKey();
		qexe += "&Param.17=" + $.UIbyID("txtPPADR" + sIdLine).getValue();
		qexe += "&Param.18=" + $.UIbyID("txtPPUSR" + sIdLine).getValue();
		qexe += "&Param.19=" + $.UIbyID("txtPPPWD" + sIdLine).getValue();
		qexe += "&Param.20=" + $.UIbyID("txtDRADR" + sIdLine).getValue();
		qexe += "&Param.21=" + $.UIbyID("txtDRPWD" + sIdLine).getValue();

    var ret = fnSQLQuery(
        qexe,
        oLng_MasterData.getText("MasterData_MachineSaved"), //"Macchina salvata correttamente",
        oLng_MasterData.getText("MasterData_MachineSavedError"), //"Errore in salvataggio macchina",
        false
    );
    if (ret){
        $.UIbyID("dlgAddMach" + sIdLine).close();
        $.UIbyID("dlgAddMach" + sIdLine).destroy();
        refreshTabMacchine(sIdLine);
    }
}

// Funzione per cancellare un macchina
function fnDelMach (sIdLine) {
    var sMachId = fnGetCellValue("MachTab" + sIdLine, "IDMACH");
    var valTxt = fnGetCellValue("MachTab" + sIdLine, "MACHTXT");

	sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_MachineDeletingWithDescription") //"Eliminare la macchina selezionata con descrizione:
         + "\n'"+valTxt+"'?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD+"Machines/delMachinesSQ&Param.1=" + sMachId;
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_MachineDeleted"), //"Macchina eliminata correttamente",
                    oLng_MasterData.getText("MasterData_MachineDeletedError"), //"Errore in eliminazione macchina",
                    false
                );
				refreshTabMacchine(sIdLine);
				$.UIbyID("btnDelMach" + sIdLine).setEnabled(false);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}
