
/***********************************************************************************************/
// Funzioni ed oggetti per Turni
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Funzione che crea la tabella Turni e ritorna l'oggetto oTable
function createTabShift() {

	//Crea L'oggetto Tabella Turni
	var oTable = UI5Utils.init_UI5_Table(
    {
		id: "ShiftTab",
		properties: {
			title: oLng_MasterData.getText("MasterData_ShiftsList"), //"Lista Turni",
			visibleRowCount: 15,
			width: "98%",
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			firstVisibleRow: 0,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function(oControlEvent)
			{
				if ($.UIbyID("ShiftTab").getSelectedIndex() == -1) {
					$.UIbyID("btnModShift").setEnabled(false);
					$.UIbyID("btnDelShift").setEnabled(false);
				}
				else {
					$.UIbyID("btnModShift").setEnabled(true);
					$.UIbyID("btnDelShift").setEnabled(true);
				}
			}
		},
		toolbarItems: [
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
				icon: "sap-icon://add",
				tooltip: oLng_MasterData.getText("MasterData_CreateNewShift"), //"Crea un nuovo turno",
				press: function () {
					openShiftEdit(false);
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
				id:   "btnModShift",
				icon: "sap-icon://edit",
				enabled: false,
				press: function () {
					openShiftEdit(true);
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina",
				id:   "btnDelShift",
				icon: "sap-icon://delete",
				enabled: false,
				press: function () {
					fnDelShift();
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
				icon: "sap-icon://refresh",
				enabled: true,
				press: function () {
					refreshShiftsTab();
				}
			})
		]
	});

	UI5Utils.addColumn({
		table: oTable,
		column: {
			Field: "NAME1",
			label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
			properties: {
				width: "100px"
			},
			template: {
				type: "TextView"
			}
		}
	});
	UI5Utils.addColumn({
		table: oTable,
		column: {
			Field: "SHIFT",
			label: oLng_MasterData.getText("MasterData_Shift"), //"Turno",
			properties: {
				width: "100px"
			},
			template: {
				type: "TextView"
			}
		}
	});
	UI5Utils.addColumn({
		table: oTable,
		column: {
			Field: "SHNAME",
			label: oLng_MasterData.getText("MasterData_ShiftDescription"), //"Desc. Turno",
			properties: {
				width: "150px"
			},
			template: {
				type: "TextView"
			}
		}
	});
	UI5Utils.addColumn({
		table: oTable,
		column: {
			Field: "TIMEFROM",
			label: oLng_MasterData.getText("MasterData_StartTime"), //"Ora Inizio",
			properties: {
				width: "100px"
			},
			template: {
				type: "Time"
			}
		}
	});
	UI5Utils.addColumn({
		table: oTable,
		column: {
			Field: "TIMETO",
			label: oLng_MasterData.getText("MasterData_EndTime"), //"Ora Fine",
			properties: {
				width: "100px"
			},
			template: {
				type: "Time"
			}
		}
	});

	var oShiftsModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oShiftsModel);
	oTable.bindRows("/Rowset/Row");

	refreshShiftsTab();

	return oTable;
}

// Aggiorna la tabella Turni
function refreshShiftsTab() {
	if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("ShiftTab").getModel().loadData(
            QService + dataProdMD +
            "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("ShiftTab").getModel().loadData(
            QService + dataProdMD +
            "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("ShiftTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Turni
function openShiftEdit(bEdit) {

	if (bEdit == undefined)
		bEdit = false;

	// contenitore dati lista divisioni
	var oModel = new sap.ui.model.xml.XMLModel()
	oModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni
	var oCmbPlant = new sap.ui.commons.ComboBox({
		id : "cmbShiftPlants",
		selectedKey : bEdit?fnGetCellValue("ShiftTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey(),
		enabled: false
	});
	oCmbPlant.setModel(oModel);

	var oItemPlant = new sap.ui.core.ListItem();
	oItemPlant.bindProperty("text", "NAME1");
	oItemPlant.bindProperty("key", "PLANT");
	oCmbPlant.bindItems("/Rowset/Row", oItemPlant);

	// contenitore dati lista CID
	//var oModel2 = new sap.ui.model.xml.XMLModel()
	//oModel2.loadData(QService + dataProdMD + "Shifts/getAllCIDQR");

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
        title: bEdit? oLng_MasterData.getText("MasterData_ModifyShift"):oLng_MasterData.getText("MasterData_NewShift"),
                    /*"Modifica Turno": "Nuovo Turno",*/
        modal:  true,
        resizable: true,
        id: "dlgShift",
        maxWidth:  "600px",
        Width:   "600px",
        showCloseButton: false
    });

	//oEdtDlg.setTitle("Aggiungi Turniaggio");
	var userTimezoneOffset = new Date().getTimezoneOffset()*60000;
	var oDateFrom = new Date(fnGetCellValue("ShiftTab", "TIMEFROM"));
	//oDateFrom = new Date(oDateFrom.getTime() + userTimezoneOffset);
	var oDateTo = new Date(fnGetCellValue("ShiftTab", "TIMETO"));
	//oDateTo = new Date(oDateTo.getTime() + userTimezoneOffset);

	//(fnGetCellValue("ShiftTab", "FixedVal")==1)?true : false
	var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var iconTmp;

	var oForm1 = new sap.ui.layout.form.Form({
		//title: new sap.ui.core.Title({text: bEdit? "Modifica Turno": "Nuovo Turno", icon: "/images/address.gif", tooltip: "Info Turni"}),
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
                        }),
						fields: oCmbPlant
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_ShiftCode"), //"Cod. Turno",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
                                value: bEdit?fnGetCellValue("ShiftTab", "SHIFT"):"",
                                enabled : !bEdit,
                                maxLength: 4,
                                id: "SHIFT",
																change: function() {
																	UI5Utils.validationTextField(this, /^[A-Za-z0-9]+$/, "saveShiftButton");
																}
                            })
                        ]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
                        }),
						fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
                                value: bEdit?fnGetCellValue("ShiftTab", "SHNAME"):"",
                                maxLength: 50,
                                id: "SHNAME"
							})
                        ]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_FromTime"), //"Da ora:",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
                        }),
						fields: [
							new sap.m.DateTimeInput({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								dateValue: oDateFrom,
								id: "TIMEFROM",
								type:sap.m.DateTimeInputType.Time,
								displayFormat: "HH:mm:ss"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_ToTime"), //"A ora:",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.m.DateTimeInput({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								dateValue: oDateTo,
								id: "TIMETO",
								type:sap.m.DateTimeInputType.Time,
								displayFormat: "HH:mm:ss"
							})
						]
					})
				]
			})
		]});
	oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(
		new sap.ui.commons.Button({
			text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
			id: "saveShiftButton",
			press:function(){fnSaveShift(bEdit);}
		})
	);
	oEdtDlg.addButton(
		new sap.ui.commons.Button({
			text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
			press:function(){
				oEdtDlg.close();
				oEdtDlg.destroy()
			}
		})
	);
	oEdtDlg.open();
}



// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveShift(bEdit) {

	var sPlant = $.UIbyID("cmbShiftPlants").getSelectedKey();
	var oDateFrom = sap.ui.getCore().getElementById("TIMEFROM").getDateValue();
	var oDateTo = sap.ui.getCore().getElementById("TIMETO").getDateValue();

    var valId = $.UIbyID("SHIFT").getValue();
    if ( valId === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

	var qexe = dataProdMD;
    if(bEdit){
        qexe += "Shifts/updShiftSQ";
    }
    else{
        qexe += "Shifts/addShiftSQ";
    }

	qexe += "&Param.1=" + sPlant;
	qexe += "&Param.2=" + valId;
	qexe += "&Param.3=" + $.UIbyID("SHNAME").getValue();
	qexe += "&Param.4=" + oDateFrom.getHours() + ":" + oDateFrom.getMinutes();
	qexe += "&Param.5=" + oDateTo.getHours() + ":" + oDateTo.getMinutes();
	qexe += "&Param.6=true";

	var ret = fnExeQuery(qexe,
		oLng_MasterData.getText("MasterData_ShiftSaved"), //"Turno salvato correttamente",
		oLng_MasterData.getText("MasterData_ShiftSavedError"), //"Errore in salvataggio Turno",
		false
	);

	if (ret) {
		$.UIbyID("dlgShift").close();
		$.UIbyID("dlgShift").destroy();
		refreshShiftsTab();
	}
}

// Funzione per cancellare una pressa
function fnDelShift() {
	var sPlant = fnGetCellValue("ShiftTab", "PLANT");
	var sShift = fnGetCellValue("ShiftTab", "SHIFT");
	var sDesc = fnGetCellValue("ShiftTab", "SHNAME");

	sap.ui.commons.MessageBox.show(
		oLng_MasterData.getText("MasterData_DeletingShift") //"Eliminare il turno"
        + " '" + sDesc + "'?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
		[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function(sResult) {
			if (sResult == 'YES') {
				var qexe = dataProdMD + "Shifts/delShiftSQ";
				qexe += "&Param.1=" + sPlant;
				qexe += "&Param.2=" + sShift;
				var ret = fnSQLQuery(qexe,
					oLng_MasterData.getText("MasterData_ShiftDeleted"), //"Turno eliminato correttamente",
					oLng_MasterData.getText("MasterData_ShiftDeletedError"), //"Errore durante l'eliminazione del Turno",
					false
				);
				refreshShiftsTab();
				$.UIbyID("btnDelShift").setEnabled(false);
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
}
