/***********************************************************************************************/
// Funzioni ed oggetti per Divisioni
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Macchine e ritorna l'oggetto oTable
function createTabPlant() {

	//Crea L'oggetto Tabella Plants
    var oTable = UI5Utils.init_UI5_Table({
			id: "PlantsTab",
			properties: {
				title: oLng_MasterData.getText("MasterData_PlantsList"), //"Lista Stabilimenti",
				visibleRowCount: 15,
				width: "98%",
				selectionMode: sap.ui.table.SelectionMode.Single,
				navigationMode: sap.ui.table.NavigationMode.Scrollbar,
				firstVisibleRow: 0,
				visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
				rowSelectionChange: function(oControlEvent) {
					if ($.UIbyID("PlantsTab").getSelectedIndex() == -1) {
						$.UIbyID("btnModPlant").setEnabled(false);
						$.UIbyID("btnDelPlant").setEnabled(false);
					}
					else {
						$.UIbyID("btnModPlant").setEnabled(true);
						$.UIbyID("btnDelPlant").setEnabled(true);
					}
			    }
			},
			toolbarItems: [
				new sap.ui.commons.Button(
				{
					text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
					icon: 'sap-icon://add',
					press: function () {
					  openPlantEdit(false);
					}
				}),
				new sap.ui.commons.Button(
				{
					text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
					id:   "btnModPlant",
					icon: 'sap-icon://edit',
					enabled: false,
					press: function () {
					  openPlantEdit(true);
					}
				}),
				new sap.ui.commons.Button(
				{
					text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina",
					id:   "btnDelPlant",
					icon: 'sap-icon://delete',
					enabled: false,
					press: function () {
					  fnDelPlant();
					}
				}),
				new sap.ui.commons.Button({
					text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
					icon: 'sap-icon://refresh',
					enabled: true,
					press: function () {
					  refreshTabPlants();
					}
				})
			],
	    columns: [{
				Field: "PLANT",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
	      properties: {
					width: "40px"
				},
	      template: {
	      	type: "TextView"
	      }
	    }, {
				Field: "NAME1",
				label: oLng_MasterData.getText("MasterData_PlantName"), //"Nome stabilimento",
	      properties: {
					width: "95px"
				},
	      template: {
	      	type: "TextView"
	      }
	    }, {
				Field: "NAME_LONG",
				label: oLng_MasterData.getText("MasterData_ExtendedName"), //"Nome esteso",
	      properties: {
					width: "95px"
				},
	      template: {
	      	type: "TextView"
	      }
	    }, {
				Field: "PACFN",
				label: oLng_MasterData.getText("MasterData_PACFunc"), //"Funzionalità PAC",
	      properties: {
					width: "60px",
					tooltip: oLng_MasterData.getText("MasterData_PACFuncTooltip")
				},
	      template: {
	      	type: "checked"
	      }
	    }, {
				Field: "UDCFN",
				label: oLng_MasterData.getText("MasterData_UDCFunc"), //"Funzionalità UDC",
	      properties: {
					width: "60px",
					tooltip: oLng_MasterData.getText("MasterData_UDCFuncTooltip")
				},
	      template: {
	      	type: "checked"
	      }
	    }, {
				Field: "CHECKSERIAL",
				label: oLng_MasterData.getText("MasterData_CheckSerial"), //"Check Seriale",
	      properties: {
					width: "50px",
					tooltip: oLng_MasterData.getText("MasterData_CheckSerialTooltip")
				},
	      template: {
	      	type: "checked"
	      }
	    }, {
				Field: "UDCINFN",
				label: oLng_MasterData.getText("MasterData_UDCINFunc"), //"Funzionalità UDC ingresso",
	      properties: {
					width: "60px",
					tooltip: oLng_MasterData.getText("MasterData_UDCINFuncTooltip")
				},
	      template: {
	      	type: "checked"
	      }
	    }, {
				Field: "TOSAPFN",
				label: oLng_MasterData.getText("MasterData_TOSAPFunc"), //"Funzionalità TOSAP",
	      properties: {
					width: "50px",
					tooltip: oLng_MasterData.getText("MasterData_TOSAPFuncTooltip")
				},
	      template: {
	        type: "checked"
	      }
			}, {
				Field: "TIMEOUT",
				label: oLng_MasterData.getText("MasterData_Timeout"), //"Timeout",
	      properties: {
					width: "70px",
					tooltip: oLng_MasterData.getText("MasterData_TimeoutTooltip")
				},
	      template: {
	        type: "Numeric",
	        textAlign: "Right"
	      }
	    }, {
				Field: "GLINK",
				label: oLng_MasterData.getText("MasterData_GLINK"), //"G-LINK",
	      properties: {
					width: "55px",
					tooltip: oLng_MasterData.getText("MasterData_GLINKTooltip")
				},
	      template: {
	      	type: "checked"
	      }
	    }, {
				Field: "TIMEDIFF",
				label: oLng_MasterData.getText("MasterData_TimeDiff"),
	      properties: {
					width: "70px",
					tooltip: oLng_MasterData.getText("MasterData_TimeDiffTooltip")
				},
				template: {
	        type: "Numeric",
	        textAlign: "Right"
	      }
	    }, {
				Field: "LANGU",
				label: oLng_MasterData.getText("MasterData_Langu"), //"Divisione",
	      properties: {
					width: "40px",
					tooltip: oLng_MasterData.getText("MasterData_LanguTooltip")
				},
	      template: {
	      	type: "TextView"
	      }
	    }]
  });

	oModel = new sap.ui.model.xml.XMLModel();

  oTable.setModel(oModel);
  oTable.bindRows("/Rowset/Row");
  refreshTabPlants();
	return oTable;
}

// Aggiorna il contenuto della combobox Plants
function updateCmbPlant() {
	var oPlantsModel = new sap.ui.model.xml.XMLModel()
	oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	var oCmbPlant = $.UIbyID("filtPlant");
	oCmbPlant.setModel(oPlantsModel);
}

// Aggiorna la tabella Plants
function refreshTabPlants() {
  $("#splash-screen").show();
  $.UIbyID("PlantsTab").getModel().loadData(QService + dataProdMD + "Plants/getPlantsSQ");
  $("#splash-screen").hide();
}

// Funzione per creare la finestra di dialogo Plants
function openPlantEdit(bEdit) {

	if (bEdit === undefined) bEdit = false;

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		title: bEdit?oLng_MasterData.getText("MasterData_ModifyPlant"):oLng_MasterData.getText("MasterData_NewPlant"),
                    /*"Modifica Divisione":"Nuova Divisione",*/
		modal:  true,
		resizable: true,
		id: "dlgPress",
		maxWidth:  "600px",
		Width:   "600px",
		showCloseButton: false
	});

		// contenitore dati lista lingue
		var oModel = new sap.ui.model.xml.XMLModel()
		oModel.loadData(
				QService + dataProdTR +
				"getLanguagesListQR"
		);

		var oCmbLang = new sap.ui.commons.ComboBox("cmbLang", {
				selectedKey: bEdit ? fnGetCellValue("PlantsTab", "LANGU"):"",
				layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"})
		});
		oCmbLang.setModel(oModel);
		var oItemLang = new sap.ui.core.ListItem();
		oItemLang.bindProperty("text", "value");
		oItemLang.bindProperty("key", "name");
		oCmbLang.bindItems("/Rowset/Row", oItemLang);

    var oLayout1 = new sap.ui.layout.form.GridLayout({singleColumn: true});

    var oForm1 = new sap.ui.layout.form.Form({
    	width: "98%",
      layout: oLayout1,
      formContainers: [
      	new sap.ui.layout.form.FormContainer({
        	formElements: [
          	new sap.ui.layout.form.FormElement({
            	label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                textAlign : "Begin",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								})
							}),
              fields: [
								new sap.ui.commons.TextField({
									enabled: !bEdit,
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "auto"
									}),
									value: bEdit?fnGetCellValue("PlantsTab", "PLANT"):"",
									maxLength: 4,
                  id: "PLANT"
								})
              ]
            }),
            new sap.ui.layout.form.FormElement({
            	label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_PlantName"), //"Nome stabilimento",
								textAlign : "Begin",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								})
							}),
              fields: [
								new sap.ui.commons.TextField({
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "auto"
									}),
									value: bEdit?fnGetCellValue("PlantsTab", "NAME1"):"",
									maxLength: 200,
                  id: "NAME1"
								})
              ]
            }),
						new sap.ui.layout.form.FormElement({
            	label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_ExtendedName"), //"Nome esteso",
								textAlign : "Begin",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								})
							}),
              fields: [
								new sap.ui.commons.TextField({
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "auto"
									}),
									value: bEdit?fnGetCellValue("PlantsTab", "NAME_LONG"):"",
									maxLength: 200,
                  id: "NAME_LONG"
								})
              ]
            }),
						new sap.ui.layout.form.FormElement({
              fields: [
								new sap.ui.commons.Label({
									text: oLng_MasterData.getText("MasterData_PACFunc"), //"Funzionalità PAC",
									textAlign : "Begin",
									layoutData:	new sap.ui.layout.form.GridElementData({
										hCells: "full"
	                })
	              }),
              	new sap.ui.commons.CheckBox({
                	tooltip : oLng_MasterData.getText("MasterData_PACFuncTooltip"), //'Funzionalità PAC',
                  id : 'chkPac',
                  checked : (fnGetCellValue("PlantsTab", "PACFN")=='1')?true : false
                }),
								new sap.ui.commons.Label({
									text: oLng_MasterData.getText("MasterData_UDCFunc"), //"Funzionalità UDC",
									textAlign : "Begin",
									layoutData:	new sap.ui.layout.form.GridElementData({
										hCells: "full"
	                })
	              }),
								new sap.ui.commons.CheckBox({
                	tooltip : oLng_MasterData.getText("MasterData_UDCFuncTooltip"), //'Funzionalità UDC',
                  id : 'chkUdc',
                  checked : (fnGetCellValue("PlantsTab", "UDCFN")=='1')?true : false
                }),
								new sap.ui.commons.Label({
									text: oLng_MasterData.getText("MasterData_CheckSerial"), //"Check Seriale",
									textAlign : "Begin",
									layoutData:	new sap.ui.layout.form.GridElementData({
										hCells: "full"
	                })
	              }),
								new sap.ui.commons.CheckBox({
                  tooltip : oLng_MasterData.getText("MasterData_CheckSerialTooltip"), //"Check Seriale",
                  id : 'chkCheckSerial',
                  checked : (fnGetCellValue("PlantsTab", "CHECKSERIAL")=='1')?true : false
                }),
								new sap.ui.commons.Label({
									text: oLng_MasterData.getText("MasterData_UDCINFunc"), //"Funzionalità UDC ingresso",
									textAlign : "Begin",
									layoutData:	new sap.ui.layout.form.GridElementData({
										hCells: "full"
	                })
	              }),
								new sap.ui.commons.CheckBox({
                  tooltip : oLng_MasterData.getText("MasterData_UDCINFuncTooltip"), //"Funzionalità UDC ingresso",
                  id : 'chkUdcIN',
                  checked : (fnGetCellValue("PlantsTab", "UDCINFN")=='1')?true : false
                }),
								new sap.ui.commons.Label({
									text: oLng_MasterData.getText("MasterData_TOSAPFunc"), //"Funzionalità TOSAP",
									textAlign : "Begin",
									layoutData:	new sap.ui.layout.form.GridElementData({
										hCells: "full"
	                })
	              }),
								new sap.ui.commons.CheckBox({
                	tooltip : oLng_MasterData.getText("MasterData_TOSAPFuncTooltip"), //"Funzionalità TOSAP",
                  id : 'chkToSap',
                  checked : (fnGetCellValue("PlantsTab", "TOSAPFN")=='1')?true : false
                })
              ]
						}),
            new sap.ui.layout.form.FormElement({
              label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_Timeout"), //"Timeout",
								textAlign : "Begin",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								})
							}),
              fields: [
								new sap.ui.commons.TextField({
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "auto"
									}),
									value: bEdit?fnGetCellValue("PlantsTab", "TIMEOUT"):"0",
									maxLength: 6,
                  id: "TIMEOUT",
                  liveChange: function(ev) {
                  	var val = ev.getParameter('liveValue');
                    var newval = val.replace(/[^\d]/g, '');
                    this.setValue(newval);
                  }
								})
              ]
            }),
            new sap.ui.layout.form.FormElement({
							label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_GLINK"), //"G-LINK",
								textAlign : "Begin",
								layoutData:	new sap.ui.layout.form.GridElementData({
									hCells: "3"
                })
              }),
              fields: [
              	new sap.ui.commons.CheckBox({
                  tooltip : oLng_MasterData.getText("MasterData_GLINKTooltip"), //"G-LINK",
                  id : 'chkGLINK',
                  checked : (fnGetCellValue("PlantsTab", "GLINK")=='1')?true : false
                })
              ]
						}),
						new sap.ui.layout.form.FormElement({
							label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_Langu"), //"G-LINK",
								textAlign : "Begin",
								layoutData:	new sap.ui.layout.form.GridElementData({
									hCells: "3"
                })
              }),
              fields: oCmbLang
						}),
						new sap.ui.layout.form.FormElement({
              label: new sap.ui.commons.Label({
								text: oLng_MasterData.getText("MasterData_TimeDiff"), //"Timeout",
								textAlign : "Begin",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								})
							}),
              fields: [
								new sap.ui.commons.TextField({
									layoutData: new sap.ui.layout.form.GridElementData({
										hCells: "auto"
									}),
									value: bEdit?fnGetCellValue("PlantsTab", "TIMEDIFF"):"0",
									maxLength: 6,
                  id: "TIMEDIFF",
									tooltip: oLng_MasterData.getText("MasterData_TimeDiffTooltip"),
                  liveChange: function(ev) {
                  	var val = ev.getParameter('liveValue');
                    var newval = val.replace(/[^\d]/g, '');
                    this.setValue(newval);
                  }
								})
              ]
            })
          ]
				})
			]
		});

		oEdtDlg.addContent(oForm1);
		oEdtDlg.addButton(new sap.ui.commons.Button({
			text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
			press:function(){
				fnSavePlant(bEdit);
			}
		}));
		oEdtDlg.addButton(new sap.ui.commons.Button({
			text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
			press:function(){oEdtDlg.close();
				oEdtDlg.destroy();
			}
		}));
		oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSavePlant(bEdit) {
    var sPlant = $.UIbyID("PLANT").getValue();
    var sName = $.UIbyID("NAME1").getValue();
    var sExtendedName = $.UIbyID("NAME_LONG").getValue();
    var bPacFn = $.UIbyID("chkPac").getChecked();
	var bChkSerial = $.UIbyID("chkCheckSerial").getChecked();
    var bUdcFn = $.UIbyID("chkUdc").getChecked();
    var bUdcInFn = $.UIbyID("chkUdcIN").getChecked();
    var bToSap = $.UIbyID("chkToSap").getChecked();
    var iTimeOut = $.UIbyID("TIMEOUT").getValue();
    var bGLink = $.UIbyID("chkGLINK").getChecked();
		var lang = $.UIbyID("cmbLang").getSelectedKey();
		var timeDiff = $.UIbyID("TIMEDIFF").getValue();

    if (sPlant === "" || sName === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    if (iTimeOut === "") {
        iTimeOut = "180";
    }

    var qexe = dataProdMD;
    if(bEdit){
        qexe += "Plants/updPlantsSQ";
    }
    else{
        qexe += "Plants/addPlantSQ";
    }

	qexe += "&Param.1=" + sPlant;
	qexe += "&Param.2=" + sName;
    qexe += "&Param.3=" + sExtendedName;
    qexe += "&Param.4=" + bPacFn;
    qexe += "&Param.5=" + bChkSerial;
    qexe += "&Param.6=" + bUdcFn;
    qexe += "&Param.7=" + bUdcInFn;
    qexe += "&Param.8=" + bToSap;
    qexe += "&Param.9=" + iTimeOut;
    qexe += "&Param.10=" + bGLink;
		qexe += "&Param.11=" + lang;
		qexe += "&Param.12=" + timeDiff;

    var ret = fnExeQuery(qexe,
		oLng_MasterData.getText("MasterData_PlantSaved"), //"Divisione salvata correttamente",
		oLng_MasterData.getText("MasterData_PlantSavedError"), //"Errore nel salvataggio della Divisione",
		false
	);

	if (ret) {
		$.UIbyID("dlgPress").close();
		$.UIbyID("dlgPress").destroy();
		refreshTabPlants();
		updateCmbPlant();
	}
}

// Funzione per cancellare una pressa
function fnDelPlant () {
	var sPlant = fnGetCellValue("PlantsTab", "PLANT");
    var sName = fnGetCellValue("PlantsTab", "NAME1");

	sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_DeletingPlant")/*"Eliminare la divisione"*/ + " '" + sName + "'?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
		[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function(sResult) {
			if (sResult == 'YES') {
				var qexe = dataProdMD + "Plants/delPlantSQ&Param.1=" + sPlant ;
				var ret = fnSQLQuery(qexe,
					oLng_MasterData.getText("MasterData_PlantDeleted"), //"Divisione eliminata correttamente",
					oLng_MasterData.getText("MasterData_PlantDeletedError"), //"Errore durante l'eliminazione della divisione",
					false
				);
				refreshTabPlants();
				$.UIbyID("btnDelPlant").setEnabled(false);
                $.UIbyID("PlantsTab").setSelectedIndex(-1);
				updateCmbPlant();
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
}
