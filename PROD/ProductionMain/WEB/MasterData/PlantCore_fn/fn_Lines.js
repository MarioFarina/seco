
/***********************************************************************************************/
// Funzioni ed oggetti per Linee
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

var tmFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
    pattern: "HH:mm:ss"
});

// Functione che crea la tabella Linee e ritorna l'oggetto oTable
function createTabLine() {
 
   //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table(
    {
        id: "LineTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_LinesList"), //"Lista Linee",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("LineTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModLine").setEnabled(false);
                    $.UIbyID("btnDelLine").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModLine").setEnabled(true);
                    $.UIbyID("btnDelLine").setEnabled(true);
                }
            }
        },
        toolbarItems: [
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                icon: 'sap-icon://add',
                press: function(){
                    openLineEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModLine",
                icon: 'sap-icon://edit',
                enabled: false,
                press: function (){
                    openLineEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina",
                id: "btnDelLine",
                icon: 'sap-icon://delete',
                enabled: false,
                press: function (){
                    fnDelLine();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function ()
                {
                    refreshTabLines();
                }
            })
        ],
        columns: [
            {
				Field: "IDLINE",
				label: oLng_MasterData.getText("MasterData_LineID"), //"ID Linea",
				properties: {
					width: "100px"
				}
			},
            {
				Field: "LINETXT",
				label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
				properties: {
					width: "125px"
				}
			},
            {
				Field: "IDREP",
				properties: {
					width: "50px",
                   			 visible: false
				}
			},
            {
				Field: "REPTXT",
				label: oLng_MasterData.getText("MasterData_Depart"), //"Reparto",
				properties: {
					width: "100px"
				}
			},
            {
				Field: "CDLID",
				label: oLng_MasterData.getText("MasterData_Cdl"), //"Centro di lavoro",
				properties: {
					width: "120px"
				}
			},
            {
				Field: "SHOWDB",
				label: oLng_MasterData.getText("MasterData_DashBoard"), //"in DashBoard",
				properties: {
					width: "75px",
 					visible: true
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
            {
				Field: "SHOWPV",
				label: oLng_MasterData.getText("MasterData_Pivot"), //"in Pivot",
				properties: {
					width: "60px",
					visible:false
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
       
			{
				Field: "PACFN",
				label: oLng_MasterData.getText("MasterData_Notes"),
				properties: {
					width: "200px",
					visible: false
				}
			},
			{
				Field: "UDCFN",
				label: oLng_MasterData.getText("MasterData_Notes"),
				properties: {
					width: "200px",
					visible: false
				}
			},
			{
				Field: "CONNECTED",
				label: oLng_MasterData.getText("MasterData_Notes"),
				properties: {
					width: "200px",
					visible: false
				}
			},
			{
				Field: "GLINK",
				label: oLng_MasterData.getText("MasterData_Notes"),
				properties: {
					width: "200px",
					visible: false
				}
			},
			{
				Field: "OPENUM",
				label: oLng_MasterData.getText("MasterData_Notes"),
				properties: {
					width: "200px",
					visible: false
				}
            },
            {
				Field: "TOLLINP",
				label: oLng_MasterData.getText("MasterData_Notes"),
				properties: {
					width: "200px",
					visible: false
				}
            },
              {
				Field: "OEE_P",
				label: oLng_MasterData.getText("MasterData_OEEP"), 
				properties: {
					width: "60px"
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
              {
				Field: "OEE_Q",
				label: oLng_MasterData.getText("MasterData_OEEQ"), 
				properties: {
					width: "60px"
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
               {
				Field: "OEE_D",
				label: oLng_MasterData.getText("MasterData_OEED"), 
				properties: {
					width: "60px"
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
                {
				Field: "REPORT_FIRST",
				label: oLng_MasterData.getText("MasterData_REPORT_FIRST"), 
				properties: {
					width: "60px"
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
                  {
				Field: "REPORT_QUALITY",
				label: oLng_MasterData.getText("MasterData_REPORT_QUALITY"), 
				properties: {
					width: "60px"
				},
				template: {
					type: "checked",
					textAlign: "Right"
                }
            },
                 {
				Field: "EXID",
				label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
				properties: {
					width: "100px"
				}
			},
            {
				Field: "NOTES",
				label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
				properties: {
					width: "200px"
				}
			},
            {
				Field: "TIMEFROM",
				label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
				properties: {
					width: "200px",
                    visible: true
				},
                template: {
                    type: "Time"
                }
			},
            {
				Field: "TIMETO",
				label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
				properties: {
					width: "200px",
                    visible: true
				},
                template: {
                    type: "Time"
                }
			}
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabLines();
	return oTable;
}

// Aggiorna la tabella Linee
function refreshTabLines() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("LineTab").getModel().loadData(
            QService + dataProdMD +
            "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("LineTab").getModel().loadData(
            QService + dataProdMD +
            "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("LineTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Linee
function openLineEdit(bEdit) {

  if (bEdit == undefined) bEdit = false;

//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgAddLine",
        maxWidth: "900px",
        maxHeight: "400px",
        //minHeight: "900px",
        title: oLng_MasterData.getText("MasterData_LineRegistry"), //"Anagrafica linea",
        showCloseButton: false
    });

	//oEdtDlg.setTitle("Aggiungi Linea");

   var oTabMasterLineEdit = new sap.ui.commons.TabStrip("TabMasterLineEdit");
    oTabMasterLineEdit.attachClose(function (oEvent) {
        var oTabStrip = oEvent.oSource;
        oTabStrip.closeTab(oEvent.getParameter("index"));
    });

    var oLayoutDetails = new sap.ui.commons.layout.MatrixLayout("tabEditLine", {
        columns: 1,
        width: "100%"
    });

	// contenitore dati lista divisioni
	var oModel = new sap.ui.model.xml.XMLModel()
	oModel.loadData(
        QService + dataProdMD +
        "Departments/getDepartsByPlantSQ&Param.1=" +
        (($.UIbyID("filtPlant").getSelectedKey() === "")?$('#plant').val():$.UIbyID("filtPlant").getSelectedKey())
    );

    var oCmbReps = new sap.ui.commons.ComboBox("cmbReps", {
        selectedKey: bEdit?fnGetCellValue("LineTab", "IDREP"):"",
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"})
    });
	oCmbReps.setModel(oModel);
    var oItemRep = new sap.ui.core.ListItem();
	oItemRep.bindProperty("text", "REPTXT");
	oItemRep.bindProperty("key", "IDREP");
	oCmbReps.bindItems("/Rowset/Row", oItemRep);
    
    var sCurrentTime, sEndTime;
    if(bEdit){
        sCurrentTime = timeFormatter(fnGetCellValue("LineTab", "TIMEFROM"));
        sEndTime = timeFormatter(fnGetCellValue("LineTab", "TIMETO"));
        console.log(sCurrentTime);
    }
    else{
        sCurrentTime = new Date().getHours() + ":" + new Date().getMinutes() + new Date().getSeconds();
        sEndTime = new Date().getHours() + ":" + new Date().getMinutes() + new Date().getSeconds();
    }

    var oTmStartTime = new sap.m.TimePicker("tmStartTime", {
        localeId: "it",
        displayFormat: "HH:mm:ss",
        valueFormat: "HH:mm:ss",
        icon: "",
        //width: "110px"
    }).setValue(sCurrentTime);

    var oTmEndTime = new sap.m.TimePicker("tmEndTime", {
        localeId: "it",
        displayFormat: "HH:mm:ss",
        valueFormat: "HH:mm:ss",
        icon: "",
        //width: "110px"
    }).setValue(sEndTime);

    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        //title: new sap.ui.core.Title({text: "Anagrafica linea", icon: 'sap-icon://edit', tooltip: "Info Linea"}),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_LineID"), //"ID Linea:",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "5"}),
                                value: bEdit?fnGetCellValue("LineTab", "IDLINE"):"",
                                editable: !bEdit,
                                maxLength: 25,
                                id: "txtLineId",
																change: function() {
																	UI5Utils.validationTextField(this, /^[A-Za-z0-9]+$/, "saveLinesButton");
																}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "5"}),
                                value: bEdit?fnGetCellValue("LineTab", "LINETXT"):"",
                                maxLength: 50,
                                id: "txtLineTxt"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Depart"), //"Reparto",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: oCmbReps
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Cdl"), //"Centro di lavoro",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("LineTab", "CDLID"):"",
                                maxLength: 10,
                                id: "txtCdLidTxt"
                            }),
														new sap.ui.commons.Button("btnWorksFilter", {
															icon: "sap-icon://arrow-down",
															tooltip: oLng_MasterData.getText("MasterData_Cdl"),
															enabled: true,
															layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
															press: function (){
																fnGetWorksTableDialog(
																	($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
																	bEdit ? fnGetCellValue("LineTab", "CDLID") : "",
																	setSelectedWorks
																);
															}
														})

                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Show") + ": ", //"Visualizza",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_DashBoard"), //"in Dashboard",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "SHOWDB") == "0" ? false : true ) : false,
                                id: "chkDashBoard"
                            }),
		     new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_REPORT_FIRST"),
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "REPORT_FIRST") == "0" ? false : true ) : false,
                                id: "chkReportFirst"
                            }),
		        new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_REPORT_QUALITY"),
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "REPORT_QUALITY") == "0" ? false : true ) : false,
                                id: "chkReportQuality"
                            })
                        ]
                    }),

		            new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: "",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_OEED"), 
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "OEE_D") == "0" ? false : true ) : false,
                                id: "chkOEED"
                            }),
		                    new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_OEEQ"), 
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "OEE_Q") == "0" ? false : true ) : false,
                                id: "chkOEEQ"
                            }),
	                   new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_OEEP"), 
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "OEE_P") == "0" ? false : true ) : false,
                                id: "chkOEEP"
                            })
                        ]
                    }),
                    
                     new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: "",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                         fields: [
                            new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_UDCFunc"), //"in Pivot",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "UDCFN") == "0" ? false : true ) : false,
                                id: "chkUdc"
                            }),
														new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_Connected"), //"in Pivot",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "CONNECTED") == "0" ? false : true ) : false,
                                id: "chkConn"
                            }),
														new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_GLINK"),
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "GLINK") == "0" ? false : true ) : false,
                                id: "chkGLink"
                            })
                        ]
                    }),
                    
                    
                      new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: "",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                            fields: [
                          new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_Pivot"), //"in Pivot",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "SHOWPV") == "0" ? false : true ) : false,
                                id: "chkPivot"
                            }),
														new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_PACFunc"), //"in Dashboard",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: (bEdit == 1)?(fnGetCellValue("LineTab", "PACFN") == "0" ? false : true ) : false,
                                id: "chkPacnf"
                            })
                                 ]
                    }),
                    
                    
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("LineTab", "EXID"):"",
                                maxLength: 15,
                                id: "txtEXID"
                            }),
														new sap.ui.commons.Label({
                                text: oLng_MasterData.getText("MasterData_Openum"),
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                            }),
														new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("LineTab", "OPENUM"):"0",
                                maxLength: 15,
                                id: "txtOPENUM",
                                                            liveChange: function(ev) {
              										var val = ev.getParameter('liveValue');;
             											val = val.replace(/[^\d]/g, '');
                                                    
              										this.setValue(val === "" ? "0" : val);
          											}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Tollinp"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit ? fnGetCellValue("LineTab", "TOLLINP"):"0",
                                id: "txtTOLLINP",
                                liveChange: function(ev) {
                      var val = ev.getParameter('liveValue');;
                         val = val.replace(/[^\d]/g, '');
                      this.setValue(val === "" ? "0" : val);
                      }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_StartTime"), //"Ora Inizio",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            oTmStartTime,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://date-time",
                                press: function(){
                                    sCurrentTime = new Date().getHours() + ":" + new Date().getMinutes() + new Date().getSeconds();
                                    $.UIbyID("tmStartTime").setValue(sCurrentTime);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_EndTime"), //"Ora Fine",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            oTmEndTime,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://date-time",
                                press: function(){
                                    sEndTime = new Date().getHours() + ":" + new Date().getMinutes() + new Date().getSeconds();
                                    $.UIbyID("tmEndTime").setValue(sEndTime);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "5"}),
                                value: bEdit?fnGetCellValue("LineTab", "NOTES"):"",
                                id: "txtNote"
                            })
                        ]
                    })
                ]
            })
        ]
    });

    var oLayout0;
    if(bEdit){
		const sIdLine = fnGetCellValue("LineTab", "IDLINE");
        var oMachTable = createTabMachine(sIdLine);

        oLayout0 = new sap.ui.commons.layout.MatrixLayout("tabdetail", {columns: 1});

        oLayout0.createRow(oMachTable);
    }

    oLayoutDetails.createRow(oForm1);
    oTabMasterLineEdit.createTab(oLng_MasterData.getText("MasterData_Parameters")/*"Parametri"*/, oLayoutDetails);
    if(bEdit){
        oTabMasterLineEdit.createTab(oLng_MasterData.getText("MasterData_MachinesInLine")/*"Macchine Linea"*/, oLayout0);
    }
    oEdtDlg.addContent(oTabMasterLineEdit);

	//oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
				id: "saveLinesButton",
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            if (bEdit)
                fnSaveLine();
            else
                fnAddLine()
            }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy()
        }
    }));
	oEdtDlg.open();
}

// Funzione per salvare l'inserimento di un linea
function fnAddLine (oEdtDlg) {

    var valId = $.UIbyID("txtLineId").getValue();
    var valTxt = $.UIbyID("txtLineTxt").getValue();
    var valRep = encodeURIComponent($.UIbyID("cmbReps").getSelectedKey());
    var sCDLid = $.UIbyID("txtCdLidTxt").getValue();
    var bDashBoard = $.UIbyID("chkDashBoard").getChecked() === true ? "true" : "false";

    var bPivot = $.UIbyID("chkPivot").getChecked() === true ? "true" : "false";
    var bUDC = $.UIbyID("chkUdc").getChecked() === true ? "true" : "false";
    var bPCNF = $.UIbyID("chkPacnf").getChecked() === true ? "true" : "false";
    var bConn = $.UIbyID("chkConn").getChecked() === true ? "true" : "false";
    var bGLink = $.UIbyID("chkGLink").getChecked() === true ? "true" : "false";

    var bOEEP = $.UIbyID("chkOEEP").getChecked() === true ? "true" : "false";
    var bOEEQ = $.UIbyID("chkOEEQ").getChecked() === true ? "true" : "false";
    var bOEED = $.UIbyID("chkOEED").getChecked() === true ? "true" : "false";
    var bReportFirst = $.UIbyID("chkReportFirst").getChecked() === true ? "true" : "false";
    var bReportQuality = $.UIbyID("chkReportQuality").getChecked() === true ? "true" : "false";
    
    var sTimeFrom = /*tmFormat.format($.UIbyID("tmStartTime").getDateValue())*/
        String($.UIbyID("tmStartTime").getDateValue()).substr(16, 8);
    var sTimeTo = /*tmFormat.format($.UIbyID("tmEndTime").getDateValue())*/
        String($.UIbyID("tmEndTime").getDateValue()).substr(16, 8);
    
    var sOpeNum = $.UIbyID("txtOPENUM").getValue();
    var sTollinp = $.UIbyID("txtTOLLINP").getValue();

    if (valId === "" || valTxt === "" || valRep === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    var qexe = dataProdMD+"Lines/addLinesSQ&Param.1=" + valId + "&Param.2=" + valTxt ;
    qexe += "&Param.3=" + $.UIbyID("txtEXID").getValue();
    qexe += "&Param.4=" + valRep;
    qexe += "&Param.5=" + $.UIbyID("txtNote").getValue();
    qexe += "&Param.6=" + sCDLid;
    qexe += "&Param.7=" + bDashBoard;
    qexe += "&Param.8=" + bPivot;
    qexe += "&Param.9=" + bUDC;
     qexe += "&Param.10=" + bPCNF;
     qexe += "&Param.11=" + bConn;
     qexe += "&Param.12=" + bGLink;
     qexe += "&Param.13=" + sOpeNum;
     qexe += "&Param.14=" + sTollinp;
     qexe += "&Param.15=" + bOEEP;
    qexe += "&Param.16=" + bOEEQ;
    qexe += "&Param.17=" + bOEED;
    qexe += "&Param.18=" + bReportFirst;
    qexe += "&Param.19=" + bReportQuality;
    qexe += "&Param.20=" + sTimeFrom;
    qexe += "&Param.21=" + sTimeTo;
    
    alert(qexe);

    var ret = fnSQLQuery(
        qexe,
        oLng_MasterData.getText("MasterData_LineAdded"), //"Linea aggiunta correttamente",
        oLng_MasterData.getText("MasterData_LineAddedError"), //"Errore in aggiunta linea",
        false
    );

    if (ret) {
        $.UIbyID("dlgAddLine").close();
        $.UIbyID("dlgAddLine").destroy();
        refreshTabLines();
    }
}

// Funzione per salvare le modifiche al linea
function fnSaveLine (oEdtDlg) {
    var valId = $.UIbyID("txtLineId").getValue();
    var valTxt = $.UIbyID("txtLineTxt").getValue();
    var valRep = encodeURIComponent($.UIbyID("cmbReps").getSelectedKey());
    var sCDLid = $.UIbyID("txtCdLidTxt").getValue();
    var bDashBoard = $.UIbyID("chkDashBoard").getChecked() === true ? "true" : "false";
    var bPivot = $.UIbyID("chkPivot").getChecked() === true ? "true" : "false";
		var bUDC = $.UIbyID("chkUdc").getChecked() === true ? "true" : "false";
		var bPCNF = $.UIbyID("chkPacnf").getChecked() === true ? "true" : "false";
		var bConn = $.UIbyID("chkConn").getChecked() === true ? "true" : "false";
		var bGLink = $.UIbyID("chkGLink").getChecked() === true ? "true" : "false";
	
var bOEEP = $.UIbyID("chkOEEP").getChecked() === true ? "true" : "false";
var bOEEQ = $.UIbyID("chkOEEQ").getChecked() === true ? "true" : "false";
var bOEED = $.UIbyID("chkOEED").getChecked() === true ? "true" : "false";

var bReportFirst = $.UIbyID("chkReportFirst").getChecked() === true ? "true" : "false";
var bReportQuality= $.UIbyID("chkReportQuality").getChecked() === true ? "true" : "false";

        var sOpeNum = $.UIbyID("txtOPENUM").getValue();
        var sTollinp = $.UIbyID("txtTOLLINP").getValue();
    
    var sTimeFrom = /*tmFormat.format($.UIbyID("tmStartTime").getDateValue())*/
        String($.UIbyID("tmStartTime").getDateValue()).substr(16, 8);
    var sTimeTo = /*tmFormat.format($.UIbyID("tmEndTime").getDateValue())*/
        String($.UIbyID("tmEndTime").getDateValue()).substr(16, 8);

    if (valId === "" || valTxt === "" || valRep === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    var qexe = dataProdMD+"Lines/saveLinesSQ&Param.1=" + valId;
    qexe += "&Param.2=" + valTxt;
    qexe += "&Param.3=" + $.UIbyID("txtEXID").getValue();
    qexe += "&Param.4=" + valRep;
    qexe += "&Param.5=" + $.UIbyID("txtNote").getValue();
    qexe += "&Param.6=" + sCDLid;
    qexe += "&Param.7=" + bDashBoard;
    qexe += "&Param.8=" + bPivot;
    qexe += "&Param.9=" + bUDC;
    qexe += "&Param.10=" + bPCNF;
    qexe += "&Param.11=" + bConn;
    qexe += "&Param.12=" + bGLink;
    qexe += "&Param.13=" + sOpeNum;
    qexe += "&Param.14=" + sTollinp;
    qexe += "&Param.15=" + bOEEP;
    qexe += "&Param.16=" + bOEEQ;
    qexe += "&Param.17=" + bOEED;
    qexe += "&Param.18=" + bReportFirst;
    qexe += "&Param.19=" + bReportQuality;
    qexe += "&Param.20=" + sTimeFrom;
    qexe += "&Param.21=" + sTimeTo;

    var ret = fnSQLQuery(
        qexe,
        oLng_MasterData.getText("MasterData_LineSaved"), //"Linea salvata correttamente",
        oLng_MasterData.getText("MasterData_LineSavedError"), //"Errore in salvataggio linea",
        false
    );
    if (ret) {
        $.UIbyID("dlgAddLine").close();
        $.UIbyID("dlgAddLine").destroy();
        refreshTabLines();
    }
}

// Funzione per cancellare un linea
function fnDelLine () {
  var valId = fnGetCellValue("LineTab", "IDLINE");
  var valTxt = fnGetCellValue("LineTab", "LINETXT");

	sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_LineDeletingWithDescription") //"Eliminare la linea selezionata con descrizione
            + "\n'" +valTxt+ "'?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD+"Lines/delLinesSQ&Param.1=" + valId ;
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_LineDeleted"), //"Linea eliminata correttamente",
                    oLng_MasterData.getText("MasterData_LineDeletedError"), //"Errore in eliminazione linea",
                    false
                );
                refreshTabLines();
                $.UIbyID("btnDelLine").setEnabled(false);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnGetWorksTableDialog(sPlant, sSelected, cbFunction) {
	if (!$.UIbyID("dlgWorksTableSelect")) {

		var oOpeModel = new sap.ui.model.xml.XMLModel();
		oOpeModel.loadData(QService + dataProdMD + "Lines/getWorksbyPlantSQ&Param.1=" + sPlant);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{CDLID}",
					title: "{CDLDESC}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgWorksTableSelect", {
			title: oLng_MasterData.getText("MasterData_Cdl"),
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "600px",
			contentHeight: "400px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
      items: [],
			columns:[
				new sap.m.Column({
					header: new sap.m.Label({
          	text: oLng_MasterData.getText("MasterData_Cdl")
          })
				})
			],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue != "") {
					var oFilter = [
						new sap.ui.model.Filter(
							[
								new sap.ui.model.Filter("CDLID", sap.ui.model.FilterOperator.Contains, sValue),
						 		new sap.ui.model.Filter("CDLDESC", sap.ui.model.FilterOperator.Contains, sValue)
							],
							false
					)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else oBinding.filter([]);
			},
			confirm: cbFunction,
      cancel: function (oControlEvent){
      	$.UIbyID("dlgWorksTableSelect").destroy();
      }
		});

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOpeModel);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgWorksTableSelect").setContentWidth("600px");

	if (sSelected == "" || sSelected == null)
        $.UIbyID("dlgWorksTableSelect").open();
	else {
		$.UIbyID("dlgWorksTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("CDLID", sap.ui.model.FilterOperator.EQ, sSelected),
				 sap.ui.model.FilterType.Application);
		$.UIbyID("dlgWorksTableSelect").open(sSelected);
	}

	$.UIbyID("dlgWorksTableSelect").focus();

}

function setSelectedWorks(oControlEvent){
	var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];

	var cdlid = oControlEvent.getSource().getModel().getProperty("CDLID", oSelContext);

	$.UIbyID("txtCdLidTxt").setValue(cdlid);
	$.UIbyID("dlgWorksTableSelect").destroy();
}

function timeFormatter(sTime){
    console.log(sTime);
    if(Date.parse(sTime))
        return sTime.substring(11, 19);
    else
        return '-';
}
