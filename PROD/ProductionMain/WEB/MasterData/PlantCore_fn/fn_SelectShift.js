/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300 , -W098*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Funzioni per il recupero dati dei  turni
//Author: Luca Adanti
//Date:   21/11/2018
//Vers:   1.0.2
//Modifiers: Luca Adanti - 26/11/2018
//**************************************************************************************

function fnGetShiftTableDialog(oParams, cbFunction) {

	//oParams.Selected
	//oParams.Plant

	if (!$.UIbyID("dlgShiftsTableSelect")) {

		var oShiftModel = new sap.ui.model.xml.XMLModel();
		oShiftModel.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + oParams.Plant);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{SHIFT}",
					//title: "{ORDER}"
				}),
				new sap.m.Text({
					text: "{SHNAME}",
					title: "{}"
				}),
				new sap.m.Text({
					text: "{TIMEFROM}",
					title: "{}"
				}).bindProperty("text",
												"TIMEFROM",
												function(Value){
					if (Value === null) {return;}
					else {return Value.substring(11, 16);}
				}),
				new sap.m.Text({
				text: "{TIMETO}",
				title: "{}"
				}).bindProperty("text",
												"TIMETO",
												function(Value){
					if (Value === null) {return;}
					else {return Value.substring(11, 16);}
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgShiftsTableSelect", {
			title: "Lista turni stabilimento",//oLng_MasterData.getText("MasterData_Orders"), //"Lista Ordini"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
			items: [],
			columns: [
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Turno", //oLng_MasterData.getText("MasterData_Order"), //"Nome Ordine"
						minScreenWidth: "40em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Descrizione", //oLng_MasterData.getText("MasterData_MaterialCode"), //"Gruppo"
						minScreenWidth: "0.5em"
					})
				}),

				new sap.m.Column({
					header: new sap.m.Label({
						text: "Dalle", //oLng_MasterData.getText("MasterData_MaterialDescr"), //"Descrizione"
						minScreenWidth: "40em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Alle", //oLng_MasterData.getText("MasterData_OrdFilType"), //"Note"
						minScreenWidth: "40em"
					})
				})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue !== "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("SHIFT", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("SHNAME", sap.ui.model.FilterOperator.Contains, sValue),
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else {
					oBinding.filter([]);
				}
			},
			confirm: cbFunction,
			cancel: function (oControlEvent) {
				$.UIbyID("dlgShiftsTableSelect").destroy();
			}
		}).data("FilterPlant", oParams.Plant);

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oShiftModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgShiftsTableSelect").setContentWidth("800px");

	if ($.UIbyID("dlgShiftsTableSelect").data("FilterPlant") !== oParams.Plant) {
		$.UIbyID("dlgShiftsTableSelect").getModel().loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + oParams.Plant);
	}

	if (oParams.Selected !== "" && oParams.Selected !== null) {
	/*	$.UIbyID("dlgShiftsTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("SHIFT", sap.ui.model.FilterOperator.EQ, oParams.Selected),
					 sap.ui.model.FilterType.Application);*/
		$.UIbyID("dlgShiftsTableSelect").open(oParams.Selected);
	}
 else {
		$.UIbyID("dlgShiftsTableSelect").open();
	}

	$.UIbyID("dlgShiftsTableSelect").focus();

}
