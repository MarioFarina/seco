sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/ui/export/Spreadsheet',
    "sap/ui/core/routing/History",
    "sap/m/Dialog"
], function (Controller, Spreadsheet, History, Dialog) {
    "use strict";
    return Controller.extend("seco.ui.app.seco.BaseController", {
        switchMenu: function () {
            var oSplitContainer = this.byId("mySplitContainer");
            oSplitContainer.setShowSecondaryContent(!oSplitContainer.getShowSecondaryContent());
        },

        getRouter: function () {
            return sap.ui.core.UIComponent.getRouterFor(this);
        },

        onNavBackToDashBoard: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                this.onNavBack();
            }
        },

        onNavBack: function () {
            this.getRouter().navTo("Welcome");
        },

        onNavToAnagraficheStabilimento: function() {
            this.getRouter().navTo("AnagraficheStabilimento")
        },

        onNavToOrariLinea: function() {
            this.getRouter().navTo("OrariLinea");
        },

        onNavToVersamentiProduzione: function () {
            this.getRouter().navTo("VersamentiProduzione");
        },

        onNavToAnalisiStatiLinea: function () {
            this.getRouter().navTo("AnalisiStatiLinea");
        },

        onNavToDashBoardSeco: function () {
            this.getRouter().navTo("DashBoardSeco");
        },

        onNavToFirst: function () {
            this.getRouter().navTo("ReportFirst");
        },

        onNavToQuality: function () {
            this.getRouter().navTo("ReportQuality");
        },

        onNavToAnalisiOEE: function () {
            this.getRouter().navTo("AnalisiOEE");
        },

        onNavToAnalisiOEEPerOrdine: function () {
            this.getRouter().navTo("AnalisiOEEPerOrdine");
        },

        onNavToPivotEventi: function () {
            this.getRouter().navTo("PivotEventi");
        },

        onNavToPivotConferme: function () {
            this.getRouter().navTo("PivotConferme");
        },

        exportTable: function (oModel, aCols) {
            var oSettings, oSheet;

            oSettings = {
                workbook: {columns: aCols},
                dataSource: oModel.getProperty('/')
            };

            oSheet = new Spreadsheet(oSettings);
            oSheet.build().then(function () {
                // MessageToast.show('Spreadsheet export has finished');
            });
        },

        formatTimePicker: function (dateToFormat) {
            var d = new Date(dateToFormat);
            d = d.toTimeString();
            return d.split(' ')[0];
        },

        messageDialog: function (title, state, text) {
            var messageBoxDialog = new Dialog({
                title: title,
                type: 'Message',
                state: state,
                content: new sap.m.Text({
                    text: text
                }),
                beginButton: new sap.m.Button({
                    text: 'OK',
                    press: function () {
                        messageBoxDialog.close();
                    }
                }),
                afterClose: function() {
                    messageBoxDialog.destroy();
                }
            });
            messageBoxDialog.open();
        }
    });
});