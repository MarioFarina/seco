var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/json&QueryTemplate=ProductionMain/MasterData/";
var dataProdRPT = "Content-Type=text/json&QueryTemplate=ProductionMain/Report/";

sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/xml/XMLModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/DateFormat",
    "sap/viz/ui5/api/env/Format",
    "sap/viz/ui5/format/ChartFormatter",
    "sap/m/MessageBox"
], function (BaseController, XmlModel, JSONModel, DateFormat, Format, ChartFormatter, MessageBox) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.VersamentiProduzione", {
        pathToExport: null,
        columnsExport: null,
        rowsExport: null,

        getViewModel: function (modelName, query) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var oModel = new JSONModel();
                oModel.loadData(query);
                oModel.attachRequestCompleted(function () {
                    var rows = oModel.oData.Rowsets.Rowset[0].Row;
                    if (typeof rows === "undefined") {
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    } else {
                        var data = {};
                        data[modelName] = typeof rows.length !== "undefined" ? rows : [rows];
                        if (modelName === "LINES") {
                            data[modelName].splice(0, 0, {"IDLINE": "%25", "LINETXT": "Tutte le linee"})
                        }
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    }
                }.bind(self));
            });
        },

        initViewModels: function () {
            var self = this;
            var view = this.getView();
            // let oPeriods = new JSONModel();
            // oPeriods.setData({
            //     "PERIODS": [{"NAME": "DAY", "TEXT": "Giorno"}, {
            //         "NAME": "WEEK",
            //         "TEXT": "Settimana"
            //     }, {"NAME": "MONTH", "TEXT": "Mese"}]
            // });
            // this.getView().setModel(oPeriods, "PERIODS");
            this.getPlants().then(function () {
                this.getView().byId("plants").setSelectedKey(this.getView().getModel("PLANTS").oData.PLANTS[0].PLANT);
                this.getView().byId("periods").setSelectedKey("daily");
                this.getDepartments().then(function () {
                    view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);

                    // getLines();
                    new Promise(function (resolve, reject) {
                        var oModel = new JSONModel();
                        oModel.loadData(QService + dataProdMD + "Lines/getLinesbyPlantRepSQ&Param.1=" + view.byId("plants").getSelectedKey() + "&Param.2=" + view.byId("departments").getSelectedKey());
                        oModel.attachRequestCompleted(function () {
                            var rows = oModel.oData.Rowsets.Rowset[0].Row;
                            if (typeof rows === "undefined") {
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            } else {
                                var data = {};
                                data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                                data["LINES"].splice(0, 0, {"IDLINE": "%25", "LINETXT": "Tutte le linee"});
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            }
                            view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].IDLINE);
                        }.bind(self));
                    });
                });

            }.bind(this));
        },

        getPlants: function () {
            return this.getViewModel("PLANTS", QService + dataProdMD + "Plants/getPlantsByUserQR");
        },

        getDepartments: function () {
            return this.getViewModel("DEPARTMENTS", QService + dataProdMD + "Departments/getDepartmentsByPlantSQ&Param.1=" + this.getView().byId("plants").getSelectedKey());
        },

        getLines: function () {
            return this.getViewModel("LINES", QService + dataProdMD + "Lines/getLinesbyPlantRepSQ&Param.1=" + this.getView().byId("plants").getSelectedKey() + "&Param.2=" + this.byId("departments").getSelectedKey());
        },

        onPlantChanged: function () {
            var view = this.getView();
            this.getDepartments().then(function () {
                view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);
                new Promise(function (resolve, reject) {
                    var oModel = new JSONModel();
                    oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                    oModel.attachRequestCompleted(function () {
                        var rows = oModel.oData.Rowsets.Rowset[0].Row;
                        if (typeof rows === "undefined") {
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        } else {
                            var data = {};
                            data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                            data[modelName].splice(0, 0, {"IDLINE": "%25", "LINETXT": "Tutte le linee"});
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        }
                        view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                    }.bind(self));
                });
            });
        },

        onRepartoChanged: function () {
            var view = this.getView();
            this.getLines().then(function () {
                view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].IDLINEA);
            });
        },

        getQueryParameters: function () {
            var params = [];
            var view = this.getView();
            var from = view.byId("TurnoDa").getDateValue();
            var to = view.byId("TurnoA").getDateValue();
            var line = view.byId("lines").getSelectedKey();
            var period = view.byId("periods").getSelectedKey();
            var type = view.byId("type").getSelectedKey();
            var material = view.byId("material").getValue();
            var order = view.byId("order").getValue();

            if (material === "") {
                material = "%25"
            }
            if (order === "") {
                order = "%25"
            }

            if (from instanceof Date && to instanceof Date && line !== "" && period !== "" && type !== "") {
                params = [from, to, line, material, order];
            }
            return params;
        },

        formatQueryParameters: function (params) {
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var view = this.getView();
            var query = "";

            query += "Param.1=" + oDateFormatter.format(params[0]);
            query += "&Param.2=" + oDateFormatter.format(params[1]);
            query += "&Param.3=" + params[2];
            query += "&Param.4=" + params[3];
            query += "&Param.5=" + params[4];

            return query;
        },

        resizeContent: function () {
            var body = document.body;
            var html = document.documentElement;
            var height = Math.max(document.body.clientHeight) - 151;
            var chartHeight = height - 70;
            // document.getElementById("content").style.height = height + "px";

            var chart = document.getElementsByClassName("chrt-columns");
            if (!chart || chart.length === 0) return;

            // document.getElementsByClassName("chrt-columns")[0].style.height = chartHeight + "px";
            return this.resizeContent;
        },

        onSelectionChange: function () {
            var el = $(".sapMLIBSelected")[0];
            var parent = $(el).parent();
            var childs = $(el).parent().children();
            var c;
            for (c = 0; c < childs.length; c++) {
                if (childs[c].id === el.id) break;
            }

            if (childs.length === c) return;
            var record = this.getView().getModel("DATASET").oData.rows[c];
            var periodColumns = this.tree[record.Reparto][record.Linea].periodColumns;

            this.getView().setModel(new JSONModel({rows: periodColumns}), "CHART");
            window.setTimeout(this.resizeContent.bind(this), 500);
        },

        getPeriodIndex: function (row) {
            var period = this.getView().byId("periods").getSelectedKey();
            var date = moment(row.GG);

            if (period === "daily") {
                return date.format("YYYY-MM-DD");
            } else if (period === "weekly") {
                var day = date.day();
                var monday = date.add((day - 1) * -1, "days").format("YYYY-MM-DD");
                return moment(monday).format("YYYY") + " " + moment(monday).week();
            } else { /*month*/
                return moment(date).format("YYYY-MM");
            }
        },

        aggregateData: function (rows, pColumns) {
            var tree = {};
            for (var r = 0; r < rows.length; r++) {
                if (typeof tree[rows[r].REPARTO] === "undefined") {
                    tree[rows[r].REPARTO] = {};
                }

                var reparto = tree[rows[r].REPARTO];
                if (typeof reparto[rows[r].LINE] === "undefined") {
                    reparto[rows[r].LINE] = {};
                }

                var line = reparto[rows[r].LINE];
                if (typeof line.periodColumns === "undefined") {
                    line.periodColumns = JSON.parse(JSON.stringify(pColumns));
                }

                var index = this.getPeriodIndex(rows[r]);
                for (var lp = 0; lp < line.periodColumns.length; lp++) {
                    var pcolumn = line.periodColumns[lp];
                    if (pcolumn.name === index) {
                        pcolumn.good += rows[r].GOOD_QTY;
                        pcolumn.scrap += rows[r].SCRAP_QTY;
                        pcolumn.count = pcolumn.good + pcolumn.scrap; //recalculate total
                        break;
                    }
                }
            }
            return tree;
        },

        explodeTree: function (tree) {
            var aggregation = this.getView().byId("type").getSelectedKey();
            var exploded = [];
            var columns = [{name: "Reparto"}, {name: "Linea"}, {name: "Totale"}];
            var periodInColumn = false;
            for (var r in tree) {
                var reparto = tree[r];
                var record = {};
                for (var l in reparto) {
                    var linea = reparto[l];
                    record.Reparto = r;
                    record.Linea = l;
                    if (typeof record.Totale === "undefined") record.Totale = 0; //only in case we have not yet defined total column (company aggregation)
                    for (var p in linea.periodColumns) {
                        var periodColumn = linea.periodColumns[p];
                        if (typeof record[periodColumn.name] === "undefined") record[periodColumn.name] = 0;
                        record.Totale += periodColumn.count;
                        record[periodColumn.name] += periodColumn.count;
                        if (!periodInColumn) {
                            columns.push({name: periodColumn.name});
                        }
                    }
                    periodInColumn = true;
                    //in case of plant aggregation we add a new record for each plant
                    if (aggregation === "plant") {
                        exploded.push(record);
                        record = {};
                    }
                }
                //in case of compny aggregation we will add a record for each company/reparto
                if (aggregation === "company") {
                    exploded.push(record);
                }
            }
            return [columns, exploded];
        },

        createMatrixByDay: function (start, end) {
            var from = moment(start);
            var end = moment(end);
            var tmp = moment(start).format("YYYY-MM-DD");
            var columns = [];

            do {
                columns.push({name: tmp, count: 0, good: 0, scrap: 0});
                tmp = moment(tmp).add(1, "days").format("YYYY-MM-DD");
            } while (moment(tmp).diff(end, "days") <= 0);

            return columns;
        },

        createMatrixByWeeks: function (start, end) {
            var from = moment(start);
            var to = moment(end);
            var day = from.day();
            var monday = from.add(day - 1, "days").format("YYYY-MM-DD");
            var tmp = monday;
            var columns = [];

            do {
                columns.push({
                    name: moment(tmp).format("YYYY") + " " + moment(tmp).week(),
                    count: 0,
                    good: 0,
                    scrap: 0
                });
                tmp = moment(tmp).add(7, "days");
            } while (moment(tmp).week() <= to.week());

            return columns;
        },

        createMatrixByMonths: function (start, end) {
            var from = moment(start);
            var to = moment(end);
            var tmp = from.format("YYYY-MM-DD");
            var columns = [];

            do {
                columns.push({name: moment(tmp).format("YYYY-MM"), count: 0, good: 0, scrap: 0});
                tmp = moment(tmp).add(1, "months").format("YYYY-MM-DD");
            } while (moment(tmp).month() <= to.month());

            return columns;
        },

        /**********************************
         * CONTROLLER LIFE-CYCLE
         **********************************/

        onInit: function () {
            var oData = {
                PERIODS: [{
                    key: "daily", name: "Giornaliero"
                },
                    {
                        key: "weekly", name: "Settimanale"
                    },
                    {
                        key: "monthly", name: "Mensile"
                    }],
                TYPES: [{key: "plant", name: "Impianto"}, {key: "company", name: "Reparto"}]
            };
            var self = this;
            this.initViewModels();
            var oModel = new JSONModel(oData);
            this.getView().setModel(oModel);
            // window.onresize = function () {
            //     self.resizeContent().bind(self); //execute and attach the event handler
            // };

            Format.numericFormatter(ChartFormatter.getInstance());
            var formatPattern = ChartFormatter.DefaultPattern;

            var propertiesReas = {
                plotArea: {
                    dataLabel: {
                        formatString: formatPattern.SHORTFLOAT_MFD2,
                        visible: true,
                        showTotal: false
                    }
                },
                valueAxis: {
                    label: {
                        formatString: formatPattern.SHORTFLOAT
                    },
                    title: {
                        visible: false
                    }
                },
                valueAxis2: {
                    label: {
                        formatString: formatPattern.SHORTFLOAT
                    },
                    title: {
                        visible: false
                    }
                },
                categoryAxis: {
                    title: {
                        visible: false
                    }
                },
                title: {
                    visible: false,
                    text: 'Revenue by City and Store Name'
                }
            };
            this.getView().byId("chrt-columns").setVizProperties(propertiesReas);

            // this.resizeContent(); //execute and attach the event handler
            var now = new Date();
            var past = new Date();
            past.setDate(past.getDate() - 7);
            this.getView().byId("TurnoA").setDateValue(now);
            this.getView().byId("TurnoDa").setDateValue(past);
        },

        onSearch: function () {
            var oModel = new JSONModel();
            var params = this.getQueryParameters();
            if (params.length === 0) {
                MessageBox.warning("Inserire tutti i campi obbligatori: Linea, Inizio Periodo, Fine range.", {styleClass: "sapUiSizeCompact"});
                return;
            }
            var queryString = this.formatQueryParameters(params);
            this.getView().setModel(new JSONModel({}), "DATASET");
            this.getView().setModel(new JSONModel({}), "CHART");

            var view = this.getView();
            var self = this;
            self.pathToExport = null;

            oModel.loadData(QService + dataProdRPT + "Conferme/getConfermeQR&" + queryString);
            oModel.attachRequestCompleted(function () {
                var period = view.byId("periods").getSelectedKey();
                var periodColumns = [];

                if (!oModel.oData.Rowsets) return;
                if (!oModel.oData.Rowsets.Rowset) return;
                var rowset = oModel.oData.Rowsets.Rowset[0];

                if (period === "daily") {
                    periodColumns = this.createMatrixByDay(moment(params[0]).format("YYYY-MM-DD"), moment(params[1]).format("YYYY-MM-DD"));
                } else if (period === "weekly") {
                    periodColumns = this.createMatrixByWeeks(moment(params[0]).format("YYYY-MM-DD"), moment(params[1]).format("YYYY-MM-DD"));
                } else {
                    periodColumns = this.createMatrixByMonths(moment(params[0]).format("YYYY-MM-DD"), moment(params[1]).format("YYYY-MM-DD"));
                }

                var tree = this.aggregateData(rowset.Row, periodColumns);
                var exploded = this.explodeTree(tree);
                var columns = exploded[0];
                var rows = exploded[1];
                this.tree = tree;

                //Table Rendering
                var oTable = view.byId("tblDataset");
                var tableColumns = [], tableCells = [], cells = [];
                oTable.destroyColumns();

                for (var c = 0; c < columns.length; c++) {
                    tableColumns.push({header: new sap.m.Label({text: columns[c].name})});
                    tableCells.push({text: "{DATASET>" + columns[c].name + "}"});
                }

                for (var cl = 0; cl < tableColumns.length; cl++) {
                    oTable.addColumn(new sap.m.Column(tableColumns[cl]));
                    cells.push(new sap.m.Text(tableCells[cl]));
                }

                this.getView().setModel(new JSONModel({"rows": rows}), "DATASET");
                oTable.bindItems("DATASET>/rows", new sap.m.ColumnListItem({cells: cells}));
                // window.setTimeout(this.resizeContent.bind(this), 500);
                //
                self.pathToExport = rows;
                self.columnsExport = columns;
                self.rowsExport = rows;

                window.setTimeout(function () {
                    if (document.getElementsByClassName("tblDataset").length > 0) {
                        var tableWidth = 90 * (periodColumns.length + 3);
                        document.getElementsByClassName("tblDataset")[0].style.width = tableWidth + "px";
                    }
                }.bind(this), 500);
            }.bind(this));
        },

        columnsToExport: function () {
            var columnsToExport = [];
            for (var i = 0; i < this.columnsExport.length; i++) {
                columnsToExport.push({
                    label: this.columnsExport[i].name,
                    property: this.columnsExport[i].name
                });
            }
            return columnsToExport;
        },

        exportxls: function () {
            var aCols;
            if(this.pathToExport !== null) {
                aCols = this.columnsToExport();
                var oModelRowset1 = new JSONModel();
                oModelRowset1.setData(this.pathToExport);
                this.exportTable(oModelRowset1, aCols)
            }
        },
    });
});