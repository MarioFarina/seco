sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.PivotEventi", {
        bRender : false,
        
        dtFormat : sap.ui.core.format.DateFormat.getDateInstance({
            pattern: "yyyyMMdd"
        }),
        dtSelect : sap.ui.core.format.DateFormat.getDateInstance({
            pattern: "yyyy-MM-dd"
        }),
        
        dateSelFrom : "",
        dateSelTo : "",
        
        oLng_Analysis : jQuery.sap.resources({
            url: "/XMII/CM/ProductionMain/" + "PivotAnalysis/" + "res/Analysis_res.i18n.properties",
            locale: sLanguage
        }),
        
        onInit: function () {
            var self = this;
            var view = this.getView();
            view.addEventDelegate({
                onAfterShow: function (oEvent) {
                    if(self.bRender == false){
                        self.bRender = true;
                        self.pivot();
                    }
                    self.renderTable();
                }
            }, view);
        },
        
        pivot: function () {
            var yesterday = new Date();
            yesterday.setDate( yesterday.getDate() - 1 );
            var ToDay = new Date();
            
            var  year = yesterday.getUTCFullYear();
            var  month = yesterday.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            var  day = yesterday.getUTCDate();
            if( day.toString().length == 1 ) day = "0" + day;
            dateSelFrom = year + "-" + month+ "-" + day;
            var dOrigDateSelFtrom = dateSelFrom;
            
            var fromday = new Date(yesterday.getFullYear(), yesterday.getMonth() + 1, 0);
            year = ToDay.getUTCFullYear();
            month = ToDay.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            day = ToDay.getUTCDate();            
            dateSelTo = year + "-" + month+ "-" + day;
            var dOrigDateSelTo = dateSelTo;
            
            var odtFrom = new sap.ui.commons.DatePicker('dtFromE');
            odtFrom.setYyyymmdd( dateSelFrom );
            odtFrom.setLocale("it"); 
            odtFrom.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelFrom = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                }
            );
            
            var odtTo = new sap.ui.commons.DatePicker('dtToE');
            odtTo.setYyyymmdd( dateSelTo );
            odtTo.setLocale("it"); 
            odtTo.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelTo = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                }
            );
            
            var self = this;
            var oToolbar1 = new sap.ui.commons.Toolbar("tb1E",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
                items: [
                    new sap.ui.commons.Label("lFromE",{
                        text : oLng_Analysis.getText("Analysis_FromDate"), 
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") 
                    }),
                    odtFrom,
                    new sap.ui.commons.Label("lToE",{
                        text : oLng_Analysis.getText("Analysis_ToDate"), 
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") 
                    }),
                    odtTo,
                    new sap.ui.commons.Button("btnRefreshE",{
                        icon: "sap-icon://refresh",
                        text : oLng_Analysis.getText("Analysis_Refresh"), 
                        tooltip : oLng_Analysis.getText("Analysis_ResultRefresh"),
                        press: function() {
                            self.renderTable();
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button("btnXlsE",{
                        icon: "sap-icon://excel-attachment",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToExcel"), 
                        press: function() {
                            exportTo("xls");
                        }
                    }),
                    
                    /**
                    new sap.ui.commons.Button("btnDoc",{
                        icon: "sap-icon://doc-attachment",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToWord"), 
                        press: function() {
                            exportTo("doc");
                        }
                    }),
                    */
                    
                    /**
                    new sap.ui.commons.Button("pdf",{
                        icon: "sap-icon://pdf-attachment",
                        tooltip : oLng_Analysis.getText("Analysis_PrintPdf"), 
                        press: function() {
                            exportTo("pdf");
                        }
                    })
                    */
                ]
            });
            
            /**
            var self = this;
            var oToolbar3 = new sap.ui.commons.Toolbar("tb3",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
               
                rightItems: [
                    
                    new sap.ui.commons.Button("btnRefresh",{
                        icon: "sap-icon://refresh",
                        text : oLng_Analysis.getText("Analysis_Refresh"), 
                        tooltip : oLng_Analysis.getText("Analysis_ResultRefresh"),
                        press: function() {
                            self.renderTable();
                        }
                    })
                ]
            });
            */
            
            var oVLayout = new sap.ui.commons.layout.VerticalLayout({
                width: "100%",
                content: [
                    oToolbar1,
                    //oToolbar3
                ]
            });        
            
            oVLayout.placeAt(this.getView().byId("ToolbarE"));    
            
            //this.renderTable();
        },
        
        renderTable: function () {
            
            var dateFormat = $.pivotUtilities.derivers.dateFormat;
            var sortAs = $.pivotUtilities.sortAs;

            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var p1 = oDateFormatter.format( this.dtSelect.parse($.UIbyID("dtFromE").getYyyymmdd()) );
            var p2 = oDateFormatter.format( this.dtSelect.parse($.UIbyID("dtToE").getYyyymmdd()) );
            
            var itm = {};
           
            const sDateStart = oLng_Analysis.getText("Analysis_DateStart"); 
            itm[sDateStart] = function(record) {return record.ZZDATETIME};
            
            const sLine = oLng_Analysis.getText("Analysis_Line"); 
            itm[sLine] = function(record) {return record.ZZLINEIDENTITY};
            
            const sOrder = oLng_Analysis.getText("Analysis_Order"); 
            itm[sOrder] = function(record) {return record.ZZORDER};
            
            const sDateEnd = oLng_Analysis.getText("Analysis_DateEnd"); 
            itm[sDateEnd] = function(record) {return record.ZZTIMETO};
            
            const sDuration = oLng_Analysis.getText("Analysis_Duration"); 
            itm[sDuration] = function(record) {return record.ZZDURATION};
            
            const sTimeUnit = oLng_Analysis.getText("Analysis_TimeUnit"); 
            itm[sTimeUnit] = function(record) {return record.ZZTIMEUNIT};
            
            const sEventType = oLng_Analysis.getText("Analysis_EventType"); 
            itm[sEventType] = function(record) {return record.ZZEVENTTYPE};
            
            const sAlarmId = oLng_Analysis.getText("Analysis_AlarmId"); 
            itm[sAlarmId] = function(record) {return record.ZZALARMID};
            
            const sOperatorId = oLng_Analysis.getText("Analysis_OperatorId"); 
            itm[sOperatorId] = function(record) {return record.ZZOPERATORID};
            
            const sGrp1 = oLng_Analysis.getText("Analysis_Grp1"); 
            itm[sGrp1] = function(record) {return record.ZZGRP1};
            
            const sGrp2 = oLng_Analysis.getText("Analysis_Grp2"); 
            itm[sGrp2] = function(record) {return record.ZZGRP2};
            
            const sGrp3 = oLng_Analysis.getText("Analysis_Grp3"); 
            itm[sGrp3] = function(record) {return record.ZZGRP3};
            
            var aColumns = [];
            
            var PivotPar = {
                PlaceAt: ".pivotE", 
                Query: {
                    Name: "getProductionEventsFromMIIQR",
                    Path: QPrefix + "ProductionMain/Report/Pivot/",
                    Params: "&Param.1=" + p1 + "&Param.2=" + p2
                },
                Pivot: {
                    Rows: [], 
                    Cols: aColumns,
                    Vals: [], 
                    HiddenAttributes:[
                         "MANDT"
                        ,"ZZDATETIME"
                        ,"ZZLINEIDENTITY"
                        ,"ZZORDER"
                        ,"ZZTIMETO"
                        ,"ZZDURATION"
                        ,"ZZTIMEUNIT"
                        ,"ZZEVENTTYPE"
                        ,"ZZALARMID"
                        ,"ZZOPERATORID"
                        ,"ZZGRP1"
                        ,"ZZGRP2"
                        ,"ZZGRP3" 
                    ],
            
                    DerivedAttributes: itm
                }
            };
     
            renderTablePivot(PivotPar);
        }
    })
});