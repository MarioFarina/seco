var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/json&QueryTemplate=ProductionMain/MasterData/";

sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/ui/layout/VerticalLayout",
    "sap/ui/layout/HorizontalLayout"
], function (BaseController, JSONModel, Dialog, Button, VerticalLayout, HorizontalLayout) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.Amministrazione.AnagraficheStabilimento", {
        divisioniField: [
            "PLANT",
            "NAME1",
            "NAME_LONG"
        ],
        divisioniNames: [
            "Divisione",
            "Nome",
            "Nome esteso"
        ],
        repartiField: [
            "IDREP",
            "REPTXT",
            "NOTES"
        ],
        repartiNames: [
            "ID Reparto",
            "Descrizione",
            "Note"
        ],
        lineeField: [
            "IDLINE",
            "LINETXT",
            "REPTXT",
            "CDLID",
            "SHOWDB",
            "OEE_P",
            "OEE_Q",
            "OEE_D",
            "REPORT_FIRST",
            "REPORT_QUALITY",
            "EXID",
            "NOTES",
            "TIMEFROM",
            "TIMETO",
            "CYCLETIMESECONDS"
        ],
        lineeNames: [
            "ID Linea",
            "Descrizione",
            "Reparto",
            "Centro di Lavoro",
            "in DashBoard",
            "OEE_P",
            "OEE_Q",
            "OEE_D",
            "R. First",
            "R. Quality",
            "ID Esterno",
            "Note",
            "Da",
            "A",
            "cycletimeseconds"
        ],

        divisioniResponse: null,
        turniResponse: null,
        repartiResponse: null,
        lineeResponse: null,
        rowToModify: [],
        CDL: null,

        getViewModel: function (modelName, query) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var oModel = new JSONModel();
                oModel.loadData(query);
                oModel.attachRequestCompleted(function () {
                    var rows = oModel.oData.Rowsets.Rowset[0].Row;
                    if (typeof rows === "undefined") {
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    } else {
                        var data = {};
                        data[modelName] = typeof rows.length !== "undefined" ? rows : [rows];
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    }
                }.bind(self));
            })
        },

        onInit: function () {
            this.initViewModels();
        },

        initViewModels: function () {
            var view = this.getView();
            var self = this;
            this.getPlants().then(function () {
                view.byId("plants").setSelectedKey(view.getModel("PLANTS").oData.PLANTS[0].PLANT);
                self.getCentriDiLavoro();
                self.doRequest();
            });
        },

        getPlants: function () {
            return this.getViewModel("PLANTS", QService + dataProdMD + "Plants/getPlantsSQ");
        },

        onPlantChanged: function () {
            var self = this;
            this.getPlants().then(function () {
                self.doRequest();
                self.getCentriDiLavoro();
            });
        },

        onChangeTable: function (oEvent) {
            this.setTables(oEvent.oSource.mProperties.text);
        },

        setTables: function (scelta) {
            var view = this.getView();
            switch (scelta) {
                case "Divisioni": {
                    view.byId("tblTurni").setVisible(false);
                    var a = view.byId("tblTurni").getSelectedIndices();
                    view.byId("tblReparti").setVisible(false);
                    var b = view.byId("tblReparti").getSelectedIndices();
                    view.byId("tblLinee").setVisible(false);
                    var c = view.byId("tblLinee").getSelectedIndices();
                    view.byId("tblDivisioni").setVisible(true);

                    view.byId("tblTurni").removeSelectionInterval(0, a);
                    view.byId("tblReparti").removeSelectionInterval(0, b);
                    view.byId("tblLinee").removeSelectionInterval(0, c);

                    this.rowToModify = [];
                    this.getView().byId("modifyButton").setEnabled(false);
                    this.getView().byId("deleteButton").setEnabled(false);
                    break;
                }
                case "Reparti": {
                    view.byId("tblDivisioni").setVisible(false);
                    view.byId("tblTurni").setVisible(false);
                    view.byId("tblLinee").setVisible(false);
                    view.byId("tblReparti").setVisible(true);

                    var a = view.byId("tblTurni").getSelectedIndices();
                    var b = view.byId("tblDivisioni").getSelectedIndices();
                    var c = view.byId("tblLinee").getSelectedIndices();
                    view.byId("tblTurni").removeSelectionInterval(0, a);
                    view.byId("tblDivisioni").removeSelectionInterval(0, b);
                    view.byId("tblLinee").removeSelectionInterval(0, c);

                    this.rowToModify = [];
                    this.getView().byId("modifyButton").setEnabled(false);
                    this.getView().byId("deleteButton").setEnabled(false);
                    break;
                }
                case "Linee": {
                    view.byId("tblDivisioni").setVisible(false);
                    view.byId("tblTurni").setVisible(false);
                    view.byId("tblReparti").setVisible(false);
                    view.byId("tblLinee").setVisible(true);

                    var a = view.byId("tblTurni").getSelectedIndices();
                    var b = view.byId("tblDivisioni").getSelectedIndices();
                    var c = view.byId("tblReparti").getSelectedIndices();
                    view.byId("tblTurni").removeSelectionInterval(0, a);
                    view.byId("tblDivisioni").removeSelectionInterval(0, b);
                    view.byId("tblReparti").removeSelectionInterval(0, c);

                    this.rowToModify = [];
                    this.getView().byId("modifyButton").setEnabled(false);
                    this.getView().byId("deleteButton").setEnabled(false);
                    break;
                }

            }
        },

        doRequest: function () {
            var self = this;
            var view = this.getView();
            var plant = view.byId("plants").getSelectedKey();

            var oModel1 = new JSONModel();
            var oModel2 = new JSONModel();
            var oModel3 = new JSONModel();
            var oModel4 = new JSONModel();

            oModel1.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
            oModel1.attachRequestCompleted(function () {
                self.divisioniResponse = oModel1.oData.Rowsets.Rowset[0].Row;
                view.byId("tblDivisioni").setModel(new JSONModel(self.divisioniResponse));
                view.byId("tblDivisioni").setVisibleRowCount(self.divisioniResponse.length < 8 ? self.divisioniResponse.length : 8);
            });

            // oModel2.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + plant);
            // oModel2.attachRequestCompleted(function () {
            //     self.turniResponse = oModel2.oData.Rowsets.Rowset[0].Row;
            //     view.byId("tblTurni").setModel(new JSONModel(self.turniResponse));
            // });

            oModel3.loadData(QService + dataProdMD + "Departments/getDepartmentsByPlantSQ&Param.1=" + plant);
            oModel3.attachRequestCompleted(function () {
                self.repartiResponse = oModel3.oData.Rowsets.Rowset[0].Row;
                view.byId("tblReparti").setModel(new JSONModel(self.repartiResponse));
                view.byId("tblReparti").setVisibleRowCount(self.repartiResponse.length < 8 ? self.repartiResponse.length : 8);
            });

            oModel4.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + plant);
            oModel4.attachRequestCompleted(function () {
                self.lineeResponse = oModel4.oData.Rowsets.Rowset[0].Row;
                view.byId("tblLinee").setModel(new JSONModel(self.lineeResponse));
                view.byId("tblLinee").setVisibleRowCount(self.lineeResponse.length < 8 ? self.lineeResponse.length : 8);
            });
        },

        getCentriDiLavoro: function () {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Lines/getWorksbyPlantSQ&Param.1=" + this.getView().byId("plants").getSelectedKey());
            oModel.attachRequestCompleted(function () {
                if (oModel.oData.Rowsets.Rowset[0].Row) {
                    self.CDL = oModel.oData.Rowsets.Rowset[0].Row;
                } else self.CDL = null;
            });
        },

        onAddRecord: function () {
            var dialog = null;
            var view = this.getView();
            var table = [
                view.byId("tblDivisioni").getVisible(),
                view.byId("tblTurni").getVisible(),
                view.byId("tblReparti").getVisible(),
                view.byId("tblLinee").getVisible(),
            ];
            if (table[0]) {
                dialog = new Dialog({
                    title: "Nuova Divisione",
                    contentWidth: "750px",
                    contentHeight: "430px",
                    draggable: true,
                    content: new HorizontalLayout({
                        content: [
                            new VerticalLayout({
                                content: [
                                    new sap.m.Label({
                                        text: "Divisione"
                                    }).addStyleClass("dialogContainerStyle"),
                                    new sap.m.Label({
                                        text: "Nome Stabilimento"
                                    }).addStyleClass("dialogContainerStyle"),
                                    new sap.m.Label({
                                        text: "Nome Esteso"
                                    }).addStyleClass("dialogContainerStyle"),

                                ]
                            }),
                            new VerticalLayout({
                                content: [
                                    new sap.m.Input({
                                        id: "divisioneInput",
                                        required: true
                                    }),
                                    new sap.m.Input({
                                        id: "stabilimentoInput",
                                        required: true
                                    }),
                                    new sap.m.Input({
                                        id: "nomeEstesoInput",
                                        required: true
                                    })
                                ]
                            })
                        ]
                    }),
                    beginButton:
                        new Button({
                            text: "Annulla",
                            press: function () {
                                dialog.close();
                            }.bind(this)
                        }),
                    endButton:
                        new Button({
                            text: "Salva",
                            press: function () {
                                var sPlant = $.UIbyID("divisioneInput").getValue();
                                var sName = $.UIbyID("stabilimentoInput").getValue();
                                var sExtendedName = $.UIbyID("nomeEstesoInput").getValue();

                                var qexe = "";
                                qexe += "&Param.1=" + sPlant;
                                qexe += "&Param.2=" + sName;
                                qexe += "&Param.3=" + sExtendedName;
                                this.addPlants(qexe);
                                dialog.close();
                            }.bind(this)
                        }),
                    afterClose: function () {
                        dialog.destroy();
                    }
                });
            } else if (table[2]) {
                dialog = new Dialog({
                    title: "Nuovo Reparto",
                    contentWidth: "750px",
                    contentHeight: "430px",
                    draggable: true,
                    content: new HorizontalLayout({
                        content: [
                            new VerticalLayout({
                                content: [
                                    new sap.m.Label({
                                        text: "Divisione"
                                    }).addStyleClass("dialogContainerStyle"),
                                    new sap.m.Label({
                                        text: "ID Reparto"
                                    }).addStyleClass("dialogContainerStyle"),
                                    new sap.m.Label({
                                        text: "Descrizione"
                                    }).addStyleClass("dialogContainerStyle"),
                                    new sap.m.Label({
                                        text: "Note"
                                    }).addStyleClass("dialogContainerStyle")
                                ]
                            }),
                            new VerticalLayout({
                                content: [
                                    new sap.m.Input({
                                        id: "divisioneInput",
                                        editable: false,
                                        value: view.byId("plants").getSelectedItem().getText()
                                    }),
                                    new sap.m.Input({
                                        id: "idRepartoInput",
                                        required: true,
                                        maxLength: 5
                                    }),
                                    new sap.m.Input({
                                        id: "descrizioneInput",
                                        required: true
                                    }),
                                    new sap.m.Input({
                                        id: "noteInput",
                                        required: false
                                    })
                                ]
                            })
                        ]
                    }),
                    beginButton:
                        new Button({
                            text: "Annulla",
                            press: function () {
                                dialog.close();
                            }.bind(this)
                        }),
                    endButton:
                        new Button({
                            text: "Salva",
                            press: function () {
                                var qexe = "";
                                qexe += "&Param.1=" + this.getView().byId("plants").getSelectedKey();
                                qexe += "&Param.2=" + $.UIbyID("idRepartoInput").getValue();
                                qexe += "&Param.3=" + $.UIbyID("descrizioneInput").getValue();
                                qexe += "&Param.4=" + $.UIbyID("noteInput").getValue();
                                this.addReparto(qexe);
                                dialog.close();
                            }.bind(this)
                        }),
                    afterClose: function () {
                        dialog.destroy();
                    }
                });
            } else if (table[3]) {
                dialog = new Dialog({
                    title: "Nuova Linea",
                    contentWidth: "750px",
                    contentHeight: "430px",
                    draggable: true,
                    content:
                        new HorizontalLayout({
                            content: [
                                new VerticalLayout({
                                    content: [
                                        new sap.m.Label({
                                            text: "ID Linea"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Descrizione"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Reparto"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Centro di Lavoro"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "In Dashboard"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "R. First"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "R. Quality"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "OEE_D"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "OEE_P"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "OEE_Q"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "ID Esterno"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Ora Inizio"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Ora Fine"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Note"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "CycleTimeSeconds"
                                        }).addStyleClass("dialogContainerStyle")
                                    ]
                                }),
                                new VerticalLayout({
                                    content: [
                                        new sap.m.Input({
                                            id: "idLineaInput",
                                            editable: true
                                        }),
                                        new sap.m.Input({
                                            id: "descrizioneInput",
                                            required: true
                                        }),
                                        new sap.m.Select({
                                            id: "repartoSelect",
                                            forceSelection: false,
                                            items: []
                                        }),
                                        new sap.m.Select({
                                            id: "centroLavoroInput",
                                            items: []
                                        }),
                                        new sap.m.CheckBox({
                                            id: "inDashboardInput",
                                            selected: false
                                        }).addStyleClass("checkBoxHeight"),
                                        new sap.m.CheckBox({
                                            id: "firstInput",
                                            selected: false
                                        }).addStyleClass("checkBoxHeight"),
                                        new sap.m.CheckBox({
                                            id: "qualityInput",
                                            selected: false
                                        }).addStyleClass("checkBoxHeight"),
                                        new sap.m.CheckBox({
                                            id: "OEE_DInput",
                                            selected: false
                                        }).addStyleClass("checkBoxHeight"),
                                        new sap.m.CheckBox({
                                            id: "OEE_PInput",
                                            selected: false
                                        }).addStyleClass("checkBoxHeight"),
                                        new sap.m.CheckBox({
                                            id: "OEE_QInput",
                                            selected: false
                                        }).addStyleClass("checkBoxHeight"),
                                        new sap.m.Input({
                                            id: "idEsternoInput",
                                            required: true
                                        }),
                                        new HorizontalLayout({
                                            content: [
                                                new sap.m.TimePicker({
                                                    id: "daInput",
                                                    valueFormat: "HH:mm:ss"
                                                }),
                                                new Button({
                                                    icon: "sap-icon://time-account",
                                                    press: function () {
                                                        $.UIbyID("daInput").setDateValue(new Date());
                                                    }
                                                })
                                            ]
                                        }),
                                        new HorizontalLayout({
                                            content: [
                                                new sap.m.TimePicker({
                                                    id: "aInput",
                                                    valueFormat: "HH:mm:ss"
                                                }),
                                                new Button({
                                                    icon: "sap-icon://time-account",
                                                    press: function () {
                                                        $.UIbyID("aInput").setDateValue(new Date());
                                                    }
                                                })
                                            ]
                                        }),
                                        new sap.m.Input({
                                            id: "noteInput",
                                            required: false
                                        }),
                                        new sap.m.Input({
                                            id: "cycleTimeSecondsInput",
                                            required: false,
                                            value: "0"
                                        })
                                    ]
                                })
                            ]
                        }),
                    beginButton:
                        new Button({
                            text: "Annulla",
                            press: function () {
                                dialog.close();
                            }.bind(this)
                        }),
                    endButton:
                        new Button({
                            text: "Salva",
                            press: function () {
                                var qexe = "";
                                qexe += "&Param.1=" + $.UIbyID("idLineaInput").getValue();
                                qexe += "&Param.2=" + $.UIbyID("descrizioneInput").getValue();
                                qexe += "&Param.3=" + $.UIbyID("repartoSelect").getSelectedKey();
                                qexe += "&Param.4=" + $.UIbyID("centroLavoroInput").getSelectedKey();
                                qexe += "&Param.5=" + $.UIbyID("inDashboardInput").getSelected();
                                qexe += "&Param.6=" + $.UIbyID("OEE_PInput").getSelected();
                                qexe += "&Param.7=" + $.UIbyID("OEE_QInput").getSelected();
                                qexe += "&Param.8=" + $.UIbyID("OEE_DInput").getSelected();
                                qexe += "&Param.9=" + $.UIbyID("firstInput").getSelected();
                                qexe += "&Param.10=" + $.UIbyID("qualityInput").getSelected();
                                qexe += "&Param.11=" + $.UIbyID("idEsternoInput").getValue();
                                qexe += "&Param.12=" + $.UIbyID("noteInput").getValue();
                                qexe += "&Param.13=" + $.UIbyID("daInput").getValue();
                                qexe += "&Param.14=" + $.UIbyID("aInput").getValue();
                                qexe += "&Param.15=" + $.UIbyID("cycleTimeSecondsInput").getValue();
                                this.addLinea(qexe);
                                dialog.close();
                            }.bind(this)
                        }),
                    afterClose: function () {
                        dialog.destroy();
                    }
                });
            }
            var modelReparto = new JSONModel(this.repartiResponse);
            view.addDependent(dialog);
            dialog.open();
            if (table[3]) {
                if (this.repartiResponse !== undefined) {
                    for (var i = 0; i < this.repartiResponse.length; i++) {
                        $.UIbyID("repartoSelect").addItem(new sap.ui.core.Item({
                            key: this.repartiResponse[i].IDREP,
                            text: this.repartiResponse[i].REPTXT
                        }));
                    }
                    $.UIbyID("repartoSelect").setSelectedKey(this.repartiResponse[0].IDREP);
                } else {
                    dialog.close();
                    this.messageDialog("Attenzione", "Warning", "Aggiungere un reparto!");
                    return 0;
                }
                $.UIbyID("centroLavoroInput").addItem(new sap.ui.core.Item({
                    key: "",
                    text: ""
                }));
                if (this.CDL !== null) {
                    for (var i = 0; i < this.CDL.length; i++) {
                        $.UIbyID("centroLavoroInput").addItem(new sap.ui.core.Item({
                            key: this.CDL[i].CDLID,
                            text: this.CDL[i].CDLDESC
                        }));
                    }
                }
                $.UIbyID("centroLavoroInput").setSelectedKey("");
            }
        },
        onModifyRecord: function () {
            var dialog = null;
            var view = this.getView();
            switch (this.rowToModify[1]) {
                case "Divisioni": {
                    var row = this.divisioniResponse[this.rowToModify[0]];
                    dialog = new Dialog({
                        title: "Modifica Divisione",
                        contentWidth: "750px",
                        contentHeight: "430px",
                        draggable: true,
                        content: new HorizontalLayout({
                            content: [
                                new VerticalLayout({
                                    content: [
                                        new sap.m.Label({
                                            text: "Divisione"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Nome Stabilimento"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Nome Esteso"
                                        }).addStyleClass("dialogContainerStyle")


                                    ]
                                }),
                                new VerticalLayout({
                                    content: [
                                        new sap.m.Input({
                                            id: "divisioneInput",
                                            editable: false,
                                            value: row.PLANT
                                        }),
                                        new sap.m.Input({
                                            id: "stabilimentoInput",
                                            required: true,
                                            value: row.NAME1
                                        }),
                                        new sap.m.Input({
                                            id: "nomeEstesoInput",
                                            required: true,
                                            value: row.NAME_LONG
                                        })
                                    ]
                                })
                            ]
                        }),
                        beginButton:
                            new Button({
                                text: "Annulla",
                                press: function () {
                                    dialog.close();
                                }.bind(this)
                            }),
                        endButton:
                            new Button({
                                text: "Salva",
                                press: function () {
                                    var sPlant = $.UIbyID("divisioneInput").getValue();
                                    var sName = $.UIbyID("stabilimentoInput").getValue();
                                    var sExtendedName = $.UIbyID("nomeEstesoInput").getValue();
                                    var qexe = "";
                                    qexe += "&Param.1=" + sPlant;
                                    qexe += "&Param.2=" + sName;
                                    qexe += "&Param.3=" + sExtendedName;
                                    this.updatePlants(qexe);
                                    dialog.close();
                                    view.byId("modifyButton").setEnabled(false);
                                    view.byId("deleteButton").setEnabled(false);
                                }.bind(this)
                            }),
                        afterClose: function () {
                            dialog.destroy();
                        }
                    });
                    break;
                }
                case "Reparti": {
                    var row = this.repartiResponse[this.rowToModify[0]];
                    dialog = new Dialog({
                        title: "Modifica Reparto",
                        contentWidth: "750px",
                        contentHeight: "430px",
                        draggable: true,
                        content: new HorizontalLayout({
                            content: [
                                new VerticalLayout({
                                    content: [
                                        new sap.m.Label({
                                            text: "Divisione"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "ID Reparto"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Descrizione"
                                        }).addStyleClass("dialogContainerStyle"),
                                        new sap.m.Label({
                                            text: "Note"
                                        }).addStyleClass("dialogContainerStyle")
                                    ]
                                }),
                                new VerticalLayout({
                                    content: [
                                        new sap.m.Input({
                                            id: "divisioneInput",
                                            editable: false,
                                            value: view.getModel("PLANTS").oData.PLANTS[0].NAME1
                                        }),
                                        new sap.m.Input({
                                            id: "idRepartoInput",
                                            editable: false,
                                            value: row.IDREP
                                        }),
                                        new sap.m.Input({
                                            id: "descrizioneInput",
                                            required: true,
                                            value: row.REPTXT
                                        }),
                                        new sap.m.Input({
                                            id: "noteInput",
                                            required: false,
                                            value: row.NOTES
                                        })
                                    ]
                                })
                            ]
                        }),
                        beginButton:
                            new Button({
                                text: "Annulla",
                                press: function () {
                                    dialog.close();
                                }.bind(this)
                            }),
                        endButton:
                            new Button({
                                text: "Salva",
                                press: function () {
                                    var sPlant = this.getView().byId("plants").getSelectedKey();
                                    var idRep = $.UIbyID("idRepartoInput").getValue();
                                    var desc = $.UIbyID("descrizioneInput").getValue();
                                    var note = $.UIbyID("noteInput").getValue();
                                    var qexe = "";
                                    qexe += "&Param.1=" + sPlant;
                                    qexe += "&Param.2=" + idRep;
                                    qexe += "&Param.3=" + desc;
                                    qexe += "&Param.4=" + note;
                                    this.updateReparto(qexe);
                                    dialog.close();
                                    view.byId("modifyButton").setEnabled(false);
                                    view.byId("deleteButton").setEnabled(false);
                                }.bind(this)
                            }),
                        afterClose: function () {
                            dialog.destroy();
                        }
                    });
                    break;
                }
                case "Linee": {
                    var row = this.lineeResponse[this.rowToModify[0]];
                    dialog = new Dialog({
                        title: "Nuova Linea",
                        contentWidth: "750px",
                        contentHeight: "430px",
                        draggable: true,
                        content:
                            new HorizontalLayout({
                                content: [
                                    new VerticalLayout({
                                        content: [
                                            new sap.m.Label({
                                                text: "ID Linea"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "Descrizione"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "Reparto"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "Centro di Lavoro"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "In Dashboard"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "R. First"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "R. Quality"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "OEE_D"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "OEE_P"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "OEE_Q"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "ID Esterno"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "Ora Inizio"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "Ora Fine"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "Note"
                                            }).addStyleClass("dialogContainerStyle"),
                                            new sap.m.Label({
                                                text: "CycleTimeSeconds"
                                            }).addStyleClass("dialogContainerStyle")
                                        ]
                                    }),
                                    new VerticalLayout({
                                        content: [
                                            new sap.m.Input({
                                                id: "idLineaInput",
                                                editable: false,
                                                value: row.IDLINE
                                            }),
                                            new sap.m.Input({
                                                id: "descrizioneInput",
                                                required: true,
                                                value: row.LINETXT
                                            }),
                                            new sap.m.Select({
                                                id: "repartoSelect",
                                                items: []
                                            }),
                                            new sap.m.Select({
                                                id: "centroLavoroInput",
                                                items: []
                                            }),
                                            new sap.m.CheckBox({
                                                id: "inDashboardInput",
                                                selected: row.SHOWDB ? true : false
                                            }).addStyleClass("checkBoxHeight"),
                                            new sap.m.CheckBox({
                                                id: "firstInput",
                                                selected: row.REPORT_FIRST ? true : false
                                            }).addStyleClass("checkBoxHeight"),
                                            new sap.m.CheckBox({
                                                id: "qualityInput",
                                                selected: row.REPORT_QUALITY ? true : false
                                            }).addStyleClass("checkBoxHeight"),
                                            new sap.m.CheckBox({
                                                id: "OEE_DInput",
                                                selected: row.OEE_D ? true : false
                                            }).addStyleClass("checkBoxHeight"),
                                            new sap.m.CheckBox({
                                                id: "OEE_PInput",
                                                selected: row.OEE_P ? true : false
                                            }).addStyleClass("checkBoxHeight"),
                                            new sap.m.CheckBox({
                                                id: "OEE_QInput",
                                                selected: row.OEE_Q ? true : false
                                            }).addStyleClass("checkBoxHeight"),
                                            new sap.m.Input({
                                                id: "idEsternoInput",
                                                required: true,
                                                value: row.EXID
                                            }),
                                            new HorizontalLayout({
                                                content: [
                                                    new sap.m.TimePicker({
                                                        id: "daInput",
                                                        valueFormat: "HH:mm:ss",
                                                        value: row.TIMEFROM.split(' ')[1]
                                                    }),
                                                    new Button({
                                                        icon: "sap-icon://time-account",
                                                        press: function () {
                                                            $.UIbyID("daInput").setDateValue(new Date());
                                                        }
                                                    })
                                                ]
                                            }),
                                            new HorizontalLayout({
                                                content: [
                                                    new sap.m.TimePicker({
                                                        id: "aInput",
                                                        valueFormat: "HH:mm:ss",
                                                        value: row.TIMETO.split(' ')[1]
                                                    }),
                                                    new Button({
                                                        icon: "sap-icon://time-account",
                                                        press: function () {
                                                            $.UIbyID("aInput").setDateValue(new Date());
                                                        }
                                                    })
                                                ]
                                            }),
                                            new sap.m.Input({
                                                id: "noteInput",
                                                required: false,
                                                value: row.NOTES
                                            }),
                                            new sap.m.Input({
                                                id: "cycleTimeSecondsInput",
                                                required: false,
                                                value: "0"
                                            })
                                        ]
                                    })
                                ]
                            }),
                        beginButton:
                            new Button({
                                text: "Annulla",
                                press: function () {
                                    dialog.close();
                                }.bind(this)
                            }),
                        endButton:
                            new Button({
                                text: "Salva",
                                press: function () {
                                    var qexe = "";
                                    qexe += "&Param.1=" + $.UIbyID("idLineaInput").getValue();
                                    qexe += "&Param.2=" + $.UIbyID("descrizioneInput").getValue();
                                    qexe += "&Param.3=" + $.UIbyID("repartoSelect").getSelectedKey();
                                    qexe += "&Param.4=" + $.UIbyID("centroLavoroInput").getSelectedKey();
                                    qexe += "&Param.5=" + $.UIbyID("inDashboardInput").getSelected();
                                    qexe += "&Param.6=" + $.UIbyID("OEE_PInput").getSelected();
                                    qexe += "&Param.7=" + $.UIbyID("OEE_QInput").getSelected();
                                    qexe += "&Param.8=" + $.UIbyID("OEE_DInput").getSelected();
                                    qexe += "&Param.9=" + $.UIbyID("firstInput").getSelected();
                                    qexe += "&Param.10=" + $.UIbyID("qualityInput").getSelected();
                                    qexe += "&Param.11=" + $.UIbyID("idEsternoInput").getValue();
                                    qexe += "&Param.12=" + $.UIbyID("noteInput").getValue();
                                    qexe += "&Param.13=" + $.UIbyID("daInput").getValue();
                                    qexe += "&Param.14=" + $.UIbyID("aInput").getValue();
                                    qexe += "&Param.15=" + $.UIbyID("cycleTimeSecondsInput").getValue();
                                    this.updateLinea(qexe);
                                    dialog.close();
                                    view.byId("modifyButton").setEnabled(false);
                                    view.byId("deleteButton").setEnabled(false);
                                }.bind(this)
                            }),
                        afterClose: function () {
                            dialog.destroy();
                        }
                    });
                    if (this.repartiResponse !== undefined) {
                        for (var i = 0; i < this.repartiResponse.length; i++) {
                            $.UIbyID("repartoSelect").addItem(new sap.ui.core.Item({
                                key: this.repartiResponse[i].IDREP,
                                text: this.repartiResponse[i].REPTXT
                            }));
                            if (row.IDREP == this.repartiResponse[i].IDREP) {
                                $.UIbyID("repartoSelect").setSelectedKey(row.IDREP);
                            }
                        }
                    }
                    if (this.CDL !== null) {
                        for (var i = 0; i < this.CDL.length; i++) {
                            $.UIbyID("centroLavoroInput").addItem(new sap.ui.core.Item({
                                key: this.CDL[i].CDLID,
                                text: this.CDL[i].CDLDESC
                            }));
                            if (row.CDLID == this.CDL[i].CDLID) {
                                $.UIbyID("centroLavoroInput").setSelectedKey(row.CDLID);
                            }
                        }
                    }
                    break;
                }
            }
            view.addDependent(dialog);
            dialog.open();
        },
        onDeleteRecord: function () {
            var self = this;
            var view = this.getView();
            var dialog = null;
            switch (this.rowToModify[1]) {
                case "Divisioni": {
                    var row = this.divisioniResponse[this.rowToModify[0]];
                    dialog = new Dialog({
                        title: 'Conferma',
                        type: 'Message',
                        content: new sap.m.Text({text: 'Eliminare definitivamente la divisione?'}),
                        beginButton: new Button({
                            text: 'Conferma',
                            press: function () {
                                var oModel = new JSONModel();
                                oModel.loadData(QService + dataProdMD + "Plants/delPlantXQ" + "&Param.1=" + row.PLANT);
                                oModel.attachRequestCompleted(function () {
                                    var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                                    if (dataResponse[0].RC === "1") self.messageDialog("Eliminazione", "Success", "Divisione eliminata correttamente");
                                    else self.messageDialog("Errore", "Error", "Errore durante l'eliminazione della divisione");
                                    view.byId("modifyButton").setEnabled(false);
                                    view.byId("deleteButton").setEnabled(false);
                                    self.doRequest();
                                    self.getPlants();
                                });
                                dialog.close();
                            }
                        }),
                        endButton: new Button({
                            text: 'Annulla',
                            press: function () {
                                dialog.close();
                            }
                        }),
                        afterClose: function () {
                            dialog.destroy();
                        }
                    });
                    break;
                }
                case "Reparti": {
                    var row = this.repartiResponse[this.rowToModify[0]];
                    dialog = new Dialog({
                        title: 'Conferma',
                        type: 'Message',
                        content: new sap.m.Text({text: 'Eliminare definitivamente il reparto?'}),
                        beginButton: new Button({
                            text: 'Conferma',
                            press: function () {
                                var oModel = new JSONModel();
                                oModel.loadData(QService + dataProdMD + "Departments/delDepartmentXQ" + "&Param.1=" + row.IDREP);
                                oModel.attachRequestCompleted(function () {
                                    var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                                    if (dataResponse[0].RC === "1") self.messageDialog("Eliminazione", "Success", "Reparto eliminato correttamente");
                                    else self.messageDialog("Errore", "Error", "Errore durante l'eliminazione del reparto");
                                    view.byId("modifyButton").setEnabled(false);
                                    view.byId("deleteButton").setEnabled(false);
                                    self.doRequest();
                                });
                                dialog.close();
                            }
                        }),
                        endButton: new Button({
                            text: 'Annulla',
                            press: function () {
                                dialog.close();
                            }
                        }),
                        afterClose: function () {
                            dialog.destroy();
                        }
                    });
                    break;
                }
                case "Linee": {
                    var row = this.lineeResponse[this.rowToModify[0]];
                    dialog = new Dialog({
                        title: 'Conferma',
                        type: 'Message',
                        content: new sap.m.Text({text: 'Eliminare definitivamente la linea?'}),
                        beginButton: new Button({
                            text: 'Conferma',
                            press: function () {
                                var oModel = new JSONModel();
                                oModel.loadData(QService + dataProdMD + "Lines/delLineXQ" + "&Param.1=" + row.IDLINE);
                                oModel.attachRequestCompleted(function () {
                                    var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                                    if (dataResponse[0].RC === "1") self.messageDialog("Eliminazione", "Success", "Linea eliminata correttamente");
                                    else self.messageDialog("Errore", "Error", "Errore durante l'eliminazione della linea");
                                    view.byId("modifyButton").setEnabled(false);
                                    view.byId("deleteButton").setEnabled(false);
                                    self.doRequest();
                                });
                                dialog.close();
                            }
                        }),
                        endButton: new Button({
                            text: 'Annulla',
                            press: function () {
                                dialog.close();
                            }
                        }),
                        afterClose: function () {
                            dialog.destroy();
                        }
                    });
                    break;
                }
            }
            this.getView().addDependent(dialog);
            dialog.open();
        },

        onRowChange: function (oEvent) {
            var view = this.getView();
            var table = [
                view.byId("tblDivisioni").getVisible(),
                view.byId("tblTurni").getVisible(),
                view.byId("tblReparti").getVisible(),
                view.byId("tblLinee").getVisible(),
            ];
            if (table[0]) {
                if (view.byId("tblDivisioni").getSelectedIndex() == -1) {
                    this.rowToModify = [];
                    this.getView().byId("modifyButton").setEnabled(false);
                    this.getView().byId("deleteButton").setEnabled(false);
                } else {
                    this.rowToModify = [
                        oEvent.mParameters.rowIndex, // Numero riga
                        "Divisioni" //Tabella selezionata
                    ];
                    this.getView().byId("modifyButton").setEnabled(true);
                    this.getView().byId("deleteButton").setEnabled(true);
                }
            } else if (table[2]) {
                if (view.byId("tblReparti").getSelectedIndex() == -1) {
                    this.rowToModify = [];
                    this.getView().byId("modifyButton").setEnabled(false);
                    this.getView().byId("deleteButton").setEnabled(false);
                } else {
                    this.rowToModify = [
                        oEvent.mParameters.rowIndex, // Numero riga
                        "Reparti" //Tabella selezionata
                    ];
                    this.getView().byId("modifyButton").setEnabled(true);
                    this.getView().byId("deleteButton").setEnabled(true);
                }
            } else if (table[3]) {
                if (view.byId("tblLinee").getSelectedIndex() == -1) {
                    this.rowToModify = [];
                    this.getView().byId("modifyButton").setEnabled(false);
                    this.getView().byId("deleteButton").setEnabled(false);
                } else {
                    this.rowToModify = [
                        oEvent.mParameters.rowIndex, // Numero riga
                        "Linee" //Tabella selezionata
                    ];
                    this.getView().byId("modifyButton").setEnabled(true);
                    this.getView().byId("deleteButton").setEnabled(true);
                }
            }
        },

        addPlants: function (query) {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Plants/addPlantXQ" + query);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Aggiunta", "Success", "Divisione aggiunta correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante l'aggiunta della divisione");
                self.doRequest();
                self.getPlants();
            });
        },

        // addTurno: function (query) {
        //     var self = this;
        //     var oModel = new JSONModel();
        //     oModel.loadData(QService + dataProdMD + "Shifts/addShiftSQ" + query);
        //     oModel.attachRequestCompleted(function () {
        //         var dataResponse = oModel.oData.Rowsets;
        //         self.doRequest();
        //     });
        // },

        addReparto: function (query) {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Departments/addDepartmentsXQ" + query);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Aggiunta", "Success", "Reparto aggiunto correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante l'aggiunta del reparto");
                self.doRequest();
            });
        },

        addLinea: function (query) {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Lines/addLinesXQ" + query);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Aggiunta", "Success", "Linea aggiunta correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante l'aggiunta della linea");
                self.doRequest();
            });
        },

        updatePlants: function (query) {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Plants/updPlantXQ" + query);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Aggiornata", "Success", "Divisione aggiornata correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante l'aggiornamento della divisione");
                self.doRequest();
            });
        },

        updateReparto: function (query) {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Departments/updDepartmentXQ" + query);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Aggiornamento", "Success", "Reparto aggiornato correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante l'aggiornamento del reparto");
                self.doRequest();
            });
        },

        updateLinea: function (query) {
            var self = this;
            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdMD + "Lines/saveLinesXQ" + query);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Aggiornamento", "Success", "Linea aggiornata correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante l'aggiornamento della linea");
                self.doRequest();
            });
        },


        columnsToExport: function (table) {
            var columnsToExport = [];
            switch (table) {
                case "Divisioni": {
                    for (var i = 0; i < this.divisioniField.length; i++) {
                        columnsToExport.push({
                            label: this.divisioniNames[i],
                            property: this.divisioniField[i],
                        });
                    }
                    break;
                }
                case "Reparti": {
                    for (var i = 0; i < this.repartiField.length; i++) {
                        columnsToExport.push({
                            label: this.repartiNames[i],
                            property: this.repartiField[i],
                        });
                    }
                    break;
                }
                case "Linee": {
                    for (var i = 0; i < this.lineeField.length; i++) {
                        columnsToExport.push({
                            label: this.lineeNames[i],
                            property: this.lineeField[i],
                        });
                    }
                    break;
                }

            }
            return columnsToExport;
        },

        exportxls: function () {
            var oModel = new JSONModel();
            var aCols;
            var view = this.getView();
            var table = [
                view.byId("tblDivisioni").getVisible(),
                // view.byId("tblTurni").getVisible(), TABELLA DA ELIMINARE
                view.byId("tblReparti").getVisible(),
                view.byId("tblLinee").getVisible(),
            ];
            if (table[0] && this.divisioniResponse !== null) {
                aCols = this.columnsToExport("Divisioni");
                oModel.setData(this.divisioniResponse);
                this.exportTable(oModel, aCols)
            } else if (table[1] && this.repartiResponse !== null) {
                aCols = this.columnsToExport("Reparti");
                oModel.setData(this.repartiResponse);
                this.exportTable(oModel, aCols)
            } else if (table[2] && this.lineeResponse !== null) {
                aCols = this.columnsToExport("Linee");
                oModel.setData(this.lineeResponse);
                this.exportTable(oModel, aCols)
            }
        }
    })
});