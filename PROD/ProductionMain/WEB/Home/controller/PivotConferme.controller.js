sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.PivotConferme", {
        bRender : false,
        
        dtFormat : sap.ui.core.format.DateFormat.getDateInstance({
            pattern: "yyyyMMdd"
        }),
        dtSelect : sap.ui.core.format.DateFormat.getDateInstance({
            pattern: "yyyy-MM-dd"
        }),
        
        dateSelFrom : "",
        dateSelTo : "",
        
        oLng_Analysis : jQuery.sap.resources({
            url: "/XMII/CM/ProductionMain/" + "PivotAnalysis/" + "res/Analysis_res.i18n.properties",
            locale: sLanguage
        }),
        
        onInit: function () {
            var self = this;
            var view = this.getView();
            view.addEventDelegate({
                onAfterShow: function (oEvent) {
                    if(self.bRender == false){
                        self.bRender = true;
                        self.pivot();
                    }
                    self.renderTable();
                }
            }, view);
        },
        
        pivot: function () {
            var yesterday = new Date();
            yesterday.setDate( yesterday.getDate() - 1 );
            var ToDay = new Date();
            
            var  year = yesterday.getUTCFullYear();
            var  month = yesterday.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            var  day = yesterday.getUTCDate();
            if( day.toString().length == 1 ) day = "0" + day;
            dateSelFrom = year + "-" + month+ "-" + day;
            var dOrigDateSelFtrom = dateSelFrom;
            
            var fromday = new Date(yesterday.getFullYear(), yesterday.getMonth() + 1, 0);
            year = ToDay.getUTCFullYear();
            month = ToDay.getUTCMonth() + 1;
            if( month.toString().length == 1 ) month = "0" + month;
            day = ToDay.getUTCDate();            
            dateSelTo = year + "-" + month+ "-" + day;
            var dOrigDateSelTo = dateSelTo;
            
            var odtFrom = new sap.ui.commons.DatePicker('dtFromC');
            odtFrom.setYyyymmdd( dateSelFrom );
            odtFrom.setLocale("it"); 
            odtFrom.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelFrom = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                }
            );
            
            var odtTo = new sap.ui.commons.DatePicker('dtToC');
            odtTo.setYyyymmdd( dateSelTo );
            odtTo.setLocale("it"); 
            odtTo.attachChange(
                function(oEvent){
                    if(oEvent.getParameter("invalidValue")){
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
                    }
                    else{
                        oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
                    }
                    var selDate = this.getYyyymmdd();
                    dateSelTo = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
                }
            );
            
            var self = this;
            var oToolbar1 = new sap.ui.commons.Toolbar("tb1C",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
                items: [
                    new sap.ui.commons.Label("lFromC",{
                        text : oLng_Analysis.getText("Analysis_FromDate"), 
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") 
                    }),
                    odtFrom,
                    new sap.ui.commons.Label("lToC",{
                        text : oLng_Analysis.getText("Analysis_ToDate"), 
                        tooltip : oLng_Analysis.getText("Analysis_SelectDateRange") 
                    }),
                    odtTo,
                    new sap.ui.commons.Button("btnRefreshC",{
                        icon: "sap-icon://refresh",
                        text : oLng_Analysis.getText("Analysis_Refresh"), 
                        tooltip : oLng_Analysis.getText("Analysis_ResultRefresh"),
                        press: function() {
                            self.renderTable();
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button("btnXlsC",{
                        icon: "sap-icon://excel-attachment",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToExcel"), 
                        press: function() {
                            exportTo("xls");
                        }
                    }),
                    
                    /**
                    new sap.ui.commons.Button("btnDoc",{
                        icon: "sap-icon://doc-attachment",
                        tooltip : oLng_Analysis.getText("Analysis_ExportToWord"), 
                        press: function() {
                            exportTo("doc");
                        }
                    }),
                    */
                    
                    /**
                    new sap.ui.commons.Button("pdf",{
                        icon: "sap-icon://pdf-attachment",
                        tooltip : oLng_Analysis.getText("Analysis_PrintPdf"), 
                        press: function() {
                            exportTo("pdf");
                        }
                    })
                    */
                ]
            });
            
            /**
            var self = this;
            var oToolbar3 = new sap.ui.commons.Toolbar("tb3",{
                width: "100%",
                design: sap.ui.commons.ToolbarDesign.Standard,
               
                rightItems: [
                    
                    new sap.ui.commons.Button("btnRefresh",{
                        icon: "sap-icon://refresh",
                        text : oLng_Analysis.getText("Analysis_Refresh"), 
                        tooltip : oLng_Analysis.getText("Analysis_ResultRefresh"),
                        press: function() {
                            self.renderTable();
                        }
                    })
                ]
            });
            */
            
            var oVLayout = new sap.ui.commons.layout.VerticalLayout({
                width: "100%",
                content: [
                    oToolbar1,
                    //oToolbar3
                ]
            });        
            
            oVLayout.placeAt(this.getView().byId("ToolbarC"));    
            
            //this.renderTable();
        },
        
        renderTable: function () {
            
            var dateFormat = $.pivotUtilities.derivers.dateFormat;
            var sortAs = $.pivotUtilities.sortAs;

            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var p1 = oDateFormatter.format( this.dtSelect.parse($.UIbyID("dtFromC").getYyyymmdd()) );
            var p2 = oDateFormatter.format( this.dtSelect.parse($.UIbyID("dtToC").getYyyymmdd()) );
            
            var itm = {};
            
            const sDateStart = oLng_Analysis.getText("Analysis_DateStart"); 
            itm[sDateStart] = function(record) {return record.ZZDATETIMESTR};
            
            const sLine = oLng_Analysis.getText("Analysis_Line"); 
            itm[sLine] = function(record) {return record.ZZLINEIDENTITY};
            
            const sMaterial = oLng_Analysis.getText("Analysis_Material"); 
            itm[sMaterial] = function(record) {return record.ZZMATERIAL};
             
            const sMaterialDesc = oLng_Analysis.getText("Analysis_MaterialDesc"); 
            itm[sMaterialDesc] = function(record) {return record.ZZMATDESCRIPTION};
            
            const sOrder = oLng_Analysis.getText("Analysis_Order"); 
            itm[sOrder] = function(record) {return record.ZZORDER};
           
            const sOperation = oLng_Analysis.getText("Analysis_Operation"); 
            itm[sOperation] = function(record) {return record.ZZOPERATION};
          
            const sDateEnd = oLng_Analysis.getText("Analysis_DateEnd"); 
            itm[sDateEnd] = function(record) {return record.ZZDATETIMELST};
            
            const sOdpQty = oLng_Analysis.getText("Analysis_OdpQty"); 
            itm[sOdpQty] = function(record) {return record.ZZORDERQTY};
            
            const sQtyGood = oLng_Analysis.getText("Analysis_QtyGood"); 
            itm[sQtyGood] = function(record) {return record.ZZGOODQTY};
            
            const sScrapQty = oLng_Analysis.getText("Analysis_ScrapQty"); 
            itm[sScrapQty] = function(record) {return record.ZZSCRAPQTY};
            
            const sLoadUnit = oLng_Analysis.getText("Analysis_LoadUnit"); 
            itm[sLoadUnit] = function(record) {return record.ZZLOADUNIT};
            
            const sOperatorId = oLng_Analysis.getText("Analysis_OperatorId"); 
            itm[sOperatorId] = function(record) {return record.ZZOPERATORID};
            
            const sFirst = oLng_Analysis.getText("Analysis_First"); 
            itm[sFirst] = function(record) {return record.ZZFIRST};
            
            const sNrCollaudo = oLng_Analysis.getText("Analysis_NrCollaudo"); 
            itm[sNrCollaudo] = function(record) {return record.ZZNRCOLLAUDO};
            
            const sCycleTime = oLng_Analysis.getText("Analysis_CycleTime"); 
            itm[sCycleTime] = function(record) {return record.ZZTEMPOCICLO};
            
            const sDateStart_no_time = oLng_Analysis.getText("Analysis_DateStart_no_time"); 
            itm[sDateStart_no_time] = function(record) {return record.DATA_START};
            
            const sLastLoadDate = oLng_Analysis.getText("Analysis_LastLoadDate"); 
            itm[sLastLoadDate] = function(record) {return record.DATA_ULTIMO_VERSAMENTO};
            
            var aColumns = [];
            
            var PivotPar = {
                PlaceAt: ".pivotC", 
                Query: {
                    Name: "getProductionConfirmFromMIIQR",
                    Path: QPrefix + "ProductionMain/Report/Pivot/",
                    Params: "&Param.1=" + p1 + "&Param.2=" + p2
                },
                Pivot: {
                    Rows: [], 
                    Cols: aColumns,
                    Vals: [], 
                    HiddenAttributes:[
                         "MANDANTE"
                        ,"ZZDATETIMESTR"
                        ,"ZZLINEIDENTITY"
                        ,"ZZMATERIAL"
                        ,"ZZMATDESCRIPTION"
                        ,"ZZORDER"
                        ,"ZZOPERATION"
                        ,"ZZDATETIMELST"
                        ,"ZZORDERQTY"
                        ,"ZZGOODQTY"
                        ,"ZZSCRAPQTY"
                        ,"ZZLOADUNIT"
                        ,"ZZOPERATORID"
                        ,"ZZFIRST"
                        ,"ZZNRCOLLAUDO"
                        ,"ZZTEMPOCICLO"
                        ,"DATA_START",
                        ,"DATA_ULTIMO_VERSAMENTO"
                    ],
            
                    DerivedAttributes: itm
                }
            };
     
            renderTablePivot(PivotPar);
        }
    })
});