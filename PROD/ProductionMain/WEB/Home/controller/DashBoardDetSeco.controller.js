var QService = "/XMII/Illuminator?";
var dataDBS = "Content-Type=text/json&QueryTemplate=ProductionMain/";

var plant, idLine = null;


sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.DashBoardDetSeco", {
        onInit: function () {
            this.getRouter().getRoute("DashBoardDetSeco").attachMatched(this._onRouteMatched, this);

            window.ranges = [];
            window.renderDaySegments = this.renderDaySegments.bind(this);
            window.onresize = function () {
                window.renderDaySegments();
            }
        },

        _onRouteMatched: function (oEvent) {
            plant = oEvent.getParameter("arguments").plant;
            idLine = oEvent.getParameter("arguments").idline;
            //alert("dett: " + idLine);

            var view = this.getView();
            var query1 = "Param.1=" + plant;
            query1 += "&Param.3=" + idLine;

            plant = null;

            // document.getElementsByClassName("day-chart")[0].innerHTML = "";
            this.getView().byId("headerLayout").setVisible(false);
            this.getView().byId("bulletMicro").setVisible(false);
            this.getView().byId("radialMicroCharts").setVisible(false);
            this.getView().byId("tblDataset").setVisible(false);

            var oModel1 = new JSONModel();
            oModel1.loadData(QService + dataDBS + "DashBoard/getDashboardSecoQR&" + query1);
            oModel1.attachRequestCompleted(function () {
                if (oModel1.oData.Rowsets.Rowset[0].Row != undefined) {
                    var response = oModel1.oData.Rowsets.Rowset[0].Row[0];
                    response.OEE = parseFloat(response.OEE);
                    response.OEED = parseFloat(response.OEED);
                    response.OEEP = parseFloat(response.OEEP);
                    response.OEEQ = parseFloat(response.OEEQ);
                    var oModelView1 = new JSONModel(response);
                    view.setModel(oModelView1, "model_oee");

                    view.byId("headerLayout").setVisible(true);
                    view.byId("bulletMicro").setVisible(true);
                    view.byId("radialMicroCharts").setVisible(true);
                }
            });


            var query2 = "Param.1=" + idLine;
            var oModel2 = new JSONModel();
            oModel2.loadData(QService + dataDBS + "DashBoard/getLineOrderListOnPeriodQR&" + query2);
            oModel2.attachRequestCompleted(function () {
                var response = [];

                if (oModel2.oData.Rowsets.Rowset[0].Row != undefined) {
                    response = oModel2.oData.Rowsets.Rowset[0].Row;

                    for (var i = 0; i < response.length; i++) {
                        if (parseFloat(response[i].ZZOEE) < parseFloat(response[i].ZZPERCOEERED)) {
                            response[i].OEE_COLOR = "Error";
                        } else if (parseFloat(response[i].ZZOEE) > parseFloat(response[i].ZZPERCOEEGREEN)) {
                            response[i].OEE_COLOR = "Success";
                        } else {
                            response[i].OEE_COLOR = "Warning";
                        }

                        if (parseFloat(response[i].ZZOEED) < parseFloat(response[i].ZZPERCOEERED)) {
                            response[i].OEED_COLOR = "Error";
                        } else if (parseFloat(response[i].ZZOEED) > parseFloat(response[i].ZZPERCOEEGREEN)) {
                            response[i].OEED_COLOR = "Success";
                        } else {
                            response[i].OEED_COLOR = "Warning";
                        }

                        if (parseFloat(response[i].ZZOEEP) < parseFloat(response[i].ZZPERCOEERED)) {
                            response[i].OEEP_COLOR = "Error";
                        } else if (parseFloat(response[i].ZZOEEP) > parseFloat(response[i].ZZPERCOEEGREEN)) {
                            response[i].OEEP_COLOR = "Success";
                        } else {
                            response[i].OEEP_COLOR = "Warning";
                        }

                        if (parseFloat(response[i].ZZOEEQ) < parseFloat(response[i].ZZPERCOEERED)) {
                            response[i].OEEQ_COLOR = "Error";
                        } else if (parseFloat(response[i].ZZOEEQ) > parseFloat(response[i].ZZPERCOEEGREEN)) {
                            response[i].OEEQ_COLOR = "Success";
                        } else {
                            response[i].OEEQ_COLOR = "Warning";
                        }
                    }
                    var oModelTable = new JSONModel();
                    oModelTable.setData(response);
                    view.byId("tblDataset").setModel(oModelTable);
                    if (response.length < 8) {
                        view.byId("tblDataset").setVisibleRowCount(response.length);
                    } else {
                        view.byId("tblDataset").setVisibleRowCount(8);
                    }
                    view.byId("tblDataset").setVisible(true);
                }
            });

            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var self = this;
            var query3 = "Param.1=" + idLine;
            var today = new Date();
            var tomorrow = self.addDays(today, 1);
            
            query3 += "&Param.2=" + oDateFormatter.format(today); 
            query3 += "&Param.3=" + oDateFormatter.format(tomorrow); 
            var oModel3 = new JSONModel();
            oModel3.loadData(QService + dataDBS + "Report/StatiLinea/GetLineStateAnalisysQR&" + query3);
            oModel3.attachRequestCompleted(function () {
                var rowsets = oModel3.oData.Rowsets.Rowset;
                var events = [];
                var ranges = {};
                var reasons = [];
                if (typeof rowsets[2] !== "undefined" && typeof rowsets[2].Row !== "undefined") {
                    events = rowsets[2].Row
                } else {
                    return;
                }
                if (typeof rowsets[3] !== "undefined" && typeof rowsets[3].Row !== "undefined") {
                    reasons = rowsets[3].Row
                } else {
                    return;
                }


                var mapReasonColors = {};
                //var palette = ['#940000','#C30101', '#560D0D', '#6F0000', '#EA0909', '#F03939', '#F26161'];
                var palette = ['#C30101', '#C30101', '#C30101', '#C30101', '#C30101', '#C30101', '#C30101'];
                for (var i = 0; i < reasons.length; i++) {
                    mapReasonColors[reasons[i].Evento] = palette[i];
                }
                //order
                events.sort(function (a, b) {
                    return moment(a.DataEvento + " " + a.OraInizio).format("YYMMDDHHmmss") - moment(b.DataEvento + " " + b.OraInizio).format("YYMMDDHHmmss");
                });

                //normalize ranges by date
                for (var e = 0; e < events.length; e++) {
                    var a = events[e];
                    if (typeof ranges[a.DataEvento] === "undefined") {
                        ranges[a.DataEvento] = [];
                    }
                    ranges[a.DataEvento].push(a);
                }

                //DAY SEGMENTS
                window.ranges = ranges;
                self.mapReasonColors = mapReasonColors;
                self.renderDaySegments();
            });
        },


        pad: function (number) {
            number = parseInt(number);
            if (number < 10) {
                return "0" + number;
            } else {
                return number;
            }
        },
        
        addDays: function(date, days) {
            //var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        },
                                 
        renderDaySegments: function () {
            //Day container width
            var containerWidth = $(".sapUiBody").width() - 150;
            var ranges = window.ranges;
            var mapReasonColors = this.mapReasonColors;
            var htmlDays = "";
            if (window.ranges.length == 0) return;
            document.getElementsByClassName("day-chart")[0].innerHTML = "";

            //Legend
            htmlDays += "<div class='day-descriptor'></div><div class='day-segment' style='width:" + containerWidth + "px;background-color:white;box-sizing:border-box;border-bottom:1px solid black;margin-bottom:2px;display:inline-block;'>";
            for (var l = 0; l <= 24; l++) {
                var second = l * 3600;
                var offset = (containerWidth * second) / 86400;
                var offsetLabel = offset - 5;
                htmlDays += "<div style='position:absolute;height:5px;width:1px;box-sizing:border-box;border-left:1px solid black;top:0px;left:" + offset + "px' ></div>";
                if (l % 2 == 0 && l !== 24) {
                    htmlDays += "<span class='hour-description' style='left:" + offsetLabel + "px'>" + this.pad(l) + "</span>";
                }
            }
            htmlDays += "</div>";
            document.getElementsByClassName("day-chart")[0].innerHTML = htmlDays;

            //iterate days in order to create day segment
            for (var d in ranges) {
                var lastEnd = "000000";
                //iterate day ranges
                var daySegment = "<div  class='day-descriptor'>" + d + "</div><div class='day-segment' style='display:inline-block;width:" + containerWidth + "px'>";
                for (var r = 0; r < ranges[d].length; r++) {
                    var offset = moment(ranges[d][r].OraInizio, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
                    var offsetFine = moment(ranges[d][r].OraFine, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
                    var rangeWidth = offsetFine - offset;
                    var color = ranges[d][r].EventType === "FERMO" ? mapReasonColors[ranges[d][r].Causale] : "#19A979";
                    ranges[d][r].offset = offset;
                    ranges[d][r].rangeWidth = rangeWidth;

                    //containerWidth : 86400 = x : seconds
                    ranges[d][r].left = (containerWidth * offset) / 86400;
                    ranges[d][r].width = (containerWidth * rangeWidth) / 86400;

                    daySegment += "<div class='day-event' data-toggle='tooltip' style='background-color:" + color + ";left:" + ranges[d][r].left + "px;width:" + ranges[d][r].width + "px'></div>";
                }
                daySegment += "</div>";
                document.getElementsByClassName("day-chart")[0].innerHTML += daySegment;
            }
        },
        
        exit: function () {
            document.getElementsByClassName("day-chart")[0].innerHTML = "";
            this.onNavBackToDashBoard();
        }
    })
});