var shadowcopyC = "";
var shadowcopyE = "";

var gChart = false;
var nf = $.pivotUtilities.numberFormat;

try {
    google.load("visualization", "1", {packages: ["corechart", "charteditor"]});
    gChart = true;
} catch (err) {
    if (window.console) console.log("errore caricamento Google Chart");
}


var frFmt = nf({
    thousandsSep: ".",
    decimalSep: ","
});
var frFmtInt = nf({
    digitsAfterDecimal: 0,
    thousandsSep: ".",
    decimalSep: ","
});
var frFmtPct = nf({
    digitsAfterDecimal: 1,
    scaler: 100,
    suffix: "%",
    thousandsSep: ".",
    decimalSep: ","
});

var tpl = $.pivotUtilities.aggregatorTemplates;
var _def_aggregators = {
    "Conteggio": tpl.count(frFmtInt),
    "Conteggio di valori univoci": tpl.countUnique(frFmtInt),
    "Lista di valori univoci": tpl.listUnique(", "),
    "Somma": tpl.sum(frFmt),
    "Somma di interi": tpl.sum(frFmtInt),
    "Media": tpl.average(frFmt),
    //"Cambio Formato": function() { return (tpl.countUnique(frFmtInt,["Formato"]) -1)},
    "Valore minimo": tpl.min(frFmt),
    "Valore massimo": tpl.max(frFmt),
    "Rapporto di somme": tpl.sumOverSum(frFmt),
    "80% del simite superiore": tpl.sumOverSumBound80(true, frFmt),
    "80% del simite inferiore": tpl.sumOverSumBound80(false, frFmt),
    "Frazione della somma totale": tpl.fractionOf(tpl.sum(), "total", frFmtPct),
    "Frazione della somma di riga": tpl.fractionOf(tpl.sum(), "row", frFmtPct),
    "Frazione della somma di colonna": tpl.fractionOf(tpl.sum(), "col", frFmtPct),
    "Conteggio della somma totale": tpl.fractionOf(tpl.count(), "total", frFmtPct),
    "Conteggio della somma di riga": tpl.fractionOf(tpl.count(), "row", frFmtPct),
    "Conteggio della somma di colonna": tpl.fractionOf(tpl.count(), "col", frFmtPct)
};


/**Effettua la visualizzazione della Pivot
 * @oParams   {Object}  Parametri chiamata
 * @oParams.PlaceAt   {String}  Selettore JQyery per il rendering
 * @oParams.Query.Name   {String}  Nome Query
 * @oParams.Query.Params   {String} Parametri query
 * @oParams.Query.Path   {String} Percorso Query
 * @oParams.Pivot.Renders   {Object} L'oggetto Renderd del pivot
 * @oParams.Pivot.Rows   {Object} L'oggetto rows del pivot
 * @oParams.Pivot.Cols   {Object} L'oggetto cols del pivot
 * @oParams.Pivot.Vals   {Object} L'oggetto vals del pivot
 * @oParams.Pivot.RenderName   {String} La proprietà RenderdName del pivot
 * @oParams.Pivot.AggregatorName   {String} La proprietà AggregatorName del pivot
 * @oParams.Pivot.HiddenAttributes   {Object} L'oggetto hiddenAttributes del pivot
 * @oParams.Pivot.Aggregators   {Object} L'oggetto aggregators del pivot
 * @oParams.Pivot.DerivedAttributes   {Object} L'oggetto derivedAttributes del pivot
 */
function renderTablePivot(oParams) {

    var ExeQuery = oParams.Query.Path + oParams.Query.Name + oParams.Query.Params;

    var jData = fnGetJsonPivot(ExeQuery, false);

    var numberFormat = $.pivotUtilities.numberFormat;
    var intFormat = numberFormat({digitsAfterDecimal: 0, thousandsSep: "."});
    var dateFormat = $.pivotUtilities.derivers.dateFormat;
    var sortAs = $.pivotUtilities.sortAs;

    var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.export_renderers_csv, $.pivotUtilities.d3_renderers, $.pivotUtilities.c3_renderers, $.pivotUtilities.gchart_renderers);
    var frFmt, frFmtInt, frFmtPct, nf;


    // Valori di default
    if (typeof (oParams.Pivot.Aggregators) == "undefined") oParams.Pivot.Aggregators = _def_aggregators;
    if (typeof (oParams.Pivot.Rows) == "undefined") oParams.Pivot.Rows = [];
    if (typeof (oParams.Pivot.Cols) == "undefined") oParams.Pivot.Cols = [];
    if (typeof (oParams.Pivot.Vals) == "undefined") oParams.Pivot.Vals = [];
    if (typeof (oParams.Pivot.RenderName) == "undefined") oParams.Pivot.RenderName = "Tabella";
    if (typeof (oParams.Pivot.AggregatorName) == "undefined") oParams.Pivot.AggregatorName = "Somma di interi";
    if (typeof (oParams.Pivot.HiddenAttributes) == "undefined") oParams.Pivot.HiddenAttributes = [];
    if (typeof (oParams.Pivot.DerivedAttributes) == "undefined") oParams.Pivot.DerivedAttributes = [];

    // se non viene passato il renders lo costruisce con i valori di default
    var pivotRenders = {};
    if (typeof (oParams.Renders) == "undefined") {
        if (oParams.gChart) {
            pivotRenders = {
                "Tabella": $.pivotUtilities.renderers["Table"],
                "Tabella a barre": $.pivotUtilities.renderers["Table Barchart"],
                "Mappa termica": $.pivotUtilities.renderers["Heatmap"],
                "Mappa termica per riga": $.pivotUtilities.renderers["Row Heatmap"],
                "Mappa termica per colonna": $.pivotUtilities.renderers["Col Heatmap"],
                "D3 Mappa ad albero": $.pivotUtilities.d3_renderers["Treemap"],
                "C3 Grafico Linea": $.pivotUtilities.c3_renderers["Line Chart"],
                "C3 Grafico Barre": $.pivotUtilities.c3_renderers["Bar Chart"],
                "C3 Grafico Stacked Bar": $.pivotUtilities.c3_renderers["Stacked Bar Chart"],
                "C3 Grafico Area": $.pivotUtilities.c3_renderers["Area Chart"],
                "Google Grafico Linea": $.pivotUtilities.gchart_renderers["Line Chart"],
                "Google Grafico Barre": $.pivotUtilities.gchart_renderers["Bar Chart"],
                "Google Grafico Stacked Bar": $.pivotUtilities.gchart_renderers["Stacked Bar Chart"],
                "Google Grafico Area": $.pivotUtilities.gchart_renderers["Area Chart"],
                //"Esporta CSV": $.pivotUtilities.export_renderers_csv["CSV Export"]
            };
        } else {
            pivotRenders = {
                "Tabella": $.pivotUtilities.renderers["Table"],
                "Tabella a barre": $.pivotUtilities.renderers["Table Barchart"],
                "Mappa termica": $.pivotUtilities.renderers["Heatmap"],
                "Mappa termica per riga": $.pivotUtilities.renderers["Row Heatmap"],
                "Mappa termica per colonna": $.pivotUtilities.renderers["Col Heatmap"],
                "D3 Mappa ad albero": $.pivotUtilities.d3_renderers["Treemap"],
                "C3 Grafico Linea": $.pivotUtilities.c3_renderers["Line Chart"],
                "C3 Grafico Barre": $.pivotUtilities.c3_renderers["Bar Chart"],
                "C3 Grafico Stacked Bar": $.pivotUtilities.c3_renderers["Stacked Bar Chart"],
                "C3 Grafico Area": $.pivotUtilities.c3_renderers["Area Chart"],
                //"Esporta CSV": $.pivotUtilities.export_renderers_csv["CSV Export"],
            };
        }
    } else {
        pivotRenders = oParams.Renders;
    }

    
    var row_copy = [];
    var col_copy = [];
    var val_copy = [];
    var aggr_copy = "";
    var rend_copy = "";
    row_copy = jData;
    val_copy = jData;
    // row_copy = oParams.Pivot.Rows;
    col_copy = oParams.Pivot.Cols;
    // val_copy = oParams.Pivot.Vals;
    aggr_copy = oParams.Pivot.AggregatorName; //"Somma di interi";
    rend_copy = oParams.Pivot.RenderName; // "Tabella";

    if (oParams.PlaceAt == '.pivotC' && shadowcopyC != "") {
        var copy_conf = JSON.parse(shadowcopyC);
        row_copy = copy_conf["rows"];
        col_copy = copy_conf["cols"];
        val_copy = copy_conf["vals"];
        aggr_copy = copy_conf["aggregatorName"];
        rend_copy = copy_conf["rendererName"];
    }
	
	if (oParams.PlaceAt == '.pivotE' && shadowcopyE != "") {
        var copy_conf = JSON.parse(shadowcopyE);
        row_copy = copy_conf["rows"];
        col_copy = copy_conf["cols"];
        val_copy = copy_conf["vals"];
        aggr_copy = copy_conf["aggregatorName"];
        rend_copy = copy_conf["rendererName"];
    }
$(oParams.PlaceAt).html("");


    $(oParams.PlaceAt).pivotUI(jData, {
//		renderers: renderers,
        rows: row_copy,
        cols: col_copy,
        vals: val_copy,
        rendererName: rend_copy,
        aggregatorName: aggr_copy,
        hiddenAttributes: oParams.Pivot.HiddenAttributes,
        derivedAttributes: oParams.Pivot.DerivedAttributes,
        sorters: function (attr) {
            if (attr == "Nome mese") {
                //return sortAs(["Gennaio","Febbraio","Marzo","Aprile", "Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"]);
                return sortAs(["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"]);
            }
            if (attr == "Nome mese est.") {
                return sortAs(["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]);
            }
            if (attr == "Nome giorno") {
                return sortAs(["Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom"]);
            }
        },
        localeStrings: {
            renderError: "Si sono verificati errori nella visualizzazione;.",
            computeError: "Si sono verificati errori nel calcolo.",
            uiRenderError: "Si sono verificati errori nella generazione dell'interfaccia dinamica",
            selectAll: "Seleziona tutto",
            selectNone: "Deseleziona tutto",
            tooMany: "(troppi valori)",
            filterResults: "Filtro sui risultati",
            totals: "Totale",
            vs: "su",
            by: "per"
        },
        aggregators: oParams.Pivot.Aggregators,
        renderers: pivotRenders,
        onRefresh: function (config) {
            var config_copy = JSON.parse(JSON.stringify(config));
            delete config_copy["aggregators"];
            delete config_copy["renderers"];
            delete config_copy["derivedAttributes"];
            delete config_copy["rendererOptions"];
            delete config_copy["localeStrings"];
            
			if(oParams.PlaceAt == '.pivotE')
				shadowcopyE = JSON.stringify(config_copy, undefined, 2);
			
			if(oParams.PlaceAt == '.pivotC')
				shadowcopyC = JSON.stringify(config_copy, undefined, 2);
        }
    }, true);
}

function renderReportPivot(oParams) {

    var ExeQuery = oParams.Query.Path + oParams.Query.Name + oParams.Query.Params;

    var jData = fnGetJsonPivot(ExeQuery, false);

    var numberFormat = $.pivotUtilities.numberFormat;
    var intFormat = numberFormat({digitsAfterDecimal: 0, thousandsSep: "."});
    var dateFormat = $.pivotUtilities.derivers.dateFormat;
    var sortAs = $.pivotUtilities.sortAs;

    var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.export_renderers_csv, $.pivotUtilities.d3_renderers, $.pivotUtilities.c3_renderers, $.pivotUtilities.gchart_renderers);
    var frFmt, frFmtInt, frFmtPct, nf;


    // Valori di default
    if (typeof (oParams.Pivot.Aggregators) == "undefined") oParams.Pivot.Aggregators = _def_aggregators;
    if (typeof (oParams.Pivot.Rows) == "undefined") oParams.Pivot.Rows = [];
    if (typeof (oParams.Pivot.Cols) == "undefined") oParams.Pivot.Cols = [];
    if (typeof (oParams.Pivot.Vals) == "undefined") oParams.Pivot.Vals = [];
    if (typeof (oParams.Pivot.RenderName) == "undefined") oParams.Pivot.RenderName = "Tabella";
    if (typeof (oParams.Pivot.AggregatorName) == "undefined") oParams.Pivot.AggregatorName = "Somma di interi";
    if (typeof (oParams.Pivot.HiddenAttributes) == "undefined") oParams.Pivot.HiddenAttributes = [];
    if (typeof (oParams.Pivot.DerivedAttributes) == "undefined") oParams.Pivot.DerivedAttributes = [];

    // se non viene passato il renders lo costruisce con i valori di default
    var pivotRenders = {};
    if (typeof (oParams.Renders) == "undefined") {
        if (oParams.gChart) {
            pivotRenders = {
                "Tabella": $.pivotUtilities.renderers["Table"],
                "Mappa termica per colonna": $.pivotUtilities.renderers["Col Heatmap"],
                "C3 Grafico Stacked Bar": $.pivotUtilities.c3_renderers["Stacked Bar Chart"],
            };
        } else {
            pivotRenders = {
                "Tabella": $.pivotUtilities.renderers["Table"],
                "Mappa termica per colonna": $.pivotUtilities.renderers["Col Heatmap"],
                "C3 Grafico Stacked Bar": $.pivotUtilities.c3_renderers["Stacked Bar Chart"],
            };
        }
    } else {
        pivotRenders = oParams.Renders;
    }

    var row_copy = [];
    var col_copy = [];
    var val_copy = [];
    var aggr_copy = "";
    var rend_copy = "";
    row_copy = oParams.Pivot.Rows;
    col_copy = oParams.Pivot.Cols;
    val_copy = oParams.Pivot.Vals;
    aggr_copy = oParams.Pivot.AggregatorName; //"Somma di interi";
    rend_copy = oParams.Pivot.RenderName; // "Tabella";

    if (oParams.PlaceAt == 'pivotC' && shadowcopyC != "") {
        var copy_conf = JSON.parse(shadowcopyC);
        row_copy = copy_conf["rows"];
        col_copy = copy_conf["cols"];
        val_copy = copy_conf["vals"];
        aggr_copy = copy_conf["aggregatorName"];
        rend_copy = copy_conf["rendererName"];
    }
	
	if (oParams.PlaceAt == 'pivotE' && shadowcopyE != "") {
        var copy_conf = JSON.parse(shadowcopyE);
        row_copy = copy_conf["rows"];
        col_copy = copy_conf["cols"];
        val_copy = copy_conf["vals"];
        aggr_copy = copy_conf["aggregatorName"];
        rend_copy = copy_conf["rendererName"];
    }

$(oParams.PlaceAt).html("");

    $(oParams.PlaceAt).pivot(jData, {
        //		renderers: renderers,
        rows: row_copy,
        cols: col_copy,
        vals: val_copy,
        rendererName: rend_copy,
        aggregatorName: aggr_copy,
        hiddenAttributes: oParams.Pivot.HiddenAttributes,
        derivedAttributes: oParams.Pivot.DerivedAttributes,
        sorters: function (attr) {
            if (attr == "Nome mese") {
                return sortAs(["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"]);
            }
            if (attr == "Nome mese est.") {
                return sortAs(["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]);
            }
            if (attr == "Nome giorno") {
                return sortAs(["Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom"]);
            }
        },
        localeStrings: {
            renderError: "Si sono verificati errori nella visualizzazione;.",
            computeError: "Si sono verificati errori nel calcolo.",
            uiRenderError: "Si sono verificati errori nella generazione dell'interfaccia dinamica",
            selectAll: "Seleziona tutto",
            selectNone: "Deseleziona tutto",
            tooMany: "(troppi valori)",
            filterResults: "Filtro sui risultati",
            totals: "Totale",
            vs: "su",
            by: "per"
        },
        aggregators: oParams.Pivot.Aggregators,
        renderers: pivotRenders,
        onRefresh: function (config) {
            var config_copy = JSON.parse(JSON.stringify(config));
            delete config_copy["aggregators"];
            delete config_copy["renderers"];
            delete config_copy["derivedAttributes"];
            delete config_copy["rendererOptions"];
            delete config_copy["localeStrings"];
            //shadowcopy = JSON.stringify(config_copy, undefined, 2);
			
			if(oParams.PlaceAt == '.pivotE')
				shadowcopyE = JSON.stringify(config_copy, undefined, 2);
			
			if(oParams.PlaceAt == '.pivotC')
				shadowcopyC = JSON.stringify(config_copy, undefined, 2);
        }
    }, true);
}

function exportTo(toType, Title) {

    if (typeof (Title) == "undefined") Title = $(document).find("title").text();
    var base64data = "";

    switch (toType) {
        case "pdf":
            html2canvas($('.pvtTable'), {
                onrendered: function (canvas) {
                    //var imgData = canvas.toDataURL("image/png");//.replace("image/png", "image/octet-stream");
                    var doc = new jsPDF('p', 'pt', 'A4');

                    doc.setFontSize(30);
                    doc.text(50, 30, 'Analisi consuntivi');
                    doc.addImage(canvas, 'PNG', 15, 40);
                    doc.output('dataurlnewwindow');

                }
            });
            /*var doc = new jsPDF('p', 'pt', 'A4');

            doc.setFontSize(30);
            doc.text(50, 30, 'Analisi consuntivi');

            doc.addHTML($('pvtTable')[0], function () {
                doc.output('dataurlnewwindow');});*/
            break;
        case "doc":
            base64data = "base64," + $.base64.encode(ToOffice(toType, Title));
            window.open('data:application/vnd.ms-doc;filename=exportData.doc;' + base64data);
            break;
        case "xls":
            base64data = "base64," + $.base64.encode(ToOffice(toType, Title));
            window.open('data:application/vnd.ms-excel;filename=exportData.xls;' + base64data);
            break;
    }
}

function parseString(data) {
    content_data = data.text().trim();
    return content_data;
}

function ToOffice(toType, Title) {

    var OffType = "excel";
    switch (toType) {
        case "xls":
            OffType = "excel";
            break;
        case "doc":
            OffType = "doc";
            break;
    }

    var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:" + OffType + "' xmlns='http://www.w3.org/TR/REC-html40'>";
    excelFile += "<head>";
    excelFile += "<!--[if gte mso 9]>";
    excelFile += "<xml>";
    excelFile += "<x:ExcelWorkbook>";
    excelFile += "<x:ExcelWorksheets>";
    excelFile += "<x:ExcelWorksheet>";
    excelFile += "<x:Name>";
    excelFile += Title;
    excelFile += "</x:Name>";
    excelFile += "<x:WorksheetOptions>";
    excelFile += "<x:DisplayGridlines/>";
    excelFile += "</x:WorksheetOptions>";
    excelFile += "</x:ExcelWorksheet>";
    excelFile += "</x:ExcelWorksheets>";
    excelFile += "</x:ExcelWorkbook>";
    excelFile += "</xml>";
    excelFile += "<![endif]-->";
    excelFile += "</head>";
    excelFile += "<body><table>";

    //excelFile += $('.pvtTable').html();

    $('.pvtTable').find('tr').each(function () {
        excelFile += "<tr>";
        $(this).find('th').each(function (index, data) {

            excelFile += "<td";
            if ($(this).attr('colspan') != undefined) {
                excelFile += " colspan=" + $(this).attr('colspan');
            }
            if ($(this).attr('rowspan') != undefined) {
                excelFile += " rowspan=" + $(this).attr('rowspan');
            }
            excelFile += ">";
            excelFile += parseString($(this)) + "</td>";

        });

        $(this).find('td').each(function (index, data) {

            excelFile += "<td";
            if ($(this).attr('colspan') != undefined) {
                excelFile += " colspan=" + $(this).attr('colspan');
            }
            if ($(this).attr('rowspan') != undefined) {
                excelFile += " rowspan=" + $(this).attr('rowspan');
            }
            excelFile += ">";
            excelFile += parseString($(this)) + "</td>";

        });
        excelFile += '</tr>';
    });
    excelFile += "</table></body>";
    excelFile += "</html>";
    return excelFile;
}