var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/json&QueryTemplate=ProductionMain/MasterData/";
var dataProdRPT = "Content-Type=text/json&QueryTemplate=ProductionMain/Report/";

var columnsName = {
    "ColumnsField": [
        "DataEvento",
        "OraInizio",
        "OraFine",
        "Durata",
        "EventType",
        "Causale"
    ],
    "ColumnsName": [
        "Data Evento",
        "Ora Inizio",
        "Ora Fine",
        "Durata",
        "Tipo Evento",
        "Causale"
    ]
};

sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/xml/XMLModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/DateFormat",
    "sap/m/MessageBox"
], function (BaseController, XmlModel, JSONModel, DateFormat, MessageBox) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.AnalisiStatiLinea", {
        pathToExport1: null,
        palette: ['#940000', '#C30101', '#560D0D', '#6F0000', '#EA0909', '#F03939', '#F26161'],

        getViewModel: function (modelName, query) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var oModel = new JSONModel();
                oModel.loadData(query);
                oModel.attachRequestCompleted(function () {
                    var rows = oModel.oData.Rowsets.Rowset[0].Row;
                    if (typeof rows === "undefined") {
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    } else {
                        var data = {};
                        data[modelName] = typeof rows.length !== "undefined" ? rows : [rows];
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    }
                }.bind(self));
            })
        },

        initViewModels: function () {
            var self = this;
            var view = this.getView();
            this.getPlants().then(function () {
                this.getView().byId("plants").setSelectedKey(this.getView().getModel("PLANTS").oData.PLANTS[0].PLANT);
                this.getDepartments().then(function () {
                    view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);

                    // getLines();
                    new Promise(function (resolve, reject) {
                        var oModel = new JSONModel();
                        oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                        oModel.attachRequestCompleted(function () {
                            var rows = oModel.oData.Rowsets.Rowset[0].Row;
                            if (typeof rows === "undefined") {
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            } else {
                                var data = {};
                                data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            }
                            view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                        }.bind(self));
                    });
                });

            }.bind(this));
        },

        getPlants: function () {
            return this.getViewModel("PLANTS", QService + dataProdMD + "Plants/getPlantsByUserQR");
        },

        getDepartments: function () {
            return this.getViewModel("DEPARTMENTS", QService + dataProdMD + "Departments/getDepartmentsByPlantSQ&Param.1=" + this.getView().byId("plants").getSelectedKey());
        },

        getLines: function () {
            return this.getViewModel("LINES", QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + this.getView().byId("departments").getSelectedKey());
        },

        onPlantChanged: function () {
            var view = this.getView();
            this.getDepartments().then(function () {
                view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);
                new Promise(function (resolve, reject) {
                    var oModel = new JSONModel();
                    oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                    oModel.attachRequestCompleted(function () {
                        var rows = oModel.oData.Rowsets.Rowset[0].Row;
                        if (typeof rows === "undefined") {
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        } else {
                            var data = {};
                            data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        }
                        view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                    }.bind(self));
                });
            });
        },

        onRepartoChanged: function () {
            var view = this.getView();
            this.getLines().then(function () {
                view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
            });
        },

        getQueryParameters: function () {
            var params = [];
            var view = this.getView();
            var from = view.byId("TurnoDa").getDateValue();
            var to = view.byId("TurnoA").getDateValue();
            var line = view.byId("lines").getSelectedKey();
            if (from instanceof Date && to instanceof Date && line !== "") {
                params = [from, to, line];
            }
            return params;
        },

        formatQueryParameters: function (params) {
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var view = this.getView();
            var query = "";
            query += "Param.1=" + params[2];
            query += "&Param.2=" + oDateFormatter.format(params[0]);
            query += "&Param.3=" + oDateFormatter.format(params[1]);
            return query;
        },

        /**********************************
         * CONTROLLER LIFE-CYCLE
         **********************************/
        onInit: function () {
            var view = this.getView();
            var now = new Date();
            var past = new Date();
            past.setDate(past.getDate() - 7);
            this.initViewModels();

            //SET DATE RANGE DEFAULT
            view.byId("TurnoA").setDateValue(now);
            view.byId("TurnoDa").setDateValue(past);

            //var statuses = {"Statuses":[{"name":"STATO 1","time":"HH:mm:ss"},{"name":"STATO 2","time":"HH:mm:ss"}],"Percentages":[{"name":"STATO 1","percentage":60},{"name":"STATO 2","percentage":40}]};
            //var reasons = {"Reasons":[{"name":"Causale 1","time":"HH:mm:ss"},{"name":"Causale 2","time":"HH:mm:ss"},{"name":"Causale 3","time":"HH:mm:ss"},{"name":"Causale 4","time":"HH:mm:ss"},{"name":"Causale 5","time":"HH:mm:ss"}],"Percentages":[{"name":"Causale 1","percentage":20},{"name":"Causale 2","percentage":20},{"name":"Causale 3","percentage":10},{"name":"Causale 4","percentage":40},{"name":"Causale 5","percentage":10}],"Quantities":[{"name":"Causale 1","quantity":2},{"name":"Causale 2","quantity":2},{"name":"Causale 3","quantity":3},{"name":"Causale 4","quantity":6},{"name":"Causale 5","quantity":4}]};
            var palette = ['#940000', '#C30101', '#560D0D', '#6F0000', '#EA0909', '#F03939', '#F26161'];

            //this.getView().setModel(new JSONModel(statuses), "STATUSES");
            //this.getView().setModel(new JSONModel(reasons), "REASONS");

            var propertiesReas = {
                title: {visible: false},
                plotArea: {dataLabel: {visible: true}, colorPalette: palette}
            };
            view.byId("reasons-percentage").setVizProperties(propertiesReas);

            window.ranges = [];
            window.renderDaySegments = this.renderDaySegments.bind(this);
            window.onresize = function () {
                window.renderDaySegments();
            }

        },

        onTilePress: function () {
            return;
            //var oView = this.getView();
            //var oDialog = new Dialog(oView);
            //oDialog.open();
        },

        onCloseDialog: function () {

        },

        pad: function (number) {
            number = parseInt(number);
            if (number < 10) {
                return "0" + number;
            } else {
                return number;
            }
        },

        renderDaySegments: function () {
            //Day container width
            var containerWidth = $(".sapUiBody").width() - 150;
            var ranges = window.ranges;
            var mapReasonColors = this.mapReasonColors;
            var htmlDays = "";
            if (window.ranges.length == 0) return;
            document.getElementsByClassName("day-chart")[0].innerHTML = "";

            //Legend
            htmlDays += "<div class='day-descriptor'></div><div class='day-segment' style='width:" + containerWidth + "px;background-color:white;box-sizing:border-box;border-bottom:1px solid black;margin-bottom:2px;display:inline-block;'>";
            for (var l = 0; l <= 24; l++) {
                var second = l * 3600;
                var offset = (containerWidth * second) / 86400;
                var offsetLabel = offset - 5;
                htmlDays += "<div style='position:absolute;height:5px;width:1px;box-sizing:border-box;border-left:1px solid black;top:0px;left:" + offset + "px' ></div>";
                if (l % 2 == 0 && l !== 24) {
                    htmlDays += "<span class='hour-description' style='left:" + offsetLabel + "px'>" + this.pad(l) + "</span>";
                }
            }
            htmlDays += "</div>";
            document.getElementsByClassName("day-chart")[0].innerHTML = htmlDays;

            //iterate days in order to create day segment
            for (var d in ranges) {
                var lastEnd = "000000";
                //iterate day ranges
                var daySegment = "<div  class='day-descriptor'>" + d + "</div><div class='day-segment' style='display:inline-block;width:" + containerWidth + "px'>";
                for (var r = 0; r < ranges[d].length; r++) {
                    var offset = moment(ranges[d][r].OraInizio, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
                    var offsetFine = moment(ranges[d][r].OraFine, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
                    var rangeWidth = offsetFine - offset;
                    var color = ranges[d][r].EventType === "FERMO" ? mapReasonColors[ranges[d][r].Causale] : "#19A979";
                    ranges[d][r].offset = offset;
                    ranges[d][r].rangeWidth = rangeWidth;

                    //containerWidth : 86400 = x : seconds
                    ranges[d][r].left = (containerWidth * offset) / 86400;
                    ranges[d][r].width = (containerWidth * rangeWidth) / 86400;

                    daySegment += "<div class='day-event' data-toggle='tooltip' style='background-color:" + color + ";left:" + ranges[d][r].left + "px;width:" + ranges[d][r].width + "px'></div>";
                }
                daySegment += "</div>";
                document.getElementsByClassName("day-chart")[0].innerHTML += daySegment;
            }
        },

        onSearch: function () {
            var view = this.getView();
            var self = this;

            view.byId("tblDataset").setVisible(false);
            view.byId("statuses-percentage").setVisible(false);
            view.byId("reasons-percentage").setVisible(false);
            view.byId("reasons-quantity").setVisible(false);
            this.pathToExport1 = null;

            var oModel = new JSONModel();
            var params = this.getQueryParameters();
            if (params.length === 0) {
                MessageBox.warning("Inserire tutti i campi obbligatori: Linea, Inizio Periodo, Fine range.", {styleClass: "sapUiSizeCompact"});
                return;
            }
            var queryString = this.formatQueryParameters(params);
            this.getView().setModel(new JSONModel({}), "TOTAL");
            this.getView().setModel(new JSONModel({}), "DATASET");
            this.getView().setModel(new JSONModel({}), "STATUSES");
            this.getView().setModel(new JSONModel({}), "REASONS");
            this.getView().setModel(new JSONModel({}));
            document.getElementsByClassName("day-chart")[0].innerHTML = "";

            oModel.loadData(QService + dataProdRPT + "StatiLinea/GetLineStateAnalisysQR&" + queryString);
            oModel.attachRequestCompleted(function () {
                var rowsets = oModel.oData.Rowsets.Rowset;
                var events = [];
                var ranges = {};
                var reasons = [];
                var givenStatuses = [];
                if (typeof rowsets[2] !== "undefined" && typeof rowsets[2].Row !== "undefined") {
                    events = rowsets[2].Row
                } else {
                    return;
                }
                if (typeof rowsets[3] !== "undefined" && typeof rowsets[3].Row !== "undefined") {
                    reasons = rowsets[3].Row
                } else {
                    return;
                }
                if (typeof rowsets[4] !== "undefined" && typeof rowsets[4].Row !== "undefined") {
                    givenStatuses = rowsets[4].Row
                } else {
                    return;
                }

                var mapReasonColors = {};
                for (var i = 0; i < reasons.length; i++) {
                    mapReasonColors[reasons[i].Evento] = this.palette[i];
                }

                //STATUSES
                var statuses = {"Statuses": [], "Percentages": []};
                var statusPalette = [];
                var total = 0
                for (var s = 0; s < givenStatuses.length; s++) {
                    total += givenStatuses[s].DURATA;
                }

                this.getView().setModel(new JSONModel({
                    "total": moment().startOf("day").seconds(total).format("HH:mm:ss"),
                    "qta": rowsets[0].Row[0].TOT_QTY
                }), "TOTAL");

                var oModel2 = new JSONModel({
                    HTML: "<strong>" + moment().startOf("day").seconds(total).format("HH:mm:ss") + "</strong> ore analisi<br/>" + "<strong>" + rowsets[0].Row[0].TOT_QTY + "</strong> pz prodotti"
                });
                this.getView().setModel(oModel2);

                for (var s = 0; s < givenStatuses.length; s++) {
                    var hours = moment().startOf("day").seconds(givenStatuses[s].DURATA).format("HH:mm:ss");
                    var percentage = (givenStatuses[s].DURATA / total) * 100;
                    statuses.Statuses.push({name: givenStatuses[s].EVENTO, time: hours});
                    statuses.Percentages.push({name: givenStatuses[s].EVENTO, percentage: percentage.toFixed(2)});
                    if (givenStatuses[s].EVENTO === "FERMO") statusPalette.push('#BF0000');
                    else statusPalette.push('#19A979');
                }
                this.getView().setModel(new JSONModel(statuses), "STATUSES");

                var propertiesStat = {
                    title: {visible: false},
                    plotArea: {dataLabel: {visible: true}, colorPalette: statusPalette}
                };
                this.getView().byId("statuses-percentage").setVizProperties(propertiesStat);

                //order
                events.sort(function (a, b) {
                    return moment(a.DataEvento + " " + a.OraInizio).format("YYMMDDHHmmss") - moment(b.DataEvento + " " + b.OraInizio).format("YYMMDDHHmmss");
                });

                //normalize ranges by date
                for (var e = 0; e < events.length; e++) {
                    var a = events[e];
                    if (typeof ranges[a.DataEvento] === "undefined") {
                        ranges[a.DataEvento] = [];
                    }
                    ranges[a.DataEvento].push(a);
                }

                //REASONS
                var reasonsDataModel = {"Reasons": [], "Percentages": [], "Quantities": []};
                total = 0;
                for (var r = 0; r < reasons.length; r++) {
                    total += reasons[r].Durata;
                }
                for (var r = 0; r < reasons.length; r++) {
                    if (reasons[r].Evento === "PRODUZIONE") continue;
                    var durata = moment().startOf("day").seconds(reasons[r].Durata).format("HH:mm:ss");
                    var percentage = (reasons[r].Durata / total) * 100;
                    reasonsDataModel.Reasons.push({name: reasons[r].Evento, time: durata});
                    reasonsDataModel.Percentages.push({name: reasons[r].Evento, percentage: percentage.toFixed(2)});
                    reasonsDataModel.Quantities.push({name: reasons[r].Evento, quantity: reasons[r].Frequenza});
                }
                this.getView().setModel(new JSONModel(reasonsDataModel), "REASONS");
                var propertiesQuant = {title: {visible: false}, plotArea: {dataPointStyle: {rules: []}}};
                for (var c = 0; c < reasonsDataModel.Reasons.length; c++) {
                    propertiesQuant.plotArea.dataPointStyle.rules.push({
                        dataContext: [{"name": reasonsDataModel.Reasons[c].name}],
                        "properties": {"color": this.palette[c]}
                    });
                }
                this.getView().byId("reasons-quantity").setVizProperties(propertiesQuant);

                //DAY SEGMENTS
                window.ranges = ranges;
                self.mapReasonColors = mapReasonColors;
                self.renderDaySegments();


                //TABLE DATASET
                view.byId("tblDataset").setModel(new JSONModel(events));
                view.byId("tblDataset").setVisible(true);
                view.byId("statuses-percentage").setVisible(true);
                view.byId("reasons-percentage").setVisible(true);
                view.byId("reasons-quantity").setVisible(true);


                self.pathToExport1 = events;
                window.dataset = events;
                window.loadPageByElement = this.loadPageByElement.bind(this);
                this.loadPage(0);
                //Adjust Reasons Pie Chart
                // window.setTimeout(function () {
                //     $(".reasons-percentage .v-legend-element text").each(function (index, element) {
                //         if ($(element).find("title").length > 0) {
                //             $(element).find("tspan").html(($(element).find("title").html()));
                //         }
                //     });
                // }, 100);

            }.bind(this));
        },

        pageSize: 25,
        getPage: function (pageNumber) {
            var offset = this.pageSize * pageNumber;
            var end = offset + this.pageSize - 1;
            var limit = Math.min(end + 1, window.dataset.length);
            var win = [];
            for (var i = offset; i < limit; i++) {
                win.push(window.dataset[i]);
            }
            return win;
        },

        loadPage: function (pageNumber) {
            var rows = this.getPage(pageNumber);
            this.getView().byId("tblDataset").setModel(new JSONModel(rows));
            this.currentPage = pageNumber;
            this.renderPagination(); //rerender pagination
        },

        renderPagination: function () {
            var pages = Math.ceil(window.dataset.length / this.pageSize);
            document.getElementsByClassName("pagination")[0].innerHTML = "";
            for (var p = 0; p < pages; p++) {
                var index = p + 1;
                var html = "<div class='page-box " + (this.currentPage == p ? "current-page" : "") + "' data-page=" + p + " onclick='loadPageByElement(this)'>" + index + "</div>";
                document.getElementsByClassName("pagination")[0].innerHTML += html;
            }
        },

        loadPageByElement: function (element) {
            this.loadPage(element.dataset.page);
        },


        columnsToExport: function () {
            var columnsToExport = [];
            for (var i = 0; i < columnsName.ColumnsName.length; i++) {
                columnsToExport.push({
                    label: columnsName.ColumnsName[i],
                    property: columnsName.ColumnsField[i],
                });
            }
            return columnsToExport;
        },
        // EXPORT TABLE
        exportxls: function () {
            var aCols;
            if (this.pathToExport1 !== null) {
                aCols = this.columnsToExport();
                var oModelRowset1 = new JSONModel();
                oModelRowset1.setData(this.pathToExport1);
                this.exportTable(oModelRowset1, aCols)
            }
        }
    });
});