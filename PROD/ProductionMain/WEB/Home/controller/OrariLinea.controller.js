var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/json&QueryTemplate=ProductionMain/MasterData/";
var dataProd = "Content-Type=text/json&QueryTemplate=ProductionMain/";

sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.OrariLinea", {
        onInit: function () {
            this.setView();
        },
        
        setView: function () {
            var view = this.getView();
            var self = this;
            this.getLines().then(function () {
                view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].IDLINE);
                view.byId("Turno").setDateValue(new Date());
                self.getTimeValue();
            });
        },

        getLines: function () {
            return this.getViewModel("LINES", QService + dataProdMD + "Lines/getLinesSQ");
        },

        getViewModel: function (modelName, query) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var oModel = new JSONModel();
                oModel.loadData(query);
                oModel.attachRequestCompleted(function () {
                    var rows = oModel.oData.Rowsets.Rowset[0].Row;
                    if (typeof rows === "undefined") {
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    } else {
                        var data = {};
                        data[modelName] = typeof rows.length !== "undefined" ? rows : [rows];
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    }
                }.bind(self));
            })
        },

        setTimeValue: function (oEvent) {
            if (oEvent.oSource.mProperties.text === "Inizia Ora") {
                this.getView().byId("TP1").setDateValue(new Date());
            } else {
                this.getView().byId("TP2").setDateValue(new Date());
            }
        },

        getQueryParameters: function () {
            var view = this.getView();
            var from = view.byId("Turno").getDateValue();
            var line = view.byId("lines").getSelectedKey();
            return [line, from];
        },

        formatQueryParameters: function (params) {

            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var query = "";
            query += "Param.1=" + params[0];
            query += "&Param.2=" + oDateFormatter.format(params[1]);
            return query;
        },

        getTimeValue: function () {
            var view = this.getView();
            var oModelTime = new JSONModel();

            var query = this.getQueryParameters();
            var finalQuery = this.formatQueryParameters(query);
            oModelTime.loadData(QService + dataProdMD + "LineAvail/getLineAvailQR&" + finalQuery);
            oModelTime.attachRequestCompleted(function () {
                var dataResponse = oModelTime.oData.Rowsets.Rowset[0].Row[0];
                var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "HH:mm"});
                var dataModel = {};
                var h_start = new Date(dataResponse.H_START);
                var h_end = new Date(dataResponse.H_END);
                dataModel.H_START = h_start.toTimeString();
                dataModel.H_END = h_end.toTimeString();
                dataResponse.H_START = dataModel.H_START.split(' ')[0];
                dataResponse.H_END = dataModel.H_END.split(' ')[0];
                var oModel = new JSONModel(dataResponse);
                view.setModel(oModel, "TIME");
            });
        },

        onSave: function () {
            var self = this;
            var view = this.getView();
            if (view.byId("TP1").getDateValue() == null || view.byId("TP2").getDateValue() == null) {
                return 0;
            }
            var oModel = new JSONModel();

            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var oDateFormatter1 = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "HH:mm:ss"});

            var query = this.getQueryParameters();
            // var finalQuery = this.formatQueryParameters(query);
            var finalQuery="";
            finalQuery+= "&Param.1="+query[0];
            finalQuery+= "&Param.2="+ oDateFormatter.format(query[1]);
            finalQuery+= "&Param.3=" + oDateFormatter1.format(view.byId("TP1").getDateValue());
            finalQuery+= "&Param.4=" + oDateFormatter1.format(view.byId("TP2").getDateValue());

            oModel.loadData(QService + dataProdMD + "LineAvail/upsLineAvailXQ&" + finalQuery);
            oModel.attachRequestCompleted(function () {
                var dataResponse = oModel.oData.Rowsets.Rowset[0].Row;
                if (dataResponse[0].RC === "1") self.messageDialog("Success", "Success", "Orari salvati correttamente");
                else self.messageDialog("Errore", "Error", "Errore durante il salvataggio degli orari");
            });
        }
    })
});