var QService = "/XMII/Illuminator?";
var dataDBS = "Content-Type=text/json&QueryTemplate=ProductionMain/";

var rowsets;

sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.DashBoardSeco", {
        onInit: function () {
            var self = this;
            var view = this.getView();
            view.addEventDelegate({
                onAfterShow: function (oEvent) {
                    self.getDashBoard();
                    self.refreshId = setInterval(function () {
                        self.getDashBoard();
                    }, 180000); //180000
                }
            }, view);
        },

        getDashBoard : function () {
            var self = this;
            var view = this.getView();
            
            var queryString = "";
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            
            queryString += "Param.1=2000";
            queryString += "&Param.2=" + oDateFormatter.format(new Date());
            //queryString += "&Param.2=2019-05-03";
            queryString += "&Param.4=";

            var oModel = new JSONModel();
            oModel.loadData(QService + dataDBS + "DashBoard/getDashboardSecoQR&" + queryString);
            oModel.attachRequestCompleted(function () {
                rowsets = oModel.oData.Rowsets.Rowset;
                
                for (var i = 0; i < rowsets[0].Row.length; i++) {       
                    var row = rowsets[0].Row[i];
                    
                    if (sap.ui.getCore().byId("hl" + i)) 
                        sap.ui.getCore().byId("hl" + i).destroy();
                         
                    var lines = new sap.ui.layout.HorizontalLayout({
                        id: "hl" + i,
                        content: [               
                            new sap.ui.layout.Grid({
                                content: [
                                    new sap.m.HBox({
                                        displayInline : false,
                                        alignItems : "End",
                                        justifyContent : sap.m.FlexAlignContent.Left,
                                        alignContent : sap.m.FlexAlignContent.End,
                                        items: [                                          
                                            new sap.m.Button({
                                                id: "btnLineDetails" + i,
                                                icon : "sap-icon://enter-more",
                                                press: function(oControlEvent){
                                                    self.getLineDetails(oControlEvent);
                                                },
                                            }), 
                                            new sap.m.Label({
                                                text:  row.LINE_LABEL,
                                                tooltip: row.IDLINE,
                                                design: "Bold",
                                                width: '15rem',
                                                textAlign: sap.ui.core.TextAlign.Center,
                                                layoutData: new sap.ui.layout.GridData({
                                                    span: "L12 M12 S12",
                                                    linebreak: false
                                                })
                                            }),
                                            new sap.m.Label({
                                                text: ' ',
                                                width: '0.3em'
                                            }),
                                            new sap.ui.core.Icon({
                                                src: row.STATE_ICON, 
                                                size: "24px",
                                                color: "black",
                                                layoutData: new sap.ui.layout.GridData({
                                                    span: "L12 M12 S12",
                                                    linebreak: true
                                                })
                                            })                                       
                                        ],
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L12 M12 S12",
                                            linebreak: true
                                        })
                                    }).addStyleClass("BorderLine"),
                                    new sap.m.Label({
                                        text: row.MATERIAL_LABEL, 
                                        tooltip: row.MATERIAL_LABEL, 
                                        width: '270px',
                                        design: "Bold",
                                        textAlign: sap.ui.core.TextAlign.Center,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L12 M12 S12",
                                            linebreak: true
                                        })
                                    }),	
                                    new sap.m.Label({
                                        text: row.MATDESC_LABEL, 
                                        tooltip: row.MATDESC_LABEL, 
                                        width: '270px',
                                        design: "Bold",
                                        textAlign: sap.ui.core.TextAlign.Center,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L12 M12 S12",
                                            linebreak: true
                                        })
                                    }),	
                                    new sap.m.Label({
                                        text: row.PRODUCTIONORDER, 
                                        tooltip: row.PRODUCTIONORDER, 
                                        width: '270px',
                                        design: "Bold",
                                        textAlign: sap.ui.core.TextAlign.Center,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L12 M12 S12",
                                            linebreak: true
                                        })
                                    }).addStyleClass("BorderLine"),	
                                    new sap.m.HBox({
                                        width: "100%",
                                        displayInline: true,
                                        alignItems: "Center" ,
                                        justifyContent: sap.m.FlexAlignContent.Left ,
                                        alignContent: sap.m.FlexAlignContent.Left,
                                        items: [
                                            new sap.ui.core.Icon({
                                                src: "sap-icon://circle-task-2",
                                                size: "15px",
                                                color: row.STATE_COLOR, 
                                                press: function (oEvent) {
                                                    var idx = oEvent.getParameter("id");
                                                }
                                            }).addStyleClass("iconAlign"),
                                            new sap.m.Label({
                                                text: ' ',
                                                width: '4px'
                                            }),
                                            new sap.m.Label({
                                                text: row.STATE_LABEL, 
                                                design: "Bold",
                                                width: '100%',
                                                textAlign: sap.ui.core.TextAlign.Center,

                                            })],
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L11 M11 S11",
                                            linebreak: true
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: 'Prodotti',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6",
                                            linebreak: true
                                        })
                                    }),                    
                                    new sap.m.Label({
                                        text: 'Target',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: row.QTY_DAY_GOOD + " pz",
                                        design: "Bold",
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6",
                                            linebreak: true
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: row.QTY_DAY_TARGET + " pz", 
                                        design: "Bold",
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }),
                                    new sap.m.HBox({
                                        width: "15rem", 
                                        height: "8rem", 
                                        displayInline: false,
                                        alignItems: "Center",
                                        justifyContent: "Center",
                                        alignContent: "Center",
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L11 M11 S11",
                                            linebreak: true
                                        }),
                                        items: [                      
                                            new sap.ui.core.HTML({
                                                id: "radial" + i,
                                                content: "<div></div>",
                                                afterRendering: function (oControlEvent) {
                                                    $("#" + oControlEvent.getSource().oParent.sId).parent().addClass("RadialChartCtrl");
                                                   
                                                    var n = oControlEvent.getSource().sId.replace("radial", "");
                                                    var rowRadial = rowsets[0].Row[n];
                                                    
                                                    var OEE_COLOR = rowRadial.OEE_COLOR; 
                                                    var OEE =  rowRadial.OEE; 
                                                    var IDLINE = rowRadial.IDLINE; 
                                                    var QTY_DAY_TARGET = rowRadial.QTY_DAY_TARGET;
                                                 
                                                    var jdata = {
                                                        "name": "grafico",
                                                        "children": [
                                                            {
                                                                "name": "TOT_SEC_DAY_DISPONIBILITA",
                                                                "size": rowRadial.TOT_SEC_DAY_DISPONIBILITA 
                                                            },
                                                            {
                                                                "name": "TOT_SEC_TRASCORSI",
                                                                "size": rowRadial.TOT_SEC_TRASCORSI 
                                                            },
                                                            {
                                                                "name": "QTY_DAY_TARGET",
                                                                "size": rowRadial.QTY_DAY_TARGET 
                                                            },
                                                            {
                                                                "name": "QTY_DAY_TOTAL",
                                                                "size": rowRadial.QTY_DAY_TOTAL 
                                                            },
                                                            {
                                                                "name": "QTY_DAY_SCRAP",
                                                                "size": rowRadial.QTY_DAY_SCRAP
                                                            }]
                                                    };
                                                    self.radialChart(180, OEE_COLOR, "#" + oControlEvent.getSource().sId, OEE, jdata, IDLINE, QTY_DAY_TARGET);            
                                                }
                                            }), 
                                        ]
                                    }).addStyleClass("RadialChartStyle"),                                  
                                    new sap.m.Label({
                                        text: row.QTY_DAY_SCRAP + " pz",
                                        design: "Bold",
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L4 M4 S4",
                                            linebreak: true
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: row.T_TOTFERMO_MIN,
                                        design: "Bold",
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L8 M8 S8",
                                            linebreak: false
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: 'Scarti',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L5 M5 S5",
                                            linebreak: true
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: 'Non disponibile',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L7 M7 S7",
                                            linebreak: false
                                        })
                                    }), 
                                    new sap.m.HBox({
                                        width: "100%",
                                        displayInline: false,
                                        alignItems: "End",
                                        justifyContent: "End",
                                        alignContent: "End",
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L12 M12 S12",
                                            linebreak: true
                                        }),
                                        items: []
                                    }).addStyleClass("BorderLineTop"),
                                    new sap.ui.core.Icon({
                                        src: row.LINE_ICON, 
                                        size: "18px",
                                        color: row.LINE_MSCOLOR,
                                        tooltip: row.LINE_MESSAGE,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L2 M2 S2",
                                            linebreak: false
                                        })
                                    }),
                                    new sap.ui.core.Icon({
                                        src: row.MII_ICON, 
                                        size: "18px",
                                        color: row.MII_MSCOLOR,
                                        tooltip: row.MII_MESSAGE,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L3 M3 S3",
                                            linebreak: false
                                        })
                                    }),
                                    new sap.m.Label({
                                        text: row.DATE_UPD,
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L7 M7 S7",
                                            linebreak: false
                                        })
                                    })
                                ]
                            }).addStyleClass("customGrid") 
                        ]        
                    });
                    
                    lines.placeAt(view.byId("lines"));
                }           
            });  
        },
        
        getLineDetails: function (oControlEvent) {
            var n = oControlEvent.getSource().sId.replace("btnLineDetails", "");
            var rowLine = rowsets[0].Row[n];
            
            //alert(rowLine.IDLINE);
            this.getRouter().navTo("DashBoardDetSeco", {
                plant: rowsets[1].Row[0].PLANT,
                idline: rowLine.IDLINE
            });
            this.exit();
        },
        
        radialChart: function (bwidth, l_BColor, divdom, l_oee, jdataQty, l_line, l_targ) {
            var totaleOre = jdataQty.children["0"].size; //TOT_SEC_DAY_DISPONIBILITA
            var totaleOreLavorate = jdataQty.children["1"].size; // TOT_SEC_TRASCORSI
            var totalePezzi = jdataQty.children["2"].size; //QTY_DAY_TARGET
            var totalePezziProdotti = jdataQty.children["3"].size; //QTY_DAY_TOTAL
            var totalePezziScartati = jdataQty.children["4"].size; //QTY_DAY_SCRAP
            var percentualeTotaleOreMancanti = (360 * totaleOreLavorate) / totaleOre;
            var percentualePezziProdotti = (percentualeTotaleOreMancanti * totalePezziProdotti) / totalePezzi;
            var percentualePezziScartati = (percentualePezziProdotti * totalePezziScartati) / totalePezziProdotti;
            percentualePezziProdotti = percentualePezziProdotti - percentualePezziScartati;
            percentualeTotaleOreMancanti = percentualeTotaleOreMancanti - percentualePezziProdotti - percentualePezziScartati;
            var percentualeTotaleOre = 360 - percentualeTotaleOreMancanti - percentualePezziProdotti - percentualePezziScartati;
            var jdata = {
                "name": "grafico",
                "children": [
                    {
                        "name": "percentualePezziProdotti",
                        "size": percentualePezziProdotti
                    },
                    {
                        "name": "percentualePezziScartati",
                        "size": percentualePezziScartati
                    },
                    {
                        "name": "percentualeTotaleOreMancanti",
                        "size": percentualeTotaleOreMancanti
                    },
                    {
                        "name": "percentualeTotaleOre",
                        "size": percentualeTotaleOre
                    }]
            };
            var bheight = bwidth,
                cwidth = bwidth * 0.82, //570
                cheight = cwidth,
                radius = Math.min(cwidth, cheight) / 2,
                thick = cwidth * 0.20, //0.15
                color = d3.scale.ordinal().range(["", "#a6a6a6", "#333", "#595959", "#f2f2f2", "#fcfcfc"]),
                angolo_inizio = -90; //angolo di inizio (-90gradi rispetto allo zero = ore 12)
            var svg = d3.select(divdom).append("svg")
                .attr("width", bwidth)
                .attr("height", bheight)
                .attr("id", "svg_" + l_line)
                .style("color", "#333")
                .append("g")
                .attr("transform", "translate(" + bwidth / 2 + "," + bheight / 2 + ")");
            var partition = d3.layout.partition()
                .sort(null)
                .size([2 * Math.PI, radius * radius])
                .value(function (d) {
                    return d.size;
                });
            var arc = d3.svg.arc()
                .startAngle(function (d) {
                    return d.x + angolo_inizio * Math.PI / 180;
                })
                .endAngle(function (d) {
                    return d.x + d.dx + angolo_inizio * Math.PI / 180;
                })
                .innerRadius(function (d) {
                    return Math.sqrt(d.y + d.dy) - thick;
                })
                .outerRadius(function (d) {
                    return Math.sqrt(d.y + d.dy);
                });
            var bColor = "";
            switch (l_BColor) {
                case "green":
                    bColor = d3.scale.ordinal()
                        .range(["", "#19ad4b", "#127038", "rgb(228, 228, 228)", "#fcfcfc", "#fcfcfc"]);
                    break;
                case "yellow":
                    bColor = d3.scale.ordinal()
                        .range(["", "#ffcc00", "#c08907", "rgb(228, 228, 228)", "#fcfcfc"]); //#f2f2f2
                    break;
                case "red":
                    bColor = d3.scale.ordinal()
                        .range(["", "#e41e35", "#961e1f", "rgb(228, 228, 228)", "#fcfcfc"]);
                    break;
                case "black":
                    bColor = d3.scale.ordinal()
                        .range(["", "#e41e35", "#961e1f", "rgb(228, 228, 228)", "#fcfcfc"]);
                    break;
            }
            var path = svg.datum(jdata).selectAll("path")
                .data(partition.nodes)
                .enter().append("path")
                .attr("d", path)
                .attr("display", function (d) {
                    return d.depth ? null : "none";
                }) //nascondi l'anello interno
                .attr("d", arc)
                .each(stash);
            var TargPz = l_targ;
            var oee = l_oee;
            path.style("fill", function (d) {
                return bColor(d.name);
            });
            if (oee === 0 && TargPz === 0) {
                oee = '';
            } else {
                oee = oee + '%';
            }
            svg.append("foreignObject")
                .attr("width", (radius - thick) * 2)
                .attr("height", radius - 10)
                .style("z-index", 100)
                .attr("transform", function (d) {
                    return "translate(" + (-radius + thick) + "," + (-radius + thick) / 1.8 + ")";
                })
                .append("xhtml:div")
                .html("<span>" + oee + "</span>")
                .style("font-size", "1.85rem")
                .style("text-align", "center")
                .style("font-weight", "bold")
                .style("color", l_BColor == "green" ? "#19ad4b" : l_BColor == "yellow" ? "#ffcc00" : "#e41e35");
            svg.append("foreignObject")
                .attr("width", (radius - thick) * 2)
                .attr("height", radius - 10)
                .style("z-index", 100)
                .attr("transform", function (d) {
                    return "translate(" + (-radius + thick) + "," + (radius - thick) / 6 + ")";
                })
                .append("xhtml:div")
                .style("font-size", cwidth * 0.09 + "px")
                .style("text-align", "center")
                .style("font-weight", "bold")
                .html("<span>OEE</span>")
                .style("color", l_BColor == "green" ? "#19ad4b" : l_BColor == "yellow" ? "#ffcc00" : "#e41e35");

            // Stash the old values for transition.
            function stash(d) {
                d.x0 = d.x;
                d.dx0 = d.dx;
            }

            // Interpolate the arcs in data space.
            function arcTween(a) {
                var i = d3.interpolate({
                    x: a.x0,
                    dx: a.dx0
                }, a);
                return function (t) {
                    var b = i(t);
                    a.x0 = b.x;
                    a.dx0 = b.dx;
                    return arc(b);
                };
            }

            d3.select(self.frameElement).style("height", bheight + "px");
        },
        
        onNavToWelcome: function() {
            clearInterval(this.refreshId);
            var view = this.getView();
            this.onNavBack();
        },
    })
});