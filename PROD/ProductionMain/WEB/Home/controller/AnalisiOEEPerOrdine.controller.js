var QService = "/XMII/Illuminator?";
var path = "Content-Type=text/json&QueryTemplate=ProductionMain/Report/OEE_Ordine/";

sap.ui.define([
    "sap/ui/model/xml/XMLModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/DateFormat",
    "sap/m/MessageBox",
    'sap/ui/export/Spreadsheet',
    "seco/ui/app/seco/BaseController"
], function (XmlModel, JSONModel, DateFormat, MessageBox, Spreadsheet, BaseController) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.AnalisiOEEPerOrdine", {
        pathToExport: null,
        onSearch: function () {
            this.getView().byId("headerLayout").setVisible(false);
            this.getView().byId("bulletMicro").setVisible(false);
            this.getView().byId("radialContainer").setVisible(false);
            document.getElementsByClassName("day-chart")[0].innerHTML = "";
            
            var oModel = new JSONModel();
            var params = this.getQueryParameters();
            if (params.length === 0) {
                MessageBox.warning(
                    "Inserire tutti i campi obbligatori: Ordine.",
                    {
                        styleClass: "sapUiSizeCompact"
                    }
                );
                return;
            }
            var queryString = this.formatQueryParameters(params);

            this.clearChartData();
            oModel.loadData(QService + path + "getOEEOrderQR&" + queryString);
            oModel.attachRequestCompleted(function () {               
                var rowsets = oModel.oData.Rowsets.Rowset;
                var row = rowsets[0].Row[0];

                if(row.ORDER == "")
                {
                    MessageBox.warning(
                        "Ordine non trovato.",
                        {
                            styleClass: "sapUiSizeCompact"
                        }
                    );
                    return;
                }
                
                this.getView().byId("headerLayout").setVisible(true);
                this.getView().byId("bulletMicro").setVisible(true);
                this.getView().byId("radialContainer").setVisible(true);
                this.pathToExport = null;
                
                row.QTY = parseFloat(row.QTY);
                row.ORDER_QTY = parseFloat(row.ORDER_QTY);    
                row.OEE = parseFloat(row.OEE);
                row.OEE_D = parseFloat(row.OEE_D);
                row.OEE_P = parseFloat(row.OEE_P);
                row.OEE_Q = parseFloat(row.OEE_Q);
                
                var oModel1 = new JSONModel(row);
                this.getView().setModel(oModel1, "ORDER1");  
                 
                var radialData = row;
                
                this.getView().setModel(new JSONModel({
                    "OEE_P": radialData.OEE_P,
                    "OEE_D": radialData.OEE_D,
                    "OEE_Q": radialData.OEE_Q,
                    "OEE": radialData.OEE
                }), "RADIAL");
                
                var percOeeGreen = rowsets[2].Row[0].PERCOEE_GREEN;
                var percOeeRed = rowsets[2].Row[0].PERCOEE_RED;
                
                this.getView().setModel(new JSONModel({
                    "OEE_P": this.getValueColor(radialData.OEE_P, percOeeGreen, percOeeRed),
                    "OEE_D": this.getValueColor(radialData.OEE_D, percOeeGreen, percOeeRed),
                    "OEE_Q": this.getValueColor(radialData.OEE_Q, percOeeGreen, percOeeRed),
                    "OEE": this.getValueColor(radialData.OEE, percOeeGreen, percOeeRed)
                }), "COLORS");
                
                var events = [];
                var ranges = {};
                
                if (typeof rowsets[1] !== "undefined" && typeof rowsets[1].Row !== "undefined") {
                    events = rowsets[1].Row
                } else {
                    return;
                }
                
                //order
                events.sort(function (a, b) {
                    return moment(a.DATAEVENTO + " " + a.ORAINIZIO).format("YYMMDDHHmmss") - moment(b.DATAEVENTO + " " + b.ORAINIZIO).format("YYMMDDHHmmss");
                });

                //normalize ranges by date
                for (var e = 0; e < events.length; e++) {
                    var a = events[e];
                    if (typeof ranges[a.DATAEVENTO] === "undefined") {
                        ranges[a.DATAEVENTO] = [];
                    }
                    ranges[a.DATAEVENTO].push(a);
                }

                //DAY SEGMENTS
                this.renderDaySegments(ranges);   
                
                //EXCEL
                this.pathToExport = rowsets[0].Row;
           }.bind(this));
        },
        
        clearChartData: function () {
            this.getView().setModel(new JSONModel({}), "OEE");
            this.getView().setModel(new JSONModel({}), "RADIAL");
        },
        
        getQueryParameters: function () {
            var params = [];
            var order = "";
            var view = this.getView();
            order = view.byId("ordineInput").getValue()
            
            if(order.length > 0) {
                params = [order];
            }
            
            return params;
        },
        
        formatQueryParameters: function (params) {
            var query = "";
            query += "Param.1=" + params[0];
            return query;
        },
        
        getValueColor: function (data, green ,red) {
            if (data <= red) {
                return "Error";
            } else if (data > red && data < red) {
                return "Critical";
            } else {
                return "#3fa45b";
            }
        },
        
        pad: function (number) {
            number = parseInt(number);
            if (number < 10) {
                return "0" + number;
            } else {
                return number;
            }
        },
        
        renderDaySegments: function (ranges) {
            //Day container width
            var containerWidth = $(".sapUiBody").width() - 150;
            var htmlDays = "";
            document.getElementsByClassName("day-chart")[0].innerHTML = "";

            //Legend
            htmlDays += "<div class='day-descriptor'></div><div class='day-segment' style='width:" + containerWidth + "px;background-color:white;box-sizing:border-box;border-bottom:1px solid black;margin-bottom:2px;display:inline-block;'>";
            for (var l = 0; l <= 24; l++) {
                var second = l * 3600;
                var offset = (containerWidth * second) / 86400;
                var offsetLabel = offset - 5;
                htmlDays += "<div style='position:absolute;height:5px;width:1px;box-sizing:border-box;border-left:1px solid black;top:0px;left:" + offset + "px' ></div>";
                if (l % 2 == 0 && l !== 24) {
                    htmlDays += "<span class='hour-description' style='left:" + offsetLabel + "px'>" + this.pad(l) + "</span>";
                }
            }
            htmlDays += "</div>";
            document.getElementsByClassName("day-chart")[0].innerHTML = htmlDays;

            //iterate days in order to create day segment
            for (var d in ranges) {
                var lastEnd = "000000";
                //iterate day ranges
                var daySegment = "<div  class='day-descriptor'>" + d + "</div><div class='day-segment' style='display:inline-block;width:" + containerWidth + "px'>";
                for (var r = 0; r < ranges[d].length; r++) {
                    var offset = moment(ranges[d][r].ORAINIZIO, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
                    var offsetFine = moment(ranges[d][r].ORAFINE, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
                    var rangeWidth = offsetFine - offset;
                    //var color = "#3fa45b"; 
                    var color = ranges[d][r].EVENTTYPE === "FERMO" ? "red" : "#3fa45b";
                    ranges[d][r].offset = offset;
                    ranges[d][r].rangeWidth = rangeWidth;

                    //containerWidth : 86400 = x : seconds
                    ranges[d][r].left = (containerWidth * offset) / 86400;
                    ranges[d][r].width = (containerWidth * rangeWidth) / 86400;

                    daySegment += "<div class='day-event' data-toggle='tooltip' style='background-color:" + color + ";left:" + ranges[d][r].left + "px;width:" + ranges[d][r].width + "px'></div>";
                }
                daySegment += "</div>";
                document.getElementsByClassName("day-chart")[0].innerHTML += daySegment;
            }
        },
        
        exportxls: function () {
            var aCols;
            if(this.pathToExport !== null) {
                aCols = this.columnsToExport();
                var oModelRowset1 = new JSONModel();
                oModelRowset1.setData(this.pathToExport);
                this.exportTable(oModelRowset1, aCols)
            }
        },
        
        columnsToExport: function () {
            var columnsName = {};
            
            columnsName = {
                columnsName: [
                    "Ordine",
                    "Linea",
                    "Descrizione linea",
                    "Materiale",
                    "Descrizione materiale",
                    "Data inizio ordine",
                    "Data fine ordine",                            
                    "Quantità ordine",
                    "Quantità buona",
                    "Scarti",
                    "Quantità prodotta",
                    "OEE",
                    "Disponibilità",
                    "Produttività",
                    "Qualità",
                    "Durata ordine"
                ],
                columnsField: [
                    "ORDER",
                    "LINE_ID",
                    "LINE_DESCRIPTION",
                    "MATERIAL",
                    "MATERIAL_DESCRIPTION",
                    "START_DATE",
                    "END_DATE",                            
                    "ORDER_QTY",
                    "GOOD_QTY",
                    "SCRAP_QTY",
                    "QTY",
                    "OEE",
                    "OEE_D",
                    "OEE_P",
                    "OEE_Q",
                    "ORDER_DURATION"
                ]
            };
                                            
            var columnsToExport = [];
            for (var i = 0; i < columnsName.columnsName.length; i++) {
                columnsToExport.push({
                    label: columnsName.columnsName[i],
                    property: columnsName.columnsField[i],
                });
            }
            return columnsToExport;
        },
    });
});