var QService = "/XMII/Illuminator?";
var dataProdRPT = "Content-Type=text/json&QueryTemplate=ProductionMain/MainPage/";
var lines = [
    "SMT-L1",
    "SMT-L2",
    "SMT-L3",
    "TH-L1",
    "TH-L2"
];

sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "seco/ui/app/seco/BaseController",
    "sap/ui/core/format/DateFormat"
], function (JSONModel, BaseController, DateFormat) {
    "use strict";
    return BaseController.extend("seco.ui.app.seco.controller.Welcome", {
        onInit: function () {
            var self = this;
            var view = this.getView();
            view.addEventDelegate({
                onAfterShow: function (oEvent) {
                    self.setView();
                }
            }, view);
        },

        setView: function () {
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-ddThh:mm:ss"});

            var queryString = "";
            queryString += "&Param.1=" + oDateFormatter.format(new Date());
            queryString += "&Param.2=1000";

            var oModel = new JSONModel();
            oModel.loadData(QService + dataProdRPT + "getMainPageDataQR" + queryString);

            var view = this.getView();
            oModel.attachRequestCompleted(function () {
                var rowsets = oModel.oData.Rowsets.Rowset;
                var row = rowsets[0].Row[0];

                var oModelFirst = new JSONModel(row);
                view.setModel(oModelFirst, "MAIN_PAGE_DATA");


                // dashboard
                var dashboard = this.getView().byId("dashboard");
                var dashboardTile = this.getView().byId("dashboardTile");
                dashboard.setValueColor("Good");
                dashboardTile.setFooterColor("Error");

                // lineState
                var lineState = this.getView().byId("lineState");
                var lineStateTile = this.getView().byId("lineStateTile");
                lineState.setValueColor("Good");
                lineStateTile.setFooterColor("Error");

                // product
                var productQty = this.getView().byId("productQty");
                var productQtyTile = this.getView().byId("productQtyTile");

                if (row.PRODUCT_QTY_YESTERDAY_HOUR < row.PRODUCT_QTY_TODAY_HOUR) {
                    productQty.setValueColor("Good");
                    productQtyTile.setFooterColor("Error");
                    productQty.setIndicator(sap.m.DeviationIndicator.Up);
                } else if (row.PRODUCT_QTY_YESTERDAY_HOUR == row.PRODUCT_QTY_TODAY_HOUR) {
                    productQty.setValueColor("Critical");
                    productQtyTile.setFooterColor("Critical");
                    productQty.setIndicator(sap.m.DeviationIndicator.None);
                } else if (row.PRODUCT_QTY_YESTERDAY_HOUR > row.PRODUCT_QTY_TODAY_HOUR) {
                    productQty.setValueColor("Error");
                    productQtyTile.setFooterColor("Good");
                    productQty.setIndicator(sap.m.DeviationIndicator.Down);
                }

                //firstIndex
                var firstIndex = this.getView().byId("firstIndex");
                var firstIndexTile = this.getView().byId("firstIndexTile");

                if (row.FIRST_INDEX_YESTERDAY < row.FIRST_INDEX_TODAY) {
                    firstIndex.setValueColor("Good");
                    firstIndexTile.setFooterColor("Error");
                    firstIndex.setIndicator(sap.m.DeviationIndicator.Up);
                } else if (row.FIRST_INDEX_YESTERDAY == row.FIRST_INDEX_TODAY) {
                    firstIndex.setValueColor("Critical");
                    firstIndexTile.setFooterColor("Critical");
                    firstIndex.setIndicator(sap.m.DeviationIndicator.None);
                } else if (row.FIRST_INDEX_YESTERDAY > row.FIRST_INDEX_TODAY) {
                    firstIndex.setValueColor("Error");
                    firstIndexTile.setFooterColor("Good");
                    firstIndex.setIndicator(sap.m.DeviationIndicator.Down);
                }

                //qualityIndex
                var qualityIndex = this.getView().byId("qualityIndex");
                var qualityIndexTile = this.getView().byId("qualityIndexTile");

                if (row.QUALITY_INDEX_YESTERDAY < row.QUALITY_INDEX_TODAY) {
                    qualityIndex.setValueColor("Good");
                    qualityIndexTile.setFooterColor("Error");
                    qualityIndex.setIndicator(sap.m.DeviationIndicator.Up);
                } else if (row.QUALITY_INDEX_YESTERDAY == row.QUALITY_INDEX_TODAY) {
                    qualityIndex.setValueColor("Critical");
                    qualityIndexTile.setFooterColor("Error");
                    qualityIndex.setIndicator(sap.m.DeviationIndicator.None);
                } else if (row.QUALITY_INDEX_YESTERDAY > row.QUALITY_INDEX_TODAY) {
                    qualityIndex.setValueColor("Error");
                    qualityIndexTile.setFooterColor("Good");
                    qualityIndex.setIndicator(sap.m.DeviationIndicator.Down);
                }
            }.bind(this));
        },

        onShowReport: function () {

            this.getView().byId("reportTile").setVisible(true);
            this.getView().byId("reportLayout").setVisible(true);
            this.getView().byId("collaudiTile").setVisible(true);
            this.getView().byId("collaudiLayout").setVisible(true);
            this.getView().byId("pivotTile").setVisible(true);
            this.getView().byId("pivotLayout").setVisible(true);

            this.getView().byId("adminTile").setVisible(false);
            this.getView().byId("adminLayout").setVisible(false);
            this.getView().byId("reportButton").addStyleClass("underlineButton");
            this.getView().byId("adminButton").removeStyleClass("underlineButton");
        },

        onShowAmministrazione: function () {
            this.getView().byId("reportTile").setVisible(false);
            this.getView().byId("reportLayout").setVisible(false);
            this.getView().byId("collaudiTile").setVisible(false);
            this.getView().byId("collaudiLayout").setVisible(false);
            this.getView().byId("pivotTile").setVisible(false);
            this.getView().byId("pivotLayout").setVisible(false);

            this.getView().byId("adminTile").setVisible(true);
            this.getView().byId("adminLayout").setVisible(true);
            this.getView().byId("reportButton").removeStyleClass("underlineButton");
            this.getView().byId("adminButton").addStyleClass("underlineButton");
        }
    });
});