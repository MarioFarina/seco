// FORMATTA NUMERI GRANDI
function formattingNumber(value) {
    var thousand = 1000;
    var million = 1000000;
    var billion = 1000000000;
    if (value < thousand) {
        return [String(value)];
    }
    if (value >= thousand && value <= 1000000) {
        return [Math.round10(value / thousand, -2), 'K'];
    }
    if (value >= million && value <= billion) {
        return [Math.round10(value / million, -2), 'M'];
    } else {
        return [Math.round10(value / billion, -2), 'B'];
    }
}

(function () {
    function decimalAdjust(type, value, exp) {
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();