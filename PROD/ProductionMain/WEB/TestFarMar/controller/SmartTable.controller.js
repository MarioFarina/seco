sap.ui.define([
    "seco/ui/app/seco/BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";
    return BaseController.extend("farmar.ui.app.farmar.controller.SmartTable", {
        onInit: function () {
            var self = this;
            var view = this.getView();
            view.addEventDelegate({
                onAfterShow: function (oEvent) {
                   
                }
            }, view);
        }
});