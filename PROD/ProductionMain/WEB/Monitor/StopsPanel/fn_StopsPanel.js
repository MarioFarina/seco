/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor prodotti non conformi (sospesi)
//Author: Bruno Rosati
//Date:   21/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor prodotti non conformi (sospesi)

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var tmFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "HH:mm"
});

//data odierna
var oCurrentDateTime = new Date();
var year = oCurrentDateTime.getUTCFullYear();
var month = oCurrentDateTime.getUTCMonth() + 1;
if( month.toString().length == 1 ) month = "0" + month;
var day = oCurrentDateTime.getUTCDate();
var sCurrentDate = year + "-" + month+ "-" + day;
var sCurrentTime = oCurrentDateTime.getHours() + ":" + oCurrentDateTime.getMinutes() + ":00";


// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabStopsPanel() {

    var oDtCurrentDateFrom = new sap.ui.commons.DatePicker('dtCurrentDateFrom');
	oDtCurrentDateFrom.setYyyymmdd(sCurrentDate);
	oDtCurrentDateFrom.setLocale("it"); // Try with "de" or "fr" instead!
	/*oDtCurrentDateFrom.attachChange(
		function(oEvent){
			if(oEvent.getParameter("invalidValue")){
				oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
			}else{
				oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
			}
			refreshTabStopsPanel();
		}
	);*/

    var oDtCurrentDateTo = new sap.ui.commons.DatePicker('dtCurrentDateTo');
	oDtCurrentDateTo.setYyyymmdd(sCurrentDate);
	oDtCurrentDateTo.setLocale("it"); // Try with "de" or "fr" instead!
	/*oDtCurrentDateTo.attachChange(
		function(oEvent){
			if(oEvent.getParameter("invalidValue")){
				oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
			}else{
				oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
			}
			refreshTabStopsPanel();
		}
	);*/

    //filtro Turno
    var oCmbShiftFilter = new sap.ui.commons.ComboBox("filtShift", {
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            /*refreshTabStopsPanel();*/
        }
    });
    var oItemShift = new sap.ui.core.ListItem();
    oItemShift.bindProperty("key", "SHIFT");
    oItemShift.bindProperty("text", "SHNAME");
    oCmbShiftFilter.bindItems("/Rowset/Row", oItemShift);
    var oShiftFilter = new sap.ui.model.xml.XMLModel();
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val());
    }
    else{
        oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    }
    oCmbShiftFilter.setModel(oShiftFilter);

    //filtro Linea
    var oCmbLineFilter = new sap.ui.commons.ComboBox("filtLine", {
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            /*refreshTabStopsPanel();*/
        }
    });
    var oItemLine = new sap.ui.core.ListItem();
    oItemLine.bindProperty("key", "IDLINE");
    //oItemLine.bindProperty("text", "LINETXT");
    oItemLine.bindProperty("text", {
        parts: [
            {path: 'IDLINE'},
            {path: 'LINETXT'}
        ],
        formatter: function(sReasId,sReasTxt){
            return sReasId + ' - ' + sReasTxt;
        }
    });
    oCmbLineFilter.bindItems("/Rowset/Row", oItemLine);
    var oLineFilter = new sap.ui.model.xml.XMLModel();
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());
    }
    else{
        oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1" + $.UIbyID("filtPlant").getSelectedKey());
    }
    oCmbLineFilter.setModel(oLineFilter);

    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "StopsPanelTab",
        properties: {
            title: oLng_Monitor.getText("Monitor_StopsManager"), //"Gestione Fermi",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("StopsPanelTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModStop").setEnabled(false);
                    $.UIbyID("btnDelStop").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModStop").setEnabled(true);
                    $.UIbyID("btnDelStop").setEnabled(true); //cancellazione momentaneamente abilitata ad entrambe le tipologie di fermo
                    /* --- Momentaneamente disabilitati i controlli sulla natura del fermo
                    if(fnGetCellValue("StopsPanelTab", "FL_MAN") === "0"){
                        //Se il fermo è automatico non può essere cancellato
                        $.UIbyID("btnDelStop").setEnabled(false);
                    }
                    else{
                        //Se il fermo manuale non ha causale allora può essere cancellato
                        if(fnGetCellValue("StopsPanelTab", "REASON") === ""){
                            $.UIbyID("btnDelStop").setEnabled(true);
                        }
                        //Se il fermo manuale ha causale allora può essere cancellato
                        else{
                            $.UIbyID("btnDelStop").setEnabled(false);
                        }
                    }*/
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Add"), //"Aggiungi",
                        id:   "btnAddStop",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openStopEdit(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Modify"), //"Modifica",
                        id:   "btnModStop",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openStopEdit(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Delete"), //"Elimina"
                        id:   "btnDelStop",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelStop();
                        }
                    }),
                    new sap.ui.commons.Label({
                        id: "lblRecordNumber",
                        text: oLng_Monitor.getText("Monitor_RecordNumber"), //"N. record"
                        textAlign: "Right"
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_ShowMicroStops"), //"Visualizza anche i rallentamenti"
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    new sap.ui.commons.CheckBox({
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                        checked: false,
                        id: "chkShowMicroStops",
                        change: function (oEvent) {
                            /*refreshTabStopsPanel();*/
                        }
                    }),
                    new sap.ui.commons.Button({
                        icon: "sap-icon://refresh",
                        press: function () {
                            refreshTabStopsPanel();
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Export"), //"Esporta"
                        id:   "btnExportStop",
                        tooltip: oLng_Monitor.getText("Monitor_ExportInCSV"), //"Esporta i dati estratti in formato .csv"
                        enabled: true,
                        press: function (){
                            exportTableToCSV("StopsPanelTab","DataTable");
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_DateShiftFrom") //Da Data Turno
                    }),
                    oDtCurrentDateFrom,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_DateShiftTo") //A Data Turno
                    }),
                    oDtCurrentDateTo,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_Shift") //Turno
                    }),
                    oCmbShiftFilter,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_Line") //Linea
                    }),
                    oCmbLineFilter,
                    new sap.ui.commons.Button({
                        icon: "sap-icon://filter",
                        enabled: false,
                        id:   "delFilter",
												tooltip: oLng_Monitor.getText("Monitor_RemoveFilters"),
                        press: function () {
                            $.UIbyID("filtShift").setSelectedKey("");
                            $.UIbyID("filtLine").setSelectedKey("");
                            $.UIbyID("delFilter").setEnabled(false);
                            refreshTabStopsPanel();
                        }
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
                    visible: false,
					flexible : false
				}
            },
            {
				Field: "NAME1",
				label: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
				properties: {
					width: "80px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "IDLINE",
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    //visible: false,
                    flexible : false
				}
            },
            {
				Field: "LINETXT",
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "150px",
                    flexible : false
				}
            },
            {
				Field: "IDMACH",
                label: oLng_Monitor.getText("Monitor_Machine"), //"Macchina",
				properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "MACHTXT",
				label: oLng_Monitor.getText("Monitor_Machine"), //"Macchina",
				properties: {
					width: "160px",
                    flexible : false
				}
            },
            {
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT",
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "LASTEVT",
				//label: oLng_Monitor.getText("Monitor_Machine"), //"Macchina",
				properties: {
					width: "75px",
                    visible: false
				}
            },
            {
				Field: "STATE",
				label: oLng_Monitor.getText("Monitor_State"), //"Stato",
				properties: {
					width: "60px",
                    visible: false,
                    flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
            },
            {
				Field: "STATDESC",
				label: oLng_Monitor.getText("Monitor_State"), //"Stato",
				properties: {
					width: "75px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "DATE_FROM",
				label: oLng_Monitor.getText("Monitor_DateFrom"), //"Da Data",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "DATE_TO",
				label: oLng_Monitor.getText("Monitor_DateTo"), //"A Data",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "TIME_DIFF",
				label: oLng_Monitor.getText("Monitor_Duration"), //"Durata",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					/*type: "Time",*/
					textAlign: "Center"
                }
            },
            {
				Field: "MINUTES_DIFF",
				label: oLng_Monitor.getText("Monitor_MinutesDuration"), //"Durata (in minuti)",
				properties: {
					width: "145px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "Number",
					textAlign: "Right"
                }
            },
            {
				Field: "SLOWTIME",
				label: oLng_Monitor.getText("Monitor_SlowStopEdge"), //"Soglia fermo/rallentamento",
				properties: {
					width: "20px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "STTYPE",
				label: oLng_Monitor.getText("Monitor_StopGRreason"), //"Gruppo causali di fermo",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "REASON",
				label: oLng_Monitor.getText("Monitor_StopReason"), //"Causale di fermo",
				properties: {
					width: "70px",
                    visible: false,
                    flexible : false
                    //resizable : false,
				}
            },
            {
				Field: "REASTXT",
				label: oLng_Monitor.getText("Monitor_StopReason"), //"Causale di fermo",
				properties: {
					width: "160px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "FL_MAN",
				label: oLng_Monitor.getText("Monitor_ManualInsert"), //"Ins. manuale",
				properties: {
					width: "110px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "OPEID",
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"nr. Badge",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "OPENAME",
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"Nome Operatore",
				properties: {
					width: "160px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "USERMOD",
				label: oLng_Monitor.getText("Monitor_UserID"), //"Id Utente",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "USERNAME",
				label: oLng_Monitor.getText("Monitor_UserName"), //"Nome Utente",
				properties: {
					width: "160px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "DATE_INS",
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "160px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "DATE_UPD",
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Data aggiornamento",
				properties: {
					width: "160px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "NOTE",
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "160px",
                    //resizable : false,
                    flexible : false
				}
            }
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    /*refreshTabStopsPanel();*/
    return oTable;
}

// Aggiorna la tabella Gestione fermi
function refreshTabStopsPanel() {

    var sDateShiftFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtCurrentDateFrom").getYyyymmdd()));
    var sDateShiftTo = dtFormat.format(dtSelect.parse($.UIbyID("dtCurrentDateTo").getYyyymmdd()));
    var sPlant = "";

	if ($.UIbyID("filtPlant").getSelectedKey() === ""){
		sPlant = $('#plant').val();
	}
	else{
		sPlant = $.UIbyID("filtPlant").getSelectedKey();
	}

	var qParams = {
		data: "ProductionMain/Production/Stops/getStopsByPlantQR" +
		"&Param.1=" + sPlant +
		"&Param.2=" + sDateShiftFrom +
		"&Param.3=" + sDateShiftTo +
		"&Param.4=" + $.UIbyID("filtLine").getSelectedKey() +
		"&Param.5=" + $.UIbyID("filtShift").getSelectedKey() +
		"&Param.6=" + ($.UIbyID("chkShowMicroStops").getChecked() === true ? "1" : "0"),
		dataType: "xml"
	};
    $.UIbyID("StopsPanelTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		$.UIbyID("StopsPanelTab").setBusy(true);
		// carica il model della tabella
		$.UIbyID("StopsPanelTab").getModel().setData(data);
	}) // End Done Function
	// on fail
		.fail(function () {
		sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
    })
	// always, either on success or fail
		.always(function () {

		// remove busy indicator
		$.UIbyID("StopsPanelTab").setBusy(false);
        var rowsetObject = $.UIbyID("StopsPanelTab").getModel().getObject("/Rowset/0");
        if(rowsetObject){
            var iRecNumber = rowsetObject.childNodes.length - 1;
            $.UIbyID("lblRecordNumber").setText(oLng_Monitor.getText("Monitor_RecordNumber") /*"N. record" */ + " " + iRecNumber);
        }
	});
    $.UIbyID("StopsPanelTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Stop
function openStopEdit(bEdit) {

    if (bEdit === undefined) bEdit = false;

    var sCurrentPlant = $.UIbyID("filtPlant").getSelectedKey();
    console.log(sCurrentPlant);

    var oModelPlant = new sap.ui.model.xml.XMLModel();
	oModelPlant.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        enabled: false,
        selectedKey : sCurrentPlant,
        change: function (oEvent) {}
    });
	oCmbPlantInput.setModel(oModelPlant);
	var oItemPlantInput = new sap.ui.core.ListItem();
    oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);

    var oModelLine = new sap.ui.model.xml.XMLModel();
    oModelLine.setSizeLimit(1000);
    if (sCurrentPlant === ""){
        oModelLine.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());
    }
    else{
        oModelLine.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + sCurrentPlant);
    }
    // Crea la ComboBox per le linee in input
    var oCmbLineInput = new sap.ui.commons.ComboBox({
        id: "cmbLineInput",
        enabled: !bEdit,
        selectedKey : bEdit?fnGetCellValue("StopsPanelTab", "IDLINE"):"",
        change: function (oEvent) {
            $.UIbyID("cmbMachineInput").setSelectedKey("");
            $.UIbyID("cmbMachineInput").getModel().loadData(
                QService + dataProdMD + "Machines/getMachinesByLineSQ&Param.1=" + $.UIbyID("cmbLineInput").getSelectedKey(),
                false,
                false
            );
            var aItems = $.UIbyID("cmbMachineInput").getItems();
            if(aItems.length === 1){
                const sKey = $.UIbyID("cmbMachineInput").getModel().getProperty("/Rowset/Row/IDMACH");
                $.UIbyID("cmbMachineInput").setSelectedKey(sKey);
            }
        }
    });
    var oItemLineInput = new sap.ui.core.ListItem();
    oItemLineInput.bindProperty("key", "IDLINE");
    //oItemLineInput.bindProperty("text", "LINETXT");
    oItemLineInput.bindProperty("text", {
        parts: [
            {path: 'IDLINE'},
            {path: 'LINETXT'}
        ],
        formatter: function(sReasId,sReasTxt){
            return sReasId + ' - ' + sReasTxt;
        }
    });
    oCmbLineInput.bindItems("/Rowset/Row", oItemLineInput);
    oCmbLineInput.setModel(oModelLine);

    var oModelMachine = new sap.ui.model.xml.XMLModel();
	oModelMachine.loadData(
        QService + dataProdMD + "Machines/getMachinesByLineSQ&Param.1=" +
        (bEdit?fnGetCellValue("StopsPanelTab", "IDLINE"):"")
    );
	// Crea la ComboBox per le macchine in input
	var oCmbMachineInput = new sap.ui.commons.ComboBox({
        id: "cmbMachineInput",
        selectedKey : bEdit?fnGetCellValue("StopsPanelTab", "IDMACH"):"",
        change: function (oEvent) {}
    });
	oCmbMachineInput.setModel(oModelMachine);
	var oItemMachineInput = new sap.ui.core.ListItem();
    oItemMachineInput.bindProperty("key", "IDMACH");
    //oItemMachineInput.bindProperty("text", "MACHTXT");
	oItemMachineInput.bindProperty("text", {
        parts: [
            {path: 'IDMACH'},
            {path: 'MACHTXT'}
        ],
        formatter: function(sReasId,sReasTxt){
            return sReasId + ' - ' + sReasTxt;
        }
    });
	oCmbMachineInput.bindItems("/Rowset/Row", oItemMachineInput);

    var oModelState = new sap.ui.model.xml.XMLModel();
	oModelState.loadData(QService + dataProdSett + "MachineState/getMachineStatesSQ");

	// Crea la ComboBox per lo stato in input
	/*var oCmbStateInput = new sap.ui.commons.ComboBox({
        id: "cmbStateInput",
        selectedKey : bEdit?fnGetCellValue("StopsPanelTab", "STATE"):"",
        change: function (oEvent) {}
    });
	oCmbStateInput.setModel(oModelState);
	var oItemStateInput = new sap.ui.core.ListItem();
    oItemStateInput.bindProperty("text", "STATDESC");
	oItemStateInput.bindProperty("key", "STATE");
	oCmbStateInput.bindItems("/Rowset/Row", oItemStateInput);*/

    var sEndDate;
    var sEndTime;
    if(bEdit){
        sCurrentDate = dateFormatter(fnGetCellValue("StopsPanelTab", "DATE_FROM"));
        sCurrentTime = timeFormatter(fnGetCellValue("StopsPanelTab", "DATE_FROM"));
        sEndDate = dateFormatter(fnGetCellValue("StopsPanelTab", "DATE_TO"));
        sEndTime = timeFormatter(fnGetCellValue("StopsPanelTab", "DATE_TO"));
        console.log(sCurrentDate);
        console.log(sCurrentTime);
    }
    else{
        var sCurrentDate = year + "-" + month+ "-" + day;
        sCurrentTime = new Date().getHours() + ":" + new Date().getMinutes() + ":00";
    }

    var oDtStartDate = new sap.ui.commons.DatePicker('dtStartDate');
    oDtStartDate.setEnabled(!bEdit);
	oDtStartDate.setYyyymmdd(sCurrentDate);
	oDtStartDate.setLocale("it"); // Try with "de" or "fr" instead!

    var oTmStartTime = new sap.m.TimePicker("tmStartTime", {
        localeId: "it",
        displayFormat: "HH:mm",
        valueFormat: "HH:mm:ss",
        icon: "",
        //width: "110px"
    })./*setValue(tmFormat.format(sCurrentTime))*/setValue(sCurrentTime);
    oTmStartTime.setEnabled(!bEdit);

    var oDtEndDate = new sap.ui.commons.DatePicker('dtEndDate');
    oDtEndDate.setYyyymmdd(sEndDate);
	oDtEndDate.setLocale("it"); // Try with "de" or "fr" instead!

    var oTmEndTime = new sap.m.TimePicker("tmEndTime", {
        localeId: "it",
        displayFormat: "HH:mm",
        valueFormat: "HH:mm:ss",
        icon: "",
        //width: "110px"
    })./*setValue(tmFormat.format(sCurrentTime))*/setValue(sEndTime);

    var oModelGRreason = new sap.ui.model.xml.XMLModel();
    oModelGRreason.setSizeLimit(1000);
    if (sCurrentPlant === ""){
        oModelGRreason.loadData(
            QService + dataProdMD + "Stopreas/getStopReasonsGroupsSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        oModelGRreason.loadData(
            QService + dataProdMD + "Stopreas/getStopReasonsGroupsSQ&Param.1=" + sCurrentPlant
        );
    }
	// Crea la ComboBox per la causale di scarto in input
	var oCmbGRreasonInput = new sap.ui.commons.ComboBox({
        id: "cmbGRreasonInput",
        selectedKey : bEdit?fnGetCellValue("StopsPanelTab", "STTYPE"):"",
        change: function (oEvent) {
            $.UIbyID("cmbReasonInput").setSelectedKey("");
            if (sCurrentPlant === ""){
                $.UIbyID("cmbReasonInput").getModel().loadData(
                    /*QService + dataProdMD + "Stopreas/getStopreasByPlantSQ&Param.1=" + $('#plant').val()*/
                    QService + dataProdMD + "Stopreas/getStopReasonsByGroupSQ" +
                    "&Param.1=" + $('#plant').val() +
                    "&Param.2=" + $.UIbyID("cmbGRreasonInput").getSelectedKey()
                );
            }
            else{
                $.UIbyID("cmbReasonInput").getModel().loadData(
                    /*QService + dataProdMD + "Stopreas/getStopreasByPlantSQ&Param.1=" + sCurrentPlant*/
                    QService + dataProdMD + "Stopreas/getStopReasonsByGroupSQ" +
                    "&Param.1=" + sCurrentPlant +
                    "&Param.2=" + $.UIbyID("cmbGRreasonInput").getSelectedKey()
                );
            }
            var aItems = $.UIbyID("cmbReasonInput").getItems();
            if(aItems.length === 1){
                const sKey = $.UIbyID("cmbReasonInput").getModel().getProperty("/Rowset/Row/STOPID");
                $.UIbyID("cmbReasonInput").setSelectedKey(sKey);
            }
        }
    });
	oCmbGRreasonInput.setModel(oModelGRreason);
	var oItemGRreasonInput = new sap.ui.core.ListItem();
    oItemGRreasonInput.bindProperty("key", "STTYPE");
    //oItemGRreasonInput.bindProperty("text", "REASTXT");
	oItemGRreasonInput.bindProperty("text", {
        parts: [
            {path: 'STTYPE'},
            {path: 'REASTXT'}
        ],
        formatter: function(sReasId,sReasTxt){
            return sReasId + ' - ' + sReasTxt;
        }
    });
	oCmbGRreasonInput.bindItems("/Rowset/Row", oItemGRreasonInput);

    var oModelReason = new sap.ui.model.xml.XMLModel();
    oModelReason.setSizeLimit(1000);
    if (sCurrentPlant === ""){
        oModelReason.loadData(
            /*QService + dataProdMD + "Stopreas/getStopreasByPlantSQ&Param.1=" + $('#plant').val()*/
            QService + dataProdMD + "Stopreas/getStopReasonsByGroupSQ" +
            "&Param.1=" + $('#plant').val() +
            "&Param.2=" + (bEdit?fnGetCellValue("StopsPanelTab", "STTYPE"):$.UIbyID("cmbGRreasonInput").getSelectedKey())
        );
    }
    else{
        oModelReason.loadData(
            /*QService + dataProdMD + "Stopreas/getStopreasByPlantSQ&Param.1=" + sCurrentPlant*/
            QService + dataProdMD + "Stopreas/getStopReasonsByGroupSQ" +
            "&Param.1=" + sCurrentPlant +
            "&Param.2=" + (bEdit?fnGetCellValue("StopsPanelTab", "STTYPE"):$.UIbyID("cmbGRreasonInput").getSelectedKey())
        );
    }
	// Crea la ComboBox per la causale di scarto in input
	var oCmbReasonInput = new sap.ui.commons.ComboBox({
        id: "cmbReasonInput",
        selectedKey : bEdit?fnGetCellValue("StopsPanelTab", "REASON"):"",
        change: function (oEvent) {}
    });
	oCmbReasonInput.setModel(oModelReason);
	var oItemReasonInput = new sap.ui.core.ListItem();
    oItemReasonInput.bindProperty("key", "STOPID");
    oItemReasonInput.bindProperty("text", "REASTXT");
	oItemReasonInput.bindProperty("text", {
        parts: [
            {path: 'STOPID'},
            {path: 'REASTXT'}
        ],
        formatter: function(sReasId,sReasTxt){
            return sReasId + ' - ' + sReasTxt;
        }
    });
	oCmbReasonInput.bindItems("/Rowset/Row", oItemReasonInput);

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgStop",
        maxWidth: "750px",
        Width: "750px",
        showCloseButton: false
    });

    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Monitor.getText("Monitor_ModifyStop"):oLng_Monitor.getText("Monitor_NewStop"),
            /*"Modifica Fermo":"Nuovo Fermo"*/
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_Line"), //"Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbLineInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_Machine"), //"Macchina",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbMachineInput,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://sys-cancel",
                                press: function(){
                                    $.UIbyID("cmbMachineInput").setSelectedKey("");
                                }
                            })
                        ]
                    }),
                    /*new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_State"), //"Stato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbStateInput,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://sys-cancel",
                                press: function(){
                                    $.UIbyID("cmbStateInput").setSelectedKey("");
                                }
                            })
                        ]
                    }),*/
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_DateFrom"), //"Da Data/ora",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oDtStartDate,
                            oTmStartTime,
                            new sap.ui.commons.Button({
                                width: "60px",
                                enabled: !bEdit,
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://date-time",
                                press: function(){
                                    $.UIbyID("dtStartDate").setYyyymmdd(sCurrentDate);
                                    sCurrentTime = new Date().getHours() + ":" + new Date().getMinutes() + ":00";
                                    $.UIbyID("tmStartTime").setValue(sCurrentTime);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_DateTo"), //"A Data/ora",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oDtEndDate,
                            oTmEndTime,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://date-time",
                                press: function(){
                                    if (bEdit){
                                        console.log("Data fine: " + sEndDate + ";");
                                        console.log("Ora fine: " + sEndTime + ";");
                                        if(sEndDate === "-" && sEndTime === "-"){
                                            $.UIbyID("dtEndDate").setYyyymmdd(sCurrentDate);
                                            sCurrentTime = new Date().getHours() + ":" + new Date().getMinutes() + ":00";
                                            $.UIbyID("tmEndTime").setValue(sCurrentTime);
                                        }
                                        else{
                                            $.UIbyID("dtEndDate").setYyyymmdd(sEndDate);
                                            $.UIbyID("tmEndTime").setValue(sEndTime);
                                        }
                                    }
                                    else{
                                        $.UIbyID("dtEndDate").setYyyymmdd(sCurrentDate);
                                        sCurrentTime = new Date().getHours() + ":" + new Date().getMinutes() + ":00";
                                        $.UIbyID("tmEndTime").setValue(sCurrentTime);
                                    }
                                }
                            }),
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://sys-cancel",
                                press: function(){
                                    $.UIbyID("dtEndDate").setYyyymmdd("");
                                    $.UIbyID("tmEndTime").setValue("");
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_StopGRreason"), //"Gruppo causali di fermo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbGRreasonInput,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://sys-cancel",
                                press: function(){
                                    $.UIbyID("cmbGRreasonInput").setSelectedKey("");
                                    $.UIbyID("cmbReasonInput").getModel().loadData(
                                        QService + dataProdMD + "Stopreas/getStopReasonsByGroupSQ"
                                    );
                                    $.UIbyID("cmbReasonInput").setSelectedKey("");
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_StopReason"), //"Causale di fermo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbReasonInput,
                            new sap.ui.commons.Button({
                                width: "60px",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                icon: "sap-icon://sys-cancel",
                                press: function(){
                                    $.UIbyID("cmbReasonInput").setSelectedKey("");
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("StopsPanelTab", "NOTE"):"",
                                maxLength: 255,
                                id: "NOTE"
                            })
                        ]
                    })
                ]
            })
        ]});

    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Save"), //"Salva",
        press:function(){
            fnSaveStop(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));
    oEdtDlg.open();
}

// Funzione per salvare le modifiche al fermo o inserirne uno nuovo
function fnSaveStop(bEdit)
{
    var bContinue = true; //flag che indica se procedere o meno

    if(bEdit === "")
        bEdit = false;

    var sLine = $.UIbyID("cmbLineInput").getSelectedKey();
    if(sLine === ""){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_SelectLine"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_Monitor.getText("Monitor_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        return;
    }

    var sMachine = $.UIbyID("cmbMachineInput").getSelectedKey();
    /*var sState = $.UIbyID("cmbStateInput").getSelectedKey();
    if(sState === ""){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_SelectState"), //Selezionare uno stato
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_Monitor.getText("Monitor_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        return;
    }*/

    //flag per verificare se è stata impostata o meno una data/ora di fine fermo
    if($.UIbyID("dtEndDate").getValue() === "" || $.UIbyID("tmEndTime").getValue() === "")
        bStopEnd = false;

    //La data/ora viene presa fino ai minuti
    var sFromDateTime = dtFormat.format(dtSelect.parse($.UIbyID("dtStartDate").getYyyymmdd())) + " " +
        tmFormat.format($.UIbyID("tmStartTime").getDateValue()) /*$.UIbyID("tmStartTime").getValue()*/;
    var sToDateTime = dtFormat.format(dtSelect.parse($.UIbyID("dtEndDate").getYyyymmdd())) + " " +
        tmFormat.format($.UIbyID("tmEndTime").getDateValue()) /*$.UIbyID("tmEndTime").getValue()*/;

    var iFl_Man = 2;
    if(bEdit){
        //se si sta attuando una modifica allora si vanno ad aggiungere i secondi del record originale alla data/ora d'inizio
        sFromDateTime = sFromDateTime + ":" + catchSeconds(fnGetCellValue("StopsPanelTab", "DATE_FROM"));

        //se si sta attuando una modifica ma la data di fine fermo non viene cambiata allora i secondi rimangono quelli di prima
        if(sToDateTime.localeCompare(
            fnGetCellValue("StopsPanelTab", "DATE_TO").substring(0,10) + " " +
            fnGetCellValue("StopsPanelTab", "DATE_TO").substring(11,16))== 0
          ){
            sToDateTime = sToDateTime + ":" + catchSeconds(fnGetCellValue("StopsPanelTab", "DATE_TO"));
        }
        else{ //in caso contrario (data fine fermo modificata) i secondi vengono impostati a ':00'
            sToDateTime = sToDateTime + ":00";
        }

        //se si sta facendo una modifica, l'inserimento sarà sempre quello determinato in fase d'inserimento
        iFl_Man = parseInt(fnGetCellValue("StopsPanelTab", "FL_MAN"));
    }
    else{
        //se si sta facendo un'aggiunta, i secondi di inizio e fine saranno sempre a ':00'
        sFromDateTime = sFromDateTime + ":00";
        sToDateTime = sToDateTime + ":00";

        //se si sta facendo un'aggiunta, l'inserimento sarà sempre manuale
        iFl_Man = 1;
    }

    //controllo per verificare se è stata impostata o meno una data/ora di fine fermo
    if($.UIbyID("dtEndDate").getValue() === "" || $.UIbyID("tmEndTime").getValue() === "")
        sToDateTime = "";

    console.log("string Data inizio: " + sFromDateTime);
    console.log("string Data fine: " + sToDateTime);

    if(new Date(sFromDateTime) >= new Date(sToDateTime)){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_ErrorDate"),
            //La data/ora di fine fermo dev'essere superiore alla data/ora di inizio fermo
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_Monitor.getText("Monitor_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        return;
    }

    //Imposto l'operatore
    //Se si sta modificando un record già esistente allora l'operatore rimane quello già presente in tabella
    //Se si sta aggiungendo un nuovo record, da backoffice non è possibile settare un operatore
    //          la transazione si occuperà di recuperare l'utente MII che sta eseguendo l'operazione per tenerlo tracciato
    var sOpeID = bEdit?fnGetCellValue("StopsPanelTab", "OPEID"):"";
    var sNotes = $.UIbyID("NOTE").getValue();

    var sReason = $.UIbyID("cmbReasonInput").getSelectedKey();

    var qexe = dataProdStop + "saveStopQR";
    qexe += "&Param.1=" + sLine + //IDLINE
            "&Param.2=" + sFromDateTime + //DATE_FROM
            "&Param.3=" + "" + //DATE_SHIFT
            "&Param.4=" + "" + //SHIFT
            "&Param.5=" + sToDateTime + //DATE_TO
            "&Param.6=" + sReason + //REASON
            "&Param.7=" + "" + //STATE
            "&Param.8=" + "" + //LASTEVT
            "&Param.9=" + sMachine + //IDMACH
            "&Param.10=" + sOpeID + //OPEID
            "&Param.11=" + encodeURIComponent(sNotes) + //NOTE
            "&Param.12=" + sLanguage + //LANGUAGE
            "&Param.13=" + iFl_Man + //FL_MAN
            "&Param.14=" + (bEdit === true ? "1" : "0") + //UPDATE
            "&Param.15=" + "1"; //flag che indica che la chiamata avviene da back-office (per il recupero dell'utente loggato)

    if(sReason === "" && !($.UIbyID("cmbGRreasonInput").getSelectedKey() === "")){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_MissingStopReasonMessage"), //"Attenzione: è stato selezionato un gruppo di causali di fermo ma non è stata specificata la causale; se si procede non verrà salvata alcuna causale. Continuare?",
            sap.ui.commons.MessageBox.Icon.WARNING,
            oLng_Monitor.getText("Monitor_MissingStopReason"), //"Causale di fermo mancante",
            [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
            function(sResult){
                if (sResult == 'YES'){
                    executeSavingQuery(qexe);
                }
            },
            sap.ui.commons.MessageBox.Action.YES
        );
    }
    else{
        executeSavingQuery(qexe);
    }

    /*

    ---     ATTENZIONE: questo blocco è stato spostato nella funzione executeSavingQuery()

    var qexe = dataProdStop + "saveStopQR";
    qexe += "&Param.1=" + sLine + //IDLINE
            "&Param.2=" + sFromDateTime + //DATE_FROM
            "&Param.3=" + "" + //DATE_SHIFT
            "&Param.4=" + "" + //SHIFT
            "&Param.5=" + sToDateTime + //DATE_TO
            "&Param.6=" + sReason + //REASON
            "&Param.7=" + "" + //STATE
            "&Param.8=" + "" + //LASTEVT
            "&Param.9=" + sMachine + //IDMACH
            "&Param.10=" + sOpeID + //OPEID
            "&Param.11=" + sNotes + //NOTE
            "&Param.12=" + sLanguage + //LANGUAGE
            "&Param.13=" + iFl_Man + //FL_MAN
            "&Param.14=" + (bEdit === true ? "1" : "0") + //UPDATE
            "&Param.15=" + "1"; //flag che indica che la chiamata avviene da back-office (per il recupero dell'utente loggato)

    console.log("query: " + qexe);
    var queryResult = fnGetAjaxVal(qexe,["code","message"],false);
    var iCheckResult = parseInt(queryResult.code);

    console.log("code: " + queryResult.code);
    console.log("message: " + queryResult.message);

    if(isNaN(iCheckResult) == false){
        if(iCheckResult < 0){
            sap.ui.commons.MessageBox.show(
                oLng_Monitor.getText("Monitor_StopsAlreadyExist"), //"L'intervallo selezionato si sovrappone con altri già esistenti",
                sap.ui.commons.MessageBox.Icon.INFORMATION,
                oLng_Monitor.getText("Monitor_Error"), //"Errore",
                [sap.ui.commons.MessageBox.Action.OK],
                function(sResult){},
                sap.ui.commons.MessageBox.Action.OK
            );
        }
        else{
            $.UIbyID("dlgStop").close();
            $.UIbyID("dlgStop").destroy();
            refreshTabStopsPanel();
        }
    }*/
}

function executeSavingQuery(qexe){

    console.log("query: " + qexe);

    var queryResult = fnGetAjaxVal(qexe,["code","message"],false);
    var iCheckResult = parseInt(queryResult.code);

    console.log("code: " + queryResult.code);
    console.log("message: " + queryResult.message);
    /*
    if (ret){
        $.UIbyID("dlgOperator").close();
        $.UIbyID("dlgOperator").destroy();
        refreshTabOperator();
    }*/

    if(isNaN(iCheckResult) == false){
        if(iCheckResult < 0){
            sap.ui.commons.MessageBox.show(
                oLng_Monitor.getText("Monitor_StopsAlreadyExist"), //"L'intervallo selezionato si sovrappone con altri già esistenti",
                sap.ui.commons.MessageBox.Icon.INFORMATION,
                oLng_Monitor.getText("Monitor_Error"), //"Errore",
                [sap.ui.commons.MessageBox.Action.OK],
                function(sResult){},
                sap.ui.commons.MessageBox.Action.OK
            );
        }
        else{
            $.UIbyID("dlgStop").close();
            $.UIbyID("dlgStop").destroy();
            refreshTabStopsPanel();
        }
    }
}

function dateFormatter(sDate){
    console.log(sDate);
    if(Date.parse(sDate))
        return sDate.substring(0, 10);
    else
        return '-';
}

function timeFormatter(sTime){
    console.log(sTime);
    if(Date.parse(sTime))
        return sTime.substring(11, 19);
    else
        return '-';
}

function catchSeconds(sDateTime){
    if(Date.parse(sDateTime))
        return sDateTime.substring(17, 19);
    else
        return '-';
}

function fnDelStop(){
    
    if(fnGetCellValue("StopsPanelTab", "FL_MAN") === "0") {
        //Se il fermo è automatico può essere cancellato solo con doppia conferma
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_AutStopDeleting"),
            sap.ui.commons.MessageBox.Icon.WARNING,
            oLng_Monitor.getText("Monitor_Warning"),
            [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
            function(sResult){
                if (sResult == 'YES'){
                    sap.ui.commons.MessageBox.show(
                        oLng_Monitor.getText("Monitor_StopDeleting"), //"Eliminare il fermo selezionato?",
                        sap.ui.commons.MessageBox.Icon.WARNING,
                        oLng_Monitor.getText("Monitor_DeletingConfirm"), //"Conferma eliminazione",
                        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
                        function(sResult){
                            if (sResult == 'YES'){
                                var qexe = dataProdStop + "delStopSQ" +
                                    "&Param.1=" + fnGetCellValue("StopsPanelTab", "IDLINE") +
                                    "&Param.2=" + fnGetCellValue("StopsPanelTab", "DATE_FROM");
                                
                                console.log("query: " + qexe);
                                var ret = fnSQLQuery(
                                    qexe,
                                    oLng_Monitor.getText("Monitor_StopDeleted"), //"Fermo eliminato correttamente",
                                    oLng_Monitor.getText("Monitor_StopDeletedError"), //"Errore in eliminazione fermo",
                                    false
                                );
                                refreshTabStopsPanel();
                                $.UIbyID("btnModStop").setEnabled(false);
                                $.UIbyID("btnDelStop").setEnabled(false);
                                $.UIbyID("StopsPanelTab").setSelectedIndex(-1);
                            }
                        },
                        sap.ui.commons.MessageBox.Action.YES
                    );
                }
            },
            sap.ui.commons.MessageBox.Action.YES
        );
    } else {
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_StopDeleting"), //"Eliminare il fermo selezionato?",
            sap.ui.commons.MessageBox.Icon.WARNING,
            oLng_Monitor.getText("Monitor_DeletingConfirm"), //"Conferma eliminazione",
            [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
            function(sResult){
                if (sResult == 'YES'){
                    var qexe = dataProdStop + "delStopSQ" +
                        "&Param.1=" + fnGetCellValue("StopsPanelTab", "IDLINE") +
                        "&Param.2=" + fnGetCellValue("StopsPanelTab", "DATE_FROM");
                    
                    console.log("query: " + qexe);
                    var ret = fnSQLQuery(
                        qexe,
                        oLng_Monitor.getText("Monitor_StopDeleted"), //"Fermo eliminato correttamente",
                        oLng_Monitor.getText("Monitor_StopDeletedError"), //"Errore in eliminazione fermo",
                        false
                    );
                    refreshTabStopsPanel();
                    $.UIbyID("btnModStop").setEnabled(false);
                    $.UIbyID("btnDelStop").setEnabled(false);
                    $.UIbyID("StopsPanelTab").setSelectedIndex(-1);
                }
            },
            sap.ui.commons.MessageBox.Action.YES
        );
    }
}
