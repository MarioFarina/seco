var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/json&QueryTemplate=ProductionMain/MasterData/";
var dataProdRPT = "Content-Type=text/json&QueryTemplate=ProductionMain/Report/";

// function getLines() {
//     return this.getViewModel("LINES", QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + this.getView().byId("departments").getSelectedKey());
// }
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/xml/XMLModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/DateFormat",
    "sap/m/MessageBox",
    'sap/ui/export/Spreadsheet'
], function (Controller, XmlModel, JSONModel, DateFormat, MessageBox, Spreadsheet) {
    "use strict";
    return Controller.extend("seco.ui.app.seco.controller.App", {
        pathToExport: null,
        pIndex: null,
        oVizFrame: null,
        settingsVizFrame1: {
            dimensions: {
                Medium: [{
                    name: 'PERIOD',
                    value: "{PERIOD}",
                }]
            },
            measures: [{
                name: 'Produttività',
                value: '{OEE_P}'
            }, {
                name: 'Disponibilità',
                value: '{OEE_D}'
            }, {
                name: 'Qualità',
                value: '{OEE_Q}'
            }]
        },
        settingsVizFrame2: {
            dataset: {
                values: [{
                    value: ["OEE"]
                }]
            },
            dimensions: {
                Medium: [{
                    name: 'PERIOD',
                    value: "{PERIOD}",
                }]
            },
            measures: [{
                name: 'OEE',
                value: '{OEE}'
            }]
        },

        getViewModel: function (modelName, query) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var oModel = new JSONModel();
                oModel.loadData(query);
                oModel.attachRequestCompleted(function () {
                    var rows = oModel.oData.Rowsets.Rowset[0].Row;
                    if (typeof rows === "undefined") {
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    } else {
                        var data = {};
                        data[modelName] = typeof rows.length !== "undefined" ? rows : [rows];
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    }
                }.bind(self));
            })
        },

        initViewModels: function () {
            var self = this;
            var view = this.getView();
            let oPeriods = new JSONModel();
            oPeriods.setData({
                "PERIODS": [{"NAME": "DAY", "TEXT": "Giorno"}, {
                    "NAME": "WEEK",
                    "TEXT": "Settimana"
                }, {"NAME": "MONTH", "TEXT": "Mese"}]
            });
            this.getView().setModel(oPeriods, "PERIODS");
            this.getPlants().then(function () {
                this.getView().byId("plants").setSelectedKey(this.getView().getModel("PLANTS").oData.PLANTS[0].PLANT);
                this.getView().byId("periods").setSelectedKey("DAY");
                this.getDepartments().then(function () {
                    view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);

                    // getLines();
                    new Promise(function (resolve, reject) {
                        var oModel = new JSONModel();
                        oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                        oModel.attachRequestCompleted(function () {
                            var rows = oModel.oData.Rowsets.Rowset[0].Row;
                            if (typeof rows === "undefined") {
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            } else {
                                var data = {};
                                data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            }
                            view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                        }.bind(self));
                    });
                });

            }.bind(this));
        },

        getPlants: function () {
            return this.getViewModel("PLANTS", QService + dataProdMD + "Plants/getPlantsByUserQR");
        },

        getDepartments: function () {
            return this.getViewModel("DEPARTMENTS", QService + dataProdMD + "Departments/getDepartmentsByPlantSQ&Param.1=" + this.getView().byId("plants").getSelectedKey());
        },

        getLines: function () {
            return this.getViewModel("LINES", QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + this.getView().byId("departments").getSelectedKey());
        },

        initChart: function () {
            //var json = {"milk":[{"Week":"Week 1 - 4","Revenue":431000.22,"Cost":230000,"Cost1":24800.63,"Cost2":205199.37,"Cost3":199999.37,"Target":500000,"Budget":210000},{"Week":"Week 5 - 8","Revenue":494000.3,"Cost":238000,"Cost1":99200.39,"Cost2":138799.61,"Cost3":200199.37,"Target":500000,"Budget":224000},{"Week":"Week 9 - 12","Revenue":491000.17,"Cost":221000,"Cost1":70200.54,"Cost2":150799.46,"Cost3":80799.46,"Target":500000,"Budget":238000},{"Week":"Week 13 - 16","Revenue":536000.34,"Cost":280000,"Cost1":158800.73,"Cost2":121199.27,"Cost3":108800.46,"Target":500000,"Budget":252000},{"Week":"Week 17 - 20","Revenue":675000,"Cost":230000,"Cost1":140000.91,"Cost2":89999.09,"Cost3":100099.09,"Target":600000,"Budget":266000},{"Week":"Week 21 - 24","Revenue":680000,"Cost":250000,"Cost1":172800.15,"Cost2":77199.85,"Cost3":57199.85,"Target":600000,"Budget":280000},{"Week":"Week 25 - 28","Revenue":659000.14,"Cost":325000,"Cost1":237200.74,"Cost2":87799.26,"Cost3":187799.26,"Target":600000,"Budget":294000},{"Week":"Week 29 - 32","Revenue":610000,"Cost":350000,"Cost1":243200.18,"Cost2":106799.82,"Cost3":206799.82,"Target":600000,"Budget":308000},{"Week":"Week 33 - 37","Revenue":751000.83,"Cost":390000,"Cost1":280800.24,"Cost2":109199.76,"Cost3":209199.76,"Target":600000,"Budget":322000},{"Week":"Week 38 - 42","Revenue":800000.63,"Cost":450000,"Cost1":320000.08,"Cost2":129999.92,"Cost3":409199.76,"Target":700000,"Budget":336000},{"Week":"Week 43 - 47","Revenue":881000.19,"Cost":480000,"Cost1":360800.09,"Cost2":119199.91,"Cost3":210109.01,"Target":700000,"Budget":350000},{"Week":"Week 47 - 52","Revenue":904000.04,"Cost":560000,"Cost1":403200.08,"Cost2":156799.92,"Cost3":139199.01,"Target":700000,"Budget":364000}]};
            //var json = {"OEE":[{"DAY":"2019-04-01T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"0.623712","OEE_P":"0.89","OEE_D":"0.73","OEE_Q":"0.96"},{"DAY":"2019-04-02T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"0.623712","OEE_P":"0.89","OEE_D":"0.73","OEE_Q":"0.96"},{"DAY":"2019-04-03T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"0.623712","OEE_P":"0.89","OEE_D":"0.73","OEE_Q":"0.96"},{"DAY":"2019-04-04T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"0.623712","OEE_P":"0.89","OEE_D":"0.73","OEE_Q":"0.96"},{"DAY":"2019-04-05T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"0.623712","OEE_P":"0.89","OEE_D":"0.73","OEE_Q":"0.96"},{"DAY":"2019-04-08T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"1.18144","OEE_P":"1.42","OEE_D":"1.04","OEE_Q":"0.8"},{"DAY":"2019-04-09T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"1.18144","OEE_P":"1.42","OEE_D":"1.04","OEE_Q":"0.8"},{"DAY":"2019-04-10T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"1.18144","OEE_P":"1.42","OEE_D":"1.04","OEE_Q":"0.8"},{"DAY":"2019-04-11T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"1.18144","OEE_P":"1.42","OEE_D":"1.04","OEE_Q":"0.8"},{"DAY":"2019-04-12T00:00:00","LINE":"SHT-L1","MATERIAL":"","OEE":"1.18144","OEE_P":"1.42","OEE_D":"1.04","OEE_Q":"0.8"}]};
            //this.getView().setModel(new JSONModel(json), "OEE");
            this.getView().setModel(new JSONModel({"OEE": 90, "OEE_P": 20, "OEE_Q": 30, "OEE_D": 45}), "RADIAL");
            console.log(this.getView().getModel("RADIAL"));
        },

        clearChartData: function () {
            this.getView().setModel(new JSONModel({}), "OEE");
            this.getView().setModel(new JSONModel({}), "RADIAL");
        },

        getQueryParameters: function () {
            var params = [];
            var line, from, to, material = "";
            var view = this.getView();
            from = view.byId("TurnoDa").getDateValue();
            to = view.byId("TurnoA").getDateValue();
            // line = view.byId("lines").getSelectedKey();
            line = this.getView().byId("lines").getSelectedKey();
            material = view.byId("Material").getValue().length > 0 ? view.byId("Material").getValue() : "%25";
            if (from instanceof Date && to instanceof Date && line !== "") {
                params = [from, to, line, material];
            }
            return params;
        },

        onSelectionChange: function (oEvent) {
            var path = oEvent.getParameter('listItem').getBindingContext().getPath()
            var selectedRow = this.byId('tblDataset').getModel().getProperty(path);

        },

        formatQueryParameters: function (params) {
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var view = this.getView();
            var query = "";
            query += "Param.1=" + oDateFormatter.format(params[0]);
            query += "&Param.2=" + oDateFormatter.format(params[1]);
            query += "&Param.3=" + params[2];
            query += "&Param.4=" + params[3];
            return query;
        },

        getColor: function (data) {
            if (data <= 45) {
                return "#FF0000";
            } else if (data > 45 && data <= 70) {
                return "#E1A84F";
            } else {
                return "#00FF00";
            }
        },

        getValueColor: function (data) {
            if (data <= 45) {
                return "Error";
            } else if (data > 45 && data <= 70) {
                return "Critical";
            } else {
                return "Good";
            }
        },

        /**********************************
         * CONTROLLER LIFE-CYCLE
         **********************************/
        onInit: function () {
            var view = this.getView();
            var now = new Date();
            var past = new Date();
            past.setDate(past.getDate() - 7);
            this.initViewModels();

            //SET DATE RANGE DEFAULT
            view.byId("TurnoA").setDateValue(now);
            view.byId("TurnoDa").setDateValue(past);

            // this.getLines().then(function () {
            //     view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].IDLINE);
            // });
        },

        onPlantChanged: function () {
            var view = this.getView();
            this.getDepartments().then(function () {
                view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);
                new Promise(function (resolve, reject) {
                    var oModel = new JSONModel();
                    oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                    oModel.attachRequestCompleted(function () {
                        var rows = oModel.oData.Rowsets.Rowset[0].Row;
                        if (typeof rows === "undefined") {
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        } else {
                            var data = {};
                            data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        }
                        view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                    }.bind(self));
                });
            });
        },

        onRepartoChanged: function () {
            var view = this.getView();
            this.getLines().then(function () {
                view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
            });
        },

        onSearch: function () {
            var oModel = new JSONModel();
            var params = this.getQueryParameters();
            if (params.length === 0) {
                MessageBox.warning(
                    "Inserire tutti i campi obbligatori: Linea, Inizio Periodo, Fine range.",
                    {
                        styleClass: "sapUiSizeCompact"
                    }
                );
                return;
            }
            var queryString = this.formatQueryParameters(params);

            this.getView().byId("radialContainer").setVisible(false);
            this.getView().byId("oChartContainer1").setVisible(false);
            this.getView().byId("oChartContainer2").setVisible(false);
            this.getView().byId("tblDataset").setVisible(false);

            this.clearChartData();
            oModel.loadData(QService + dataProdRPT + "OEE_Periodo/getOEEperiodQR&" + queryString);
            oModel.attachRequestCompleted(function () {
                var rowsets = oModel.oData.Rowsets.Rowset;
                var periods = ["DAY", "MONTH", "WEEK"];
                var map = ["DAY", "YEARMONTH", "WEEK"];
                var mapFormat = ["dd/MM/yyyy", "MM/yyyy", "WEEK"];
                var selectedKey = this.getView().byId("periods").getSelectedKey();
                var periodIdx = periods.indexOf(selectedKey);
                this.pIndex = periodIdx;
                var rows = rowsets[periodIdx].Row;
                var rows1 = rowsets[periodIdx].Row;
                var columns = rowsets[periodIdx];
                var result = [];
                var view = this.getView();
                var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: mapFormat[periodIdx]});

                for (var r = 0; r < rows.length; r++) {
                    rows[r].PERIOD = rows[r][map[periodIdx]];
                    if (map[periodIdx] !== "WEEK") {
                        rows[r].PERIOD = oDateFormat.format(new Date(rows[r][map[periodIdx]]));
                    }
                    rows[r].OEE_CLR = this.getColor(rows[r].OEE);
                    rows[r].OEE_P_CLR = this.getColor(rows[r].OEE_P);
                    rows[r].OEE_Q_CLR = this.getColor(rows[r].OEE_Q);
                    rows[r].OEE_D_CLR = this.getColor(rows[r].OEE_D);

                    // set Data to String
                    rows[r].OEE = rows[r].OEE + "";
                    rows[r].OEE_P = rows[r].OEE_P + "";
                    rows[r].OEE_Q = rows[r].OEE_Q + "";
                    rows[r].OEE_D = rows[r].OEE_D + "";

                    // set Data for Table to FLOAT
                    rows1[r].OEE = parseFloat(rows1[r].OEE);
                    rows1[r].OEE_P = parseFloat(rows1[r].OEE_P);
                    rows1[r].OEE_Q = parseFloat(rows1[r].OEE_Q);
                    rows1[r].OEE_D = parseFloat(rows1[r].OEE_D);

                    rows1[r].OEE_CLR = this.getValueColor(rows1[r].OEE);
                    rows1[r].OEE_P_CLR = this.getValueColor(rows1[r].OEE_P);
                    rows1[r].OEE_Q_CLR = this.getValueColor(rows1[r].OEE_Q);
                    rows1[r].OEE_D_CLR = this.getValueColor(rows1[r].OEE_D);
                }
                // initialize combined chart model
                // this.getView().setModel(new JSONModel({"OEE": rows}), "OEE");
                // this.oVizFrame = this.getView().byId("periodChart");
                // this.getView().byId("periodChart").setVizProperties({
                //     title: {
                //         text: "Analisi Periodo"
                //     },
                //     plotArea: {
                //         colorPalette: ["#3CF67D", "#20C2FF", "#F3A915", "#CECEC0"]
                //     }
                // });
                // //initialize radial chart model
                // var radialData = oModel.oData.Rowsets.Rowset.length === 4 ? oModel.oData.Rowsets.Rowset[3].Row : [];
                // if (radialData.length > 0) {
                //     this.getView().setModel(new JSONModel({
                //         "OEE_P": radialData[0].OEE_P,
                //         "OEE_D": radialData[0].OEE_D,
                //         "OEE_Q": radialData[0].OEE_Q,
                //         "OEE": radialData[0].OEE
                //     }), "RADIAL");
                //     this.getView().setModel(new JSONModel({
                //         OEE_P: this.getColor(radialData[0].OEE_P),
                //         OEE_D: this.getColor(radialData[0].OEE_D),
                //         OEE_Q: this.getColor(radialData[0].OEE_Q),
                //         OEE: this.getColor(radialData[0].OEE)
                //     }), "COLORS");
                //     this.getView().byId("radialContainer").setVisible(true);
                // }

                // Primo ISTOGRAMMA
                this.oVizFrame = this.getView().byId("periodChart").setModel(new JSONModel(this.settingsVizFrame1));
                this.oVizFrame.setVizProperties({
                    plotArea: {
                        showGap: true,
                        dataLabel: {
                            visible: true
                        }
                    },
                    valueAxis: {
                        label: {},
                        title: {
                            visible: false
                        }
                    },
                    categoryAxis: {
                        title: {
                            visible: false
                        }
                    },
                    title: {
                        visible: true,
                        text: "Grafico andamento generale nel periodo"
                    }
                });
                // SETTA LE TILE
                var radialData = oModel.oData.Rowsets.Rowset.length === 4 ? oModel.oData.Rowsets.Rowset[3].Row : [];
                if (radialData.length > 0) {
                    this.getView().setModel(new JSONModel({
                        "OEE_P": radialData[0].OEE_P,
                        "OEE_D": radialData[0].OEE_D,
                        "OEE_Q": radialData[0].OEE_Q,
                        "OEE": radialData[0].OEE
                    }), "RADIAL");
                    this.getView().setModel(new JSONModel({
                        OEE_P: this.getColor(radialData[0].OEE_P),
                        OEE_D: this.getColor(radialData[0].OEE_D),
                        OEE_Q: this.getColor(radialData[0].OEE_Q),
                        OEE: this.getColor(radialData[0].OEE)
                    }), "COLORS");
                }
                var modelVizFrame1 = new JSONModel({});
                modelVizFrame1.setData(rows);
                this.oVizFrame.setModel(modelVizFrame1);

                // SECONDO ISTOGRAMMA
                var oVizFrame2 = this.getView().byId("periodChart2");
                oVizFrame2.setModel(new JSONModel(this.settingsVizFrame2));
                oVizFrame2.setVizProperties({
                    plotArea: {
                        showGap: true,
                        dataLabel: {
                            visible: true
                        },
                        dataPointStyle: {
                            "rules":
                                [

                                    {
                                        "dataContext": {"OEE": {"max": 45}},
                                        "properties": {
                                            "color": "sapUiChartPaletteSemanticBad"
                                        },
                                        "displayName": "OEE < 45 "
                                    },
                                    {
                                        "dataContext": {"OEE": {"max": 70}},
                                        "properties": {
                                            "color": "sapUiChartPaletteSemanticCritical"
                                        },
                                        "displayName": "45 < OEE <= 70"
                                    }
                                ],
                            "others":
                                {
                                    "properties": {
                                        "color": "sapUiChartPaletteSemanticGood"
                                    },
                                    "displayName": "OEE > 70"
                                }
                        }
                    },
                    valueAxis: {
                        label: {},
                        title: {
                            visible: false
                        }
                    },
                    categoryAxis: {
                        title: {
                            visible: false
                        }
                    },
                    title: {
                        visible: true,
                        text: "Grafico andamento OEE nel periodo"
                    }
                });
                oVizFrame2.setModel(modelVizFrame1);

                //reset table columns
                // var oTable = view.byId("tblDataset");
                // var cells = [];
                // columns = columns.Columns.Column;
                // oTable.destroyColumns();
                // for (var c = 0; c < columns.length; c++) {
                //     oTable.addColumn(new sap.m.Column({
                //         width: "200px",
                //         header: new sap.m.Label({text: columns[c].Name})
                //     }));
                //     if (columns[c].Name.indexOf("OEE") === -1) {
                //         cells.push(new sap.m.Text({text: "{DATASET>" + columns[c].Name + "}"}));
                //     } else {
                //         cells.push(new sap.suite.ui.microchart.RadialMicroChart({
                //             size: sap.m.Size.S,
                //             percentage: "{= ${DATASET>" + columns[c].Name + "} * 1}",
                //             valueColor: "{= ${DATASET>" + columns[c].Name + "_CLR} }"
                //         }));
                //     }
                // }
                // this.getView().setModel(new JSONModel({"rows": rows}), "DATASET");
                // oTable.bindItems("DATASET>/rows", new sap.m.ColumnListItem({cells: cells}));
                switch (periodIdx) {
                    case 0: {
                        this.getView().byId("DAY").setVisible(true);
                        this.getView().byId("WEEK").setVisible(false);
                        this.getView().byId("MONTH").setVisible(false);
                        break;
                    }
                    case 1: {
                        this.getView().byId("DAY").setVisible(false);
                        this.getView().byId("WEEK").setVisible(false);
                        this.getView().byId("MONTH").setVisible(true);
                        break;
                    }
                    case 2: {
                        this.getView().byId("DAY").setVisible(false);
                        this.getView().byId("WEEK").setVisible(true);
                        this.getView().byId("MONTH").setVisible(false);
                        break;
                    }
                }
                this.getView().byId("tblDataset").setModel(new JSONModel(rows1));
                this.pathToExport = rows1;

                this.getView().byId("radialContainer").setVisible(true);
                this.getView().byId("oChartContainer1").setVisible(true);
                this.getView().byId("oChartContainer2").setVisible(true);
                this.getView().byId("tblDataset").setVisible(true);

                // PAGINATION (IMMAGINO)
                window.dataset = rows1;
                window.loadPageByElement = this.loadPageByElement.bind(this);
                this.loadPage(0);

            }.bind(this));
        },

        onOee_pSelected: function (oEvent) {
            var val = oEvent.getParameter("selected");
            if (val) {
                var feedValueAxis = this.getView().byId('valueAxisFeed1');
                this.oVizFrame.addFeed(feedValueAxis);
            } else {
                if (this.getView().byId("oee_dCheckBox").getSelected() || this.getView().byId("oee_qCheckBox").getSelected()) {
                    var feedValueAxis = this.getView().byId('valueAxisFeed1');
                    this.oVizFrame.removeFeed(feedValueAxis);
                } else {
                    this.getView().byId("oee_pCheckBox").setSelected(true);
                }
            }
        },
        onOee_dSelected: function (oEvent) {
            var val = oEvent.getParameter("selected");
            if (val) {
                var feedValueAxis = this.getView().byId('valueAxisFeed2');
                this.oVizFrame.addFeed(feedValueAxis);
            } else {
                if (this.getView().byId("oee_pCheckBox").getSelected() || this.getView().byId("oee_qCheckBox").getSelected()) {
                    var feedValueAxis = this.getView().byId('valueAxisFeed2');
                    this.oVizFrame.removeFeed(feedValueAxis);
                } else {
                    this.getView().byId("oee_dCheckBox").setSelected(true);
                }
            }
        },
        onOee_qSelected: function (oEvent) {
            var val = oEvent.getParameter("selected");
            if (val) {
                var feedValueAxis = this.getView().byId('valueAxisFeed3');
                this.oVizFrame.addFeed(feedValueAxis);
            } else {
                if (this.getView().byId("oee_dCheckBox").getSelected() || this.getView().byId("oee_pCheckBox").getSelected()) {
                    var feedValueAxis = this.getView().byId('valueAxisFeed3');
                    this.oVizFrame.removeFeed(feedValueAxis);
                } else {
                    this.getView().byId("oee_qCheckBox").setSelected(true);
                }
            }
        },

        columnsToExport: function () {
            var columnsName = {};
            switch (this.pIndex) {
                case 0: {
                    columnsName = {
                        columnsName: [
                            "Data Ora",
                            "Linea",
                            "Materiale",
                            "OEE",
                            "Produttività",
                            "Disponibilità",
                            "Quantità"
                        ],
                        columnsField: [
                            "DAY",
                            "LINE",
                            "MATERIAL",
                            "OEE",
                            "OEE_P",
                            "OEE_D",
                            "OEE_Q"
                        ]
                    };
                    break;
                }
                case 1: {
                    columnsName = {
                        columnsName: [
                            "Anno Mese",
                            "Linea",
                            "Materiale",
                            "OEE",
                            "Produttività",
                            "Disponibilità",
                            "Quantità"
                        ],
                        columnsField: [
                            "YEARMONTH",
                            "LINE",
                            "MATERIAL",
                            "OEE",
                            "OEE_P",
                            "OEE_D",
                            "OEE_Q"
                        ]
                    };
                    break;
                }
                case 2: {
                    columnsName = {
                        columnsName: [
                            "Settimana",
                            "Linea",
                            "Materiale",
                            "OEE",
                            "Produttività",
                            "Disponibilità",
                            "Quantità"
                        ],
                        columnsField: [
                            "WEEK",
                            "LINE",
                            "MATERIAL",
                            "OEE",
                            "OEE_P",
                            "OEE_D",
                            "OEE_Q"
                        ]
                    };
                    break;
                }
            }
            var columnsToExport = [];
            for (var i = 0; i < columnsName.columnsName.length; i++) {
                columnsToExport.push({
                    label: columnsName.columnsName[i],
                    property: columnsName.columnsField[i],
                });
            }
            return columnsToExport;
        },

        exportxls: function () {
            var aCols, oSettings, oSheet;

            aCols = this.columnsToExport();
            var oModelRowset1 = new JSONModel();
            oModelRowset1.setData(this.pathToExport);

            oSettings = {
                workbook: {columns: aCols},
                dataSource: oModelRowset1.getProperty('/')
            };

            oSheet = new Spreadsheet(oSettings);
            oSheet.build()
                .then(function () {
                    MessageToast.show('Spreadsheet export has finished');
                })
                .finally(function () {
                    oSheet.destroy();
                });
        },


        // PAGINATION
        pageSize: 5,
        getPage: function (pageNumber) {
            var offset = this.pageSize * pageNumber;
            var end = offset + this.pageSize - 1;
            var limit = Math.min(end + 1, window.dataset.length);
            var win = [];
            for (var i = offset; i < limit; i++) {
                win.push(window.dataset[i]);
            }
            return win;
        },

        loadPage: function (pageNumber) {
            var rows = this.getPage(pageNumber);
            this.getView().byId("tblDataset").setModel(new JSONModel(rows));
            this.currentPage = pageNumber;
            this.renderPagination(); //rerender pagination
        },

        renderPagination: function () {
            var pages = Math.ceil(window.dataset.length / this.pageSize);
            document.getElementsByClassName("pagination")[0].innerHTML = "";
            for (var p = 0; p < pages; p++) {
                var index = p + 1;
                var html = "<div class='page-box " + (this.currentPage == p ? "current-page" : "") + "' data-page=" + p + " onclick='loadPageByElement(this)'>" + index + "</div>";
                document.getElementsByClassName("pagination")[0].innerHTML += html;
            }
        },

        loadPageByElement: function (element) {
            this.loadPage(element.dataset.page);
        }
    });
});