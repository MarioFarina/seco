/**
 * DashBoardDetSeco.js
 *
 * 29/05/2019 - MF - primo rilascio
 /*

 /* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/";
var dataMonitorJson = "Content-Type=text/json&QueryTemplate=ProductionMain/";
var PrecLine = 1;

jQuery.ajaxSetup({
    cache: false
});

jQuery.sap.require("sap.ui.core.format.NumberFormat");
jQuery.sap.require("sap.suite.ui.microchart.RadialMicroChart");

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/

$(document).ready(function () {
    var oDetailsPage = createDetailsPage();
    oDetailsPage.placeAt("MasterCont");

    $("#splash-screen").hide();

    setOEEColors();
});

function setOEEColors() {
    var items = sap.ui.getCore().byId("orderListOnPeriodTable").getItems();

    for (i = 0; i < items.length; i++) {
        var item = items[i];
        var cells = item.getCells();

        var oeeValue = cells[8].getText();
        var oeedValue = cells[9].getText();
        var oeepValue = cells[10].getText();
        var oeeqValue = cells[11].getText();

        var green = cells[12].getText();
        var red = cells[12].getText();

        //OEE
        if (parseInt(oeeValue) < parseInt(red))
            cells[8].addStyleClass("redText");
        else if (parseInt(oeeValue) > parseInt(green))
            cells[8].addStyleClass("greenText");
        else cells[8].addStyleClass("yellowText");

        //OEED
        if (parseInt(oeedValue) < parseInt(red))
            cells[9].addStyleClass("redText");
        else if (parseInt(oeedValue) > parseInt(green))
            cells[9].addStyleClass("greenText");
        else cells[9].addStyleClass("yellowText");

        //OEEP
        if (parseInt(oeepValue) < parseInt(red))
            cells[10].addStyleClass("redText");
        else if (parseInt(oeepValue) > parseInt(green))
            cells[10].addStyleClass("greenText");
        else cells[10].addStyleClass("yellowText");

        //OEEQ
        if (parseInt(oeeqValue) < parseInt(red))
            cells[11].addStyleClass("redText");
        else if (parseInt(oeeqValue) > parseInt(green))
            cells[11].addStyleClass("greenText");
        else cells[11].addStyleClass("yellowText");
    }
}

function refreshInfo(oMainPanel) {

    var queryString = new URLSearchParams(window.location.search);
    var sIdLine = queryString.get('idline');
    var sPlant = queryString.get('plant');

    var oModel_oee = new sap.ui.model.xml.XMLModel();
    var sQuery_oee = QService + dataMonitor + "DashBoard/getDashboardSecoQR";
    sQuery_oee += "&Param.1=" + sPlant;
    sQuery_oee += "&Param.3=" + sIdLine;
    oModel_oee.loadData(sQuery_oee, false, false);
    oMainPanel.setModel(oModel_oee, 'model_oee');

    var oModel_orderListOnPeriod = new sap.ui.model.xml.XMLModel();
    var sQuery_orderListOnPeriod = QService + dataMonitor + "DashBoard/getLineOrderListOnPeriodQR";
    sQuery_orderListOnPeriod += "&Param.1=" + sIdLine;
    oModel_orderListOnPeriod.loadData(sQuery_orderListOnPeriod, false, false);
    oMainPanel.setModel(oModel_orderListOnPeriod, 'model_orderListOnPeriod');

    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    if (month.toString().length == 1)
        month = "0" + month;

    var day = new Date().getDay();
    if (day.toString().length == 1)
        day = "0" + day;

    var oModel = new sap.ui.model.json.JSONModel; // sap.ui.model.xml.XMLModel();
    var sQuery = QService + dataMonitorJson + "Report/StatiLinea/GetLineStateAnalisysQR";


    sQuery+= "&Param.1=" + sIdLine;
    sQuery+= "&Param.2=2019-02-15"; // fixme  DATI VERITIERI
    sQuery+= "&Param.3=2019-05-15";
    // sQuery += "&Param.1=" + sIdLine;
    // sQuery += "&Param.2=" + year + "-" + month + "-" + day;
    // sQuery += "&Param.3=" + year + "-" + month + "-" + day;
    // //sQuery += "&Param.1=SMT-L1"; // @todo ripristinare
    // //sQuery += "&Param.2=2019-05-15"; // @todo ripristinare
    // //sQuery += "&Param.3=2019-05-15"; // @todo ripristinare



    oModel.loadData(sQuery);

    oModel.attachRequestCompleted(function () {
        var rowsets = oModel.oData.Rowsets.Rowset;

        var events = [];
        var ranges = {};
        var reasons = [];

        if (typeof rowsets[2] !== "undefined" && typeof rowsets[2].Row !== "undefined") {
            events = rowsets[2].Row
        } else {
            return;
        }
        if (typeof rowsets[3] !== "undefined" && typeof rowsets[3].Row !== "undefined") {
            reasons = rowsets[3].Row
        } else {
            return;
        }

        var mapReasonColors = {};
        //var palette = ['#940000','#C30101', '#560D0D', '#6F0000', '#EA0909', '#F03939', '#F26161'];
        var palette = ['#C30101', '#C30101', '#C30101', '#C30101', '#C30101', '#C30101', '#C30101'];
        for (var i = 0; i < reasons.length; i++) {
            mapReasonColors[reasons[i].Evento] = palette[i];
        }
        //order
        events.sort(function (a, b) {
            return moment(a.DataEvento + " " + a.OraInizio).format("YYMMDDHHmmss") - moment(b.DataEvento + " " + b.OraInizio).format("YYMMDDHHmmss");
        });

        //normalize ranges by date
        for (var e = 0; e < events.length; e++) {
            var a = events[e];
            if (typeof ranges[a.DataEvento] === "undefined") {
                ranges[a.DataEvento] = [];
            }
            ranges[a.DataEvento].push(a);
        }

        //DAY SEGMENTS
        window.ranges = ranges;
        this.mapReasonColors = mapReasonColors;
        renderDaySegments();

    }.bind(this));
}

function pad(number) {
    number = parseInt(number);
    if (number < 10) {
        return "0" + number;
    } else {
        return number;
    }
}

function renderDaySegments() {
    //Day container width
    var containerWidth = "1200"; //$(".filterBar.sapMFlexBox").width() - 100;
    var ranges = window.ranges;
    var mapReasonColors = this.mapReasonColors;
    var htmlDays = "";
    if (window.ranges.length == 0) return;
    $("#day-chart").text("");

    //Legend
    htmlDays += "<br /><div class='day-segment' style='width:" + containerWidth + "px;background-color:white;box-sizing:border-box;border-bottom:1px solid black;margin-bottom:2px;display:inline-block;'>";
    for (var l = 0; l <= 24; l++) {
        var second = l * 3600;
        var offset = (containerWidth * second) / 86400;
        var offsetLabel = offset - 5;
        htmlDays += "<div style='position:absolute;height:5px;width:1px;box-sizing:border-box;border-left:1px solid black;top:0px;left:" + offset + "px' ></div>";
        if (l % 2 == 0 && l !== 24) {
            htmlDays += "<span class='hour-description' style='left:" + offsetLabel + "px'>" + pad(l) + "</span>";
        }
    }
    htmlDays += "</div>";
    $("#day-chart").append(htmlDays);

    //iterate days in order to create day segment
    for (var d in ranges) {
        var lastEnd = "000000";
        //iterate day ranges
        //var daySegment = "<div  class='day-descriptor'>" + d + "</div><div class='day-segment' style='display:inline-block;width:" + containerWidth + "px'>";
        var daySegment = "<div  class='day-descriptor'></div><div class='day-segment' style='display:inline-block;width:" + containerWidth + "px'>";

        for (var r = 0; r < ranges[d].length; r++) {
            var offset = moment(ranges[d][r].OraInizio, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
            var offsetFine = moment(ranges[d][r].OraFine, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds');
            var rangeWidth = offsetFine - offset;
            var color = ranges[d][r].EventType === "FERMO" ? mapReasonColors[ranges[d][r].Causale] : "#19A979";
            ranges[d][r].offset = offset;
            ranges[d][r].rangeWidth = rangeWidth;

            //containerWidth : 86400 = x : seconds
            ranges[d][r].left = (containerWidth * offset) / 86400;
            ranges[d][r].width = (containerWidth * rangeWidth) / 86400;

            daySegment += "<div class='day-event' data-toggle='tooltip' style='background-color:" + color + ";left:" + ranges[d][r].left + "px;width:" + ranges[d][r].width + "px'></div>";
        }
        daySegment += "</div>";
        $("#day-chart").append(daySegment);
    }
}

function createDetailsPage() {
    var sMode = $('#mode').val();
    if (sMode === "{mode}") {
        sMode = "embedded";
    }
    var sWidth = (sMode === "single" ? '1120px' : '100%');
    var sPageClass = (sMode === "single" ? 'SinglePage' : 'noPaddingPanel');

    var percentageFunc = function (sKpField, sColorFIeld, oText) {
        return {
            parts: [
                {path: sKpField},
                {path: sColorFIeld}
            ],
            formatter: function (sPercentage, sColor) {
                var sStyle;
                switch (sColor) {
                    case "Good":
                        sStyle = 'greenText';
                        break;
                    case "Critical":
                        sStyle = 'yellowText';
                        break;
                    case "Error":
                        sStyle = 'redText';
                        break;
                }
                oText.addStyleClass(sStyle);
                var fPercentage = parseFloat(sPercentage).toFixed();
                return parseFloat(fPercentage);
            }
        };
    };

    var valueFunc = function (sValueField) {
        return {
            parts: [
                {path: sValueField}
            ],
            formatter: function (sValue) {
                return parseFloat(sValue);
            }
        };
    };

    var oOEEText = new sap.m.Text({
        width: "100%",
        text: "OEE",
        textAlign: sap.ui.core.TextAlign.Center,
        color: "red"
    }).addStyleClass('boldText');

    var oOEERadial = new sap.suite.ui.microchart.RadialMicroChart({
        valueColor: "{model_oee>/Rowset/Row/OEE_COLOR}",
        tooltip: "OEE"
    }).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEE", "model_oee>/Rowset/Row/OEE_COLOR", oOEEText));

    var oOEEDText = new sap.m.Text({
        width: "100%",
        text: "DIS",
        textAlign: sap.ui.core.TextAlign.Center
    }).addStyleClass('boldText');

    var oOEEDRadial = new sap.suite.ui.microchart.RadialMicroChart({
        valueColor: "{model_oee>/Rowset/Row/OEED_COLOR}",
        tooltip: "Disponibilità"
    }).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEED", "model_oee>/Rowset/Row/OEED_COLOR", oOEEDText));

    var oOEEPText = new sap.m.Text({
        width: "100%",
        text: "REN",
        textAlign: sap.ui.core.TextAlign.Center
    }).addStyleClass('boldText');

    var oOEEPRadial = new sap.suite.ui.microchart.RadialMicroChart({
        valueColor: "{model_oee>/Rowset/Row/OEEP_COLOR}",
        tooltip: "Rendimento"
    }).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEEP", "model_oee>/Rowset/Row/OEEP_COLOR", oOEEPText));

    var oOEEQText = new sap.m.Text({
        width: "100%",
        text: "QUA",
        textAlign: sap.ui.core.TextAlign.Center
    }).addStyleClass('boldText');

    var oOEEQRadial = new sap.suite.ui.microchart.RadialMicroChart({
        valueColor: "{model_oee>/Rowset/Row/OEEQ_COLOR}",
        tooltip: "Qualità"
    }).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEEQ", "model_oee>/Rowset/Row/OEEQ_COLOR", oOEEQText));

    var oMonthComparison = new sap.suite.ui.microchart.ComparisonMicroChart({
        width: "100%",
        minValue: 0,
        maxValue: 50000
    });

    var oOrderListOnPeriodTable = new sap.m.Table({
        id: "orderListOnPeriodTable",
        mode: sap.m.ListMode.None,
        columns: [
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Ordine"
                }),
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Data inizio"
                }),
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Data fine"
                }),
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Durata (minuti)"
                }),
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Articolo"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Quantità ordine"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Buoni"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "Scarti"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "OEE"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "DIS"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "REN"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                header: new sap.m.Text({
                    text: "QUA"
                }),
                hAlign: sap.ui.core.TextAlign.End,
            }),
            new sap.m.Column({
                visible: false
            }),
            new sap.m.Column({
                visible: false
            })
        ]
    }).addStyleClass('trendTable');

    var oOrderListOnPeriodTemplate = new sap.m.ColumnListItem({
        type: sap.m.ListType.Inactive,
        cells: [
            new sap.m.Text({
                id: "ZZORDER",
                text: "{model_orderListOnPeriod>ZZORDER}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZDATETIMESTR}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZDATETIMELST}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZORDERDURATIONMINUTES}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZMATERIAL}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZORDERQTY}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZGOODQTY}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZSCRAPQTY}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZOEE}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZOEED}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZOEEP}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZOEEQ}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZPERCOEEGREEN}"
            }),
            new sap.m.Text({
                text: "{model_orderListOnPeriod>ZZPERCOEERED}"
            })
        ]
    });
    oOrderListOnPeriodTable.bindAggregation("items", "model_orderListOnPeriod>/Rowset/Row", oOrderListOnPeriodTemplate);

    var oMainPanel = new sap.m.Panel({
        height: '900px', // '420px',
        width: '100%', // sWidth ,
        content: [
            new sap.ui.layout.Grid({
                hSpacing: 1,
                vSpacing: 0.5,
                content: [
                    new sap.m.Panel({
                        content: [
                            new sap.ui.layout.Grid({
                                content: [
                                    new sap.m.Text({
                                        width: "100%",
                                        text: "{model_oee>/Rowset/Row/LINE_LABEL}",
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('boldText'),
                                    new sap.m.Text({
                                        width: "100%",
                                        text: "{model_oee>/Rowset/Row/DATE_UPD}",
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    })
                                ]
                            })
                        ],
                        layoutData: new sap.ui.layout.GridData({
                            span: "L12 M12 S12"
                        })
                    }).addStyleClass('smallPanelPadding'),

                    new sap.m.Panel({
                        content: [
                            new sap.m.Text({
                                width: "100%",
                                text: "{model_oee>/Rowset/Row/STATE_LABEL}",
                                textAlign: sap.ui.core.TextAlign.Right
                            }).addStyleClass('whiteText boldText'),
                        ],
                        layoutData: new sap.ui.layout.GridData({
                            span: "L12 M12 S12"
                        })
                    }).addStyleClass('bluePanel smallPanelPadding'),

                    new sap.m.Panel({
                        content: [
                            new sap.ui.layout.Grid({
                                width: "100%",
                                content: [
                                    new sap.m.Text({
                                        text: 'Ordine:',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }),
                                    new sap.m.Text({
                                        text: '{model_oee>/Rowset/Row/PRODUCTIONORDER}',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('boldText'),
                                ]
                            }),
                            new sap.ui.layout.Grid({
                                width: "100%",
                                content: [
                                    new sap.m.Text({
                                        text: 'Articolo:',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }),
                                    new sap.m.Text({
                                        text: '{model_oee>/Rowset/Row/MATNR}',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('boldText'),
                                ]
                            }),
                            new sap.ui.layout.Grid({
                                width: "100%",
                                content: [
                                    new sap.m.Text({
                                        text: 'Descrizione:',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }),
                                    new sap.m.Text({
                                        text: '{model_oee>/Rowset/Row/MAKTX}',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('boldText'),
                                ]
                            }),
                            new sap.m.Text({
                                width: "100%",
                                text: ""
                            }),
                            new sap.ui.layout.Grid({
                                width: "100%",
                                content: [
                                    new sap.m.Text({
                                        text: 'Prodotti',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('smallText'),
                                    new sap.m.Text({
                                        text: 'Target',
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('smallText'),
                                    new sap.m.Text({
                                        text: "{model_oee>/Rowset/Row/QTY_ORDER_TOTAL}",
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Left,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6",
                                            linebreak: true
                                        })
                                    }).addStyleClass('boldText'),
                                    new sap.m.Text({
                                        text: "{model_oee>/Rowset/Row/QTY_ORDER_TARGET}",
                                        width: '100%',
                                        textAlign: sap.ui.core.TextAlign.Right,
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L6 M6 S6"
                                        })
                                    }).addStyleClass('boldText'),
                                ]
                            }),
                            new sap.suite.ui.microchart.BulletMicroChart({
                                width: "100%",
                                minValue: 0,
                                maxValue: "{model_oee>/Rowset/Row/QTY_ORDER_TARGET}",
                                showActualValue: false,
                                actual: new sap.suite.ui.microchart.BulletMicroChartData({
                                    color: "Good"
                                }).bindProperty("value", valueFunc("model_oee>/Rowset/Row/QTY_ORDER_TOTAL")),
                                thresholds: [
                                    new sap.suite.ui.microchart.BulletMicroChartData({
                                        color: "Error"
                                    }).bindProperty("value", valueFunc("model_oee>/Rowset/Row/QTY_ORDER_TARGET"))
                                ]
                            }),

                        ],
                        layoutData: new sap.ui.layout.GridData({
                            span: "L12 M12 S12"
                        })
                    }).addStyleClass('smallPanelPadding'),

                    new sap.m.Panel({
                        content: [
                            new sap.m.Text({
                                width: "100%",
                                text: "Rendimento giornaliero",
                                textAlign: sap.ui.core.TextAlign.Left,
                                layoutData: new sap.ui.layout.GridData({
                                    span: "L6 M6 S6"
                                })
                            }).addStyleClass('boldText'),
                            new sap.m.Text({
                                width: "100%",
                                text: "",
                            }).addStyleClass('boldText'),
                            new sap.ui.layout.Grid({
                                content: [
                                    new sap.ui.layout.VerticalLayout({
                                        content: [
                                            new sap.m.FlexBox({
                                                width: "170px",
                                                height: "200px",
                                                items: [
                                                    new sap.ui.layout.VerticalLayout({
                                                        width: "100%",
                                                        content: [
                                                            oOEEText,
                                                            oOEERadial
                                                        ],
                                                        layoutData: new sap.m.FlexItemData({
                                                            growFactor: 1
                                                        })
                                                    })
                                                ]
                                            }),
                                        ],
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L4 M4 S4"
                                        })
                                    }),
                                    new sap.ui.layout.VerticalLayout({
                                        width: '100%',
                                        content: [
                                            new sap.ui.layout.Grid({
                                                content: [
                                                    new sap.m.FlexBox({
                                                        width: "110px",
                                                        height: "130px",
                                                        items: [
                                                            new sap.ui.layout.VerticalLayout({
                                                                width: "100%",
                                                                content: [
                                                                    oOEEDText,
                                                                    oOEEDRadial
                                                                ]
                                                            })
                                                        ],
                                                        layoutData: new sap.ui.layout.GridData({
                                                            span: "L4 M4 S4"
                                                        })
                                                    }),
                                                    new sap.m.FlexBox({
                                                        width: "110px",
                                                        height: "130px",
                                                        items: [
                                                            new sap.ui.layout.VerticalLayout({
                                                                width: "100%",
                                                                content: [
                                                                    oOEEPText,
                                                                    oOEEPRadial
                                                                ]
                                                            })
                                                        ],
                                                        layoutData: new sap.ui.layout.GridData({
                                                            span: "L4 M4 S4"
                                                        })
                                                    }),
                                                    new sap.m.FlexBox({
                                                        width: "110px",
                                                        height: "130px",
                                                        items: [
                                                            new sap.ui.layout.VerticalLayout({
                                                                width: "100%",
                                                                content: [
                                                                    oOEEQText,
                                                                    oOEEQRadial
                                                                ]
                                                            })
                                                        ],
                                                        layoutData: new sap.ui.layout.GridData({
                                                            span: "L4 M4 S4"
                                                        })
                                                    })
                                                ]
                                            }),
                                        ],
                                        layoutData: new sap.ui.layout.GridData({
                                            span: "L8 M8 S8"
                                        })
                                    })
                                ]
                            })
                        ],
                        layoutData: new sap.ui.layout.GridData({
                            span: "L12 M12 S12",
                            linebreak: true
                        })
                    }).addStyleClass('smallPanelPadding'),

                    new sap.m.Panel({
                        content: [
                            new sap.m.Text({
                                width: "100%",
                                text: "Rendimento ordini nel periodo",
                                textAlign: sap.ui.core.TextAlign.Left,
                                layoutData: new sap.ui.layout.GridData({
                                    span: "L6 M6 S6"
                                })
                            }).addStyleClass('boldText'),
                            new sap.m.Text({
                                width: "100%",
                                text: "",
                                textAlign: sap.ui.core.TextAlign.Left,
                                layoutData: new sap.ui.layout.GridData({
                                    span: "L6 M6 S6"
                                })
                            }),
                            oOrderListOnPeriodTable
                        ],
                        layoutData: new sap.ui.layout.GridData({
                            span: "L12 M12 S12"
                        })
                    }).addStyleClass('smallPanelPadding'),
                    new sap.m.Panel({
                        id: "day-chart",
                        layoutData: new sap.ui.layout.GridData({
                            span: "L12 M12 S12",
                            linebreak: true
                        })
                    })
                ]
            })
        ]
    }).addStyleClass(sPageClass).addStyleClass('noPaddingPanel');

    refreshInfo(oMainPanel);

    return oMainPanel;
}
