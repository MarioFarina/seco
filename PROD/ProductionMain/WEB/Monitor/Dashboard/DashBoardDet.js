/**
 * DashBoardDetSeco.js
 *
 * 29/05/2019 - MF - primo rilascio
/*

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/";
var PrecLine = 1;

jQuery.ajaxSetup({
	cache: false
});

jQuery.sap.require("sap.ui.core.format.NumberFormat");
jQuery.sap.require("sap.suite.ui.microchart.RadialMicroChart");

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/

$(document).ready(function () {
	var oDetailsPage = createDetailsPage();

	oDetailsPage.placeAt("MasterCont");
	$("#splash-screen").hide();
});

function refreshInfo(oMainPanel) {

	var queryString = new URLSearchParams(window.location.search);
	var sIdLine = queryString.get('idline');
	var sPlant = queryString.get('plant');

	var oModel_oee = new sap.ui.model.xml.XMLModel();
	var sQuery_oee = QService + dataMonitor + "DashBoard/getDashboardSecoQR";
	sQuery_oee += "&Param.1=" + sPlant;
	sQuery_oee += "&Param.3=" + sIdLine;
	oModel_oee.loadData(sQuery_oee,false,false);
	oMainPanel.setModel(oModel_oee, 'model_oee');
}

function createDetailsPage() {
	var sMode = $('#mode').val();
	if (sMode==="{mode}") {sMode = "embedded";}
	var sWidth = ( sMode === "single" ?'1120px':'100%');
	var sPageClass = (sMode==="single"?'SinglePage':'noPaddingPanel');

	var percentageFunc = function(sKpField, sColorFIeld, oText) {
		return {
			parts: [
				{path: sKpField },
				{path: sColorFIeld }
			],
			formatter: function(sPercentage, sColor){
				var sStyle;
				switch(sColor) {
					case "Good":
						sStyle = 'greenText';
						break;
					case "Critical":
						sStyle = 'yellowText';
						break;
					case "Error":
						sStyle = 'redText';
						break;
				}
				oText.addStyleClass(sStyle);
				var fPercentage = parseFloat(sPercentage).toFixed();
				return parseFloat(fPercentage);
			}
		};
	};

	var valueFunc = function(sValueField) {
		return {
			parts: [
				{path: sValueField }
			],
			formatter: function(sValue){
				return parseFloat(sValue);
			}
		};
	};
    
	var oOEEText = new sap.m.Text({
		width: "100%",
		text: "OEE",
		textAlign: sap.ui.core.TextAlign.Center
	}).addStyleClass('boldText');

    var oOEERadial = new sap.suite.ui.microchart.RadialMicroChart({
		valueColor: "{model_oee>/Rowset/Row/OEE_COLOR}", 
        tooltip: "OEE"
	}).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEE","model_oee>/Rowset/Row/OEE_COLOR",oOEEText));
    
	var oOEEDText = new sap.m.Text({
		width: "100%",
		text: "OEED",
		textAlign: sap.ui.core.TextAlign.Center
	}).addStyleClass('boldText');

	var oOEEDRadial = new sap.suite.ui.microchart.RadialMicroChart({
		valueColor: "{model_oee>/Rowset/Row/OEED_COLOR}",
        tooltip: "OEED"
	}).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEED","model_oee>/Rowset/Row/OEED_COLOR",oOEEDText));

	var oOEEPText = new sap.m.Text({
		width: "100%",
		text: "OEEP",
		textAlign: sap.ui.core.TextAlign.Center
	}).addStyleClass('boldText');

	var oOEEPRadial = new sap.suite.ui.microchart.RadialMicroChart({
		valueColor: "{model_oee>/Rowset/Row/OEEP_COLOR}",
        tooltip: "OEEP"
	}).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEEP","model_oee>/Rowset/Row/OEEP_COLOR",oOEEPText));

	var oOEEQText = new sap.m.Text({
		width: "100%",
		text: "OEEQ",
		textAlign: sap.ui.core.TextAlign.Center
	}).addStyleClass('boldText');

	var oOEEQRadial = new sap.suite.ui.microchart.RadialMicroChart({
		valueColor: "{model_oee>/Rowset/Row/OEEQ_COLOR}",
        tooltip: "OEEQ"
	}).bindProperty("percentage", percentageFunc("model_oee>/Rowset/Row/OEEQ","model_oee>/Rowset/Row/OEEQ_COLOR",oOEEQText));

	var oMonthComparison = new sap.suite.ui.microchart.ComparisonMicroChart({
		width: "100%",
		minValue : 0,
		maxValue : 50000
	});
  
	var oMainPanel = new sap.m.Panel({
		height: '420px',
		width: sWidth ,
		content: [
			new sap.ui.layout.Grid ({
				hSpacing: 1,
				vSpacing: 0.5,
				content: [
					new sap.m.Panel({
						content: [
							new sap.ui.layout.Grid ({
								content: [
									new sap.m.Text({
										width: "100%",
										text: "{model_oee>/Rowset/Row/LINE_LABEL}",
										textAlign: sap.ui.core.TextAlign.Left,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('boldText'),
									new sap.m.Text({
										width: "100%",
										text: "{model_oee>/Rowset/Row/DATE_UPD}",
										textAlign: sap.ui.core.TextAlign.Right,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									})									
								]
							})
						],
						layoutData : new sap.ui.layout.GridData({
							span: "L8 M8 S8"
						})
					}).addStyleClass('smallPanelPadding'),
					new sap.m.Panel({
						content: [
							new sap.m.Text({
								width: "100%",
								text: "{model_oee>/Rowset/Row/STATE_LABEL}",
								textAlign: sap.ui.core.TextAlign.Right
							}).addStyleClass('whiteText boldText'),							
						],
						layoutData : new sap.ui.layout.GridData({
							span: "L4 M4 S4"
						})
					}).addStyleClass('bluePanel smallPanelPadding'),
					new sap.m.Panel({
						height: "355px",
						content: [
                            new sap.m.Text({
								width: "100%",
								text: "Rendimento giornaliero",
								textAlign: sap.ui.core.TextAlign.Left,
								layoutData : new sap.ui.layout.GridData({
									span: "L6 M6 S6"
								})
				            }).addStyleClass('boldText'),	
                            new sap.m.Text({
								width: "100%",
								text: "",
							}).addStyleClass('boldText'),	
							new sap.ui.layout.Grid ({
								content: [
									new sap.ui.layout.VerticalLayout({
										content: [
											new  sap.m.FlexBox({
												width: "170px",
												height: "200px",
												items: [
													new sap.ui.layout.VerticalLayout({
														width: "100%",
														content: [
															oOEEText,
															oOEERadial
														],
														layoutData: new sap.m.FlexItemData({
															growFactor: 1
														})
													})
												]
											}),                                       
										],
										layoutData : new sap.ui.layout.GridData({
											span: "L4 M4 S4"
										})
									}),
									new sap.ui.layout.VerticalLayout({
										width: '100%',
										content: [
											new sap.ui.layout.Grid ({
												content: [
													new  sap.m.FlexBox({
														width: "110px",
														height: "130px",
														items: [
															new sap.ui.layout.VerticalLayout({
																width: "100%",
																content: [
																	oOEEDText,
																	oOEEDRadial
																]
															})
														],
														layoutData : new sap.ui.layout.GridData({
															span: "L4 M4 S4"
														})
													}),
													new  sap.m.FlexBox({
														width: "110px",
														height: "130px",
														items: [
															new sap.ui.layout.VerticalLayout({
																width: "100%",
																content: [
																	oOEEPText,
																	oOEEPRadial
																]
															})
														],
														layoutData : new sap.ui.layout.GridData({
															span: "L4 M4 S4"
														})
													}),
													new  sap.m.FlexBox({
														width: "110px",
														height: "130px",
														items: [
															new sap.ui.layout.VerticalLayout({
																width: "100%",
																content: [
																	oOEEQText,
																	oOEEQRadial
																]
															})
														],
														layoutData : new sap.ui.layout.GridData({
															span: "L4 M4 S4"
														})
													})
												]
											}),
										],
										layoutData : new sap.ui.layout.GridData({
											span: "L8 M8 S8"
										})
									})
								]
							})
						],
						layoutData : new sap.ui.layout.GridData({
							span: "L8 M8 S8",
							linebreak: true
						})
					}).addStyleClass('smallPanelPadding'),
                    
                    new sap.m.Panel({
						height: "355px",
						content: [                        
                            new sap.ui.layout.Grid ({
								width: "100%",
								content: [
									new sap.m.Text({
										text: 'Rendimento ordine:',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Left,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}),
                                    new sap.m.Text({
										text: '{model_oee>/Rowset/Row/PRODUCTIONORDER}',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('boldText'),						
								]
							}),
                            new sap.ui.layout.Grid ({
								width: "100%",
								content: [
									new sap.m.Text({
										text: 'Articolo:',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Left,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}),
                                    new sap.m.Text({
										text: '{model_oee>/Rowset/Row/MATNR}',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('boldText'),						
								]
							}),
                            new sap.ui.layout.Grid ({
								width: "100%",
								content: [
									new sap.m.Text({
										text: 'Descrizione:',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Left,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}),
                                    new sap.m.Text({
										text: '{model_oee>/Rowset/Row/MAKTX}',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('boldText'),						
								]
							}),                   
                            new sap.m.Text({
								width: "100%",
								text: ""
							}),
							new sap.ui.layout.Grid ({
								width: "100%",
								content: [
									new sap.m.Text({
										text: 'Prodotti',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Left,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('smallText'),
									new sap.m.Text({
										text: 'Target',
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('smallText'),
									new sap.m.Text({
										text: "{model_oee>/Rowset/Row/QTY_ORDER_TOTAL}",
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Left,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6",
											linebreak:true
										})
									}).addStyleClass('boldText'),
									new sap.m.Text({
										text: "{model_oee>/Rowset/Row/QTY_ORDER_TARGET}",
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right,
										layoutData : new sap.ui.layout.GridData({
											span: "L6 M6 S6"
										})
									}).addStyleClass('boldText'),
								]
							}),
							new sap.suite.ui.microchart.BulletMicroChart({
								width: "100%",
								minValue: 0,
								maxValue: "{model_oee>/Rowset/Row/QTY_ORDER_TARGET}",
								showActualValue: false,
								actual: new sap.suite.ui.microchart.BulletMicroChartData({
									//value: "{model_2>/Rowset/Row/QTY_OUT}",
									color: "Good"
								}).bindProperty("value", valueFunc("model_oee>/Rowset/Row/QTY_ORDER_TOTAL")),
								thresholds: [
									new sap.suite.ui.microchart.BulletMicroChartData({
										//value: "{model_2>/Rowset/Row/QTY_TARG}",
										color: "Error"
									}).bindProperty("value", valueFunc("model_oee>/Rowset/Row/QTY_ORDER_TARGET"))
								]
							}),						
						],
						layoutData : new sap.ui.layout.GridData({
							span: "L4 M4 S4"
						})
					}).addStyleClass('smallPanelPadding')
				]
			})
		]
	}).addStyleClass(sPageClass).addStyleClass('noPaddingPanel');

	refreshInfo(oMainPanel);
    
	return oMainPanel;
}
