/* jshint -W117, -W098 */
// Script per pagina pannello impianti
//*******************************************
//**  Autore: Luca Adanti - IT-Link
//**
//**  Ver: 2.4.9
//**
//*******************************************


/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";
var PrecLine = 1;

jQuery.ajaxSetup({
	cache: false
});
//jQuery.sap.includeScript("/XMII/CM/ProductionMain/Monitor/DashBoard/DashBoardItem.js?v=" + Date.now());
jQuery.sap.require("sap.ui.core.format.NumberFormat");
jQuery.sap.require("sap.suite.ui.microchart.RadialMicroChart");
/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/

function refreshTabLines() {
	var sPlant = ($('#plant').val() === '{plant}') ? '' : $('#plant').val();
	var sDepart = ($('#depart').val() === '{depart}') ? '' : $('#depart').val();
	var sGroup = ($('#group').val() === '{group}') ? '' : $('#group').val();
	var qParams = {
		data: "ProductionMain/DashBoard/getDashBoardQR&Param.1=" + sPlant +
		"&Param.2=" + sDepart +
		"&Param.3=" + sGroup +
		"&Param.5=" + "it",
		dataType: "json"
	};

	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		// pupulate the model
		var oModel = $.UIbyID("dashboardMon").getModel();
		oModel.setData(data);
		updatePage(oModel);
	})
	// on fail
		.fail(function () {
		//sap.ui.commons.MessageBox.alert("Errore nell'aggiornamento dati");
		sap.m.MessageToast.show("Errore nell'aggiornamento dati", {
			duration: 45000,
			at: sap.ui.core.Popup.Dock.CenterTop,
			my: sap.ui.core.Popup.Dock.CenterTop,
		});
		$.UIbyID("appHeader").setLogoText("Cruscotto - (errore in aggiornamento)");
	})
	// always, either on success or fail
		.always(function () {
		// remove busy indicator
		//oTable.setBusy(false);
		$("#splash-screen").hide();
	});
}


function updatePage(oModel) {
	$("#splash-screen").hide();
	var sTitle = oModel.getProperty("/Rowsets/Rowset/0/Row/0/REPTXT");

	if (typeof sTitle != 'undefined') {
		var currentdate = new Date();
		var curTime = ((currentdate.getHours() < 10) ? "0" : "") + currentdate.getHours() + ":" +
				((currentdate.getMinutes() < 10) ? "0" : "") + currentdate.getMinutes() + ":" +
				((currentdate.getSeconds() < 10) ? "0" : "") + currentdate.getSeconds();
		$.UIbyID("appHeader").setLogoText("Cruscotto " +
											oModel.getProperty("/Rowsets/Rowset/1/Row/0/NAME1") +
										 " - (aggior.: " + curTime + ")");
	}

	if (window.console) {
		console.log("Refresh data");
	}
	$(".sapUiProgInd").each(function () {
		var curPerc = $.UIbyID($(this).attr("id")).getDisplayValue();
		if (curPerc.indexOf("+") > 0) {
			$.UIbyID($(this).attr("id")).setBarColor(sap.ui.core.BarColor.NEGATIVE);
		} else {
			$.UIbyID($(this).attr("id")).setBarColor(sap.ui.core.BarColor.NEUTRAL);
		}
	});
}

$(document).ready(function () {

	var oDataSet = createDashboard();
	oDataSet.placeAt("MasterCont");

	//$("#splash-screen").hide();

	tabRefr = $.timer(function () {
		refreshTabLines();
	}, 180 * 1000, false);

	tabRefr.set({
		autostart: true
	});

});






function createDashboard() {

	var oTileContainer = new sap.m.TileContainer({
		id: "dashboardMon",
		size: sap.m.Size.XS,
		height: '65rem',
		tiles: {
			path: "/Rowsets/Rowset/0/Row",
			template: createCustomTile()
		}
	});
	oTileContainer.setModel(new sap.ui.model.json.JSONModel());

	refreshTabLines();

	return new sap.m.Panel({
		height: '100%',
		content: oTileContainer
	});

}


//Initialize the Dataset and the layouts
function createCustomTile() {
	jQuery.sap.declare("itlink.control.DashBoardTile");


	//....custom tile created and extended
	sap.m.CustomTile
		.extend(
		"itlink.control.DashBoardTile", {

			metadata: {
				properties: {
					"OEEBackColor": {
						type: "string",
						defaultValue: "OEE_C"
					}
				},
			},

			init: function () {},

			renderer: {},

			onAfterRendering: function (rm, ctrl) {
				sap.m.CustomTile.prototype.onAfterRendering
					.call(this);
				var $This = this.$();
				//$This.addClass(this.getOEEBackColor());
			}

		});

	return new itlink.control.DashBoardTile({
		width: "300px",
		height: "30rem",
		OEEBackColor: "{OEE_BackColor}",
		/*{
					parts: [
						{path: "OEE_BackColor"},
					],
					formatter: function(sBackColor) {
						if(sBackColor)
						{
							this.addStyleClass(sBackColor);
						}
						return sBackColor;
					}
				},*/
		content: createCustomContent()
	}).addStyleClass("CustomTileBorder").addStyleClass("dbTileSize");

}


function createCustomContent() {
	return [
		new sap.ui.layout.Grid({
			width: "100%",
			height: "30rem",
			content: [
				new sap.m.HBox({
					displayInline: false,
					alignItems: "End",
					justifyContent: "End",
					alignContent: "End" /*sap.m.FlexAlignContent.Center*/ ,
					items: [
						new sap.m.Label({
							text: "{LINE_LABEL}",
							tooltip: "{IDLINE}",
							design: "Bold",
							width: '11.9em',
							textAlign: sap.ui.core.TextAlign.Left,
							layoutData: new sap.ui.layout.GridData({
								span: "L12 M12 S12",
								linebreak: false
							})
						}).data("LineID", "{IDLINE}"),
						new sap.m.Label({
							text: ' ',
							width: '0.3em'
						}),
						new sap.ui.core.Icon({
							src: "{STATE_ICON}",
							size: "24px",
							color: "black",
							layoutData: new sap.ui.layout.GridData({
								span: "L12 M12 S12",
								linebreak: true
							})
						})],
					layoutData: new sap.ui.layout.GridData({
						span: "L12 M12 S12",
						linebreak: true
					})
				}).addStyleClass("BorderLine"),
				new sap.m.Label({
					text: "{MATERIAL_LABEL}",
					tooltip: "{MATDESC_LABEL}",
					width: '240px',
					design: "Bold",
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData: new sap.ui.layout.GridData({
						span: "L12 M12 S12",
						linebreak: true
					})
				}),
				new sap.m.HBox({
					width: "100%",
					displayInline: true,
					alignItems: "Center" ,
					justifyContent: sap.m.FlexAlignContent.Left ,
					alignContent: sap.m.FlexAlignContent.Left , /*sap.m.FlexAlignContent.Left */
					items: [
						new sap.ui.core.Icon({
							src: "sap-icon://circle-task-2",
							size: "10px",
							color: "{STATE_COLOR}",
							press: function (oEvent) {
								var idx = oEvent.getParameter("id");
							}
						}).addStyleClass("iconAlign"),
						new sap.m.Label({
							text: ' ',
							width: '4px'
						}),
						new sap.m.Label({
							text: '{STATE_LABEL}',
							design: "Bold",
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Center,

						})],
					layoutData: new sap.ui.layout.GridData({
						span: "L11 M11 S11",
						linebreak: true
					})
				}),
				new sap.m.Label({
					text: 'Prodotti',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData: new sap.ui.layout.GridData({
						span: "L6 M6 S6",
						linebreak: true
					})
				}),
				new sap.m.Label({
					text: 'Target',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L6 M6 S6"
					})
				}),
				new sap.m.Label({
					text: '{QTY_OUT_PZ}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData: new sap.ui.layout.GridData({
						span: "L6 M6 S6",
						linebreak: true
					})
				}),
				new sap.m.Label({
					text: '{QTY_TARG_PZ}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L6 M6 S6"
					})
				}),
				new sap.m.HBox({
					width: "10rem",
					height: "8rem",
					displayInline: false,
					alignItems: "Center",
					justifyContent: "Center",
					alignContent: "Center" /*sap.m.FlexAlignContent.Center*/ ,
					layoutData: new sap.ui.layout.GridData({
						span: "L11 M11 S11",
						linebreak: true
					}),
					items: [
						new sap.ui.core.HTML({
							content: "<div></div>",
							afterRendering: function (oControlEvent) {
								//console.debug("afterRendering per Evento: " + oControlEvent.getSource().sId);
								//console.debug("BackColor: " + oControlEvent.getSource().data("OEE_BColor"));
								$("#" + oControlEvent.getSource().oParent.sId).parent().addClass("RadialChartCtrl");
								var BCol = oControlEvent.getSource().data("OEE_BColor");
								var vOEE = oControlEvent.getSource().data("KP_OEE");
								var sLine = oControlEvent.getSource().data("LineID");
								var qTarg =  oControlEvent.getSource().data("QTY_TARGET");
								var jdata = {
									"name": "grafico",
									"children": [
										{
											"name": "prodotti_buoni",
											"size": 0 //oControlEvent.getSource().data("QTY_OUT_PZ")
										},
										{
											"name": "prodotti_sospesi",
											"size": 0 //oControlEvent.getSource().data("QTY_SCRAP_PZ")
										},
										{
											"name": "rallentamenti",
											"size": 0 //oControlEvent.getSource().data("QTY_SLO")
										},
										{
											"name": "disponibilita",
											"size": 100 //oControlEvent.getSource().data("QTY_IND")
										},
										{ 
											"name": "avanzamento",
											"size":50 // oControlEvent.getSource().data("QTY_PROG")
										}]
								};
								console.debug(sLine + ": B=" + jdata.children["0"].size + " - S=" + jdata.children["1"].size);
								radialChart(180, BCol, "#" + oControlEvent.getSource().sId, vOEE, jdata, sLine,qTarg);
								$("#svg_" + sLine).parent().on("click", function () {
									ShowDashBoardItem(sLine);
								}); //
							}
						}).data("LineID", "{IDLINE}")
						.data("OEE_BColor", "{OEE_BackColor}")
						.data("KP_OEE", "{KP_OEE}")
						.data("QTY_SCRAP_PZ", "{QTY_SCRAP}")
						.data("QTY_OUT_PZ", "{QTY_OUT}")
						.data("QTY_PROG", "{QTY_PROG}")
						.data("QTY_IND", "{QTY_IND}")
						.data("QTY_SLO", "{QTY_SLO}")
						.data("QTY_TARGET", "{QTY_TARG}")
					]
				}).addStyleClass("RadialChartStyle"),
				new sap.m.Label({
					text: '{QTY_SCRAP_PZ}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S4",
						linebreak: true
					})
				}),
				new sap.m.Label({
					text: '{T_DUR_MIN}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L8 M8 S8",
						linebreak: false
					})
				}),
				new sap.m.Label({
					text: 'Scarti',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData: new sap.ui.layout.GridData({
						span: "L5 M5 S5",
						linebreak: true
					})
				}),
				new sap.m.Label({
					text: 'Non disponibile',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L7 M7 S7",
						linebreak: false
					})
				}),
				new sap.m.HBox({
					width: "100%",
					displayInline: false,
					alignItems: "End",
					justifyContent: "End",
					alignContent: "End",
					layoutData: new sap.ui.layout.GridData({
						span: "L12 M12 S12",
						linebreak: true
					}),
					items: []

				}).addStyleClass("BorderLineTop"),
				new sap.ui.core.Icon({
					src: "{LINE_ICON}", /*sap-icon://message-warning*/
					size: "18px",
					color: "{LINE_MSCOLOR}",
					tooltip: "{LINE_MESSAGE}",
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S2",
						linebreak: false
					})
				}),
				new sap.ui.core.Icon({
					src: "{MII_ICON}", /* sap-icon://warning2 */
					size: "18px",
					color: "{MII_MSCOLOR}",
					tooltip: "{MII_MESSAGE}",
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S3",
						linebreak: false
					})
				}),
				new sap.m.Label({
					text: '{DATE_UPD}',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L7 M7 S7",
						linebreak: false
					})
				})]
		})

	];
}

function ShowDashBoardItem(sIdLine) {

	var sLink = "/XMII/CM/ProductionMain/Monitor/DashBoard/DashBoardDetSeco.irpt";
	sLink += "?plant=" + $('#plant').val();
	sLink += "&idline=" + sIdLine;

	var oHtml = new sap.ui.core.HTML();
	oHtml.setContent('<iframe width=100% height=100% style=" border: 0px; width: calc(100% + 1.5rem);" src="' + sLink + '"></iframe>');

	var oItemDialog = new sap.m.Dialog({
		title: 'Dettaglio per linea ' + sIdLine,
		contentWidth: "950px",
		contentHeight: "420px",
		//resizable: true,
		horizontalScrolling: false,
		verticalScrolling: false,
		content: oHtml,
		buttons: [
			new sap.m.Button({
				text: 'Apri pagina',
				press: function () {
					var win = window.open(sLink, '_blank');
					win.focus();
				}
			}),
			new sap.m.Button({
				text: 'Chiudi',
				press: function () {
					oHtml.destroy();
					oItemDialog.close();
				}
			})
		],
		beforeOpen: function () {
			//sap.ui.core.BusyIndicator.hide();
		},
		afterOpen: function () {
			$("#splash-screen").hide();
		},
		afterClose: function () {
			oItemDialog.destroy();
		}
	}).addStyleClass('noPaddingDialog');

	oItemDialog.open();
}

function radialChart(bwidth, l_BColor, divdom, l_oee, jdata, l_line, l_targ) {

	var bheight = bwidth,
			cwidth = bwidth * 0.82, //570
			cheight = cwidth,
			radius = Math.min(cwidth, cheight) / 2,
			thick = cwidth * 0.15,
			color = d3.scale.ordinal().range(["", "#a6a6a6", "#333", "#595959", "#f2f2f2", "#fcfcfc"]),
			angolo_inizio = -90; //angolo di inizio (-90gradi rispetto allo zero = ore 12)

	var svg = d3.select(divdom).append("svg")
	.attr("width", bwidth)
	.attr("height", bheight)
	.attr("id", "svg_" + l_line)
	//.style("background-color", "#19ad4b")
	.style("color", "#333")
	.append("g")
	.attr("transform", "translate(" + bwidth / 2 + "," + bheight / 2 + ")");

	var partition = d3.layout.partition()
	.sort(null)
	.size([2 * Math.PI, radius * radius])
	.value(function (d) {
		return d.size;
	});

	var arc = d3.svg.arc()
	.startAngle(function (d) {
		return d.x + angolo_inizio * Math.PI / 180;
	})
	.endAngle(function (d) {
		return d.x + d.dx + angolo_inizio * Math.PI / 180;
	})
	.innerRadius(function (d) {
		return Math.sqrt(d.y + d.dy) - thick;
	})
	.outerRadius(function (d) {
		return Math.sqrt(d.y + d.dy);
	});

	var bColor = "";

	if (l_BColor === "OEE_G") {
		bColor = d3.scale.ordinal()
			.range(["", "#19ad4b", "#333", "#127038", "#f2f2f2", "#fcfcfc"]);
	} else if (l_BColor === "OEE_C") {
		bColor = d3.scale.ordinal()
			.range(["", "#ffcc00", "#333", "#c08907", "#f2f2f2", "#fcfcfc"]);
	} else {
		bColor = d3.scale.ordinal()
			.range(["", "#e41e35", "#333", "#961e1f", "rgb(228, 228, 228)", "#fcfcfc"]);
	}

	var path = svg.datum(jdata).selectAll("path")
	.data(partition.nodes)
	.enter().append("path")
	.attr("d", path)
	.attr("display", function (d) {
		return d.depth ? null : "none";
	}) //nascondi l'anello interno
	.attr("d", arc)
	//.style("stroke", "#fff") //colore contorno
	.each(stash);

	p_buoni = jdata.children["0"].size; // path.node().__data__.children[0].size;
	p_sosp = jdata.children["1"].size; // path.node().__data__.children[1].size;
	avanz = jdata.children["4"].size; //path.node().__data__.children[4].size;
	var TargPz = l_targ ; //jdata.children["5"].size;
	totalSize = path.node().__data__.value ;



	var oee = l_oee; 

	path.style("fill", function (d) {
		return bColor(d.name);
	});
	console.log ("Target " + l_line + " = " + TargPz);
	console.log ("oee " + l_line + " = " + oee);
	if (oee === 0 && TargPz === 0) {oee= '';}
	else  {oee = oee +'%';}

	svg.append("foreignObject")
	//.attr("class", "explanation-obj")
		.attr("width", (radius - thick) * 2)
		.attr("height", radius - 10)
		.style("z-index", 100)
		.attr("transform", function (d) {
		return "translate(" + (-radius + thick) + "," + (-radius + thick) / 1.8 + ")";
	})
		.append("xhtml:div")
		.html("<span>" + oee + "</span>")
		.style("font-size", "1.85rem")
		.style("text-align", "center")
		.style("font-weight", "bold")
		.style("color", l_BColor == "OEE_G" ? "#19ad4b" : l_BColor == "OEE_C" ? "#ffcc00" : "#e41e35");

	svg.append("foreignObject")
	//.attr("class", "explanation-obj")
		.attr("width", (radius - thick) * 2)
		.attr("height", radius - 10)
		.style("z-index", 100)
		.attr("transform", function (d) {
		return "translate(" + (-radius + thick) + "," + (radius - thick) / 6 + ")";
	})
		.append("xhtml:div")
		.style("font-size", cwidth * 0.09 + "px")
		.style("text-align", "center")
		.style("font-weight", "bold")
		.html("<span>OEE</span>")
		.style("color", l_BColor == "OEE_G" ? "#19ad4b" : l_BColor == "OEE_C" ? "#ffcc00" : "#e41e35");


	// Stash the old values for transition.
	function stash(d) {
		d.x0 = d.x;
		d.dx0 = d.dx;
	}

	// Interpolate the arcs in data space.
	function arcTween(a) {
		var i = d3.interpolate({
			x: a.x0,
			dx: a.dx0
		}, a);
		return function (t) {
			var b = i(t);
			a.x0 = b.x;
			a.dx0 = b.dx;
			return arc(b);
		};
	}

	d3.select(self.frameElement).style("height", bheight + "px");

}
