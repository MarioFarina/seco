var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/json&QueryTemplate=ProductionMain/MasterData/";
var dataProdRPT = "Content-Type=text/json&QueryTemplate=ProductionMain/Report/";
var columnsName = {
    "ColumnsField": [
        "COLORE",
        "INDICE",
        "ZZDATETIMELST",
        "ZZLINEIDENTITY",
        "ZZMATERIAL",
        "ZZMATDESCRIPTION",
        "ZZORDER",
        "ZZOPERATION",
        "ZZGOODQTY",
        "ZZSCRAPQTY",
        "ZZSERIAL",
        "ZZLOADUNIT",
        "ZZOPERATORID",
        "ZZNRCOLLAUDO"
    ],
    "ColumnsName": [
        "INDICE",
        "IDReparto",
        "Descrizione",
        "Materiale",
        "ID Esterno",
        "Ordine",
        "Toller. (sec.)",
        "Note",
        "Note",
        "Note",
        "Note",
        "Note",
        "Note"
    ]
};

sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/xml/XMLModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/DateFormat",
    "sap/m/MessageBox",
    'jquery.sap.global',
    'sap/ui/export/Spreadsheet'
], function (Controller, XmlModel, JSONModel, DateFormat, MessageBox, jQuery, Spreadsheet) {
    "use strict";
    return Controller.extend("seco.ui.app.seco.controller.App", {

        palette: ['#940000', '#C30101', '#560D0D', '#6F0000', '#EA0909', '#F03939', '#F26161'],
        pathToExport1: null,

        settingsModel: {
            dimensions: {
                Medium: [{
                    name: 'Data',
                    value: "{DATA}",
                }]
            },
            measures: [
                {
                    name: 'Totale Collaudi',
                    value: '{TOTALECOLLAUDI}'
                }, {
                    name: 'Totale Prodotti',
                    value: '{TOTALEPRODOTTI}'
                }, {
                    name: 'Totale Scarti',
                    value: '{TOTALESCARTI}'
                }
            ]
        },
        settingsModel2: {
            dataset: {
                values: [
                    {
                        value: ["INDICE"],
                    }
                ]
            },
            dimensions: {
                Medium: [{
                    name: 'Data',
                    value: "{DATA}",
                }]
            },
            measures: [{
                name: 'Indice',
                value: '{INDICE}'
            }]
        },
        oVizFrame: null,
        oVizFrame2: null,

        getViewModel: function (modelName, query) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var oModel = new JSONModel();
                oModel.loadData(query);
                oModel.attachRequestCompleted(function () {
                    var rows = oModel.oData.Rowsets.Rowset[0].Row;
                    if (typeof rows === "undefined") {
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    } else {
                        var data = {};
                        data[modelName] = typeof rows.length !== "undefined" ? rows : [rows];
                        oModel.setData(data);
                        this.getView().setModel(oModel, modelName);
                        resolve();
                    }
                }.bind(self));
            })
        },

        initViewModels: function () {
            var self = this;
            var view = this.getView();
            let oPeriods = new JSONModel();
            oPeriods.setData({
                "PERIODS": [{"NAME": "DAY", "TEXT": "Giorno"}, {
                    "NAME": "WEEK",
                    "TEXT": "Settimana"
                }, {"NAME": "MONTH", "TEXT": "Mese"}]
            });
            this.getView().setModel(oPeriods, "PERIODS");
            this.getPlants().then(function () {
                this.getView().byId("plants").setSelectedKey(this.getView().getModel("PLANTS").oData.PLANTS[0].PLANT);
                // this.getView().byId("periods").setSelectedKey("DAY");
                this.getDepartments().then(function () {
                    view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);

                    new Promise(function (resolve, reject) {
                        var oModel = new JSONModel();
                        oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                        oModel.attachRequestCompleted(function () {
                            var rows = oModel.oData.Rowsets.Rowset[0].Row;
                            if (typeof rows === "undefined") {
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            } else {
                                var data = {};
                                data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                                oModel.setData(data);
                                view.setModel(oModel, "LINES");
                                resolve();
                            }
                            view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                        }.bind(self));
                    });
                });

            }.bind(this));
        },

        getPlants: function () {
            return this.getViewModel("PLANTS", QService + dataProdMD + "Plants/getPlantsByUserQR");
        },

        getDepartments: function () {
            return this.getViewModel("DEPARTMENTS", QService + dataProdMD + "Departments/getDepartmentsByPlantSQ&Param.1=" + this.getView().byId("plants").getSelectedKey());
        },

        getLines: function () {
            return this.getViewModel("LINES", QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + this.getView().byId("departments").getSelectedKey());
        },

        onPlantChanged: function () {
            var view = this.getView();
            this.getDepartments().then(function () {
                view.byId("departments").setSelectedKey(view.getModel("DEPARTMENTS").oData.DEPARTMENTS[0].IDREP);
                new Promise(function (resolve, reject) {
                    var oModel = new JSONModel();
                    oModel.loadData(QService + dataProdMD + "Lines/getLinesByRepSQ&Param.1=" + view.byId("departments").getSelectedKey());
                    oModel.attachRequestCompleted(function () {
                        var rows = oModel.oData.Rowsets.Rowset[0].Row;
                        if (typeof rows === "undefined") {
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        } else {
                            var data = {};
                            data["LINES"] = typeof rows.length !== "undefined" ? rows : [rows];
                            oModel.setData(data);
                            view.setModel(oModel, "LINES");
                            resolve();
                        }
                        view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
                    }.bind(self));
                });
            });
        },

        onRepartoChanged: function () {
            var view = this.getView();
            this.getLines().then(function () {
                view.byId("lines").setSelectedKey(view.getModel("LINES").oData.LINES[0].LINEA);
            });
        },

        getQueryParameters: function () {
            var params = [];
            var view = this.getView();
            var from = view.byId("TurnoDa").getDateValue();
            var to = view.byId("TurnoA").getDateValue();
            var line = view.byId("lines").getSelectedKey();
            if (from instanceof Date && to instanceof Date && line !== "") {
                params = [from, to, line];
            }
            return params;
        },

        formatQueryParameters: function (params) {
            var materiale = this.getView().byId("materialeInput").getValue();
            var ordine = this.getView().byId("ordineInput").getValue();
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "yyyy-MM-dd"});
            var view = this.getView();
            var query = "";
            query += "Param.1=false";
            query += "&Param.2=" + oDateFormatter.format(params[0]);
            query += "&Param.3=" + materiale;
            query += "&Param.4=" + oDateFormatter.format(params[1]);
            query += "&Param.5=" + params[2];
            query += "&Param.6=" + ordine;
            return query;
        },

        /**********************************
         * CONTROLLER LIFE-CYCLE
         **********************************/
        onInit: function () {
            var view = this.getView();
            var now = new Date();
            var past = new Date();
            past.setDate(past.getDate() - 7);
            this.initViewModels();

            //SET DATE RANGE DEFAULT
            view.byId("TurnoA").setDateValue(now);
            view.byId("TurnoDa").setDateValue(past);
        },

        onSearch: function () {
            var oModel = new JSONModel();
            var params = this.getQueryParameters();
            if (params.length === 0) {
                MessageBox.warning("Inserire tutti i campi obbligatori: Linea, Inizio Periodo, Fine range.", {styleClass: "sapUiSizeCompact"});
                return;
            }
            var queryString = this.formatQueryParameters(params);

            this.getView().byId("tblDataset").setVisible(false);
            // this.getView().byId("settingsPanelVizFrame").setVisible(false);
            this.getView().byId("oChartContainer1").setVisible(false);
            this.getView().byId("oChartContainer2").setVisible(false);
            this.getView().byId("tileCollaudi").setValue(0);
            this.getView().byId("tileProdotti").setValue(0);
            this.getView().byId("tileScarti").setValue(0);
            this.getView().byId("tileIndice").setValue(0);
            // this.getView().byId("tileIndice").setValueColor("Neutral");
            this.getView().byId("indice").removeStyleClass("sapMGT");
            this.getView().byId("indice").removeStyleClass("goodTile");
            this.getView().byId("indice").removeStyleClass("warningTile");
            this.getView().byId("indice").removeStyleClass("errorTile");
            this.getView().byId("indice").addStyleClass("noneTile");
            this.getView().byId("tileIndice").setIndicator(sap.m.DeviationIndicator.None);

            oModel.loadData(QService + dataProdRPT + "Quality/getQualityQR&" + queryString);
            oModel.attachRequestCompleted(function () {
                var rowsets = oModel.oData.Rowsets.Rowset;
                var rowset1, rowset2, rowset3;

                if (typeof rowsets[0] !== "undefined" && typeof rowsets[0].Row !== "undefined") {
                    rowset1 = rowsets[0].Row;
                } else {
                    return;
                }
                if (typeof rowsets[1] !== "undefined" && typeof rowsets[1].Row !== "undefined") {
                    rowset2 = rowsets[1].Row;
                } else {
                    return;
                }
                if (typeof rowsets[2] !== "undefined" && typeof rowsets[2].Row !== "undefined") {
                    rowset3 = rowsets[2].Row;
                } else {
                    return;
                }

                this.getView().byId("tblDataset").setVisible(true);
                // this.getView().byId("settingsPanelVizFrame").setVisible(true);
                this.getView().byId("oChartContainer1").setVisible(true);
                this.getView().byId("oChartContainer2").setVisible(true);

                // Tile VALORE TOTALE INDICE(COLORATA)
                var colTile = this.getView().byId("tileCollaudi");
                var proTile = this.getView().byId("tileProdotti");
                var scaTile = this.getView().byId("tileScarti");
                var indTile = this.getView().byId("tileIndice");

                var col = formattingNumber(rowset2[0].TOTALECOLLAUDI);
                var pro = formattingNumber(rowset2[0].TOTALEPRODOTTO);
                var sca = formattingNumber(rowset2[0].TOTALESCARTI);
                var ind = formattingNumber(rowset2[0].INDICE);

                colTile.setValue(col[0]);
                proTile.setValue(pro[0]);
                scaTile.setValue(sca[0]);
                indTile.setValue(ind[0]);

                colTile.setScale(col[1]);
                proTile.setScale(pro[1]);
                scaTile.setScale(sca[1]);
                indTile.setScale(ind[1]);


                if (rowset2[0].COLORE === 'green') {
                    indTile.setValueColor("Good");
                    indTile.setIndicator(sap.m.DeviationIndicator.Up);
                    // this.getView().byId("indice").removeStyleClass("noneTile");
                    // this.getView().byId("indice").addStyleClass("goodTile");
                } else if (rowset2[0].COLORE === 'yellow' || rowset2[0].COLORE === 'orange') {
                    indTile.setValueColor("Critical");
                    indTile.setIndicator(sap.m.DeviationIndicator.None);
                    // this.getView().byId("indice").removeStyleClass("noneTile");
                    // this.getView().byId("indice").addStyleClass("warningTile");
                } else if (rowset2[0].COLORE === 'red') {
                    indTile.setValueColor("Error");
                    indTile.setIndicator(sap.m.DeviationIndicator.Down);
                    // this.getView().byId("indice").removeStyleClass("noneTile");
                    // this.getView().byId("indice").addStyleClass("errorTile");
                }


                // ISTOGRAMMA
                var oModelVizFrame = new JSONModel(this.settingsModel);
                oModelVizFrame.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
                this.getView().setModel(oModelVizFrame);

                var oVizFrame = this.oVizFrame = this.getView().byId("idVizFrame");
                oVizFrame.setVizProperties({
                    plotArea: {
                        showGap: true,
                        dataLabel: {
                            visible: true
                        }
                    },
                    valueAxis: {
                        label: {},
                        title: {
                            visible: false
                        }
                    },
                    categoryAxis: {
                        title: {
                            visible: false
                        }
                    },
                    title: {
                        visible: true,
                        text: "Grafico andamento generale nel periodo"
                    }
                });

                var libraries = sap.ui.getVersionInfo().libraries || [];
                var bSuiteAvailable = libraries.some(function (lib) {
                    return lib.name.indexOf("sap.suite.ui.commons") > -1;
                });
                var myObj = {};
                myObj.Row = [];
                for (var i = 0; i < rowset3.length; i++) {
                    var date = rowset3[i].DATA;
                    var today = new Date(date);
                    date = today.toLocaleDateString("it-IT");
                    rowset3[i].DATA = date;
                    // rowset3[i].INDICE = "" + rowset3[i].INDICE + "";
                    rowset3[i].TOTALECOLLAUDI = "" + rowset3[i].TOTALECOLLAUDI + "";
                    rowset3[i].TOTALEPRODOTTI = "" + rowset3[i].TOTALEPRODOTTI + "";
                    rowset3[i].TOTALESCARTI = "" + rowset3[i].TOTALESCARTI + "";
                    myObj.Row.push(rowset3[i]);
                }
                var modelVizFrameI = new JSONModel({
                    width: "auto"
                });
                modelVizFrameI.setData(myObj);
                this.getView().byId("idVizFrame").setModel(modelVizFrameI);
                // this.getView().byId("settingsPanelVizFrame").setVisible(true);


                // COLUMN CHART PER INDICE IN ROWSET3
                var oModelVizFrame2 = new JSONModel(this.settingsModel2);
                oModelVizFrame2.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
                this.getView().setModel(oModelVizFrame2);

                var oVizFrame2 = this.oVizFrame2 = this.getView().byId("idVizFrame2");
                oVizFrame2.setVizProperties({
                    plotArea: {
                        showGap: true,
                        dataLabel: {
                            visible: true
                        },
                        dataPointStyle: {
                            "rules":
                                [

                                    {
                                        "dataContext": {"INDICE": {"max": 4}},
                                        "properties": {
                                            "color": "sapUiChartPaletteSemanticCritical"
                                        },
                                        "displayName": "4 > INDICE > 3 "
                                    },
                                    {
                                        "dataContext": {"INDICE": {"max": 3}},
                                        "properties": {
                                            "color": "sapUiChartPaletteSemanticGood"
                                        },
                                        "displayName": "INDICE < 3"
                                    }
                                ],
                            "others":
                                {
                                    "properties": {
                                        "color": "sapUiChartPaletteSemanticBad"
                                    },
                                    "displayName": "INDICE > 4"
                                }
                        }
                    },
                    valueAxis: {
                        title: {
                            visible: false
                        }
                    },
                    categoryAxis: {
                        title: {
                            visible: false
                        }
                    },
                    title: {
                        visible: true,
                        text: "Grafico andamento dell'Indice nel periodo"
                    }
                });

                var libraries = sap.ui.getVersionInfo().libraries || [];
                var bSuiteAvailable = libraries.some(function (lib) {
                    return lib.name.indexOf("sap.suite.ui.commons") > -1;
                });
                var modelVizFrameI2 = new JSONModel({
                    width: "auto"
                });
                modelVizFrameI2.setData(myObj);
                this.getView().byId("idVizFrame2").setModel(modelVizFrameI2);


                // TABLE
                for (var i = 0; i < rowset1.length; i++) {
                    if (rowset1[i].COLORE === 'green') {
                        rowset1[i].COLORE = 'Success';
                    } else if (rowset1[i].COLORE === 'yellow' || rowset1[i].COLORE === 'orange') {
                        rowset1[i].COLORE = 'Warning';
                    } else if (rowset1[i].COLORE === 'red') {
                        rowset1[i].COLORE = 'Error';
                    } else {
                        rowset1[i].COLORE = 'None';
                    }
                }

                var oModelTable = new JSONModel();
                oModelTable.setData(rowset1);
                this.getView().byId("tblDataset").setModel(oModelTable);
                if (rowset1.length < 8) {
                    this.getView().byId("tblDataset").setVisibleRowCount(rowset1.length);
                } else {
                    this.getView().byId("tblDataset").setVisibleRowCount(8);
                }

                var temp1 = [
                    {value: true, id: "INDICE"}, {value: true, id: "ZZDATETIMELST"}, {
                        value: true,
                        id: "ZZLINEIDENTITY"
                    }, {value: true, id: "ZZMATERIAL"}, {value: true, id: "ZZMATDESCRIPTION"}, {
                        value: true,
                        id: "ZZORDER"
                    }, {value: true, id: "ZZOPERATION"}, {value: true, id: "ZZGOODQTY"}, {
                        value: true,
                        id: "ZZSCRAPQTY"
                    }, {value: true, id: "ZZSERIAL"}, {value: true, id: "ZZLOADUNIT"}, {
                        value: true,
                        id: "ZZOPERATORID"
                    }, {value: true, id: "ZZNRCOLLAUDO"}];
                for (var i = 0; i < rowset1.length; i++) {
                    var obj = rowset1[i];
                    if (obj.INDICE === "" && temp1[0].value) {
                        this.getView().byId(temp1[0].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[0].id).setVisible(true);
                        temp1[0].value = false
                    }
                    if (obj.ZZDATETIMELST === "" && temp1[1].value) {
                        this.getView().byId(temp1[1].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[1].id).setVisible(true);
                        temp1[1].value = false
                    }
                    if (obj.ZZLINEIDENTITY === "" && temp1[2].value) {
                        this.getView().byId(temp1[2].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[2].id).setVisible(true);
                        temp1[2].value = false
                    }
                    if (obj.ZZMATERIAL === "" && temp1[3].value) {
                        this.getView().byId(temp1[3].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[3].id).setVisible(true);
                        temp1[3].value = false
                    }
                    if (obj.ZZMATDESCRIPTION === "" && temp1[4].value) {
                        this.getView().byId(temp1[4].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[4].id).setVisible(true);
                        temp1[4].value = false
                    }
                    if (obj.ZZORDER === "" && temp1[5].value) {
                        this.getView().byId(temp1[5].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[5].id).setVisible(true);
                        temp1[5].value = false
                    }
                    if (obj.ZZOPERATION === "" && temp1[6].value) {
                        this.getView().byId(temp1[6].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[6].id).setVisible(true);
                        temp1[6].value = false
                    }
                    if (obj.ZZGOODQTY === "" && temp1[7].value) {
                        this.getView().byId(temp1[7].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[7].id).setVisible(true);
                        temp1[7].value = false
                    }
                    if (obj.ZZSCRAPQTY === "" && temp1[8].value) {
                        this.getView().byId(temp1[8].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[8].id).setVisible(true);
                        temp1[8].value = false
                    }
                    if (obj.ZZSERIAL === "" && temp1[9].value) {
                        this.getView().byId(temp1[9].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[9].id).setVisible(true);
                        temp1[9].value = false
                    }
                    if (obj.ZZLOADUNIT === "" && temp1[10].value) {
                        this.getView().byId(temp1[10].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[10].id).setVisible(true);
                        temp1[10].value = false
                    }
                    if (obj.ZZOPERATORID === "" && temp1[11].value) {
                        this.getView().byId(temp1[11].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[11].id).setVisible(true);
                        temp1[11].value = false
                    }
                    if (obj.ZZNRCOLLAUDO === "" && temp1[12].value) {
                        this.getView().byId(temp1[12].id).setVisible(false);
                    } else {
                        this.getView().byId(temp1[12].id).setVisible(true);
                        temp1[12].value = false
                    }
                }

                this.pathToExport1 = rowset1;

                // PAGINATION (IMMAGINO)
                window.dataset = rowset1;
                window.loadPageByElement = this.loadPageByElement.bind(this);
                this.loadPage(0);
            }.bind(this));
        },

        createColumn: function (label, bind) {
            return new sap.ui.table.Column({
                Field: bind,
                label: new sap.ui.commons.Label({
                    text: label
                })
            });
        },

        onCollaudiSelected: function (oEvent) {
            var valore = oEvent.getParameter("selected");
            if (valore) {
                var feedValueAxis = this.getView().byId('valueAxisFeed');
                this.oVizFrame.addFeed(feedValueAxis);
            } else {
                if (this.getView().byId("prodottiCheckBox").getSelected() || this.getView().byId("scartiCheckBox").getSelected()) {
                    var feedValueAxis = this.getView().byId('valueAxisFeed');
                    this.oVizFrame.removeFeed(feedValueAxis);
                } else {
                    this.getView().byId("collaudiCheckBox").setSelected(true);
                }
            }
        },
        onProdottiSelected: function (oEvent) {
            var totale = oEvent.getParameter("selected");
            if (totale) {
                var feedValueAxis = this.getView().byId('valueAxisFeed1');
                this.oVizFrame.addFeed(feedValueAxis);
            } else {
                if (this.getView().byId("collaudiCheckBox").getSelected() || this.getView().byId("scartiCheckBox").getSelected()) {
                    var feedValueAxis = this.getView().byId('valueAxisFeed1');
                    this.oVizFrame.removeFeed(feedValueAxis);
                } else {
                    this.getView().byId("prodottiCheckBox").setSelected(true);
                }
            }
        },
        onScartiSelected: function (oEvent) {
            var totale = oEvent.getParameter("selected");
            if (totale) {
                var feedValueAxis = this.getView().byId('valueAxisFeed2');
                this.oVizFrame.addFeed(feedValueAxis);
            } else {
                if (this.getView().byId("prodottiCheckBox").getSelected() || this.getView().byId("collaudiCheckBox").getSelected()) {
                    var feedValueAxis = this.getView().byId('valueAxisFeed2');
                    this.oVizFrame.removeFeed(feedValueAxis);
                } else {
                    this.getView().byId("scartiCheckBox").setSelected(true);
                }
            }
        },

        columnsToExport: function () {
            var columnsToExport = [];
            for (var i = 0; i < columnsName.ColumnsName.length; i++) {
                columnsToExport.push({
                    label: columnsName.ColumnsName[i],
                    property: columnsName.ColumnsField[i + 1],
                });
            }
            return columnsToExport;
        },

        exportxls: function () {
            var aCols, aProducts, oSettings, oSheet;

            aCols = this.columnsToExport();
            var oModelRowset1 = new JSONModel();
            oModelRowset1.setData(this.pathToExport1);
            aProducts = this.getView().getModel().getProperty('/');

            oSettings = {
                workbook: {columns: aCols},
                dataSource: oModelRowset1.getProperty('/')
            };

            oSheet = new Spreadsheet(oSettings);
            oSheet.build()
                .then(function () {
                    MessageToast.show('Spreadsheet export has finished');
                })
                .finally(function () {
                    oSheet.destroy();
                });
        },


        // PAGINATION
        pageSize: 8,
        getPage: function (pageNumber) {
            var offset = this.pageSize * pageNumber;
            var end = offset + this.pageSize - 1;
            var limit = Math.min(end + 1, window.dataset.length);
            var win = [];
            for (var i = offset; i < limit; i++) {
                win.push(window.dataset[i]);
            }
            return win;
        },

        loadPage: function (pageNumber) {
            var rows = this.getPage(pageNumber);
            this.getView().byId("tblDataset").setModel(new JSONModel(rows));
            this.currentPage = pageNumber;
            this.renderPagination(); //rerender pagination
        },

        renderPagination: function () {
            var pages = Math.ceil(window.dataset.length / this.pageSize);
            document.getElementsByClassName("pagination")[0].innerHTML = "";
            for (var p = 0; p < pages; p++) {
                var index = p + 1;
                var html = "<div class='page-box " + (this.currentPage == p ? "current-page" : "") + "' data-page=" + p + " onclick='loadPageByElement(this)'>" + index + "</div>";
                document.getElementsByClassName("pagination")[0].innerHTML += html;
            }
        },

        loadPageByElement: function (element) {
            this.loadPage(element.dataset.page);
        }

        // fixme EXPORT PER I CHARTS
        // exportChart: function () {
        //     var aCols, aProducts, oSettings, oSheet;
        //
        //     // aCols = this.columnsToExport();
        //     // var oModelRowset1 = new JSONModel();
        //     // oModelRowset1.setData(this.pathToExport1);
        //     // aProducts = this.getView().getModel().getProperty('/');
        //
        //     oSettings = {
        //         workbook: {
        //             columns: [
        //                 {
        //                     label: "VALORE",
        //                     property: "VALORE",
        //                 },
        //                 {
        //                     label: "TOTALE",
        //                     property: "TOTALE",
        //                 }
        //
        //             ],
        //             hierarchyLevel: "line"
        //         },
        //         dataSource: this.getView().byId("idVizFrame").getModel().getProperty('/')
        //     };
        //
        //     oSheet = new Spreadsheet(oSettings);
        //     oSheet.build()
        //         .then(function () {
        //             MessageToast.show('Spreadsheet export has finished');
        //         })
        //         .finally(function () {
        //             oSheet.destroy();
        //         });
        // }
    });
});