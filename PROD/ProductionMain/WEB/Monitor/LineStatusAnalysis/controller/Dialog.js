sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/xml/XMLModel",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/format/DateFormat",
	"sap/m/MessageBox",
	"sap/ui/base/ManagedObject"
], function (Controller, XmlModel, JsonModel, DateFormat, MessageBox, ManagedObject) {
	"use strict";
	return ManagedObject.extend("seco.ui.oee.period.controller.Dialog", {

		oDialog: null,
		_oView: null,
	
		constructor: function(oView){
			this._oView = oView;
		},

		open: function(){
			var oView = this._oView;
			this.oDialog = oView.byId("chartsDialog");
			debugger;
			// create dialog lazily
			if (!this.oDialog) {
				// create dialog via fragment factory
				this.oDialog = sap.ui.xmlfragment(oView.getId(), "seco.ui.oee.period.view.Dialog");
				// connect dialog to the root view of this component (models, lifecycle)
				oView.addDependent(this.oDialog);
			}
			this.oDialog.open();
		},

		onCloseDialog : function () {
			this.oDialog.close();
		}

	})
});